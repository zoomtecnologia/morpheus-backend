package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.Comissao;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaPagamentosModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoManutencaoInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoRepositorioQueryImpl;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoTotaisModeloAPI;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TesteComissaoRepositorio {

    @Autowired
    private ComissaoRepositorio comissaoRepositorio;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    @Disabled
    public void listarComissoesSolicitadas() {
        List<ComissaoListaPagamentosModeloAPI> listarPagamentosComissao = this.comissaoRepositorio.listarPagamentosComissao("11593321000163", "SOLICITADO", "");
        listarPagamentosComissao.forEach(System.out::println);
    }

    @Test
    @Disabled
    public void consultarTotaisComissao() {
        LocalDate dataInicial = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate dataFinal = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
        ComissaoTotaisModeloAPI totais = comissaoRepositorio.listarTotalDeComissoes("11593321000163",dataInicial,dataFinal);
        Assertions.assertNotNull(totais);
    }

    @Test
    @Disabled
    public void listarComissoes() {
        LocalDate dataInicial = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate dataFinal = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
        List<ComissaoListaModeloAPI> listarComissaoes = this.comissaoRepositorio.listarComissaoes("", "PAGO",dataInicial,dataFinal);
        Assertions.assertFalse(listarComissaoes.isEmpty());
    }

    @Test
    @Disabled
    public void listarComissoesPorCliente() {
        ComissaoRepositorioQueryImpl.FiltroComissaoQuery filtroComissaoQuery = new ComissaoRepositorioQueryImpl.FiltroComissaoQuery();
        filtroComissaoQuery.setCodigoSolicitacao("");
        filtroComissaoQuery.setCodigoCliente("11593321000163");
        filtroComissaoQuery.setCodigoRepresentante("");
        filtroComissaoQuery.setStatus(Arrays.asList("PAGO"));
        filtroComissaoQuery.setDataVencimento(null);
        List<ComissaoListaModeloAPI> listarComissaoes = this.comissaoRepositorio.listarComissaoes(filtroComissaoQuery);
        listarComissaoes.forEach(System.out::println);
    }

    @Test
    @Disabled
    public void solicitarComissoes() {
        List<Integer> comissoes = Arrays.asList(18, 19, 20, 21, 22, 23);
        String status = "SOLICITADO";
        this.comissaoRepositorio.socilitarLiberacao(comissoes, status, LocalDate.now());
    }

    @Test
    @Disabled
    public void salvarComissaoManutencao() {
        ComissaoManutencaoInclusaoModeloAPI comissaoManutencaoInclusaoModeloAPI = new ComissaoManutencaoInclusaoModeloAPI();
        comissaoManutencaoInclusaoModeloAPI.setCodigoClienteCnpj("11593321000163");
        comissaoManutencaoInclusaoModeloAPI.setCodigoRepresentanteDocumento("11593321000163");
        comissaoManutencaoInclusaoModeloAPI.setDataMensalidade(LocalDate.now());
        comissaoManutencaoInclusaoModeloAPI.setTipo("MENSALIDADE");
        comissaoManutencaoInclusaoModeloAPI.setValorMensalidade(50);
        Comissao comissao = this.modelMapper.map(comissaoManutencaoInclusaoModeloAPI, Comissao.class);
        Comissao comissaoSalva = this.comissaoRepositorio.save(comissao);
        Assertions.assertNotNull(comissaoSalva);
    }

    @Test
    @Disabled
    public void realizarPagamentoDaSolicitacao() {
        this.comissaoRepositorio.realizarPagamentoSolicitacao("27775267000107-22012021-142324", LocalDate.now());
    }

    @Test
    @Disabled
    public void buscarSolicitacaoPorCodigo() {
        ComissaoListaPagamentosModeloAPI comissaoListaPagamentosModeloAPI = this.comissaoRepositorio.buscarSolicitacao("40223178000116-19022021-144315");
        System.out.println("comissaoListaPagamentosModeloAPI = " + comissaoListaPagamentosModeloAPI);
    }

}
