package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoManutencaoInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoRetornoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.util.FormatterUtil;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca.CarneCobranca;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca.Cobranca;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca.PagamentoCobranca;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TesteCobranca {

    @Autowired
    private ComissaoServico comissaoServico;

    @Autowired
    private PedidoRepositorio pedidoRepositorio;

    @Test
    @Disabled
    public void realizarQuitacaoArquivoDeCobrancaPorDataExpiracao() {
        try {
            Cobranca[] cobrancas = this.lerArquivoDeCobranca();
            List<Cobranca> cobrancasDeCarnes = this.filtrarApenasCobrancasDeCarnes(cobrancas);
            Map<String, List<Cobranca>> agruparPorClientes = this.agruparPorCliente(cobrancasDeCarnes);
            agruparPorClientes.entrySet().stream().forEach(this::pesquisandoCobrancaPorCliente);
            this.comissaoServico.bloquearClienteComPagamentosVencidos();
            this.comissaoServico.ativarClienteComPagamentosQuitados();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void pesquisandoCobrancaPorCliente(Map.Entry<String, List<Cobranca>> entry) {
        String cnpjCliente = entry.getKey();
        Optional<Pedido> pedidoExiste = this.pedidoRepositorio.buscarPedidoPorCliente(cnpjCliente);
        if (!pedidoExiste.isPresent()) {
            return;
        }
        List<Cobranca> cobrancasPorCliente = entry.getValue();
        cobrancasPorCliente.forEach(cobranca -> pesquisarCobrancaCliente(cobranca, pedidoExiste.get()));
    }

    private void pesquisarCobrancaCliente(Cobranca cobranca, Pedido pedido) {
        Cliente cliente = pedido.getCliente();
        String dataExpiracaoPagamento = cobranca.getPayment().getCarnet().getExpire_at();
        LocalDate dataMensalidade = LocalDate.parse(dataExpiracaoPagamento);
        boolean comissao = this.comissaoServico.existsByDataMensalidadeAndCodigoClienteCnpj(dataMensalidade, cliente.getCnpj());
        if (!comissao) {
            this.salvarComissao(cliente, dataMensalidade, cobranca, pedido);
            return;
        }
        if (this.verificarSFoiPagoECancelado(cobranca)) {
            this.adicionarCobrancaComoPagaECancelada(cobranca);
        }
    }

    public Pedido buscarPedidoPorCliente(String cnpjCliente) {
        return this.pedidoRepositorio.buscarPedidoPorCliente(cnpjCliente).get();
    }

    private Map<String, List<Cobranca>> agruparPorCliente(List<Cobranca> cobrancasDeCarnes) {
        return cobrancasDeCarnes.stream().collect(Collectors.groupingBy(cobranca -> cobranca.getCustomer().getDocument()));
    }

    private Cobranca[] lerArquivoDeCobranca() throws IOException {
        String caminhoDoArquivo = "D:\\Download\\cobrancas-inadimplentes.json";
        InputStream inputStream = Files.newInputStream(Paths.get(caminhoDoArquivo));
        Cobranca[] cobrancas = new ObjectMapper().readValue(inputStream, Cobranca[].class);
        return cobrancas;
    }

    private List<Cobranca> filtrarApenasCobrancasDeCarnes(Cobranca[] cobrancas) {
        return Arrays.asList(cobrancas).stream().filter(this::eCobrancaDeCarne).map(cobranca -> {
            PagamentoCobranca pagamentoCobranca = cobranca.getPayment();
            CarneCobranca carne = pagamentoCobranca.getCarnet();
            carne.setExpire_at(carne.getExpire_at().substring(0, 10));
            return cobranca;
        }).collect(Collectors.toList());
    }

    private boolean verificarSFoiPagoECancelado(Cobranca cobranca) {
        return this.verificarSeFoiPago(cobranca) || this.verificarSeFoiCancelada(cobranca);
    }

    private boolean verificarSeFoiPago(Cobranca cobranca) {
        return cobranca.getStatus().equals("paid");
    }

    private boolean verificarSeFoiCancelada(Cobranca cobranca) {
        return cobranca.getStatus().equals("canceled");
    }

    private boolean eCobrancaDeCarne(Cobranca cobranca) {
        return cobranca.getCharge_method().equals("carnet");
    }

    private void adicionarCobrancaComoPagaECancelada(Cobranca cobranca) {
        String statusCobranca = "PAGO";
        if (this.verificarSeFoiCancelada(cobranca)) {
            statusCobranca = "CANCELADO";
            this.atualizarMensalidadePorDataMensalidade(cobranca, statusCobranca);
            return;
        }
        this.atualizarMensalidadePorDataMensalidade(cobranca, statusCobranca);
    }

    private void atualizarMensalidadePorDataMensalidade(Cobranca cobranca, String status) {
        String dataExpiracaoPagamento = cobranca.getPayment().getCarnet().getExpire_at();
        String cnpjCliente = cobranca.getCustomer().getDocument();
        this.comissaoServico.atualizarStatusComissaoPorDataMensalidade(status, cnpjCliente, LocalDate.parse(dataExpiracaoPagamento));
    }

    private void salvarComissao(Cliente cliente, LocalDate dataMensalidade, Cobranca cobranca, Pedido pedido) {
        ComissaoManutencaoInclusaoModeloAPI comissaoManutencaoInclusaoModeloAPI = new ComissaoManutencaoInclusaoModeloAPI();
        comissaoManutencaoInclusaoModeloAPI.setCodigoClienteCnpj(cliente.getCnpj());
        comissaoManutencaoInclusaoModeloAPI.setCodigoRepresentanteDocumento(cliente.getCodigoRepresentante());
        comissaoManutencaoInclusaoModeloAPI.setDataMensalidade(dataMensalidade);
        comissaoManutencaoInclusaoModeloAPI.setQuantidadeParcela(1);
        comissaoManutencaoInclusaoModeloAPI.setTipo("MENSALIDADE");
        float valorTotalCobranca = cobranca.getTotal() / 100;
        comissaoManutencaoInclusaoModeloAPI.setValorMensalidade(FormatterUtil.arredondarValor(2, valorTotalCobranca));
        comissaoManutencaoInclusaoModeloAPI.setPedido(this.preencherPedidoRetornoModeloAPI(pedido));
        this.comissaoServico.salvarComissaoManutencao(comissaoManutencaoInclusaoModeloAPI);
    }

    private PedidoRetornoModeloAPI preencherPedidoRetornoModeloAPI(Pedido pedido) {
        return new PedidoRetornoModeloAPI(
                pedido.getNumero(),
                pedido.getData(),
                pedido.getRepresentante().getDocumento(),
                pedido.getRepresentante().getNome(),
                pedido.getCliente().getCnpj(),
                pedido.getCliente().getRazaoSocial(),
                pedido.getStatus(),
                pedido.getTipoComissao(),
                pedido.getTotal(),
                pedido.getValorMensalidade(),
                pedido.getRepresentante().getGeraReceita()
        );
    }

}
