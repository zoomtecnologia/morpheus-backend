package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteRetornoModeloAPI;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SpringBootTest
public class TesteClienteRepositorio {

    @Autowired
    private ClienteRepositorio clienteRepositorio;

    @Test
    @Disabled
    void teste() {
        Page<ClienteRetornoModeloAPI> listarClientes = this.clienteRepositorio.listarClientes("", "", PageRequest.of(0, 10));
        listarClientes.getContent().forEach(cliente -> System.err.println(cliente.getRazaoSocial()));
    }   

}
