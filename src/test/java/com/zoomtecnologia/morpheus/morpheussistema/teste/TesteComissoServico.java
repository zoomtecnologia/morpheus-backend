package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoManutencaoInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoServico;
import java.time.LocalDate;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TesteComissoServico {

    @Autowired
    private ComissaoServico comissaoServico;

    @Autowired
    private PedidoServico pedidoServico;

    @Test
    @Disabled
    public void gerarParcelasMensaldiade() {
        Pedido pedido = this.pedidoServico.buscarPedidoPorNumero(20);
        this.comissaoServico.gerarParcelasMensalidade(pedido);
    }

    @Test
    @Disabled
    public void gerarParcelasMensalidadeManualmente() {

        ComissaoManutencaoInclusaoModeloAPI comissaoManutencaoInclusaoModeloAPI = new ComissaoManutencaoInclusaoModeloAPI();
        comissaoManutencaoInclusaoModeloAPI.setCodigoClienteCnpj("11593321000163");
        comissaoManutencaoInclusaoModeloAPI.setCodigoRepresentanteDocumento("11593321000163");
        comissaoManutencaoInclusaoModeloAPI.setDataMensalidade(LocalDate.now().withDayOfMonth(15));
        comissaoManutencaoInclusaoModeloAPI.setTipo("MENSALIDADE");
        comissaoManutencaoInclusaoModeloAPI.setValorMensalidade(50);
        comissaoManutencaoInclusaoModeloAPI.setQuantidadeParcela(12);

        this.comissaoServico.salvarComissaoManutencao(comissaoManutencaoInclusaoModeloAPI);

    }

}
