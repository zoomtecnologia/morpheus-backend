/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.zoomtecnologia.morpheus.morpheussistema.util.CurrencyWriter;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TesteValorPorExtenso {
    
    @Test
    void teste(){
        CurrencyWriter currencyWriter = CurrencyWriter.getInstance();
        String write = currencyWriter.write(BigDecimal.valueOf(30));
        System.out.println("write = " + write);
    }
    
}
