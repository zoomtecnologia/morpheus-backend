package com.zoomtecnologia.morpheus.morpheussistema.teste.pdv;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteValidacao;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteValidacaoServico;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestePDV {
    
    @Autowired
    private ClienteValidacaoServico clienteValidacaoServico;
    
    @Test
    public void validarChave() {
        
        ClienteValidacao clienteValidacao = this.clienteValidacaoServico.validarChavePDV("08258424000162", 4);
        LocalDate dataProximoVencimento = clienteValidacao.getDataProximoVencimento();
        System.out.println("dataProximoVencimento = " + dataProximoVencimento);
        
    }
    
    
}
