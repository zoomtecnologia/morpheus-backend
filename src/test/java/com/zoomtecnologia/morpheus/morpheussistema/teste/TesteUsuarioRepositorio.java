package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioResumidoModeloAPI;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TesteUsuarioRepositorio {
    
    @Autowired
    private UsuarioRepositorio usuarioRepositorio;
    
    @Test
    public void buscarUsuarioResumido() {
        
        Optional<UsuarioResumidoModeloAPI> usuarioOptional = this.usuarioRepositorio.buscarUsuarioResumido(1);
        
        boolean naoExisteUsuario = !usuarioOptional.isPresent();
        if(naoExisteUsuario){
            System.out.println("Não existe usuario com esse codigo"); 
            return;
        }
        System.out.println("usuarioOptional = " + usuarioOptional.get().getEmail());
    }
    
    
}
