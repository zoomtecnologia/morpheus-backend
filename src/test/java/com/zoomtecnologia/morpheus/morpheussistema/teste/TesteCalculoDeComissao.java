package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.tipocomissao.TipoComissaoServico;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TesteCalculoDeComissao {

    @Autowired
    private TipoComissaoServico tipoComissaoServico;
    
    @Test
    @Disabled
    void calcularValorDaComissaoEmCiquentaPorcento() {

        Pedido pedido = new Pedido();
        
        pedido.setTipoComissao(1);
        pedido.setTotal(600.0);
        
        double calcularComissaoInstalacao = this.tipoComissaoServico.calcularComissaoInstalacao(pedido);
        System.out.println("calcularComissaoInstalacao = " + calcularComissaoInstalacao);
    }
    
    @Test
    @Disabled
    void calcularValorDaComissaoPorDiferenca() {

        Pedido pedido = new Pedido();
        
        pedido.setTipoComissao(1);
        pedido.setTotal(399.0);
        
        double calcularComissaoInstalacao = this.tipoComissaoServico.calcularComissaoInstalacao(pedido);
        System.out.println("calcularComissaoInstalacao = " + calcularComissaoInstalacao);
    }
    
    @Test
    @Disabled        
    void calcularValorSemComissao() {

        Pedido pedido = new Pedido();
        
        pedido.setTipoComissao(1);
        pedido.setTotal(200.00);
        
        double calcularComissaoInstalacao = this.tipoComissaoServico.calcularComissaoInstalacao(pedido);
        System.out.println("calcularComissaoInstalacao = " + calcularComissaoInstalacao);
    }

}
