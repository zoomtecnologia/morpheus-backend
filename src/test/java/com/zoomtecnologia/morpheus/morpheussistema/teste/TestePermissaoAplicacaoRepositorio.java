package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao.PermissaoAplicacaoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao.PermissaoAplicacaoResumida;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestePermissaoAplicacaoRepositorio {
    
    @Autowired
    private PermissaoAplicacaoRepositorio permissaoAplicacaoRepositorio;
    
    @Test
    public void listarAplicacoesPorPermissao() {
        
        List<PermissaoAplicacaoResumida> permissaoAplicacaoResumidaModeloAPI = this.permissaoAplicacaoRepositorio.listarAplicacoesPorPermissao(1);
        for (PermissaoAplicacaoResumida permissaoAplicacao : permissaoAplicacaoResumidaModeloAPI) {
            System.out.println("aplicacao = " + permissaoAplicacao);
        }
    }
    
    
}
