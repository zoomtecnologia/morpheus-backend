package com.zoomtecnologia.morpheus.morpheussistema.teste.zionplanocontratado;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratadoInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratadoServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao.ZionPlanoContratacaoInclusaoModeloAPI;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TesteZionPlanoContratadoServico {

    @Autowired
    private ZionPlanoContratadoServico zionPlanoContratadoServico;
    
    @Test
    public void gerarContrato(){
        ZionPlanoContratadoInclusaoModeloAPI zionPlanoContratadoInclusaoModeloAPI = new ZionPlanoContratadoInclusaoModeloAPI();
        zionPlanoContratadoInclusaoModeloAPI.setDocumentoIdentificacaoCliente("11593321000163");
        zionPlanoContratadoInclusaoModeloAPI.setImplantacao(1000.0);
        zionPlanoContratadoInclusaoModeloAPI.setMensalidade(500.0);
        zionPlanoContratadoInclusaoModeloAPI.setPdvs(2);
        zionPlanoContratadoInclusaoModeloAPI.setQuantidadeUsuarios(5);
        List<ZionPlanoContratacaoInclusaoModeloAPI> planos = new ArrayList<>();
        ZionPlanoContratacaoInclusaoModeloAPI zionPlanoContratacaoInclusaoModeloAPI = new ZionPlanoContratacaoInclusaoModeloAPI();
        zionPlanoContratacaoInclusaoModeloAPI.setCodigoPlano("bf018326-6ff4-4b61-a205-1c908cb51124");
        zionPlanoContratacaoInclusaoModeloAPI.setUsuario("eudeskyo@hotmail.com");
        planos.add(zionPlanoContratacaoInclusaoModeloAPI);
        zionPlanoContratadoInclusaoModeloAPI.setPlanos(planos);
        
        this.zionPlanoContratadoServico.gerarContrato(zionPlanoContratadoInclusaoModeloAPI);
        
        
    }
    
    
}
