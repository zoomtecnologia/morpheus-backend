package com.zoomtecnologia.morpheus.morpheussistema.modulos.teste.conectabanco;

import com.zoomtecnologia.morpheus.morpheussistema.bancodedados.ConectaBancoMysql;
import com.zoomtecnologia.morpheus.morpheussistema.bancodedados.GeraBancoDeDados;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratado;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratadoServico;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;

@SpringBootTest
public class TesteConectaBanco {

    @Autowired
    private ClienteServico clienteServico;

    @Autowired
    private ZionPlanoContratadoServico zionPlanoContratadoServico;

    @Autowired
    private ResourceLoader resourceLoader;

    @Disabled
    @Test
    public void conectarBanco() {
        try {
            ConectaBancoMysql conectaBancoMysql = new ConectaBancoMysql();
            conectaBancoMysql.conectar();
            PreparedStatement preparedStatement = conectaBancoMysql.getConnection().prepareStatement("create database teste;use teste;");
            preparedStatement.executeUpdate();
            preparedStatement.close();
            conectaBancoMysql.desconectar();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TesteConectaBanco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(TesteConectaBanco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TesteConectaBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    @Disabled
    @Test
    public void criarBancoDeDadosSimplesNacional() {
        Cliente cliente = this.clienteServico.buscarClientePorCNPJ("01239465000162");
        ZionPlanoContratado zionPlanoContratado = this.zionPlanoContratadoServico.buscarContrato("90d4b93a-cc82-4f78-8660-8113bf52cb96");
        GeraBancoDeDados geraBancoDeDadosSimplesNacional = new GeraBancoDeDados(
                this.resourceLoader,
                cliente,
                zionPlanoContratado,
                List.of(),
                List.of()
        );
        try {
            geraBancoDeDadosSimplesNacional.gerarBanco();
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }

}
