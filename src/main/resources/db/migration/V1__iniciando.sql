CREATE TABLE IF NOT EXISTS `aplicacao` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `grupo_empresa` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `codigo_representante` varchar(20) not null,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `cliente` (
  `cnpj` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inscricao_estadual` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `razao_social` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nome_fantasia` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regime_tributario` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `logradouro` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `numero` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bairro` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cidade` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `complemento` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `obs` text COLLATE utf8_unicode_ci,
  `se_tef` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `contingencia` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `on_saiph` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `dia_vencimento` int(2) DEFAULT '5',
  `prazo_maximo` int(2) DEFAULT '30',
  `data_cadastro` date NOT NULL DEFAULT '0000-00-00',
  `data_ultimo_acesso` date DEFAULT NULL,
  `data_ultima_validacao` date DEFAULT NULL,
  `data_proximo_vencimento` date DEFAULT NULL,
  `status_atividade` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `status_pagamento` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `codigo_representante` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `codigo_grupo_empresa` int(11) DEFAULT NULL,
  `chave_saiph` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atualizou_sistema` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `aceite_contrato` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_aceite` date DEFAULT NULL,
  `nome_contato` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(13) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `whatsapp` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `seguimento` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_pedido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contrato` text COLLATE utf8_unicode_ci,
  `cpf_pessoa_responsavel` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome_pessoa_responsavel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_fim_vigencia_contrato` date DEFAULT NULL,
  PRIMARY KEY (`cnpj`),
  KEY `clienteFKgrupo_empresa` (`codigo_grupo_empresa`),
  CONSTRAINT `clienteFKgrupo_empresa` FOREIGN KEY (`codigo_grupo_empresa`) REFERENCES `grupo_empresa` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `comissao` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_representante` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `codigo_cliente` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'INSTALACAO,MENSALIDADE',
  `valor` double(15,2) NOT NULL DEFAULT '0.00',
  `data_mensalidade` date DEFAULT NULL,
  `data_liberacao` date DEFAULT NULL,
  `data_solicitacao` date DEFAULT NULL,
  `data_pagamento` date DEFAULT NULL,
  `status` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'PENDENTE,LIBERADA,SOLICITADA,PAGA',
  `obs` text COLLATE utf8_unicode_ci,
  `codigo_solicitacao` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `valor_mensalidade` double(15,2) NOT NULL DEFAULT '0.00',
  `valor_instalacao` double(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`codigo`),
  KEY `comissaoFKcliente` (`codigo_cliente`),
  CONSTRAINT `comissaoFKcliente` FOREIGN KEY (`codigo_cliente`) REFERENCES `cliente` (`cnpj`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `item` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `valor` double(15,2) NOT NULL DEFAULT '0.00',
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `item_unico` smallint(1) NOT NULL DEFAULT '1',
  `valor_minimo` double(15,2) NOT NULL DEFAULT '0.00',
  `opcional` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `itens_pedido` (
  `numero_pedido` int(11) NOT NULL,
  `codigo_item` int(11) NOT NULL,
  `valor` double(15,2) NOT NULL DEFAULT '0.00',
  `quantidade` int(11) NOT NULL DEFAULT '0',
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  PRIMARY KEY (`numero_pedido`,`codigo_item`),
  KEY `itens_pedidoFKitem` (`codigo_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `pedido` (
  `numero` int(11) NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL DEFAULT '0000-00-00',
  `representante` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo_comissao` int(11) NOT NULL DEFAULT '0',
  `total` double(15,2) NOT NULL DEFAULT '0.00',
  `valor_mensalidade` double(15,2) NOT NULL DEFAULT '0.00',
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'LANCADO',
  `obs` text COLLATE utf8_unicode_ci,
  `data_marcacao_instalacao` timestamp NULL DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `permissao` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `representante` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `permissao_aplicacao` (
  `codigo_permissao` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_aplicacao` int(11) NOT NULL DEFAULT '0',
  `status` smallint(1) NOT NULL DEFAULT '1',
  `acessar` smallint(1) NOT NULL DEFAULT '1',
  `incluir` smallint(1) NOT NULL DEFAULT '1',
  `alterar` smallint(1) NOT NULL DEFAULT '1',
  `excluir` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo_permissao`,`codigo_aplicacao`),
  KEY `permissao_aplicacaoFKaplicacao` (`codigo_aplicacao`),
  CONSTRAINT `permissao_aplicacaoFKaplicacao` FOREIGN KEY (`codigo_aplicacao`) REFERENCES `aplicacao` (`codigo`),
  CONSTRAINT `permissao_aplicacaoFKpermissao` FOREIGN KEY (`codigo_permissao`) REFERENCES `permissao` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `representante` (
  `documento` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nome_social` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logradouro` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `numero` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `complemento` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bairro` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cidade` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cep` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nome_contato` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(13) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `whatsapp` varchar(13) COLLATE utf8_unicode_ci DEFAULT '',
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` varchar(1) COLLATE utf8_unicode_ci DEFAULT '',
  `data_cadastro` date NOT NULL DEFAULT '0000-00-00',
  `dia_pagamento_comissao` int(2) NOT NULL DEFAULT '20',
  `dias_tolerancia_pagamento_comissao` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '5',
  `data_ultima_atualizacao` date DEFAULT NULL,
  `data_ultimo_acesso` date DEFAULT NULL,
  `usuario_atualizacao` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nivel` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `obs` text COLLATE utf8_unicode_ci,
  `banco` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agencia` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conta` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_conta` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `documento_pessoa_conta` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome_pessoa_conta` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chave_pix` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto` longblob,
  `gera_receita` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`documento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `tipo_comissao` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `porcentagem_representante` double(15,2) NOT NULL DEFAULT '0.00',
  `porcentagem_zoom` double(15,2) NOT NULL DEFAULT '0.00',
  `data_cadastro` date DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A,I',
  `tipo` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'I,M',
  `valor_minimo` double(15,2) NOT NULL DEFAULT '0.00',
  `valor_sem_comissao` double(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;


CREATE TABLE IF NOT EXISTS `usuario` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `senha` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `codigo_representante` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `codigo_permissao` int(11) NOT NULL DEFAULT '0',
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `recuperando_senha` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_cadastro` date DEFAULT NULL,
  `data_recuperacao_senha` date DEFAULT NULL,
  `data_acesso` date DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;


REPLACE INTO `aplicacao` VALUES (1,'REPRESENTANTE'),(2,'CLIENTE'),(3,'COMISSAO'),(4,'ITEM'),(5,'PEDIDO'),(6,'PERMISSAO'),(7,'TIPO COMISSAO'),(8,'USUARIO'),(9,'PAGAMENTOS'),(10,'PAGAMENTO-PIX'),(11,'CONTRATO');
REPLACE INTO `permissao` (`codigo`,`nome`,`representante`) VALUES (9,'ADMINISTRADOR',0),(10,'REPRESENTANTE COMERCIAL',1);
REPLACE INTO `permissao_aplicacao` (`codigo_permissao`,`codigo_aplicacao`,`status`,`acessar`,`incluir`,`alterar`,`excluir`) VALUES (9,1,1,1,1,1,1),(9,2,1,1,1,1,1),(9,3,1,1,1,1,1),(9,4,1,1,1,1,1),(9,5,1,1,1,1,1),(9,6,1,1,1,1,1),(9,7,1,1,1,1,1),(9,8,1,1,1,1,1),(9,9,1,1,1,1,1),(9,10,1,1,1,1,1),(9,11,1,1,1,1,1),(10,1,0,0,0,0,0),(10,2,1,1,1,0,0),(10,3,1,1,1,0,0),(10,4,0,0,0,0,0),(10,5,1,1,1,0,0),(10,6,0,0,0,0,0),(10,7,0,0,0,0,0),(10,8,0,0,0,0,0),(10,9,1,1,0,0,0),(10,10,0,0,0,0,0),(10,11,0,0,0,0,0);
REPLACE INTO `grupo_empresa` (`codigo`,`nome`) VALUES (2,'ZOOM','3055');
REPLACE INTO `usuario` (`codigo`,`nome`,`email`,`senha`,`codigo_representante`,`codigo_permissao`,`status`,`recuperando_senha`,`data_cadastro`,`data_recuperacao_senha`,`data_acesso`) VALUES (7,'Zoom Tecnologia','wagnercaminha@zoomtecnologia.com','wz30551404','3055',9,'A',NULL,'2021-03-02',NULL,'2021-03-03');
REPLACE INTO `tipo_comissao` (`codigo`,`nome`,`porcentagem_representante`,`porcentagem_zoom`,`data_cadastro`,`status`,`tipo`,`valor_minimo`,`valor_sem_comissao`) VALUES (1,'MESTRE-JEDI',42.00,58.00,NULL,'A','I',400.00,200.00),(2,'JEDI',40.00,60.00,NULL,'A','M',0.00,0.00),(3,'MESTRE-JEDI',50.00,50.00,NULL,'A','M',0.00,0.00),(4,'SITH',60.00,40.00,NULL,'A','M',0.00,0.00),(5,'MESTRE-SITH',70.00,30.00,NULL,'A','M',0.00,0.00),(6,'VADER',50.00,50.00,NULL,'A','I',0.00,0.00);
REPLACE INTO `representante` (`documento`,`nome`,`nome_social`,`logradouro`,`numero`,`complemento`,`bairro`,`cidade`,`estado`,`cep`,`nome_contato`,`telefone`,`celular`,`whatsapp`,`email`,`tipo`,`data_cadastro`,`dia_pagamento_comissao`,`dias_tolerancia_pagamento_comissao`,`data_ultima_atualizacao`,`data_ultimo_acesso`,`usuario_atualizacao`,`nivel`,`status`,`obs`,`banco`,`agencia`,`conta`,`tipo_conta`,`documento_pessoa_conta`,`nome_pessoa_conta`,`chave_pix`,`foto`,`gera_receita`) VALUES ('3055','W LOURENCO CAMINHA INFORMATICA','ZOOM TECNOLOGIA','Avenida Segunda Perimetral Norte','105','','Peixinhos','Olinda','PE','0','','','81986683055','','zoomtecnologiawlc@gmail.com','S','2021-01-26',20,'3','2021-03-09',NULL,'EUDES BATISTA','M','A','','237','3202','2407-4','001','03185950445','WAGNER','57309429-B32C-40E4-AB49-B3753278C6D8',NULL,0);
REPLACE INTO `item` (`codigo`,`descricao`,`valor`,`status`,`item_unico`,`valor_minimo`,`opcional`) VALUES (1,'LicenÃ§a de uso do Sistema Omega',600.00,'A',1,245.00,1),(2,'PONTO ADICIONAL',50.00,'A',0,50.00,1),(5,'On Saiph',25.00,'A',1,25.00,1),(6,'Mensalidade',150.00,'A',1,150.00,0);