INSERT INTO `empresa` (
    codigo_empresa, cnpj, razao_social, nome_fantasia,
    codigo_regime_tributario, inscricao_estadual, inscricao_estadual_st,
    inscricao_municipal, telefone, logradouro, numero,
    bairro, codigo_cidade, complemento, cep,
    cnae, grupo_empresa, certificado_digital, senha_certificado,
    cesta_tributacao_padrao, matriz, mensagem_nfe, mensagem_cupom, mensagem_orcamento,
    mensagem_pedido_venda, mensagem_pedido_compra, mensagem_cte, codigo_tabela_preco_padrao,
    identificar_vendedor_pdv, identificar_consumidor_pdv, senha_gerente_vale_pdv,
    controla_caixa_pdv, senha_gerente_controle_caixa_pdv, aviso_sangria_pdv,
    conciliacao_pdv, adicionar_obervacao_cupom_pdv, exibir_imagem_produto_pesquisa_pdv,
    valor_maximo_venda_pdv, valor_maximo_no_caixa_pdv, valor_quebra_caixa_pdv,
    data_ultima_atualizacao, data_cadastro, logo,
    tipo_emissao_pdv, tema_principal_pdv, numero_vias_tef,
    movimenta_estoque, senha_gerente_contingencia, usa_catraca, aviso_estoque_minimo,
    salva_ultima_pesquisa, apelido, vencimento_certificado,
    prazo_garantia_servico, prazo_orcamento, prazo_pedido, usa_comanda, nome_certificado,
    nome_cidade, estado, sigla_estado, codigo_estado,
    faz_pedido_estoque_zerado, unidade_principal, grupo_produto_padrao, email,
    indicador_inscricao_estadual, finalizar_pedido_sem_forma_pagamento, nao_baixar_estoque_na_emissao_nota_fiscal,
    nao_reserva_estoque_no_pedido, tipo_comissionamento_padrao, codigo_plano_contratado,
    lancar_pedido_conta_corrente_cliente, pedido_correntista_baixa_estoque_gera_venda, pedido_gera_venda
) VALUES (
    VALOR_CODIGO_DOCLIENTE, VALOR_CNPJ_DOCLIENTE, VALOR_RAZAO_SOCIAL_DOCLIENTE, VALOR_NOME_FANTASIA_DOCLIENTE,
    VALOR_CODIGO_REGIME_TRIBUTARIO_DOCLIENTE, VALOR_INSCRICAO_ESTADUAL_DOCLIENTE, NULL,
    VALOR_INSCRICAO_MUNICIPAL_DOCLIENTE, VALOR_TELEFONE_DOCLIENTE, VALOR_LOGRADOURO_DOCLIENTE, VALOR_NUMERO_DOCLIENTE,
    VALOR_BAIRRO_DOCLIENTE, VALOR_CODIGO_MUNICIPIO_DOCLIENTE, VALOR_COMPLEMENTO_DOCLIENTE, VALOR_CEP_DOCLIENTE,
    VALOR_CNAE_DOCLIENTE, VALOR_GRUPO_DOCLIENTE, VALOR_CERTIFICADO_DIGITAL_DOCLIENTE, VALOR_SENHA_CERTIFICADO_DOCLIENTE,
    VALOR_CODIGO_CESTA_TRIBUTACAO_CLIENTE, VALOR_MATRIZ_DOCLIENTE, '', '', '',
    '', '', '', 20,
    0, 0, 0, 1,
    0, 0, 1, 1, 1,
    5000.00, 0.00, 0.00, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), NULL,
    'NFCE', 'principal', 2, 1, 1, 0, 0, 1,
    VALOR_APELIDO_DOCLIENTE, NULL, 30, 30, 30, 0, NULL,
    VALOR_MUNICIPIO_DOCLIENTE, VALOR_ESTADO_DOCLIENTE, VALOR_UF_DOCLIENTE, VALOR_CODIGO_ESTADO_DOCLIENTE,
    1, 'UN', 1, VALOR_EMAIL_DOCLIENTE,
    VALOR_INIDICADOR_INSCRICAO_ESTADUAL_DOCLIENTE, 0, 0,
    0, 'CIF', VALOR_CODIGO_PLANO_CONTRATADO_DOCLIENTE,
    1, 0, 0
)
ON DUPLICATE KEY UPDATE
    cnpj=values(cnpj),
	razao_social=values(razao_social),
	nome_fantasia=values(nome_fantasia),
	codigo_regime_tributario=values(codigo_regime_tributario),
	inscricao_estadual=values(inscricao_estadual),
	inscricao_estadual_st=values(inscricao_estadual_st),
	inscricao_municipal=values(inscricao_municipal),
	telefone=values(telefone),
	logradouro=values(logradouro),
	numero=values(numero),
	bairro=values(bairro),
	codigo_cidade=values(codigo_cidade),
	complemento=values(complemento),
	cep=values(cep),
	cnae=values(cnae),
	grupo_empresa=values(grupo_empresa),
	certificado_digital=values(certificado_digital),
	senha_certificado=values(senha_certificado),
	cesta_tributacao_padrao=values(cesta_tributacao_padrao),
	matriz=values(matriz),
	mensagem_nfe=values(mensagem_nfe),
	mensagem_cupom=values(mensagem_cupom),
	mensagem_orcamento=values(mensagem_orcamento),
	mensagem_pedido_venda=values(mensagem_pedido_venda),
	mensagem_pedido_compra=values(mensagem_pedido_compra),
	mensagem_cte=values(mensagem_cte),
	codigo_tabela_preco_padrao=values(codigo_tabela_preco_padrao),
	identificar_vendedor_pdv=values(identificar_vendedor_pdv),
	identificar_consumidor_pdv=values(identificar_consumidor_pdv),
	senha_gerente_vale_pdv=values(senha_gerente_vale_pdv),
	controla_caixa_pdv=values(controla_caixa_pdv),
	senha_gerente_controle_caixa_pdv=values(senha_gerente_controle_caixa_pdv),
	aviso_sangria_pdv=values(aviso_sangria_pdv),
	conciliacao_pdv=values(conciliacao_pdv),
	adicionar_obervacao_cupom_pdv=values(adicionar_obervacao_cupom_pdv),
	exibir_imagem_produto_pesquisa_pdv=values(exibir_imagem_produto_pesquisa_pdv),
	valor_maximo_venda_pdv=values(valor_maximo_venda_pdv),
	valor_maximo_no_caixa_pdv=values(valor_maximo_no_caixa_pdv),
	valor_quebra_caixa_pdv=values(valor_quebra_caixa_pdv),
	data_ultima_atualizacao=values(data_ultima_atualizacao),
	data_cadastro=values(data_cadastro),
	logo=values(logo),
	tipo_emissao_pdv=values(tipo_emissao_pdv),
	tema_principal_pdv=values(tema_principal_pdv),
	numero_vias_tef=values(numero_vias_tef),
	movimenta_estoque=values(movimenta_estoque),
	senha_gerente_contingencia=values(senha_gerente_contingencia),
	usa_catraca=values(usa_catraca),
	aviso_estoque_minimo=values(aviso_estoque_minimo),
	salva_ultima_pesquisa=values(salva_ultima_pesquisa),
	apelido=values(apelido),
	vencimento_certificado=values(vencimento_certificado),
	prazo_garantia_servico=values(prazo_garantia_servico),
	prazo_orcamento=values(prazo_orcamento),
	prazo_pedido=values(prazo_pedido),
	usa_comanda=values(usa_comanda),
	nome_certificado=values(nome_certificado),
	nome_cidade=values(nome_cidade),
	estado=values(estado),
	sigla_estado=values(sigla_estado),
	codigo_estado=values(codigo_estado),
	faz_pedido_estoque_zerado=values(faz_pedido_estoque_zerado),
	unidade_principal=values(unidade_principal),
	grupo_produto_padrao=values(grupo_produto_padrao),
	email=values(email),
	indicador_inscricao_estadual=values(indicador_inscricao_estadual),
	finalizar_pedido_sem_forma_pagamento=values(finalizar_pedido_sem_forma_pagamento),
	nao_baixar_estoque_na_emissao_nota_fiscal=values(nao_baixar_estoque_na_emissao_nota_fiscal),
	nao_reserva_estoque_no_pedido=values(nao_reserva_estoque_no_pedido),
	tipo_comissionamento_padrao=values(tipo_comissionamento_padrao),
	codigo_plano_contratado=values(codigo_plano_contratado),
	lancar_pedido_conta_corrente_cliente=values(lancar_pedido_conta_corrente_cliente),
	pedido_correntista_baixa_estoque_gera_venda=values(pedido_correntista_baixa_estoque_gera_venda),
	pedido_gera_venda=values(pedido_gera_venda);

DROP PROCEDURE IF EXISTS inserir_cesta_tributacao;
CREATE PROCEDURE inserir_cesta_tributacao()
 BEGIN
 DECLARE insertCestaTributacao text;
SET @insertCestaTributacao = 'INSERT INTO `cestatributacao` VALUES ';
IF VALOR_CODIGO_REGIME_TRIBUTARIO_DOCLIENTE = '1' THEN
  BEGIN
    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(7,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADO COM PERMISSÃO DE CREDITO ','1','101','1',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,5.000,NULL,'53','999',0.000,'08',0.000,0.000,'08',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(8,'",VALOR_CODIGO_DOCLIENTE,"','NÃO TRIBUTADO PELO SIMPLES NACIONAL','1','400','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(10,'",VALOR_CODIGO_DOCLIENTE,"','SUBSTITUIÇÃO TRIBUTARIA','1','500','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'53','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(12,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADA COM PERMISSÃO DE CREDITO ICMS ST','1','201','3',0.000,NULL,0.000,'4',0.000,0.000,18.000,'',NULL,0.000,NULL,'53','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(13,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADA SEM PERMISSÃO DE CRÉDITO ICMS ST','1','202','3',0.000,NULL,0.000,'4',0.000,0.000,0.000,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(14,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADO SEM PERMISSÃO DE CREDITO ','1','102','1',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'08',0.000,0.000,'08',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00)");
  END;
ELSE
  BEGIN
   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(9,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADO 20,5% PELO REGIME NORMAL','3','00','3',0.000,20.500,0.000,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'50','999',0.000,'01',1.650,0.000,'01',7.600,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(15,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADA E COM COBRANÇA DO ICMS POR SUBSTITUIÇÃO ','3','10','3',0.000,18.000,0.000,'4',0.000,0.000,18.000,'',NULL,NULL,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(16,'",VALOR_CODIGO_DOCLIENTE,"','ICMS COBRADO ANTERIORMENTE POR ST','3','60','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(17,'",VALOR_CODIGO_DOCLIENTE,"','COM REDUÇÃO DE BC E COBRANÇA DO ICMS ST','3','70','3',0.000,0.000,0.000,'4',0.000,0.000,18.000,'',NULL,NULL,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(18,'",VALOR_CODIGO_DOCLIENTE,"','NÃO TRIBUTADA','3','41','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(24,'",VALOR_CODIGO_DOCLIENTE,"',' COM REDUÇÃO DE BASE DE CÁLCULO','3','20','3',15.000,18.000,0.000,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'',NULL,NULL,'08',0.000,0.000,'08',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(25,'",VALOR_CODIGO_DOCLIENTE,"','ISENTO','3','40','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'49',0.000,0.000,'49',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00)");
  END;
END IF;
  SET @insertCestaTributacao = concat(@insertCestaTributacao, ' ON DUPLICATE KEY UPDATE ');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'codigo=values(codigo),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'codigo_empresa=values(codigo_empresa),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'descricao=values(descricao),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'regime_tributario=values(regime_tributario),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cst=values(cst),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'modalidade_basecalculo_icms=values(modalidade_basecalculo_icms),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_basecalculo_icms=values(percentual_basecalculo_icms),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_icms_dentro_estado=values(aliquota_icms_dentro_estado),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_icms_fora_estado=values(aliquota_icms_fora_estado),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'modalidade_basecalculo_st=values(modalidade_basecalculo_st),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_margem_icms_st=values(percentual_margem_icms_st),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_reducao_basecalculo_icms_st=values(percentual_reducao_basecalculo_icms_st),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_icms_st=values(aliquota_icms_st),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'motivo_desoneracao_icms=values(motivo_desoneracao_icms),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_base_operacao_propria=values(percentual_base_operacao_propria),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_credito_nascional=values(aliquota_credito_nascional),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_diferimento=values(percentual_diferimento),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'situacao_tributaria_ipi=values(situacao_tributaria_ipi),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'enquadramento_ipi=values(enquadramento_ipi),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_ipi=values(aliquota_ipi),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'codigo_situacao_tributaria_pis=values(codigo_situacao_tributaria_pis),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_pis=values(aliquota_pis),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_pis_st=values(aliquota_pis_st),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cofins_situacao_tributaria=values(cofins_situacao_tributaria),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_cofins=values(aliquota_cofins),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_cofins_st=values(aliquota_cofins_st),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_imposto_servico=values(aliquota_imposto_servico),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'indicador_iss=values(indicador_iss),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'indicador_incentivo_fiscal=values(indicador_incentivo_fiscal),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_aliquota_combate_probreza=values(percentual_aliquota_combate_probreza),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_aliquota_produto_combate_probreza=values(percentual_aliquota_produto_combate_probreza),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_interestadual_envolvidas=values(aliquota_interestadual_envolvidas),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_icms_interestadual=values(percentual_icms_interestadual),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cfop_de_entrada=values(cfop_de_entrada),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cfop_de_saida=values(cfop_de_saida),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'observacao_fiscal=values(observacao_fiscal),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aplicacao=values(aplicacao),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_fcp=values(aliquota_fcp),');
  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_fcp_st=values(aliquota_fcp_st);');
  PREPARE stmt FROM @insertCestaTributacao;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END;
CALL inserir_cesta_tributacao();
DROP PROCEDURE IF EXISTS inserir_cesta_tributacao;

INSERT INTO `configuracao_documentos_fiscais`
     (codigo_empresa,tipo,ultima_nota,serie,data_ultima_nota,ambiente_fiscal,token,csc,modelo,usuario) VALUES
     (VALOR_CODIGO_DOCLIENTE, 55, '0', '1', CURRENT_DATE(), 2, NULL, NULL, 'NFE', VALOR_EMAIL_DOCLIENTE),
     (VALOR_CODIGO_DOCLIENTE, 65, '0', '1', CURRENT_DATE(), 2, '', '', 'NFCE', VALOR_EMAIL_DOCLIENTE)
ON DUPLICATE KEY UPDATE
 codigo_empresa=values(codigo_empresa)
,usuario=values(usuario);

INSERT INTO `conta_bancaria` (empresa,codigo_banco,agencia,numero_conta,nome_conta,chave_pix,nome_banco,site_banco,status,saldo_conta)
VALUES (VALOR_CODIGO_DOCLIENTE,NULL,NULL,NULL,'CAIXA PRINCIPAL',NULL,'',NULL,1,0)
 ON DUPLICATE KEY UPDATE
  empresa=values(empresa)
 ,codigo_banco=values(codigo_banco)
 ,agencia=values(agencia)
 ,numero_conta=values(numero_conta)
 ,nome_conta=values(nome_conta)
 ,chave_pix=values(chave_pix)
 ,nome_banco=values(nome_banco)
 ,site_banco=values(site_banco)
 ,status=values(status);

SET @conta:=0;
SELECT codigo FROM conta_bancaria ORDER BY codigo DESC LIMIT 1 INTO @conta;

INSERT INTO `forma_pagamento` 
  (`codigo_empresa`, `tipo_pagamento`, `conta_bancaria`, `descricao`, `desconto`, `acrescimo`, `taxa`, `valor_minimo`, `prazo_recebimento`, `pdv`, `administradora_cartao`, `data_cadastro`, `data_atualizacao`, `quantidade_parcelas`, `status`, `padrao`, `dias_liberacao_comissao`, `parcela_inicial_para_juros`, `gera_financeiro`, `baixa_automatica`)
VALUES
 (VALOR_CODIGO_DOCLIENTE, 1, @conta, 'DINHEIRO', 20.00, 0.00, 0.00, 0.00, NULL, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
,(VALOR_CODIGO_DOCLIENTE, 17, @conta, 'PIX', 10.00, NULL, NULL, NULL, NULL, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
,(VALOR_CODIGO_DOCLIENTE, 99, @conta, 'CORRENTISTA', NULL, NULL, NULL, NULL, 30, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
,(VALOR_CODIGO_DOCLIENTE, 19, @conta, 'VALE', NULL, NULL, NULL, NULL, NULL, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
,(VALOR_CODIGO_DOCLIENTE, 3, @conta, 'CARTÃO CRÉDITO', 0.00, 0.00, 0.00, NULL, 30, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 6, 1, 0, 0, 0, 1, 1)
,(VALOR_CODIGO_DOCLIENTE, 4, @conta, 'CARTÃO DE DÉBITO', 0.00, 0.00, 0.00, 0.00, 0, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 1, 1)
,(VALOR_CODIGO_DOCLIENTE, 15, @conta, 'BOLETO', 0.00, 0.00, 0.00, NULL, 0, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 12, 1, 0, 0, 0, 1, 1)
,(VALOR_CODIGO_DOCLIENTE, 17, @conta, 'PIX MANUAL', 0.00, 0.00, 0.00, 0.00, 0, 1, NULL, CURRENT_DATE(), NULL, 0, 1, 0, 0, 0, 1, 1)
,(VALOR_CODIGO_DOCLIENTE, 5, @conta, 'TEF CRÉDITO', 0.00, 0.00, 0.00, 0.00, 0, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 1, 1, 0, 0, 0, 1, 1)
,(VALOR_CODIGO_DOCLIENTE, 6, @conta, 'TEF DÉBITO', 0.00, 0.00, 0.00, 0.00, 0, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 1, 1)
 ON DUPLICATE KEY UPDATE
 codigo_empresa=values(codigo_empresa),
tipo_pagamento=values(tipo_pagamento),
conta_bancaria=values(conta_bancaria),
descricao=values(descricao),
desconto=values(desconto),
acrescimo=values(acrescimo),
taxa=values(taxa),
valor_minimo=values(valor_minimo),
prazo_recebimento=values(prazo_recebimento),
pdv=values(pdv),
administradora_cartao=values(administradora_cartao),
data_cadastro=values(data_cadastro),
data_atualizacao=values(data_atualizacao),
quantidade_parcelas=values(quantidade_parcelas),
status=values(status),
padrao=values(padrao),
dias_liberacao_comissao=values(dias_liberacao_comissao),
parcela_inicial_para_juros=values(parcela_inicial_para_juros),
gera_financeiro=values(gera_financeiro);

INSERT INTO `local_estoque` (codigo_empresa,descricao,status,padrao) VALUES (VALOR_CODIGO_DOCLIENTE,'ESTOQUE PRINCIPAL',1,1)
ON DUPLICATE KEY UPDATE
 codigo_empresa=values(codigo_empresa)
,descricao=values(descricao)
,status=values(status)
,padrao=values(padrao);

INSERT INTO `metas_vendedores` VALUES (VALOR_CODIGO_DOCLIENTE,VALOR_EMAIL_DOCLIENTE,0.00,0.00,CURRENT_TIMESTAMP(),VALOR_EMAIL_DOCLIENTE,'CIF')
ON DUPLICATE KEY UPDATE
 codigo_empresa=values(codigo_empresa)
,codigo_vendedor=values(codigo_vendedor)
,meta=values(meta)
,porcentagem_comissao=values(porcentagem_comissao)
,data_lancamento=values(data_lancamento)
,usuario_atualizacao=values(usuario_atualizacao)
,tipo_comissionamento=values(tipo_comissionamento);

INSERT INTO `seguencia_nota_fiscal` VALUES (1,VALOR_CODIGO_DOCLIENTE,'2',VALOR_EMAIL_DOCLIENTE,0,NULL,NULL,'P')
ON DUPLICATE KEY UPDATE
 codigo_empresa=values(codigo_empresa)
,usuario=values(usuario);

DELETE FROM permissao WHERE codigo_empresa=VALOR_CODIGO_DOCLIENTE;
INSERT INTO `permissao` VALUES VALOR_PERMISSOES;

 INSERT INTO `usuario_empresa` (usuario,empresa,perfil,status,porcentagem_maxima_desconto,vendedor,principal,data_atualizacao,usuario_atualizacao)
 SELECT
    usuarioEmpresa.usuario,
    VALOR_CODIGO_DOCLIENTE,
    usuarioEmpresa.perfil ,
    1,
    usuarioEmpresa.porcentagem_maxima_desconto,
    usuarioEmpresa.vendedor,
    0,
    usuarioEmpresa.data_atualizacao,
    usuarioEmpresa.usuario_atualizacao
 FROM
    	usuario_empresa AS usuarioEmpresa
   where
   	usuarioEmpresa.empresa<>VALOR_CODIGO_DOCLIENTE
on duplicate key update
 usuario = values(usuario)
,empresa = values(empresa)
,perfil = values(perfil)
,status = values(status)
,porcentagem_maxima_desconto = values(porcentagem_maxima_desconto)
,vendedor = values(vendedor)
,principal = values(principal)
,data_atualizacao = values(data_atualizacao)
,usuario_atualizacao = values(usuario_atualizacao);

INSERT INTO embalagem_empresa (
    embalagem,
    codigo_empresa,
    referencia_produto,
    preco_custo,
    outros_custos,
    preco_minimo,
    cesta_tributacao,
    fornecedor_principal,
    estoque_minimo,
    estoque_atual,
    quantidade_atacado,
    embalagem_principal,
    status,
    codigo_unidade,
    fator_multiplicacao,
    data_ultima_atualizacao,
    usuario_ultima_atualizacao,
    usuario_email_atualizacao,
    data_cadastro,
    embalagem_padrao,
    estoque_resevado
)
SELECT
    embalagem,
    VALOR_CODIGO_DOCLIENTE AS codigo_empresa,
    referencia_produto,
    preco_custo,
    outros_custos,
    preco_minimo,
    cesta_tributacao,
    fornecedor_principal,
    estoque_minimo,
	 0,
    quantidade_atacado,
    embalagem_principal,
    status,
    codigo_unidade,
    fator_multiplicacao,
    CURRENT_TIMESTAMP AS data_ultima_atualizacao,
    usuario_ultima_atualizacao,
    usuario_email_atualizacao,
    CURRENT_TIMESTAMP AS data_cadastro,
    embalagem_padrao,
	 0
 FROM
    embalagem_empresa
 WHERE
    codigo_empresa = VALOR_CODIGO_DO_EMPRESA_PRINCIPAL
 on duplicate key update
    embalagem = values(embalagem),
    codigo_empresa = values(codigo_empresa),
    referencia_produto = values(referencia_produto),
    preco_custo = values(preco_custo),
    outros_custos = values(outros_custos),
    preco_minimo = values(preco_minimo),
    cesta_tributacao = values(cesta_tributacao),
    fornecedor_principal = values(fornecedor_principal),
    estoque_minimo = values(estoque_minimo),
    estoque_atual = values(estoque_atual),
    quantidade_atacado = values(quantidade_atacado),
    embalagem_principal = values(embalagem_principal),
    status = values(status),
    codigo_unidade = values(codigo_unidade),
    fator_multiplicacao = values(fator_multiplicacao),
    data_ultima_atualizacao = values(data_ultima_atualizacao),
    usuario_ultima_atualizacao = values(usuario_ultima_atualizacao),
    usuario_email_atualizacao = values(usuario_email_atualizacao),
    data_cadastro = values(data_cadastro),
    embalagem_padrao = values(embalagem_padrao),
    estoque_resevado = values(estoque_resevado);


INSERT INTO tabela_preco_embalagem_empresa (
    referencia_produto,
    embalagem,
    codigo_tabela_preco,
    codigo_empresa,
    preco_venda,
    comissao_venda,
    status,
    principal,
    tipo_comissao,
    preco_atacado,
    data_ultima_atualizacao,
    usuario_ultima_atualizacao,
    usuario_email_atualizacao,
    padrao,
    markup,
    data_cadastro
)
SELECT
    referencia_produto,
    embalagem,
    codigo_tabela_preco,
    VALOR_CODIGO_DOCLIENTE AS codigo_empresa,
    preco_venda,
    comissao_venda,
    status,
    principal,
    tipo_comissao,
    preco_atacado,
    CURRENT_TIMESTAMP AS data_ultima_atualizacao,
    usuario_ultima_atualizacao,
    usuario_email_atualizacao,
    padrao,
    markup,
    data_cadastro
 FROM
    tabela_preco_embalagem_empresa
 WHERE
    codigo_empresa = VALOR_CODIGO_DO_EMPRESA_PRINCIPAL
 on duplicate key update
	referencia_produto = values(referencia_produto),
    embalagem = values(embalagem),
    codigo_tabela_preco = values(codigo_tabela_preco),
    codigo_empresa = values(codigo_empresa),
    preco_venda = values(preco_venda),
    comissao_venda = values(comissao_venda),
    status = values(status),
    principal = values(principal),
    tipo_comissao = values(tipo_comissao),
    preco_atacado = values(preco_atacado),
    data_ultima_atualizacao = values(data_ultima_atualizacao),
    usuario_ultima_atualizacao = values(usuario_ultima_atualizacao),
    usuario_email_atualizacao = values(usuario_email_atualizacao),
    padrao = values(padrao),
    markup = values(markup),
    data_cadastro = values(data_cadastro);


INSERT INTO grade_produto_empresa (
    codigo,
    codigo_empresa,
    estoque_minimo,
    estoque_atual,
    referencia_produto,
    estoque_reservado
)
SELECT
    codigo,
    VALOR_CODIGO_DOCLIENTE AS codigo_empresa,
    estoque_minimo,
    0,
    referencia_produto,
    0
 FROM
    grade_produto_empresa
 WHERE
    codigo_empresa = VALOR_CODIGO_DO_EMPRESA_PRINCIPAL
 on duplicate key update
	codigo = values(codigo),
    codigo_empresa = values(codigo_empresa),
    estoque_minimo = values(estoque_minimo),
    estoque_atual = values(estoque_atual),
    referencia_produto = values(referencia_produto),
    estoque_reservado = values(estoque_reservado);

    VALOR_PDVS 