CREATE DATABASE IF NOT EXISTS `zoomtecn_zion_NOME_BANCO_DADOS`;
USE `zoomtecn_zion_NOME_BANCO_DADOS`;         

CREATE TABLE IF NOT EXISTS `grupo` (
  `codigo` int NOT NULL AUTO_INCREMENT COMMENT 'CODIGO DO GRUPO',
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'DESCRICAO DO GRUPO',
  `imagem` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `status` smallint NOT NULL DEFAULT '1',
  `data_ultima_atualizacao` timestamp NULL DEFAULT NULL,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `padrao` smallint NOT NULL DEFAULT '0',
  `identificador_imagem` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='CADASTRO DE GRUPO';

CREATE TABLE IF NOT EXISTS `subgrupo` (
  `codigo_subgrupo` int NOT NULL AUTO_INCREMENT,
  `codigo_grupo` int NOT NULL,
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  `imagem` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `data_ultima_atualizacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `identificador_imagem` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo_subgrupo`),
  KEY `sub_grupoFKgrupo` (`codigo_grupo`) USING BTREE,
  CONSTRAINT `sub_grupoFKgrupo` FOREIGN KEY (`codigo_grupo`) REFERENCES `grupo` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `unidade` (
  `codigo` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `descricao` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `fracionado` smallint NOT NULL DEFAULT '1',
  `padrao` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`),
  KEY `codigo` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `tabela_preco` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `desconto` double(12,2) DEFAULT '0.00',
  `acrescimo` double(12,2) DEFAULT '0.00',
  `status` smallint NOT NULL DEFAULT '1',
  `comissao_venda` double(15,2) unsigned DEFAULT '0.00',
  `tipo_comissao` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'P',
  `padrao` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `empresa` (
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'identificador unico das empresas',
  `cnpj` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `razao_social` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `nome_fantasia` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_regime_tributario` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CRT: 1 - Simples Nacional; 2 - Simples Nacional - excesso de sublimite de receita bruta; 3 - Regime Normal',
  `inscricao_estadual` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `inscricao_estadual_st` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `inscricao_municipal` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `logradouro` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `numero` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `bairro` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_cidade` varchar(7) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `complemento` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `cep` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `cnae` varchar(7) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'codigo de atividade da empresa',
  `grupo_empresa` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '' COMMENT 'codigo do grupo de empresa',
  `certificado_digital` blob COMMENT 'o arquivo do certificado digital da empresa esta em bytes',
  `senha_certificado` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `cesta_tributacao_padrao` int DEFAULT NULL,
  `matriz` smallint NOT NULL DEFAULT '0' COMMENT '0-NÃO,1-SIM; Defini se a empresa e matriz',
  `mensagem_nfe` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'campo para adicionar obervacao padrao da nfe',
  `mensagem_cupom` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'observacao padrao para o cupom fiscal',
  `mensagem_orcamento` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'observacao padrao para o orcamento',
  `mensagem_pedido_venda` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'observacao padrao para o pedido de venda',
  `mensagem_pedido_compra` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'observacao padrao para o pedido compra',
  `mensagem_cte` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'observacao padrao para o conhecimento de transporte',
  `codigo_tabela_preco_padrao` int DEFAULT NULL COMMENT 'codigo da tabela de preco',
  `identificar_vendedor_pdv` smallint unsigned DEFAULT '0' COMMENT '0 - NAO,1-SIM',
  `identificar_consumidor_pdv` smallint unsigned DEFAULT '0' COMMENT '0-NAO,1-SIM',
  `senha_gerente_vale_pdv` smallint unsigned DEFAULT '0',
  `controla_caixa_pdv` smallint unsigned DEFAULT '1' COMMENT '0-NAO,1-SIM',
  `senha_gerente_controle_caixa_pdv` smallint unsigned DEFAULT '1' COMMENT '0-NAO,1-SIM',
  `aviso_sangria_pdv` smallint unsigned DEFAULT '0' COMMENT '0-NAO,1-SIM',
  `conciliacao_pdv` smallint unsigned DEFAULT '0' COMMENT '0-NAO,1-SIM',
  `adicionar_obervacao_cupom_pdv` smallint unsigned DEFAULT '0' COMMENT '0-NAO,1-SIM',
  `exibir_imagem_produto_pesquisa_pdv` smallint unsigned DEFAULT '0' COMMENT '0-NAO,1-SIM',
  `valor_maximo_venda_pdv` double(15,2) unsigned DEFAULT '1000.00',
  `valor_maximo_no_caixa_pdv` double(15,2) unsigned DEFAULT '0.00',
  `valor_quebra_caixa_pdv` double(10,2) unsigned DEFAULT '0.00',
  `data_ultima_atualizacao` timestamp NULL DEFAULT NULL,
  `data_cadastro` timestamp NULL DEFAULT NULL,
  `logo` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `tipo_emissao_pdv` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'NFCe',
  `tema_principal_pdv` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'principal',
  `numero_vias_tef` int NOT NULL DEFAULT '2',
  `movimenta_estoque` smallint NOT NULL DEFAULT '1',
  `senha_gerente_contingencia` smallint NOT NULL DEFAULT '1',
  `usa_catraca` smallint NOT NULL DEFAULT '0',
  `aviso_estoque_minimo` smallint NOT NULL DEFAULT '0',
  `salva_ultima_pesquisa` smallint NOT NULL DEFAULT '0',
  `apelido` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `vencimento_certificado` date DEFAULT NULL,
  `prazo_garantia_servico` int DEFAULT NULL,
  `prazo_orcamento` int DEFAULT NULL,
  `prazo_pedido` int DEFAULT NULL,
  `usa_comanda` smallint unsigned NOT NULL DEFAULT '0',
  `nome_certificado` varchar(180) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nome_cidade` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `estado` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `sigla_estado` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_estado` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `faz_pedido_estoque_zerado` smallint NOT NULL DEFAULT '0',
  `unidade_principal` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'UN',
  `grupo_produto_padrao` int NOT NULL,
  `email` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `indicador_inscricao_estadual` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 - CONTRIBUINTE ICMS, 2- CONTRIBUINTE ISENTO, 9-NAO CONTRIBUINTE',
  `finalizar_pedido_sem_forma_pagamento` smallint NOT NULL DEFAULT '1' COMMENT '1-PRECISA TER AO MENOS UMA FORMA DE PAGAMENTO,2-POR FINALIZAR SEM FORMA PAGAMENTO',
  `nao_baixar_estoque_na_emissao_nota_fiscal` smallint DEFAULT '0',
  `nao_reserva_estoque_no_pedido` smallint DEFAULT '0',
  `tipo_comissionamento_padrao` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CVP-COMISSAO VINCULADA PARCELADA,CIF-COMISSAO INTEGRAl FATURAMENTO,CIP-COMISSAO INTEGRAL PRIMEIRA PARCELA,CIU-COMISSAO INTEGRAL ULTIMA PARCELA',
  `codigo_plano_contratado` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `lancar_pedido_conta_corrente_cliente` smallint unsigned NOT NULL DEFAULT '0' COMMENT '0-Não lança pedio na conta corrente do cliente,1-lança pedido na conta corrente do cliente',
  `pedido_correntista_baixa_estoque_gera_venda` smallint DEFAULT '0' COMMENT '0 - nao marcado, 1 - marcado',
  `pedido_gera_venda` smallint DEFAULT '0' COMMENT '0 - nao marcado, 1 - marcado',
  PRIMARY KEY (`codigo_empresa`),
  KEY `cnpj` (`cnpj`) USING BTREE,
  KEY `empresaFKtabela_preco` (`codigo_tabela_preco_padrao`) USING BTREE,
  KEY `nome_fantasia` (`nome_fantasia`) USING BTREE,
  KEY `razao_social` (`razao_social`) USING BTREE,
  CONSTRAINT `empresaFKtabela_preco` FOREIGN KEY (`codigo_tabela_preco_padrao`) REFERENCES `tabela_preco` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='cadastro de empresas';

CREATE TABLE IF NOT EXISTS `tipo_movimentacao` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'ENTRADA',
  `descricao` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `atualiza_estoque` smallint NOT NULL DEFAULT '1',
  `gera_financeiro` smallint NOT NULL DEFAULT '1',
  `valor_base` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'VENDA',
  `data_cadastro` date NOT NULL,
  `data_atualizacao` timestamp NULL DEFAULT NULL,
  `padrao` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`),
  KEY `descricao` (`descricao`) USING BTREE,
  KEY `tipo` (`tipo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `tipo_pagamento` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `a_prazo` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `perfil_usuario` (
  `nome` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `status` smallint NOT NULL DEFAULT '1',
  `usuario_cadastro` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `data_cadastro` date NOT NULL,
  `data_atualizacao` date NOT NULL,
  `usuario_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `permissao` (
  `codigo` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `perfil` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `aplicacao` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `modulo` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `nome` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `funcao` json NOT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  KEY `perfil` (`perfil`),
  KEY `codigo_empresa` (`codigo_empresa`,`aplicacao`),
  CONSTRAINT `permissao_ibfk_1` FOREIGN KEY (`perfil`) REFERENCES `perfil_usuario` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `usuario` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `email` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `senha` varchar(72) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `nome` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `foto` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `data_cadastro` date DEFAULT NULL,
  `data_atualizacao` timestamp NULL DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  `identificador_foto` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `identificador` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `nao_acessa_sistema` smallint NOT NULL DEFAULT '0' COMMENT '0 - acessa, 1 - nao acessa',
  `aplicacao_principal` text COLLATE utf8mb3_unicode_ci,
  `usuario_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_validacao_pdv` smallint NOT NULL DEFAULT '0',
  `usuario_acessa_pdv` smallint DEFAULT '0',
  PRIMARY KEY (`codigo`),
  KEY `email` (`email`) USING BTREE,
  KEY `senha` (`senha`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `usuario_empresa` (
  `usuario` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `perfil` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  `porcentagem_maxima_desconto` double(6,2) DEFAULT '0.00',
  `vendedor` smallint DEFAULT NULL,
  `principal` smallint NOT NULL DEFAULT '1',
  `data_atualizacao` timestamp NULL DEFAULT NULL,
  `usuario_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usuario`,`empresa`),
  KEY `empresa` (`empresa`) USING BTREE,
  KEY `usuario` (`usuario`,`status`) USING BTREE,
  KEY `principal` (`principal`),
  KEY `usuario_empresaFKperfil_usuario` (`perfil`),
  CONSTRAINT `usuario_empresaFKempresa` FOREIGN KEY (`empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `usuario_empresaFKperfil_usuario` FOREIGN KEY (`perfil`) REFERENCES `perfil_usuario` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `autorizacoes` (
  `codigo` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `aplicacao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `usuario` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `operacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data_solicitacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_autorizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_autorizacao` timestamp NULL DEFAULT NULL,
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `centro_custo` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `cestatributacao` (
  `codigo` int NOT NULL AUTO_INCREMENT COMMENT 'CODIGO DA CESTA DE TRIBUTACAO ',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `descricao` varchar(50) NOT NULL COMMENT 'DESCRICAO DA CESTA DE TRIBUTACAO',
  `regime_tributario` varchar(1) NOT NULL DEFAULT '1' COMMENT 'REGIME DE TRIBUTCAO: 1-SIMPLES NACIONAL / 2-LUCRO PRESUMIDO / 3- REGIME NORMAL',
  `cst` varchar(3) NOT NULL COMMENT 'CODIGO DO CST',
  `modalidade_basecalculo_icms` varchar(1) NOT NULL DEFAULT '1' COMMENT 'exemplo:0,1,2,3 INFORMAR A MODALIDADE DE DETERMINACAO DA BC DO ICMS: 0 - Margem Valor Agregado (%), 1 - Pauta (valor), 2 - Preço Tabelado Máximo (valor) ou 3 - Valor da Operação.',
  `percentual_basecalculo_icms` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR O PERCENTUAL DE REDUCAO DA BC DO ICMS DA OPERACAO PROPRIA',
  `aliquota_icms_dentro_estado` double(6,3) DEFAULT '0.000' COMMENT 'ALIQUOTA DE ICMS DENTRO DO ESTADO',
  `aliquota_icms_fora_estado` double(6,3) DEFAULT '0.000' COMMENT 'ALIQUOTA DE ICMS FORA DO ESTADO',
  `modalidade_basecalculo_st` varchar(1) DEFAULT NULL COMMENT 'exemplos: 0,1,2,3,4,5, INFORMAR A MODALIDADE DE DETERMINACAO DA BC DO ICMS ST:',
  `percentual_margem_icms_st` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR O PERCENTUAL DA MARGEM DE VALOR ADICIONADO ICMS ST (QUANTIDADE DE DECIMAIS ALTERADO PARA',
  `percentual_reducao_basecalculo_icms_st` double(6,3) DEFAULT '0.000' COMMENT 'PERCENTUAL DE REDUCAO DA BASE DE CALCULO DO ICMS ST',
  `aliquota_icms_st` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR A ALIQUOTA DO ICMS ST',
  `motivo_desoneracao_icms` varchar(1) DEFAULT NULL COMMENT 'exemplos: 1,2,3,4,5,6,7,8,9 , MOTIVO DA DESONERACAO DO ICMS',
  `percentual_base_operacao_propria` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR O PERCENTUAL DA BC OPERACAO PROPRIA (QUANTIDADE DE DECIMAIS ALTERADO PARA ACEITAR DE 2 A 4',
  `aliquota_credito_nascional` double(6,3) DEFAULT '0.000' COMMENT 'ALIQUOTA APLICAVEL DO CREDITO DO SIMPLES NACIONAL',
  `percentual_diferimento` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR O PERCENTUAL DO DIFERIMENTO',
  `situacao_tributaria_ipi` varchar(2) DEFAULT NULL COMMENT 'CODIGO DE SITUACAO TRIBUTARIA DO IPI',
  `enquadramento_ipi` varchar(3) DEFAULT '999' COMMENT 'INFORMAR O CODIGO DE ENQUADRAMENTO LEGAL DO IPI, INFORMAR 999 ENQUANTO A TABELA NAO TIVER SIDO CRIADA PELA',
  `aliquota_ipi` double(6,3) DEFAULT '0.000' COMMENT 'ALIQUOTA DO IPI',
  `codigo_situacao_tributaria_pis` varchar(2) NOT NULL DEFAULT '7' COMMENT 'INFORMAR O CODIGO DE SITUACAO TRIBUTÁRIA DO PIS, PARA CST=05 INFORME O GRUPO PISST PISST',
  `aliquota_pis` double(6,3) DEFAULT '0.000' COMMENT 'ALIQUOTA DO PIS',
  `aliquota_pis_st` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR A ALIQUOTA PERCENTUAL DO PIS ST, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA AD VALOREM',
  `cofins_situacao_tributaria` varchar(2) NOT NULL DEFAULT '7' COMMENT 'INFORMAR O CODIGO DE SITUACAO TRIBUTÁRIA DO COFINS, PARA CST=05 INFORME O GRUPO COFINSST COFINSST',
  `aliquota_cofins` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR A ALIQUOTA PERCENTUAL DO COFINS, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA AD VALOREM.',
  `aliquota_cofins_st` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR A ALIQUOTA PERCENTUAL DO COFINS ST, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA AD VALOREM',
  `aliquota_imposto_servico` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR A ALIQUOTA DO ISSQN',
  `indicador_iss` varchar(1) DEFAULT NULL COMMENT 'exemplos: 1,2,3,4,5,6,7, INFORMAR INDICADOR DA EXIGIBILIDADE DO ISS',
  `indicador_incentivo_fiscal` varchar(1) DEFAULT '2' COMMENT 'exemplo: 1,2, INFORMAR INDICADOR DE INCENTIVO FISCAL: 1=SIM / 2=NAO',
  `percentual_aliquota_combate_probreza` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR O PERCENTUAL ADICIONAL INSERIDO NA ALIQUOTA INTERNA DA UF DE DESTINO, RELATIVO AO FUNDO DE COMBATE À POBREZA (FCP) NAQUELA UF',
  `percentual_aliquota_produto_combate_probreza` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR A ALIQUOTA ADOTADA NAS OPERAÇÕES INTERNAS NA UF DE DESTINO PARA O PRODUTO / MERCADORIA. A ALIQUOTA DO FUNDO DE COMBATE A POBREZA,\r\nSE EXISTENTE PARA O PRODUTO / MERCADORIA, DEVE SER INFORMADA NO CAMPO PROPRIO (PFCPUFDEST) NAO DEVENDO SER SOMADA À ESSA ALIQUOTA INTERNA',
  `aliquota_interestadual_envolvidas` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR A ALIQUOTA INTERESTADUAL DAS UF ENVOLVIDAS',
  `percentual_icms_interestadual` double(6,3) DEFAULT '0.000' COMMENT 'INFORMAR PERCENTUAL DE ICMS INTERESTADUAL PARA A UF DE DESTINO:',
  `cfop_de_entrada` int NOT NULL DEFAULT '0' COMMENT 'CFOP DE ENTRADA CA',
  `cfop_de_saida` int NOT NULL DEFAULT '0' COMMENT 'CFOP DE SAIDA CA',
  `observacao_fiscal` text COMMENT 'OBSERVACAO FISCAL',
  `aplicacao` text COMMENT 'APLICACAO DA SEXTA (NAO OBRIGATORIO)',
  `aliquota_fcp` double(15,2) DEFAULT '0.00' COMMENT 'ALIQUOTA DO FUNDO E COMBATE A POBREZA',
  `aliquota_fcp_st` double(15,2) DEFAULT '0.00' COMMENT 'ALIQUOTA DO FUNDO E COMBATE A POBREZA DE SUBSTITUICAO TRIBUTARIA',
  PRIMARY KEY (`codigo`,`codigo_empresa`),
  KEY `cesta_tributacaoFKempresa` (`codigo_empresa`) USING BTREE,
  CONSTRAINT `cesta_tributacaoFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb3 COMMENT='CADASTRO DE CESTA DE TRIBUTACAO';


CREATE TABLE IF NOT EXISTS `pessoa` (
  `documento_identificacao` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'DOC DE IDENTIFICACAO (CPF,CNPJ OU PASSAPORTE)',
  `razao_social` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'RAZAO SOCIAL',
  `nome_fantasia` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME FANTASIA / APELIDO',
  `tipo_pessoa` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'O' COMMENT 'TIPO DE PESSOA (J,F OU O)',
  `inscricao_estadual_rg` varchar(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSC ESTADUAL OU RG',
  `inscricao_municipal` varchar(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSC MUNICIPAL',
  `data_nascimento_fundacao` date DEFAULT NULL COMMENT 'DATA DE NASCIMENTO OU FUNDACAO',
  `estado_civil` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'O' COMMENT 'ESTADO CIVIL (C) CASADO (S) SOLTEIRO (D) DIVORCIADO (V) VIUVO (O) OUTROS)',
  `tipo_sexo` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'O' COMMENT 'TIPO DE SEXO (M) MASCULINO (F) FEMININO (O) OUTROS',
  `regime_tributario` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'REGIME TRIBUTARIO (0,1,2 ou 3)',
  `cnae` varchar(7) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CLASSIFICACAO NACIONAL DE ATIV. ECONOMICAS',
  `suframa` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO SUFRAMA (SUPERITENDENCIA ZONA FRANCA MANAUS)',
  `inscricao_estadual_st` varchar(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO ESTADUAL DE SUBSTITUICAO TRIBUTARIO',
  `funcionario` smallint NOT NULL DEFAULT '0' COMMENT 'DEFINE SE A PESSOA E OU NAO FUNCIONARIO (S OU N)',
  `fornecedor` smallint NOT NULL DEFAULT '0' COMMENT 'DEFINE SE A PESSOA E OU NAO FORNECEDOR (S OU N)',
  `cliente` smallint NOT NULL DEFAULT '0' COMMENT 'DEFINE SE A PESSOA E OU NAO CLIENTE (S OU N)',
  `transportadora` smallint NOT NULL DEFAULT '0' COMMENT 'DEFINE SE A PESSOA E OU NAO TRANSPORTADORA (S OU N)',
  `produtor` smallint NOT NULL DEFAULT '0' COMMENT 'DEFINE SE A PESSOA E OU NAO PRODUTOR',
  `fabricante` smallint NOT NULL DEFAULT '0' COMMENT 'DEFINE SE A PESSOA E OU NAO FABRICANTE (S OU N)',
  `limite_credito` double(12,2) DEFAULT '0.00' COMMENT 'LIMETE DE CREDITO CASO SEJA CLIENTE',
  `valor_pendente_conta_receber` double(12,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL PENDENTE NO CONTAS A RECEBER',
  `valor_pendente_conta_pagar` double(12,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL PENDENTE NO CONTAS A PAGAR',
  `correntista` smallint NOT NULL DEFAULT '0' COMMENT 'CORRENTISTA (0-NAO OU 1-SIM)',
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'CAMPO PARA OBSERVACOES SOBRE ESTA PESSOA',
  `status` smallint NOT NULL DEFAULT '1' COMMENT 'SITUACAO DA PESSOA ATIVO OU INATIVO (1 OU 0)',
  `data_ultima_atualizacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'DATA DA ULTIMA ATUALIZACAO',
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'USUARIO DA ULTIMA ATUALIZACOO',
  `indicador_ie` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT 'INDICADOR DA IE DO DESTINATARIO (1) CONTRIBUINTE ICMS ,\r\n(2) CONTRIBUINTE ISENTO DE INSCRICAO NO CADASTRO DE CONTRIBUINTES DO ICMS ou (9) NAO CONTRIBUINTE, QUE PODE OU NAO POSSUIR INSCRICAO ESTADUAL NO CADASTRO DE CONTRIBUINTES DO ICMS',
  `imagem` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci COMMENT 'IMAGEM OU FOTO DA PESSOA',
  `tabela_preco` int DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL,
  `usuario_cadastro` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `membro` smallint NOT NULL DEFAULT '0',
  `visitante` smallint NOT NULL DEFAULT '0',
  `nacionalidade` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `representante` smallint NOT NULL DEFAULT '0',
  `dia_vencimento` int DEFAULT '30',
  `juros` double(12,2) DEFAULT '0.00',
  `dia_fechamento_fatura` int DEFAULT '30',
  `bloquear_compras_apos_vencimento` smallint DEFAULT '0',
  `total_compras` decimal(18,2) DEFAULT '0.00',
  `total_pago` decimal(18,2) DEFAULT '0.00',
  `identificador_imagem` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`documento_identificacao`),
  UNIQUE KEY `documento_identificacao` (`documento_identificacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='CADASTRO UNICO DE PESSOAS';

CREATE TABLE IF NOT EXISTS `marca` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `produto` (
  `referencia` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `descricao` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT NULL,
  `data_atualizacao` timestamp NULL DEFAULT NULL,
  `ncm` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `cest` varchar(7) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `grupo` int NOT NULL DEFAULT '0',
  `subgrupo` int DEFAULT NULL,
  `tipo_produto` int NOT NULL DEFAULT '0',
  `dias_validade` int DEFAULT NULL,
  `peso_bruto` double(15,3) DEFAULT NULL,
  `peso_liquido` double(15,3) DEFAULT NULL,
  `codigo_marca` int DEFAULT NULL,
  `aplicacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `composicao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `kit` smallint DEFAULT '0',
  `grade` smallint DEFAULT '0',
  `balanca` smallint DEFAULT '0',
  `status` smallint NOT NULL DEFAULT '1',
  `produto_importado` smallint NOT NULL DEFAULT '0',
  `imagem_principal` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `imagem1` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `imagem2` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `imagem3` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `imagem4` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `imagem5` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `confirma_preco` smallint NOT NULL DEFAULT '0',
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `dias_garantia` int DEFAULT NULL,
  `controla_numero_serie` smallint DEFAULT '0',
  `imagem_principal_identificador` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `imagem1_identificador` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `imagem2_identificador` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `imagem3_identificador` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `imagem4_identificador` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `imagem5_identificador` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `codigo_beneficio_fiscal` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`referencia`),
  KEY `descricao` (`descricao`) USING BTREE,
  KEY `produtoFKgrupo` (`grupo`) USING BTREE,
  KEY `produtoFKmarca` (`codigo_marca`) USING BTREE,
  KEY `produtoFKsubgrupo` (`subgrupo`) USING BTREE,
  KEY `Index 6` (`status`),
  CONSTRAINT `produtoFKgrupo` FOREIGN KEY (`grupo`) REFERENCES `grupo` (`codigo`),
  CONSTRAINT `produtoFKmarca` FOREIGN KEY (`codigo_marca`) REFERENCES `marca` (`codigo`),
  CONSTRAINT `produtoFKsubgrupo` FOREIGN KEY (`subgrupo`) REFERENCES `subgrupo` (`codigo_subgrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `local_estoque` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `descricao` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  `padrao` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`,`codigo_empresa`),
  KEY `local_estoqueFKempresa` (`codigo_empresa`) USING BTREE,
  CONSTRAINT `local_estoqueFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `comissao_vendedores` (
  `codigo` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valor_venda` double(18,2) NOT NULL DEFAULT '0.00',
  `numero_venda` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'PD' COMMENT 'PD-PENDENTE,PG-PAGO,CA-CANCELADO',
  `codigo_venda` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigo_empresa` (`codigo_empresa`),
  KEY `codigo_vendedor` (`codigo_vendedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS `comissao_vendedores_pagamento` (
  `codigo` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_pagamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `porcentagem_comissao` double(6,2) NOT NULL DEFAULT '0.00',
  `porcentagem_comissao_paga` double(6,2) NOT NULL DEFAULT '0.00',
  `meta` double(15,2) NOT NULL DEFAULT '0.00',
  `valor_comissao_paga` double(18,2) NOT NULL DEFAULT '0.00',
  `valor_venda` double(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`codigo`),
  KEY `codigo_empresa` (`codigo_empresa`,`codigo_vendedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `comissao_vendedores_parcela` (
  `seguencia_item` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0',
  `codigo_comissao` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `valor_comissao` double(18,2) NOT NULL DEFAULT '0.00',
  `numero_parcela` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo_comissionamento` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'CIF' COMMENT 'CVP-COMISSAO VINCULADA PARCELADA,CIF-COMISSAO INTEGRAl FATURAMENTO,CIP-COMISSAO INTEGRAL PRIMEIRA PARCELA,CIU-COMISSAO INTEGRAL ULTIMA PARCELA',
  `data_liberacao` date DEFAULT NULL,
  `tipo` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'C' COMMENT 'C-CREDITO,D-DEBITO',
  `status` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'LB' COMMENT 'LB - LIBERADO,PG - PAGO,CA - CANCELADO,AP-A PAGAR',
  `valor_item_devolvido` double(18,2) DEFAULT '0.00',
  `codigo_venda` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`seguencia_item`),
  KEY `codigo_empresa_vendedor` (`codigo_empresa`,`codigo_vendedor`),
  KEY `comissao_vendedores_parcelaFKcomissao_vendedores` (`codigo_comissao`),
  CONSTRAINT `comissao_vendedores_parcelaFKcomissao_vendedores` FOREIGN KEY (`codigo_comissao`) REFERENCES `comissao_vendedores` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `configuracao_documentos_fiscais` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tipo` int NOT NULL DEFAULT '55',
  `ultima_nota` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0',
  `serie` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1',
  `data_ultima_nota` date NOT NULL,
  `ambiente_fiscal` int NOT NULL DEFAULT '2',
  `token` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '1',
  `csc` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `modelo` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`,`codigo_empresa`),
  KEY `FK_configuracao_documentos_fiscais_empresa` (`codigo_empresa`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `conta_bancaria` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_banco` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `agencia` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_conta` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nome_conta` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `chave_pix` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nome_banco` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `site_banco` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  `saldo_conta` double(18,2) DEFAULT '0.00',
  PRIMARY KEY (`codigo`,`empresa`),
  KEY `contaBancariaFKbanco` (`codigo_banco`) USING BTREE,
  KEY `empresa` (`empresa`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `contato` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tipo` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `contato` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `nome` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  PRIMARY KEY (`codigo`),
  KEY `contatoFKpessoa` (`codigo_pessoa`) USING BTREE,
  CONSTRAINT `contatoFKpessoa` FOREIGN KEY (`codigo_pessoa`) REFERENCES `pessoa` (`documento_identificacao`)
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `correntistas` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data_lancamento` date NOT NULL,
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_fatura` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tipo` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'CR',
  `valor` decimal(18,2) NOT NULL DEFAULT '0.00',
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `data_vencimento` date NOT NULL,
  `data_cadastro` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `documento_fiscal` varchar(44) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status_pagamento` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'FA' COMMENT 'FA - Fatura aberta, FF - Fatura-finalizada',
  `numero_pedido` int DEFAULT NULL,
  `codigo_venda` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  UNIQUE KEY `unique_codigo` (`codigo`),
  KEY `index_codigo_pessoa1` (`codigo_pessoa`) USING BTREE,
  KEY `index_data_lancamento` (`data_lancamento`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `dados_bancarios_pessoa` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_banco` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `agencia` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_conta` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `documento_identificacao` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nome_pessoa` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `chave_pix` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nome_banco` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `site_banco` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigo` (`codigo`) USING BTREE,
  KEY `codigo_pessoa` (`codigo_pessoa`) USING BTREE,
  CONSTRAINT `dados_bancariosFKpessoa` FOREIGN KEY (`codigo_pessoa`) REFERENCES `pessoa` (`documento_identificacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `devolucoes_venda` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_venda` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_venda` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_nota` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_devolucao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `valor` double(18,3) NOT NULL DEFAULT '0.000',
  `quantidade_itens` double(15,3) NOT NULL DEFAULT '0.000',
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nome_pessoa` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'N-Normal,C-Cancelado',
  `usuario_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `nota_gerada` smallint NOT NULL DEFAULT '0' COMMENT '0-Nota nao gerada,1-Nota gerada',
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  PRIMARY KEY (`codigo`),
  KEY `codigo_empresa` (`codigo_empresa`),
  KEY `codigo_venda` (`codigo_venda`),
  KEY `numero_venda` (`numero_venda`),
  KEY `codigo_nota` (`codigo_nota`),
  KEY `codigo_vendedor` (`codigo_vendedor`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `devolucoes_venda_itens` (
  `codigo_devolucao` int NOT NULL DEFAULT '0',
  `numero_item` int NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_venda` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_venda` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_nota` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `quantidade` double(15,3) NOT NULL DEFAULT '1.000',
  `valor` double(18,2) NOT NULL DEFAULT '0.00',
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_grade` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo_devolucao`,`numero_item`),
  CONSTRAINT `devolucoes_venda_itensFKdevolucoes_venda` FOREIGN KEY (`codigo_devolucao`) REFERENCES `devolucoes_venda` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `documentos` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `documento` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tipo` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data_vencimento` date DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoPessoa` (`codigo_pessoa`) USING BTREE,
  KEY `documento` (`documento`) USING BTREE,
  KEY `documentosFKpessoa` (`codigo_pessoa`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `embalagem_empresa` (
  `embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `preco_custo` double(15,2) DEFAULT '0.00',
  `outros_custos` double(15,2) DEFAULT '0.00',
  `preco_minimo` double(15,2) DEFAULT '0.00',
  `cesta_tributacao` int NOT NULL DEFAULT '0',
  `fornecedor_principal` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `estoque_minimo` double(15,3) DEFAULT '0.000',
  `estoque_atual` double(15,3) DEFAULT '0.000',
  `quantidade_atacado` double(15,3) DEFAULT NULL,
  `embalagem_principal` smallint NOT NULL DEFAULT '0',
  `status` smallint NOT NULL DEFAULT '0',
  `codigo_unidade` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'UN',
  `fator_multiplicacao` double(15,3) NOT NULL DEFAULT '1.000',
  `data_ultima_atualizacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL,
  `embalagem_padrao` smallint NOT NULL DEFAULT '0',
  `estoque_resevado` double(15,3) DEFAULT '0.000',
  PRIMARY KEY (`embalagem`,`codigo_empresa`),
  KEY `FK_embalagem_empresa_unidade` (`codigo_unidade`),
  KEY `FK_embalagem_empresa_cestatributacao` (`cesta_tributacao`),
  KEY `FK_embalagem_empresa_pessoa` (`fornecedor_principal`),
  KEY `embalagemEmpresaFKproduto` (`referencia_produto`) USING BTREE,
  KEY `FK_embalagem_empresa_empresa` (`codigo_empresa`),
  KEY `Index 7` (`codigo_empresa`),
  KEY `Index 8` (`referencia_produto`),
  CONSTRAINT `embalagemEmpresaFKproduto` FOREIGN KEY (`referencia_produto`) REFERENCES `produto` (`referencia`),
  CONSTRAINT `FK_embalagem_empresa_cestatributacao` FOREIGN KEY (`cesta_tributacao`) REFERENCES `cestatributacao` (`codigo`),
  CONSTRAINT `FK_embalagem_empresa_empresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `FK_embalagem_empresa_pessoa` FOREIGN KEY (`fornecedor_principal`) REFERENCES `pessoa` (`documento_identificacao`),
  CONSTRAINT `FK_embalagem_empresa_unidade` FOREIGN KEY (`codigo_unidade`) REFERENCES `unidade` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `endereco` (
  `codigo_endereco` int NOT NULL AUTO_INCREMENT,
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `logradouro` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `bairro` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `complemento` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_cidade` varchar(7) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `endereco_principal` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'N',
  `cep` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `cidade` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `estado` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `sigla_estado` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_estado` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo_endereco`,`codigo_pessoa`),
  KEY `enderecoFKpessoa` (`codigo_pessoa`) USING BTREE,
  CONSTRAINT `enderecoFKpessoa` FOREIGN KEY (`codigo_pessoa`) REFERENCES `pessoa` (`documento_identificacao`)
) ENGINE=InnoDB AUTO_INCREMENT=622 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `estoque_reservado` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_pedido` int NOT NULL DEFAULT '0',
  `quantidade` double(15,3) NOT NULL DEFAULT '0.000',
  `codigo_embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `codigo_grade` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `etiquetas` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_etiqueta` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `descricao` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modelo` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `quantidade` int NOT NULL DEFAULT '1',
  `codigo_nota` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `fluxo_caixa` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tipo` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'E',
  `descricao` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `valor` double(15,2) unsigned NOT NULL,
  `data_lancamento` datetime NOT NULL,
  `data_cadastro` timestamp NOT NULL,
  `codigo_plano_contas` int DEFAULT NULL,
  `codigo_centro_custo` int DEFAULT NULL,
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `tipo_movimentacao` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INICIO,FECHAMENTO,SUPRIMENTO,SANGRIA',
  `usuario_lancamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1-Ativo,0-Inativo',
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `usuario_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_pdv` smallint DEFAULT NULL,
  `codigo_lancamento_contas` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `forma_pagamento` smallint DEFAULT NULL,
  `codigo_venda` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  UNIQUE KEY `unique_codigo` (`codigo`),
  KEY `index_tipo1` (`tipo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS `forma_pagamento` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tipo_pagamento` int DEFAULT NULL,
  `conta_bancaria` int NOT NULL,
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `desconto` double(15,2) DEFAULT NULL,
  `acrescimo` double(15,2) DEFAULT NULL,
  `taxa` double(15,2) DEFAULT NULL,
  `valor_minimo` double(15,2) DEFAULT NULL,
  `prazo_recebimento` int DEFAULT NULL,
  `pdv` smallint DEFAULT '1',
  `administradora_cartao` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_cadastro` date NOT NULL,
  `data_atualizacao` timestamp NULL DEFAULT NULL,
  `quantidade_parcelas` int NOT NULL DEFAULT '1',
  `status` smallint NOT NULL DEFAULT '1',
  `padrao` smallint NOT NULL DEFAULT '0' COMMENT '0 - formas pre-cadastradas, 1 - formas cadastradas pelo cliente',
  `dias_liberacao_comissao` int DEFAULT '0',
  `parcela_inicial_para_juros` int NOT NULL DEFAULT '0',
  `gera_financeiro` smallint DEFAULT '0',
  `baixa_automatica` smallint DEFAULT '0',
  PRIMARY KEY (`codigo`) USING BTREE,
  KEY `codigo_empresa` (`codigo_empresa`) USING BTREE,
  KEY `conta_bancaria` (`conta_bancaria`) USING BTREE,
  KEY `descricao` (`descricao`) USING BTREE,
  CONSTRAINT `forma_pagamentoFKconta_bancaria` FOREIGN KEY (`conta_bancaria`) REFERENCES `conta_bancaria` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS `fornecedor_produto` (
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `documento_forncecedor` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'DOCUMENTO DO FORNECEDOR CNPJ/CPF',
  `codigo_embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMBALAGEM',
  `codigo_produto_fornecedor` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DO PRODUTO NO FORNECEDOR',
  `codigo_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`codigo_empresa`,`codigo_embalagem`,`codigo_produto`,`codigo_produto_fornecedor`,`documento_forncecedor`),
  KEY `Index 2` (`codigo_embalagem`,`codigo_empresa`),
  KEY `FK_fornecedor_produto_produto` (`codigo_produto`),
  CONSTRAINT `FK_fornecedor_produto_embalagem_empresa` FOREIGN KEY (`codigo_embalagem`) REFERENCES `embalagem_empresa` (`embalagem`),
  CONSTRAINT `FK_fornecedor_produto_empresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `FK_fornecedor_produto_produto` FOREIGN KEY (`codigo_produto`) REFERENCES `produto` (`referencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `grade_produto` (
  `codigo` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `cor` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `tamanho` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `referencia_principal` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `referencia_grade` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_ultima_atualizacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  KEY `grade_produtoFKproduto` (`referencia_principal`) USING BTREE,
  CONSTRAINT `grade_produtoFKproduto` FOREIGN KEY (`referencia_principal`) REFERENCES `produto` (`referencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `grade_produto_empresa` (
  `codigo` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `estoque_minimo` double(6,3) DEFAULT NULL,
  `estoque_atual` double(10,3) DEFAULT NULL,
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `estoque_reservado` double(15,3) DEFAULT '0.000',
  PRIMARY KEY (`codigo`,`codigo_empresa`),
  KEY `grade_produto_empresaFKempresa` (`codigo_empresa`) USING BTREE,
  CONSTRAINT `grade_produto_empresaFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `grade_produto_empresaFKgrade_produto` FOREIGN KEY (`codigo`) REFERENCES `grade_produto` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS `grupo_empresa` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `status` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'A',
  `identificacao_pasta_imagens` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `identificador_pasta_principal` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `identificacao_pasta_xml` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS `historico_alteracao_preco_embalagens` (
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_tabela_preco` int NOT NULL DEFAULT '0',
  `preco_venda` double(18,2) NOT NULL DEFAULT '0.00',
  `preco_custo` double(18,2) NOT NULL DEFAULT '0.00',
  `preco_atacado` double(18,2) NOT NULL DEFAULT '0.00',
  `preco_minimo` double(18,2) NOT NULL DEFAULT '0.00',
  `data_lancamento` timestamp NOT NULL,
  `usuario_alteracao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `data_atualizacao` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo_empresa`,`codigo_embalagem`,`codigo_tabela_preco`,`preco_venda`,`preco_custo`,`preco_atacado`,`preco_minimo`),
  KEY `codigo_empresa` (`codigo_empresa`),
  KEY `codigo_embalagem` (`codigo_embalagem`),
  KEY `codigo_tabela_preco` (`codigo_tabela_preco`),
  KEY `data_lancamento` (`data_lancamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `identificacao_pastas_xml` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `identificacao_pasta` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `mes_ano` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

              
CREATE TABLE IF NOT EXISTS `kit_produto` (
  `referencia_principal` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `referencia_composicao` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `quantidade` double(15,3) NOT NULL DEFAULT '0.000',
  `codigo_grade` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`referencia_principal`,`referencia_composicao`),
  CONSTRAINT `kit_produtoFKproduto` FOREIGN KEY (`referencia_principal`) REFERENCES `produto` (`referencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

               
CREATE TABLE IF NOT EXISTS `lancamento_contas` (
  `codigo` varchar(36) COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `valor` double(15,2) NOT NULL DEFAULT '0.00',
  `data_lancamento` date NOT NULL,
  `data_vencimento` date NOT NULL,
  `codigo_plano_contas` int DEFAULT NULL,
  `codigo_centro_custo` int DEFAULT NULL,
  `numero_documento` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `numero_documento_original` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `documento_fiscal` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_barra` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `chave_pix` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `tipo` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'CR',
  `data_pagamento` date DEFAULT NULL,
  `valor_pagamento` double(15,2) DEFAULT '0.00',
  `codigo_forma_pagamento` int DEFAULT NULL,
  `descricao` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1',
  `usuario_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_pagamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `numero_total_parcelas` int NOT NULL DEFAULT '1',
  `codigo_venda` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`,`codigo_empresa`),
  UNIQUE KEY `unique_codigo` (`codigo`),
  KEY `index_codigo_centro_custo` (`codigo_centro_custo`) USING BTREE,
  KEY `index_codigo_empresa` (`codigo_empresa`) USING BTREE,
  KEY `index_codigo_pessoa` (`codigo_pessoa`) USING BTREE,
  KEY `index_codigo_plano_contas` (`codigo_plano_contas`) USING BTREE,
  KEY `index_tipo` (`tipo`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS `metas_vendedores` (
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `meta` double(18,2) NOT NULL DEFAULT '0.00',
  `porcentagem_comissao` double(6,2) NOT NULL DEFAULT '0.00',
  `data_lancamento` timestamp NOT NULL,
  `usuario_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `tipo_comissionamento` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'CIF' COMMENT 'CVP-COMISSAO VINCULADA PARCELADA,CIF-COMISSAO INTEGRAl FATURAMENTO,CIP-COMISSAO INTEGRAL PRIMEIRA PARCELA,CIU-COMISSAO INTEGRAL ULTIMA PARCELA',
  PRIMARY KEY (`codigo_empresa`,`codigo_vendedor`),
  KEY `codigo_empresa` (`codigo_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `movimentacao_estoque` (
  `documento` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_local_estoque` int NOT NULL DEFAULT '0',
  `tipo_movimento` int NOT NULL,
  `valor_total_itens` double(15,3) DEFAULT '0.000',
  `valor_total_custo` double(15,3) DEFAULT '0.000',
  `numero_pedido` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nota_fiscal` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_movimento` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descricao` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'F',
  `documento_referenciado` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_nota` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `codigo_venda` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`documento`,`codigo_empresa`,`codigo_local_estoque`,`tipo_movimento`),
  KEY `movimentacao_estoqueFKempresa` (`codigo_empresa`) USING BTREE,
  KEY `movimentacao_estoqueFKlocal_estoque` (`codigo_local_estoque`) USING BTREE,
  KEY `movimentacao_estoqueFKtipo_movimentacao` (`tipo_movimento`) USING BTREE,
  CONSTRAINT `movimentacao_estoqueFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `movimentacao_estoqueFKlocal_estoque` FOREIGN KEY (`codigo_local_estoque`) REFERENCES `local_estoque` (`codigo`),
  CONSTRAINT `movimentacao_estoqueFKtipo_movimentacao` FOREIGN KEY (`tipo_movimento`) REFERENCES `tipo_movimentacao` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `movimentacao_estoque_itens` (
  `documento` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_local_estoque` int NOT NULL,
  `tipo_movimento` int NOT NULL,
  `codigo_embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `numero_item` int NOT NULL,
  `quantidade` double(15,3) NOT NULL,
  `preco_venda` double(15,2) NOT NULL,
  `preco_custo` double(15,2) NOT NULL,
  `codigo_grade` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_lote` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_movimento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fator_multiplicacao` double(6,3) DEFAULT '1.000',
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`documento`,`codigo_empresa`,`codigo_embalagem`,`codigo_local_estoque`,`numero_item`,`tipo_movimento`),
  KEY `movimentacao_estoque_itensFKembalagem` (`codigo_embalagem`) USING BTREE,
  KEY `movimentacao_estoque_itensFKempresa` (`codigo_empresa`) USING BTREE,
  KEY `movimentacao_estoque_itensFKgrade_produto` (`codigo_grade`) USING BTREE,
  KEY `movimentacao_estoque_itensFKlocal_estoque` (`codigo_local_estoque`) USING BTREE,
  KEY `movimentacao_estoque_itensFKmovimentacao_estoque` (`documento`,`codigo_empresa`,`codigo_local_estoque`,`tipo_movimento`) USING BTREE,
  KEY `movimentacao_estoque_itensFKtipo_movimentacao` (`tipo_movimento`) USING BTREE,
  CONSTRAINT `movimentacao_estoque_itensFKembalagem` FOREIGN KEY (`codigo_embalagem`) REFERENCES `embalagem_empresa` (`embalagem`),
  CONSTRAINT `movimentacao_estoque_itensFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `movimentacao_estoque_itensFKgrade_produto` FOREIGN KEY (`codigo_grade`) REFERENCES `grade_produto` (`codigo`),
  CONSTRAINT `movimentacao_estoque_itensFKlocal_estoque` FOREIGN KEY (`codigo_local_estoque`) REFERENCES `local_estoque` (`codigo`),
  CONSTRAINT `movimentacao_estoque_itensFKmovimentacao_estoque` FOREIGN KEY (`documento`, `codigo_empresa`, `codigo_local_estoque`, `tipo_movimento`) REFERENCES `movimentacao_estoque` (`documento`, `codigo_empresa`, `codigo_local_estoque`, `tipo_movimento`),
  CONSTRAINT `movimentacao_estoque_itensFKtipo_movimentacao` FOREIGN KEY (`tipo_movimento`) REFERENCES `tipo_movimentacao` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

               
CREATE TABLE IF NOT EXISTS `nota_fiscal` (
  `codigo` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `numero_nota` int(9) unsigned zerofill NOT NULL DEFAULT '000000001' COMMENT 'NUMERO DA NOTA FISCAL',
  `serie_nota` int NOT NULL DEFAULT '1' COMMENT 'NUMERO DA SERIE DA NOTA FISCAL',
  `codigo_pessoa_emitente` varchar(20) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'DOCUMENTO DE IDENTIFICACAO DO EMITENTE (CNPJ OU CPF)',
  `tipo_nota` enum('0','1') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1' COMMENT 'TIPO DE MOVIMENTACAO DA NOTA FISCAL (0) ENTRADA (1) SAIDA',
  `status_nota` enum('0','1','2','3','4','5','6','7','8') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'SITUACAO DA NOTA (0) PENDENTE,  (1) AUTORIZADA, (2) DENEGADA,\r\n (3) REJEITADA, (4) CANCELADA ou (5) INUTILIZADA',
  `razao_social_emitente` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'RAZAO SOCIAL DO EMITENTE',
  `nome_fantasia_emitente` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME FANTASIA DO EMITENTE',
  `logradouro_emitente` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'ENDERECO DO EMITENTE',
  `numero_endereco_emitente` varchar(10) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NUMERO DO ENDERECO DO EMITENTE',
  `complemento_endereco_emitente` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'COMPLEMENTO DO ENDERECO DO EMITENTE',
  `bairro_emitente` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'BAIRRO DO EMITENTE',
  `codigo_ibge_pais_emitente` int(4) unsigned zerofill NOT NULL DEFAULT '1058' COMMENT 'CODIGO IBGE DO PAIS DO EMITENTE',
  `nome_pais_emitente` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT 'BRASIL' COMMENT 'NOME DO PAIS DO EMITENTE',
  `codigo_ibge_estado_emitente` int(2) unsigned zerofill NOT NULL DEFAULT '26' COMMENT 'CODIGO DO ESTADO DO EMITENTE NA TABELA IBGE',
  `sigla_estado_emitente` varchar(2) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'PE' COMMENT 'SIGLA DO ESTADO DO EMITENTE',
  `codigo_municipio_emitente` varchar(7) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '2611606' COMMENT 'CODIGO DO MUNICIPIO DO EMITENTE',
  `nome_municipio_emitente` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'RECIFE' COMMENT 'NOME DO MUNICIPIO DO EMITENTE',
  `cep_emitente` varchar(8) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'CEP DO EMITENTE',
  `telefone_emitente` varchar(14) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'TELEFONE DO EMITENTE DDD + NUMERO',
  `inscricao_estadual_emitente` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO ESTADUAL DO EMITENTE',
  `inscricao_estadual_st_emitente` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO ESTADUAL DE SUBSTITUICAO TRIBUTARIA',
  `inscricao_municipal_emitente` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO MUNICIPAL DO EMITENTE',
  `cnae_emitente` varchar(7) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CNAE DO EMITENTE',
  `regime_tributario_emitente` enum('1','2','3') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1' COMMENT 'CODIGO DE REGIME TRIBUTARIO - CRT, VALORES VALIDOS: (1) - SIMPLES NACIONAL,\r\n (2) - SIMPLES NACIONAL - EXCESSO DE SUBLIMITE DE RECEITA BRUTA ou 3 - REGIME NORMAL',
  `natureza_operacao` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'VENDA' COMMENT 'NATUREZA DA OPERACAO: VENDA, COMPRA, TRANSFERENCIA, DEVOLUCAO, IMPORTACAO, CONSIGNACAO,\r\n REMESSA (PARA FINS DE DEMONSTRACAO, DE INDUSTRIALIZACAO OUTRA)',
  `chave_nota` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NUMEO DA CHAVE DA NOTA',
  `protocolo_autorizacao` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'POTOCOLO DE AUTOIZACAO DA NOTA',
  `recibo_nota_fiscal_autorizacao` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RECIBO DA NOTA FISCAL AUTORIZADA',
  `protocolo_cancelamento` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'PROTOCOLO DE CANCELAMENTO, INUTILIZACAO DA NOTA OU CARTA DE CORRECAO',
  `protocolo_denegacao` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'PROTOCOLO DE DENEGACAO DA NOTA',
  `data_emissao` timestamp NOT NULL COMMENT 'DATA DE EMISSAO DA NOTA FISCAL',
  `data_saida` timestamp NULL DEFAULT NULL COMMENT 'DATA DE SAIDA DA NOTA FISCAL',
  `data_cancelamento` timestamp NULL DEFAULT NULL COMMENT 'DATA DE CANCELAMENTO OU INUTILIZACAO DA NOTA',
  `data_autorizacao` timestamp NULL DEFAULT NULL COMMENT 'DATA DE EMISSAO DA NOTA FISCAL',
  `modelo_nota` int NOT NULL DEFAULT '55' COMMENT 'MODELO DA NOTA FISCAL (55) NF-e OU (65) NFC-e',
  `identificador_local_destino` enum('1','2','3') COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT 'IDENTIFICADOR DE LOCAL DE DESTINO DA OPERACAO (1) OPERACAO INTERNA, (2) INTERESTADUAL E (3) EXTERIOR',
  `codigo_finalidade_emissao` enum('1','2','3','4') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1' COMMENT 'CODIGO DA FINALIDADE DE EMISSAO (1) NORMAL, (2) COMPLEMENTAR, (3) AJUSTE ou (4) DEVOLUCAO',
  `indicador_presenca_comprador` enum('0','1','2','3','4','9') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'INFORMAR O INDICADOR DE PRESENCA DO COMPRADOR NO ESTABELECIMENTO COMERCIAL NO MOMENTO DA OPERACAO',
  `identificador_processo_emissao` enum('0','1','2','3') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'IDENTIFICADOR DO PROCESSO DE EMISSAO (0) APLICATIVO DO CONTRIBUINTE,(1) AVULSA PELO FISCO,\r\n (2) AVULSA PEL CONTRIBUINTE ou (3) APLICATIVO DO FISCO',
  `versao_aplicativo_processo` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT 'ZOX 2017.01' COMMENT 'VERSAO DO APLICATIVO DO PROCESSO',
  `data_contingencia` timestamp NULL DEFAULT NULL COMMENT 'DATA DA CONTINGENCIA',
  `justificativa_contingencia` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'JUSTIFICATIVA DA CONTINGENCIA',
  `tipo_ambiente_emissao` enum('1','2') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1' COMMENT 'TIPO DE AMBIENTE DE EMISSAO (1) PRODUCAO ou (2) HOMOLOGACAO',
  `consumidor_final` enum('0','1') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'CONSUMIDOR FINAL (0) NAO ou (1) SIM',
  `codigo_pessoa_destinatario` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DOCUMENTO DE IDENTIFICACAO DO DESTINATARIO (CNPJ, CPF OU OUTROS PARA ESTRANGEIRO)',
  `razao_social_destinatario` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RAZAO SOCIAL DO DESTINATARIO',
  `logradouro_destinatario` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'ENDERECO DO DESTINATARIO',
  `numero_endereco_destinatario` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DO ENDERECO DO DESTINATARIO',
  `complemento_endereco_destinatario` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'COMPLEMENTO DO ENDERECO DO DESTINATARIO',
  `bairro_destinatario` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'BAIRRO DO DESTINATARIO',
  `codigo_ibge_pais_destinatario` int(4) unsigned zerofill DEFAULT '1058' COMMENT 'CODIGO IBGE DO PAIS DO DESTINATARIO',
  `nome_pais_destinatario` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT 'BRASIL' COMMENT 'NOME DO PAIS DO DESTINATARIO',
  `sigla_estado_destinatario` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'SIGLA DO ESTADO DO DESTINATARIO',
  `codigo_municipio_destinatario` varchar(7) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO DO MUNICIPIO DO DESTINATARIO',
  `nome_municipio_destinatario` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO MUNICIPIO DO DESTINATARIO',
  `cep_destinatario` varchar(8) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CEP DO DESTINATARIO',
  `telefone_destinatario` varchar(14) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'TELEFONE DO DESTINATARIO DDD + NUMERO',
  `indicador_ie_destinatario` enum('1','2','9') COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT 'INDICADOR DA IE DO DESTINATARIO (1) CONTRIBUINTE ICMS ,\r\n (2) CONTRIBUINTE ISENTO DE INSCRICAO NO CADASTRO DE CONTRIBUINTES DO ICMS ou (9) NAO CONTRIBUINTE, QUE PODE OU NAO POSSUIR INSCRICAO ESTADUAL NO CADASTRO DE CONTRIBUINTES DO ICMS',
  `inscricao_estadual_destinatario` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO ESTADUAL DO DESTINATARIO',
  `inscricao_suframa_destinatario` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO SUFRAMA DO DESTINATARIO',
  `inscricao_municipal_destinatario` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO MUNICIPAL DO DESTINATARIO',
  `email_destinatario` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'EMAIL DO DESTINATARIO',
  `documento_identificacao_retirada` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DOCUMENTO DE IDENTIFICACAO DA RETIRADA',
  `logradouro_retirada` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'ENDERECO DA RETIRADA',
  `numero_endereco_retirada` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DE ENDERECO DA RETIRADA',
  `complemento_endereco_retirada` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'COMPLEMENTO DO ENDERECO DA RETIRADA',
  `bairro_retirada` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'BAIRRO DA RETIRADA',
  `codigo_municipal_retirada` varchar(7) COLLATE utf8mb3_unicode_ci DEFAULT '2611606' COMMENT 'CODIGO DO MUNICIPIO DA RETIRADA',
  `nome_municipio_retirada` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO MUNICIPIO DA RETIRADA',
  `sigla_estado_retirada` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'SIGLA DO ESTADO DA RETIRADA',
  `cep_endereco_retirada` int DEFAULT '0' COMMENT 'CEP DO ENDERECO DE RETIRADA',
  `documento_identificacao_entrega` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DOCUMENTO DE IDENTIFICACAO DA ENTREGA',
  `logradouro_entrega` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'ENDERECO DE ENTREGA',
  `numero_endereco_entrega` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DO ENDERECO DE ENTREGA',
  `complemento_endereco_entrega` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'COMPLEMENTO DO ENDERECO DE ENTREGA',
  `bairro_endereco_entrega` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'BAIRRO DA ENTREGA',
  `codigo_municipio_entrega` varchar(7) COLLATE utf8mb3_unicode_ci DEFAULT '2611606' COMMENT 'CODIGO DO MUNICIPIO DA ENTREGA',
  `nome_municipio_entrega` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO MUNICIPIO DA ENTREGA',
  `sigla_estado_entrega` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'SIGLA DO ESTADO DA ENTREGA',
  `modalidade_frete` enum('0','1','2','3','4','9') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'MODALIDADE DO FRETE (0) POR CONTA DO EMITENTE, (1) POR CONTA DO DESTINATAIO, (2) TERCERIOS, (3) TRANSPORTE PROPRIO POR CONTA DO EMITENTE, (4) TRANSPORTE PROPRIO POR CONTA DO DESTINATARIO ou (9) SEM FRETE',
  `documento_identificacao_transportador` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DOCUMENTO DE IDENTIFICACAO DO TRANSPORTADOR',
  `razao_social_transportador` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RAZAO SOCIAL DO TRANSPORTADOR',
  `inscricao_estadual_transportador` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO ESTADUAL DO TRANSPORTADOR',
  `logradouro_transportador` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'ENDERECO DO TRANSPORTADOR',
  `nome_municipio_transportador` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO MUNICIPIO DO TRANSPORTADOR',
  `sigla_estado_transportador` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'SIGLA DO ESTADO DO TRANSPORTADOR',
  `valor_frete_transporte` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DO SERVICO DE TRANSPORTE',
  `base_calculo_icms_retido_transportador` double(18,2) DEFAULT '0.00' COMMENT 'BASE DE CALCULO DO ICMS RETIDO PELO TRANSPORTADOR',
  `aliquota_icms_retido_transportador` double(5,2) DEFAULT '0.00' COMMENT 'ALIQUOTA DO ICMS RETIDO PELO TRANSPORTADOR',
  `valor_icms_retido_transportador` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DO ICMS RETIDO PELO TRANSPORTADOR',
  `municipio_fato_gerador_icms_transporte` varchar(7) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'MUNICIPIO DE OCORRENCIA DO FATO GERADOR DO ICMS DO TRANSPORTE (LOCAL DE INICIO DA PRESTACAO DO SERVICO)',
  `cfop_transporte` varchar(5) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CFOP (CODIGO FISCAL DE OPERACOES) DO SERVICO DE TRANSPORTE',
  `tipo_transporte` enum('0','1','2') COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'TIPO DE TRANSPORTE (0) VEICULOS, (1) BALSA ou (2) VAGAO',
  `placa_veiculo` varchar(8) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'PLACA DO VEICULO DO TRANSPORTADOR',
  `sigla_uf_veiculo_transporte` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'SIGLA DA UF DO REGISTRO DO VEICULO DO TRANSPORTADOR',
  `rntc_antt` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RNTC - REGISTRO NACIONAL DE TRANSPORTADOR DE CARGA (ANTT) DO VEICULO DO TRANSPORTADOR',
  `identificacao_balsa_transporte` varchar(40) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'IDENTIFICACAO DA BALSA DO TRANSPORTADOR',
  `identificacao_vagao_transporte` varchar(40) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'IDENTIFICACAO DO VAGAO DO TRANSPORTADOR',
  `numero_fatura_cobranca` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DO FATURA DA COBRANCA',
  `valor_cobranca` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DA COMBRANCA',
  `desconto_cobranca` double(18,2) DEFAULT '0.00' COMMENT 'DESCONTO DA COBRANCA',
  `valor_liquido_cobranca` double(18,2) DEFAULT '0.00' COMMENT 'VALOR LIQUIDO DA COBRANCA',
  `observacao_fisco` text COLLATE utf8mb3_unicode_ci COMMENT 'INFORMAR AS INFORMACOES DE INTERESSE DO FISCO',
  `observacao_contribuinte` text COLLATE utf8mb3_unicode_ci COMMENT 'INFORMAR AS INFORMACOES COMPLEMENTARES DE INTERESSE DO CONTRIBUITE',
  `sigla_uf_embarque` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'SIGLA DA UF DE EMBARQUE OU DE TRANSPOSICAO DE FRONTEIRA. A UF DE EMBARQUE E A UF DO LOCAL ONDE SERA EMBARCADA PARA\r\nO EXTERIOR (PORTO/AEROPORTO), NO CASO DE SER TRANSPORTE TERRESTRE DEVE SER O LOCAL DE TRANSPOSICAO DE FRONTEIRA.',
  `local_embarque` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'LOCAL DE EMBARQUE, LOCAL ONDE SERA EMBARCADA PARA O EXTERIOR (PORTO/AEROPORTO), NO CASO DE SER TRANSPORTE TERRESTRE\r\n DEVE SER O LOCAL DE TRANSPOSICAO DE FRONTEIRA.',
  `local_despacho` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RECINTO ALFANDEGADO DO LOCAL DE DESPACHO.',
  `identificacao_nota_empenho` varchar(17) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'IDENTIFICACAO DA NOTA DE EMPENHO, QUANDO SE TRATAR DE COMPRAS PUBLICAS',
  `informacao_pedido_compra` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INFORMACAO DO PEDIDO DE COMPRA',
  `informacao_contrato_compra` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INFORMACAO DO CONTRATO DE COMPRA',
  `chave_nota_autorizacao` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CHAVE DE AUTORIZACAO DA NOTA',
  `chave_nota_cancelada` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CHAVE DE CANCELAMENTO DA NOTA',
  `chave_nota_carta_correcao` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CHAVE DA CARTA DE CORRECAO DA NOTA',
  `base_calculo_total_icms` double(18,2) DEFAULT '0.00' COMMENT 'BASE DE CALCULO TOTAL DO ICMS',
  `valor_total_icms` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ICMS',
  `valor_total_icms_desonerado` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ICMS DESONERADO',
  `valor_total_icms_fundo_pobreza` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ICMS RELATIVO AO FUNDO DE COMBATE A POBREZA (FCP) PARA UF DE DESTINATARIO',
  `valor_total_icms_patilha_uf_remetente` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ICMS DE PARTILHA PARA A UF DO REMETENTE',
  `valor_total_icms_patilha_uf_destinatario` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ICMS DE PARTILHA PARA A UF DO DESTINATARIO',
  `base_calculo_total_icms_st` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'BASE DE CALCULO TOTAL DO ICMS DE SUBSTITUICAO TRIUTARIA',
  `valor_total_icms_st` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ICMS DE SUBSTITUICAO TRIBUTARIA',
  `valor_total_produtos` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DOS PRODUTOS',
  `valor_frete` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR DO FRETE',
  `valor_seguro` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR DO SEGURO',
  `valor_total_desconto` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DE DESCONTO',
  `valor_total_ii` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL DO II (IMPOSTO DE IMPORTACAO)',
  `valor_total_ipi` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DO IPI (IMPOSTO SOBRE PRODUTOS INDUSTRIALIZADOS)',
  `valor_total_pis` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DO PIS (PROGRAMA DE INTEGRACAO SOCIAL)',
  `valor_total_cofins` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DO COFINS (CONTRIBUICAO PARA O FINANCIAMENTO DA SEGURIDADE SOCIAL)',
  `valor_total_outras_despesas` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DE OUTRAS DESPESAS ACESSORIAS',
  `valor_total_nota` double(18,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL DA NOTA',
  `valor_total_impostos_federais_estaduais_municipais` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL ESTIMADO DE IMPOSTOS FEDERAIS, ESTADUAIS E MUNICIPAIS',
  `valor_retido_pis` double(18,2) DEFAULT '0.00' COMMENT 'VALOR RETIDO DO PIS',
  `valor_retido_cofins` double(18,2) DEFAULT '0.00' COMMENT 'VALOR RETIDO DE COFINS',
  `valor_retido_csll` double(18,2) DEFAULT '0.00' COMMENT 'VALOR RETIDO DE CSLL (CONTRIBUICAO SOCIAL SOBRE O LUCRO LIQUIDO)',
  `base_calculo_irrf` double(18,2) DEFAULT '0.00' COMMENT 'BASE DE CALCULO DO IRRF (IMPOSTO DE RENDA RETIDO NA FONTE)',
  `valor_retido_irrf` double(18,2) DEFAULT '0.00' COMMENT 'VALOR RETIDO DO IRRF (IMPOSTO DE RENDA RETIDO NA FONTE)',
  `base_calculo_retencao_previdencia_social` double(18,2) DEFAULT '0.00' COMMENT 'BASE DE CALCULO DE RETENCAO DA PEVIDENCIA SOCIAL',
  `valor_retencao_previdencia_social` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DA RETENCAO DA PREVIDENCIA SOCIAL',
  `valor_total_servico_nao_incidencia_icms` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL DO SERVICO SOB NAO-INCIDENCIA OU NAO TRIBUTADOS PELO ICMS',
  `base_calculo_iss` double(18,2) DEFAULT '0.00' COMMENT 'BASE DE CALCULO DO ISS',
  `valor_total_iss` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ISS',
  `valor_pis_servico` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DO PIS SOBRE SERVICO',
  `valor_cofins_servico` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DO COFINS SOBRE SERVICO',
  `data_prestacao_servico` date DEFAULT NULL COMMENT 'DATA DA PRESTACAO DO SERVICO',
  `valor_deducao_reducao_base_calculo` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DA DEDUCAO PARA REDUCAO DA BASE DE CALCULO',
  `valor_outras_retencoes` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DE OUTRAS RETENCOES',
  `valor_desconto_incondicionado` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DO DESCONTO INCONDICIONADO',
  `valor_desconto_condicionado` double(18,2) DEFAULT '0.00' COMMENT 'VALOR DO DESCONTO CONDICIONADO',
  `valor_total_iss_retido` double(18,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL DO ISS RETIDO',
  `codigo_regime_especial_tributacao_iss` enum('1','2','3','4','5','6') COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT 'CODIGO DO REGIME ESPECIAL DE TRIBUTACAO ISS',
  `formato_impressao_danfe` enum('0','1','2','3','4','5') COLLATE utf8mb3_unicode_ci DEFAULT '0' COMMENT 'FORMATO DE IMPRESSAO DO DANFE: 0 - SEM GERACAO DE DANFE, 1 - DANFE NORMAL, RETRATO, 2 - DANFE NORMAL,\r\n PAISAGEM, 3 - DANFE SIMPLIFICADO, 4 - DANFE NFC-E OU 5 - DANFE NFC-E EM MENSAGEM ELETRÔNICA (O ENVIO DE MENSAGEM ELETRÔNICA PODE SER FEITA DE FORMA SIMULT',
  `forma_emissao` enum('1','2','3','4','5','6','7','9') COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT 'FORMA DE EMISSAO: 1 - EMISSAO NORMAL , 2 - CONTINGENCIA FS-IA, 3 - CONTINGENCIA SCAN,\r\n 4 - CONTINGENCIA DPEC, 5 - CONTINGENCIA FS-DA, 6 - CONTINGENCIA SVC-AN, 7 - CONTINGENCIA SVC-RS OU 9 - CONTINGENCIA OFF-LINE DA NFC-E',
  `indicador_intermediador` int NOT NULL DEFAULT '0' COMMENT 'INDICADOR DO INTERMEDIADOR/MARKETPLACE, (0) OPERACAO SEM ITERMEDIADOR, (1) OPERACAO EM SITE OU PLATAFORMA DE TERCEIROS',
  `identificador_pessoa_estrangeiro` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DOCUMENTO DE IDENTIFICACAO DE PESSOA ESTRAGEIRA, EX: NUMERO PASSAPORTE',
  `nome_recebedor` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO RECEBEDOR',
  `codigo_pais_retirada` varchar(4) COLLATE utf8mb3_unicode_ci DEFAULT '1058' COMMENT 'CODIGO DO PAIS DE RETIRADA',
  `nome_pais_retirada` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT 'BRASIL' COMMENT 'NOME DO PAIS DE RETIRADA',
  `telefone_retirada` varchar(14) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'TELEFONE DE RETIRADA',
  `email_recebedor` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'E-MAIL DO RECEBEDOR',
  `inscricao_estadual_recebedor` varchar(14) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO ESTADUAL DO ESTABELECIMENTO RECEBEDOR',
  `nome_recebedor_entrega` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO RECEBEDOR NA HORA DA ENTREGA',
  `codigo_pais_entrega` varchar(4) COLLATE utf8mb3_unicode_ci DEFAULT '1058' COMMENT 'CODIGO DO PAIS DE ENTREGA',
  `nome_pais_entrega` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT 'BRASIL' COMMENT 'NOME DO PAIS DE ENTREGA',
  `telefone_entrega` varchar(14) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'TELEFONE DE ENTREGA',
  `email_entrega` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'E-MAIL DE ENTREGA',
  `inscricao_estadual_expedidor` varchar(14) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSCRICAO ESTADUAL DO ESTABELECIMENTO EXPEDIDOR',
  `valor_total_fundo_combate_probreza_retido_anteriormente_st` double(15,2) DEFAULT NULL COMMENT 'VALOR DO FCP RETIDO ANTERIORMENTE POR SUBSTITUIÇÃO. CORRESPONDE AO TOTAL DA SOMA DOS CAMPOS VFCPSTRET INFORMADO NOS ITENS.',
  `valor_total_ipi_devolvido_devolucao` double(15,2) DEFAULT NULL COMMENT 'VALOR DO IPI DEVOLVIDO.DEVE SER INFORMADO QUANDO PREENCHIDO O GRUPO TRIBUTOS DEVOLVIDOS NA EMISSÃO DE NOTA FINNFE=4 (DEVOLUÇÃO) NAS OPERAÇÕES COM NÃO CONTRIBUINTES DO IPI.',
  `valor_total_fundo_combate_probreza_st` double(15,2) DEFAULT NULL COMMENT 'Valor do FCP retido anteriormente por Substituição. Corresponde ao total da soma dos campos vFCPST informado nos itens.',
  `documento_identificacao_intermediador` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DOCUMENTO DE IDENTIFICACAO DO INTERMEDIADOR DA TRANSACAO',
  `nome_vendedor_intermediador` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO USUARIO OU IDENTIFICACAO DO PERFIL DO VENDEDOR NO SITE DO INTERMEDIADOR',
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cep_endereco_entrega` varchar(8) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `link_aquivo_xml` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `valor_total_difal` double(15,2) DEFAULT NULL COMMENT 'Valor do VICMSUFDest',
  `usuario_cadastro` varchar(150) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `usuario_emissao` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_cancelamento` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_edicao` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `chave_temporaria` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_nota_temporario` int DEFAULT NULL,
  `data_carta_correcao` timestamp NULL DEFAULT NULL,
  `motivo_cancelamento` varchar(200) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_evento_cancelamento` varchar(6) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `versao_cancelamento` varchar(25) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_status_cancelamento` varchar(3) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_seguencia_cancelamento` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `descricao_evento_cancelamento` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `descricao_motivo_evento_cancelamento` text COLLATE utf8mb3_unicode_ci,
  `nota_terceiros` smallint NOT NULL DEFAULT '0',
  `codigo_evento_carta_correcao` varchar(6) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `versao_evento_carta_correcao` varchar(25) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_status_evento_carta_correcao` varchar(3) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_seguencia_evento_carta_correcao` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `descricao_evento_carta_correcao` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `descricao_motivo_evento_carta_correcao` text COLLATE utf8mb3_unicode_ci,
  `texto_carta_correcao` text COLLATE utf8mb3_unicode_ci,
  `protocolo_carta_correcao` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_carta_correcao` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `motivo_rejeicao` text COLLATE utf8mb3_unicode_ci,
  `vendedor` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_tabela_preco` int DEFAULT NULL,
  `tipo_emissao` varchar(15) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'VENDA,DEVOLUCAO,REMESSA,TRANSFERENCIA,OUTROS',
  `observacao_usuario` text COLLATE utf8mb3_unicode_ci,
  `indicador_inscricao_estadual_emitente` varchar(1) COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT '1-CONTRIBUINTE,2-CONTRIBUINTE ISENTO,9-NAO CONTRIBUINTE',
  `regime_tributario_destinatario` varchar(1) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO DE REGIME TRIBUTARIO - CRT, VALORES VALIDOS: (1) - SIMPLES NACIONAL,',
  `numero_pedido` int DEFAULT NULL,
  `codigo_nota_referenciada` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_devolucao` int DEFAULT NULL,
  `xml` longtext COLLATE utf8mb3_unicode_ci,
  `data_emissao_temporaria` timestamp NULL DEFAULT NULL,
  `codigo_venda` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `chave_autorizada` (`chave_nota_autorizacao`) USING BTREE,
  KEY `chave` (`chave_nota`),
  KEY `numero_nota` (`numero_nota`),
  KEY `serie_nota` (`serie_nota`),
  KEY `codigo_empresa` (`codigo_empresa`),
  KEY `codigo_pessoa_emitente` (`codigo_pessoa_emitente`),
  KEY `data_emissao` (`data_emissao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='CABECALHO DA NOTA FISCAL';

CREATE TABLE IF NOT EXISTS `nota_fiscal_duplicatas` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `numero_duplicata` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NUMERO DA DUPLICATA',
  `data_vencimento` date NOT NULL COMMENT 'DATA DO VENCIMENTO DA DUPLICATA',
  `valor` float(15,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR DA DUPLICATA',
  `codigo_forma_pagamento` int DEFAULT NULL,
  PRIMARY KEY (`codigo_nota`,`numero_duplicata`,`codigo_empresa`),
  KEY `codigo_empresa` (`codigo_empresa`),
  KEY `codigo_nota` (`codigo_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='DUPLICATAS DA NOTA FISCAL';

CREATE TABLE IF NOT EXISTS `nota_fiscal_inutilizacao` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nota_inicial` int NOT NULL DEFAULT '0',
  `nota_final` int NOT NULL DEFAULT '0',
  `serie` int NOT NULL DEFAULT '1',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `justificativa` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `ambiente_emissao` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT '1-Producao,2-Homologacao',
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_cadastro` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `protocolo_autorizacao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`),
  KEY `nota_inicial` (`nota_inicial`,`nota_final`,`serie`,`codigo_empresa`,`ambiente_emissao`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `nota_fiscal_itens` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA NO NOTA FISCAL',
  `numero_item` int(3) unsigned zerofill NOT NULL DEFAULT '001' COMMENT 'NUMERO DO ITEM',
  `codigo_embalagem` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DO PRODUTO OU SERVICO. PREENCHER COM CFOP, CASO SE TRATE DE ITENS NAO RELACIONADOS COM MERCADORIAS/PRODUTOS\r\n E QUE O CONTRIBUINTE NAO POSSUA CODIFICACAO PROPRIA. FORMATO ”CFOP9999”.',
  `ean` varchar(15) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'GTIN (GLOBAL TRADE ITEM NUMBER) DO PRODUTO, ANTIGO CODIGO EAN OU CODIGO DE BARRAS. PREENCHER COM O CODIGO GTIN-8, GTIN-12, \r\nGTIN-13 OU GTIN-14 (ANTIGOS CODIGOS EAN, UPC E DUN-14), NAO INFORMAR O CONTEUDO DA TAG EM CASO DE O PRODUTO NAO POSSUIR ESTE CODIGO.',
  `ean_tributado` varchar(15) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'GTIN (GLOBAL TRADE ITEM NUMBER) DA UNIDADE DE TRIBUTACAO DO PRODUTO, ANTIGO CODIGO EAN OU CODIGO DE BARRAS. PREENCHER\r\n COM O CODIGO GTIN-8, GTIN-12, GTIN-13 OU GTIN-14 (ANTIGOS CODIGOS EAN, UPC E DUN-14)',
  `descricao` varchar(120) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'DESCRICAO DO PRODUTO/SERVICO',
  `ncm` varchar(8) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO NCM COM 8 DIGITOS; INFORMAR A POSICAO DO CAPITULO DO NCM (AS DUAS PRIMEIRAS POSICOES DO NCM) QUANDO A OPERACAO NAO\r\n FOR DE COMERCIO EXTERIOR (IMPORTACAO/ EXPORTACAO) OU O PRODUTO NAO SEJA TRIBUTADO PELO IPI; SE FOR SERVICOS, INFORMAR 00.',
  `cest` varchar(7) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO CEST (CODIGO ESPECIFICADOR DA SUBSTITUICAO TRIBUTARIA)',
  `codigo_ex_tipi` varchar(3) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO EX DA TIPI (TABELA DE INCIDENCIA DO IPI) SE HOUVER PARA O NCM DO PRODUTO.',
  `cfop` int(4) unsigned zerofill NOT NULL DEFAULT '5102' COMMENT 'CFOP - CODIGO FISCAL DE OPERACOES E PRESTACOES.',
  `unidade_comercializacao` varchar(6) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'UND' COMMENT 'UNIDADE DE COMERCIALIZACAO DO PRODUTO (EX. PC, UND, DZ, KG, ETC.).',
  `unidade_tributacao` varchar(6) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'UND' COMMENT 'UNIDADE DE TRIBUTACAO DO PRODUTO (EX. PC, UND, DZ, KG, ETC.).',
  `quantidade_comercializacao` double(15,4) NOT NULL DEFAULT '1.0000' COMMENT 'QUANTIDADE DE COMERCIALIZACAO DO PRODUTO',
  `quantidade_tributado` double(15,4) NOT NULL DEFAULT '1.0000' COMMENT 'QUANTIDADE DO PRODUTO TRIBUTADO',
  `valor_unitario_comercializacao` double(21,10) NOT NULL DEFAULT '0.0000000000' COMMENT 'VALOR UNITARIO DE COMERCIALIZACAO DO PRODUTO, O CONTRIBUINTE PODE UTILIZAR A PRECISAO DESEJADA (0-10 DECIMAIS).',
  `valor_unitario_tributacao` double(21,10) NOT NULL DEFAULT '0.0000000000' COMMENT 'VALOR UNITARIO DE TRIBUTACAO DO PRODUTO. PARA EFEITOS DE CALCULO, O VALOR UNITARIO SERA OBTIDO PELA DIVISAO DO VALOR\r\n DO PRODUTO PELA QUANTIDADE TRIBUTAVEL.',
  `valor_total_bruto` float(15,2) NOT NULL DEFAULT '0.00' COMMENT 'VALOR TOTAL BRUTO DO PRODUTO OU SERVICOS.',
  `valor_seguro_item` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO SEGURO POR ITEM',
  `valor_desconto_item` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO DESCONTO POR ITEM',
  `valor_frete` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO FRETE, O FRETE DEVE SER RATEADO ENTRE OS ITENS DE PRODUTO.',
  `valor_outras_despesas` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DE OUTRAS DESPESAS ACESSORIAS DO ITEM DE PRODUTO OU SERVICO.',
  `indicador_composicao_total_nota` enum('0','1') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1' COMMENT 'INDICADOR DE TRIBUTACAO. (0) - O VALOR DO ITEM (VPROD) NAO COMPOE O VALOR TOTAL DA NF-E (VPROD)  (1) - O VALOR DO ITEM (VPROD)\r\n COMPOE O VALOR TOTAL DA NF-E.',
  `numero_pedido_compra` varchar(15) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DO PEDIDO DE COMPRA',
  `numero_item_pedido_compra` varchar(6) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DO ITEM DO PEDIDO DE COMPRA',
  `numero_controle_fci` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DE CONTROLE DA FCI - FICHA DE CONTEUDO DE IMPORTACAO',
  `tipo_item` enum('P','S') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'P' COMMENT 'TIPO DE ITEM (P) PRODUTOS E (S) SERVICOS',
  `valor_total_aproximado_tributos_venda_consumidor_final` double(15,2) DEFAULT '0.00' COMMENT 'VALOR TOTAL APROXIMADO DOS TRIBUTOS EM VENDAS PARA CONSUMIDOR FINAL.',
  `cnpj_produtor_mercadoria` varchar(14) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CNPJ DO PRODUTOR DA MERCADORIA, QUANDO DIFERENTE DO EMITENTE NAS EXPORTACOES DIRETA OU INDIRETA.',
  `codigo_selo_controle_ipi` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO DO SELO DE CONTROLE DO IPI CONFORME ATOS NORMATIVOS EDITADOS PELA RECEITA FEDERAL DO BRASIL',
  `quantidade_selo_controle_ipi` double(15,0) DEFAULT NULL COMMENT 'QUANTIDADE DE SELO DE CONTROLE DO IPI UTILIZADOS.',
  `codigo_enquadramento_legal_ipi` varchar(3) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO DE ENQUADRAMENTO LEGAL DO IPI, INFORMAR 999 ENQUANTO A TABELA NAO TIVER SIDO CRIADA PELA RECEITA FEDERAL DO BRASIL.',
  `codigo_st_ipi` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO DE SITUACAO TRIBUTARIA DO IPI.',
  `valor_bc_ipi` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DA BASE DE CALCULO DO IPI',
  `aliquota_ipi` double(8,3) DEFAULT '0.000' COMMENT 'ALIQUOTA DO IPI',
  `valor_ipi` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO IPI',
  `quantidade_unidade_padrao_tributacao_ipi` double(15,4) DEFAULT '0.0000' COMMENT 'QUANTIDADE TOTAL NA UNIDADE PADRAO DE TRIBUTACAO DE IPI. ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `valor_unidade_tributavel_ipi` double(15,2) DEFAULT '0.00' COMMENT 'VALOR POR UNIDADE TRIBUTAVEL DE IPI, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `valor_bc_ii` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DA BC DO IMPOSTO DE IMPORTACAO',
  `valor_despesas_aduaneiras` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DAS DESPESAS ADUANEIRAS',
  `valor_ii` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO II (IMPOSTO DE IMPORTACAO)',
  `valor_iof` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO IOF - IMPOSTO SOBRE OPERACOES FINANCEIRAS',
  `cst_pis` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CST (CODIGO DE SITUACAO TRIBUTARIA) DO PIS',
  `valor_bc_pis` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DA BASE DE CALCULO DO PIS',
  `percentual_pis` double(8,2) DEFAULT '0.00' COMMENT 'PERCENTUAL DO PIS',
  `valor_pis` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO PIS',
  `quantidade_venda_pis` double(16,4) DEFAULT '0.0000' COMMENT 'QUANTIDADE VENDIDA COM PIS, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `aliquota_pis_em_reais` double(15,4) DEFAULT '0.0000' COMMENT 'ALIQUOTA DO PIS EM REAIS, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `valor_bc_pis_st` double(15,2) DEFAULT '0.00' COMMENT 'VALOR A BASE DE CALCULO DO PIS ST',
  `percentual_pis_st` double(8,3) DEFAULT '0.000' COMMENT 'PERCENTUAL DO PIS ST',
  `valor_pis_st` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO PIS ST',
  `quantidade_vendida_pis_st` double(16,4) DEFAULT '0.0000' COMMENT 'QUANTIDADE VENDIDA COM PIS ST, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `aliquota_pis_em_reais_st` double(15,4) DEFAULT '0.0000' COMMENT 'ALIQUOTA DO PIS EM REAIS COM PIS ST, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `cst_cofins` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CST COFINS',
  `valor_bc_cofins` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DA BASE DE CALCULO DO COFINS',
  `percentual_cofins` double(8,3) DEFAULT '0.000' COMMENT 'PERCENTUAL DO COFINS',
  `valor_cofins` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO COFINS',
  `quantidade_venda_cofins` double(16,4) DEFAULT '0.0000' COMMENT 'QUANTIDADE VENDIDA COM COFINS, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `aliquota_cofins_em_reais` double(15,4) DEFAULT '0.0000' COMMENT 'ALIQUOTA DO COFINS EM REAIS, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `valor_bc_cofins_st` double(15,2) DEFAULT '0.00' COMMENT 'VAOR DA BASE DE CALCULO DO COFINS ST',
  `aliquota_percentual_cofins_st` double(8,3) DEFAULT '0.000' COMMENT 'ALIQUOTA DO PERCENTUAL DO COFINS ST',
  `valor_cofins_st` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO COFINS ST',
  `quantidade_vendida_cofins_st` double(16,4) DEFAULT '0.0000' COMMENT 'QUANTIDADE VENDIDA COM COFINS ST. ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `aliquota_cofins_st_em_reais` double(15,4) DEFAULT '0.0000' COMMENT 'ALIQUOTA DO COFINS ST EM REAIS, ESTE CAMPO DEVE SER INFORMADO EM CASO DE ALIQUOTA ESPECIFICA.',
  `valor_bc_issqn` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DA BASE DE CALCULO DO ISSQN',
  `aliquota_issqn` double(8,3) DEFAULT '0.000' COMMENT 'ALIQUOTA DO ISSQN',
  `valor_issqn` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DO ISSQN',
  `codigo_municipio_ocorrencia_fato_gerador_issqn` int(7) unsigned zerofill DEFAULT '0000000' COMMENT 'CODIGO DO MUNICIPIO (DE ACORDO COM O IBGE) DE OCORRENCIA DO FATO GERADOR DO ISSQN',
  `codigo_pais_servico_prestado` int(4) unsigned zerofill DEFAULT '1058' COMMENT 'CODIGO DO PAIS ONDE O SERVICO FOI PRESTADO SEGUNDO O IBGE.',
  `item_lista_servico_abrasf` varchar(5) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'ITEM DA LISTA DE SERVICOS EM QUE SE CLASSIFICA O SERVICO NO PADRAO ABRASF (FORMATO: NN.NN)',
  `valor_deducao_reducao_bc_issqn` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DEDUCAO PARA REDUCAO DA BASE DE CALCULO NO ISSQN',
  `valor_outras_retencoes_issqn` double(15,2) DEFAULT '0.00' COMMENT 'VALOR OUTRAS RETENCOES DO ISSQN',
  `valor_desconto_incodicional_issqn` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DESCONTO INCONDICIONADO DO ISSQN.',
  `valor_desconto_condicional_issqn` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DESCONTO CONDICIONADO DO ISSQN.',
  `valor_retencao_issqn` double(15,2) DEFAULT '0.00' COMMENT 'VALOR DA RETENSAO DO ISSQN',
  `indicador_exigibilidade_iss` enum('1','2','3','4','5','6','7') COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT 'INDICADOR DA EXIGIBILIDADE DO ISS: 1=EXIGIVEL, 2=NAO INCIDENCIA, 3=ISENCAO, 4=EXPORTACAO, 5=IMUNIDADE, \r\n6=EXIGIBILIDADE SUSPENSA POR DECISAO JUDICIAL OU 7=EXIGIBILIDADE SUSPENSA POR PROCESSO ADMINISTRATIVO',
  `codigo_servico_prestado_dentro_municipio` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CODIGO DO SERVICO PRESTADO DENTRO DO MUNICIPIO.',
  `numero_processo_judicial_suspensao_exigibilidade` varchar(30) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DO PROCESSO JUDICIAL OU ADMINISTRATIVO DE SUSPENSAO DA EXIGIBILIDADE. INFORMAR SOMENTE QUANDO DECLARADA A SUSPENSAO DA \r\nEXIGIBILIDADE DO ISSQN.',
  `indicador_incentivo_fiscal_issqn` enum('1','2') COLLATE utf8mb3_unicode_ci DEFAULT '2' COMMENT 'INDICADOR DE INCENTIVO FISCAL NO ISSQN: 1=SIM; 2=NAO;',
  `recopi` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RECOPI(SISTEMA DE REGISTRO E CONTROLE DAS OPERACOES COM O PAPEL IMUNE NACIONAL)',
  `iss_st` enum('','N','R','S','I') COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'SITUACAO TRIBUTARIA DO ISS',
  `retencao_iss` enum('1','2') COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RENTENCAO DO ISS (1) SIM (2) NAO',
  `definicao_grupo_especifico_item` enum('O','V','M','A','P','C') COLLATE utf8mb3_unicode_ci DEFAULT 'O' COMMENT 'DEFINICAO DE GRUPO ESPECIFICO DO ITEM O (OUTROS), V (VEICULO), M (MEDICAMENTO), A (ARMA) OU C (COMBUSTIVEL)',
  `informacoes_adicionais_item` text COLLATE utf8mb3_unicode_ci COMMENT 'INFORMACOES ADCIONAIS DO ITEM',
  `percentual_devolucao_item` double(8,2) DEFAULT '0.00' COMMENT 'PERCENTUAL DE DEVOLUCAO DO ITEM',
  `valor_ipi_devolvido` double(13,2) DEFAULT '0.00' COMMENT 'VALOR DO IPI DEVOLVIDO',
  `codigo_grade` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_lancamento` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `data_atualizacao` timestamp NULL DEFAULT NULL,
  `codigo_beneficio_fiscal` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `produzido_escala_relevante` varchar(1) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `indicador_compoe_valor_total_nota_pis_st` varchar(1) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `indicador_compoe_valor_total_nota_cofins_st` varchar(1) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `origem_mercadoria_icms` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'NOME DO CAMPO: ORIG\ninformar o código da origem da mercadoria: codificação válida a partir de 01/10/2013 [28-09-13] 0 - Nacional, exceto as indicadas nos códigos 3, 4, 5 e 8; 1 - Estrangeira - Importação direta, exceto a indicada no código 6; 2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7; 3 - Nacional, mercadoria ou bem com Conteúdo de Importação superior a 40% e inferior ou igual a 70%; 4 - Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam as legislações citadas nos Ajustes; 5 - Nacional, mercadoria ou bem com Conteúdo de Importação inferior ou igual a 40%; 6 - Estrangeira - Importação direta, sem similar nacional, constante em lista da CAMEX e gás natural; 7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista da CAMEX e gás natural. 8 - Nacional, mercadoria ou bem com Conteúdo de Importação superior a 70%;',
  `cst_icms` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '00' COMMENT 'NOME DO CAMPO: CST\ninformar o CST da operação: 00 - Tributada integralmente; 10 - Tributada e com cobrança do ICMS por substituição tributária; 20 - Com redução de base de cálculo; 30 - Isenta ou não tributada e com cobrança do ICMS por substituição tributária; 40 - Isenta; 41 - Não tributada; 50 - Suspensão; 51 - Diferimento; 60 - ICMS cobrado anteriormente por substituição tributária; 70 - Com redução de base de cálculo e cobrança do ICMS por substituição tributária; 90 - Outros; P10 - ICMSPart com CST=10; P90 - ICMSPart com CST=90; S41 - ICMSST para CST=41; S60 - ICMSST para CST=60. ou Quando o CRT=1, informar o Código de Situação da Operação - Simples Nacional (CSOSN) 101 - Tributada pelo Simples Nacional com permissão de crédito; 102 - Tributada pelo Simples Nacional sem permissão de crédito; 103 - Isenção do ICMS no Simples Nacional para faixa de receita bruta; 201 - Tributada pelo Simples Nacional com permissão de crédito e com cobrança do ICMS por substituição tributária; 202 - Tributada pelo Simples ',
  `modalidade_base_calculo_icms` int DEFAULT '0' COMMENT 'NOME DO CAMPO: modBC\ninformar a modalidade de determinação da BC do ICMS: 0 - Margem Valor Agregado (%); 1 - Pauta (valor); 2 - Preço Tabelado Máximo (valor); 3 - Valor da Operação.',
  `percentual_reducao_base_calculo_icms_operacao_propria` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pRedBC\n informar o Percentual de redução da BC do ICMS da operação própria',
  `valor_base_calculo_icms` double(15,2) DEFAULT NULL COMMENT 'NOME DO CAMPO: vBC \ninformar o Valor da BC do ICMS do ICMS da operação própria',
  `aliquota_icms` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO:  pICMS\ninformar a Alíquota do ICMS do ICMS da operação própria',
  `valor_icms` double(15,2) DEFAULT NULL COMMENT 'NOME DO CAMPO: vICMS\ninformar o Valor do ICMS do ICMS da operação própria',
  `modalidade_determinacao_base_calculo_icms_st` int DEFAULT '4' COMMENT 'NOME DO CAMPO:modBCST\ninformar a modalidade de determinação da BC do ICMS ST: 0 - Preço tabelado ou máximo sugerido; 1 - Lista Negativa (valor); 2 - Lista Positiva (valor); 3 - Lista Neutra (valor); 4 - Margem Valor Agregado (%); 5 - Pauta (valor).',
  `percentual_margem_valor_adicionado_icms_st` double(8,4) DEFAULT '0.0000' COMMENT 'NOME CAMPO: pMVAST\ninformar o Percentual da Margem de Valor Adicionado ICMS ST',
  `percentual_reducao_base_icms_st` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pRedBCST\n informar o Percentual de redução da BC ICMS ST',
  `valor_base_calculo_icms_st` double(15,2) DEFAULT NULL COMMENT 'NOME DO CAMPO: vBCST\ninformar o Valor da BC do ICMS ST',
  `aliquota_icms_st` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pICMSST\ninformar a Alíquota do ICMS ST',
  `valor_icms_st` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSST\ninformar o Valor do ICMS ST',
  `valor_base_calculo_icms_st_retido` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vBCSTRet\ninformar o Valor da BC do ICMS ST retido',
  `valor_icms_st_retido` double(15,2) DEFAULT NULL COMMENT 'NOME DO CAMPO: vICMSSTRet\ninformar o Valor do ICMS ST retido',
  `valor_base_calculo_icms_st_uf_destino` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vBCSTDest\ninformar o Valor da BC do ICMS ST da UF Destino',
  `valor_icms_st_uf_destino` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSSTDest\ninformar o Valor do ICMS ST da UF destino',
  `motivo_desoneracao_icms` int DEFAULT NULL COMMENT 'NOME DO CAMPO:motDesICMS\n informar o Motivo da desoneração do ICMS: 1 - Táxi; 2 - Deficiente Físico; 3 - Produtor Agropecuário; 4 - Frotista/Locadora; 5 - Diplomático/Consular; 6 - Utilitários e Motocicletas da Amazônia Ocidental e Áreas de Livre Comércio (Resolução 714/88 e 790/94 - CONTRAN e suas alterações); 7 - SUFRAMA; 8 – Venda a Órgãos Públicos; 9 - outros. 10- Deficiente Condutor; 11- Deficiente não condutor; 16 - Olimpíadas Rio 2016; 90 - Solicitação do Fisco. Informar em conjunto com o vICMSDeson. IMPORTANTE: informe zero quando a operação não tenha isenção condicional.',
  `percentual_base_calculo_operacao_propria` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO:pBCOp\ninformar o Percentual da BC operação própria',
  `uf_devido_icms_st` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NOME DO CAMPO: UFST\ninformar a UF para qual é devido o ICMS ST',
  `aliquota_aplicavel_credito_simples_nacional` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: pCredSN\ninformar o Alíquota aplicável de cálculo do crédito (Simples Nacional)',
  `valor_credito_icms_aproveitado_simples_nacional` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vCredICMSSN\ninformar o Valor crédito do ICMS que pode ser aproveitado nos termos do art. 23 da LC 123 (SIMPLES NACIONAL)',
  `valor_icms_desoneracao` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSDeson\ninformar o Valor do ICMS da desoneração, deve ser informado quando em con junto com motDesICMS',
  `valor_icms_operacao_nao_diferido` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSOp\ninformar o Valor do ICMS da operação que não será diferido',
  `percentual_diferimento` double(8,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: pDif\ninformar o percentual do diferimento',
  `valor_icms_sera_diferido` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSDif\ninformar o Valor do ICMS que será diferido.',
  `valor_base_calculo_icms_fcp` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vBCFCP \ninformar o Valor da Base de Cálculo do FCP',
  `percentual_fcp_icms` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pFCP \ninformar o Percentual do FCP Nota: Percentual máximo de 2%, conforme a legislação.',
  `valor_fcp_icms` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vFCP\ninformar o Valor do FCP',
  `valor_base_calculo_fcp_retido_st_icms` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vBCFCPST \ninformar o Valor da Base de Cálculo do FCP retido por Substituição Tributária.',
  `percentual_fcp_retido_st_icms` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pFCPST \ninformar o Percentual do FCP retido por Substituição Tributária. Nota: Percentual máximo de 2%, conforme a legislação.',
  `valor_fcp_retido_st_icms` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vFCPST \ninformar o Valor do FCP retido por Substituição Tributária.',
  `valor_base_calculo_fcp_retido_anteriormente_st_icms` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vBCFCPSTRet\ninformar o Valor da Base de Cálculo do FCP retido anteriormente por Substituição Tributária.',
  `percentual_fcp_retido_anteriormente_st_icms` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pFCPSTRet\ninformar o Percentual do FCP retido anteriormente por Substituição Tributária. Nota: Percentual máximo de 2%, conforme a legislação.',
  `valor_fcp_retido_anteriormente_st_icms` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vFCPSTRet\n\n\ninformar o Valor do FCP retido anteriormente por Substituição Tributária.\n',
  `aliquota_suportada_consumidor_final_st_icms_incluso_fcp` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pST \ninformar Alíquota suportada pelo Consumidor Final. Deve ser informada a alíquota do cálculo do ICMS-ST, já incluso o FCP caso incida sobre a mercadoria. Exemplo: alíquota da mercadoria na venda ao consumidor final = 18% e 2% de FCP. A alíquota a ser informada no campo pST deve ser 20%.',
  `percentual_diferimento_icms_relativo_fcp` double(8,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: pFCPDif\nPercentual do diferimento do ICMS relativo ao Fundo de Combate à Pobreza (FCP). No caso de diferimento total, informar o percentual de diferimento "100" Nota: Percentual máximo de 2%, conforme a legislação.(campo novo)\n',
  `valor_icms_relativo_fcp_diferido` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vFCPDif\nValor do ICMS relativo ao Fundo de Combate à Pobreza (FCP) diferido.',
  `valor_efetivo_icms_relativo_fcp_icms` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vFCPEfet\nValor da Base de Cálculo do FCP',
  `valor_base_calculo_fcp_retido_st_icms_desonerado` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSSTDeson\n Valor da Base de Cálculo do FCP retido por Substituição Tributária',
  `motivo_desoneracao_icms_st` int DEFAULT NULL COMMENT 'NOME DO CAMPO: motDesICMSST\nMotivo da desoneração do ICMS-ST 3=Uso na agropecuária; 9=Outros; 12=Órgão de fomento e desenvolvimento agropecuário; Informar em conjunto com o vICMSSTDeson. IMPORTANTE: informe zero quando a operação não tenha isenção condicional.',
  `valor_base_uf_destino_icmsufdest` double(15,2) DEFAULT NULL COMMENT 'NOME DO CAMPO:  vBCUFDest\nInformar o Valor da Base de Cálculo do ICMS na UF de destino.',
  `valor_base_calculo_fcp_uf_destino_icmsufdest` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vBCFCPUFDest\ninformar o Valor da Base de Cálculo do FCP na UF Destino',
  `percentual_fcp_uf_destino_icmsufdest` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pFCPUFDest\n Informar o Percentual adicional inserido na alíquota interna da UF de destino, relativo ao Fundo de Combate à Pobreza (FCP) naquela UF. Nota: Percentual máximo de 2%, conforme a legislação.',
  `aliquota_adotada_operacoes_internas_uf_destino_icmsufdest` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pICMSUFDest  Informar a Alíquota adotada nas operações internas na UF de destino para o produto / mercadoria. A alíquota do Fundo de Combate a Pobreza, se existente para o produto / mercadoria, deve ser informada no campo próprio (pFCPUFDest) não devendo ser somada à essa alíquota interna.',
  `aliquota_interestadual_uf_envolvidas_icmsufdest` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pICMSInter \nInformar a Alíquota interestadual das UF envolvidas: - 4% alíquota interestadual para produtos importados; - 7% para os Estados de origem do Sul e Sudeste (exceto ES), destinado para os Estados do Norte, Nordeste, Centro-Oeste e Espírito Santo; - 12% para os demais casos.',
  `percentual_icms_interestadual_uf_destino_icmsufdest` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pICMSInterPart \nInformar Percentual de ICMS Interestadual para a UF de destino: - 40% em 2016; - 60% em 2017; - 80% em 2018; - 100% a partir de 2019.',
  `valor_icms_relativo_fcp_uf_destino_icmsufdest` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vFCPUFDest\nInformar o Valor do ICMS relativo ao Fundo de Combate à Pobreza (FCP) da UF de destino.',
  `valor_icms_interestadual_uf_destino_icmsufdest` double(15,2) DEFAULT NULL COMMENT 'NOME DO CAMPO: vICMSUFDest\nInformar o Valor do ICMS Interestadual para a UF de destino, UF de destino',
  `valor_icms_interestadual_uf_remetente_icmsufdest` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSUFRemet\nInformar o Valor do ICMS Interestadual para a UF do remetente. Nota: A partir de 2019, este valor será zero..',
  `valor_icms_proprio_st_operacao_anterior` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO:vICMSSbustituto  Valor do ICMS Próprio do Substituto cobrado em operação anterior.',
  `percentual_redeucao_submetida_regime_comum` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: pRedBCEfet informar percentual de redução, caso estivesse submetida ao regime comum de tributação, para obtenção da base de cálculo efetiva (vBCEfet).',
  `aliquota_icms_operacao_consumidor_submetida_regime_comum` double(8,4) DEFAULT '0.0000' COMMENT 'NOME DO CAMPO: pICMSEfet Informar a alíquota do ICMS na operação a consumidor final, caso estivesse submetida ao regime comum de tributação.',
  `valor_icms_efetivo_operacao_regime_comum` double(15,2) DEFAULT '0.00' COMMENT 'NOME DO CAMPO: vICMSEfet  Informar o valor obtido pelo produto do valor do campo pICMSEfet pelo valor do campo vBCEfet, caso estivesse submetida ao regime comum de tributação.',
  `quantidade_recebida` double(15,4) DEFAULT '0.0000',
  `usuario_cadastro` varchar(150) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `usuario_emissao` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_cancelamento` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_edicao` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `valor_imposto_federal` double(15,2) DEFAULT '0.00',
  `valor_imposto_municipal` double(15,2) DEFAULT '0.00',
  `valor_imposto_estadual` double(15,2) DEFAULT '0.00',
  `numeros_serie` text COLLATE utf8mb3_unicode_ci,
  `referencia_produto` varchar(20) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `valor_comissao` double(18,2) DEFAULT '0.00',
  `codigo_tabela_preco` int DEFAULT NULL,
  `codigo_fornecedor` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `preco_custo` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo_nota`,`codigo_empresa`,`numero_item`),
  KEY `zoxnotaitensFKzoxcadcfop` (`cfop`) USING BTREE,
  KEY `codigo_empresa` (`codigo_empresa`),
  KEY `codigo_nota` (`codigo_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='ITENS DA NOTA FISCAL';

CREATE TABLE IF NOT EXISTS `nota_fiscal_volume` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `codigo_volume` int NOT NULL DEFAULT '0',
  `quantidade_volume` int NOT NULL DEFAULT '0' COMMENT 'QUANTIDADE DO VOLUME',
  `descricao_especie_volume` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DESCRICAO DA ESPECIE DO VOLUME',
  `descricao_marca_volume` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DESCRICAO DA MARCA DO VOLUME',
  `numeracao_volume` varchar(60) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERACAO DO VOLUME',
  `peso_bruto` double(15,3) DEFAULT '0.000' COMMENT 'PESO BRUTO DO VOLUME',
  `peso_liquido` double(15,3) DEFAULT '0.000' COMMENT 'PESO LÍQUIDO DO VOLUME',
  PRIMARY KEY (`codigo_empresa`,`codigo_volume`,`codigo_nota`),
  KEY `codigo_empresa` (`codigo_empresa`),
  KEY `codigo_nota` (`codigo_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='VOLUMES DA NOTA FISCAL';

CREATE TABLE IF NOT EXISTS `nota_fiscal_lacres_volume` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `codigo_volume` int NOT NULL DEFAULT '0',
  `numero_lacre_volume` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NUMERO DO LACRE DO VOLUME',
  PRIMARY KEY (`codigo_nota`,`codigo_volume`,`codigo_empresa`,`numero_lacre_volume`),
  KEY `nota_fiscal_lacres_volumeFKnota_fiscal_volume` (`codigo_nota`,`codigo_empresa`,`codigo_volume`),
  CONSTRAINT `nota_fiscal_lacres_volumeFKnota_fiscal_volume` FOREIGN KEY (`codigo_nota`, `codigo_empresa`, `codigo_volume`) REFERENCES `nota_fiscal_volume` (`codigo_nota`, `codigo_empresa`, `codigo_volume`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='LACRES DO VOLUME DA NOTA FISCAL';


CREATE TABLE IF NOT EXISTS `nota_fiscal_observacao_contribuite_item` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `nome_campo_observacao` varchar(20) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NOME DO CAMPO DA OBSERVACAO',
  `observacao_contribuinte` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'OBSERVACAO DO CONTRIBUINTE',
  PRIMARY KEY (`codigo_nota`,`codigo_empresa`,`nome_campo_observacao`,`observacao_contribuinte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='OBSERVACOES DO CONTRINUINTE';


CREATE TABLE IF NOT EXISTS `nota_fiscal_observacao_fisco_item` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `nome_campo_observacao` varchar(20) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NOMDE DO CAPO DA OBSERVACAO',
  `observacao_fisco` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'OBSERVACAO DO FISCO',
  PRIMARY KEY (`codigo_empresa`,`nome_campo_observacao`,`observacao_fisco`,`codigo_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='OBSERVACOES DO FISCO DA NOTA FISCAL';


CREATE TABLE IF NOT EXISTS `nota_fiscal_pagamento` (
  `codigo_nota` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `seguencia_pagamento` int NOT NULL,
  `indicador_forma_pagamento` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '1' COMMENT 'INDICADOR DA FORMA DE PAGAMENTO: 0 - PAGAMENTO A VISTA; 1 - PAGAMENTO A PRAZO.',
  `tipo_pagamento` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '01' COMMENT 'MEIO DE PAGAMENTO:01=DINHEIRO02=CHEQUE03=CARTAO DE CREDITO04=CARTAO DE DEBITO05=CREDITO LOJA10=VALE ALIMENTACAO11=VALE REFEICAO12=VALE PRESENTE13=VALE COMBUSTIVEL15=BOLETO BANCARIO16=DEPOSITO BANCARIO17=PAGAMENTO INSTANTANEO (PIX)18=TRANSFERENCIA BANCARIA, CARTEIRA DIGITAL19=PROGRAMA DE FIDELIDADE, CASHBACK, CREDITO VIRTUAL90=SEM PAGAMENTO;99=OUTROS.',
  `descricao_tipo_pagamento` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DESCRICAO DO MEIO DE PAGAMENTO QUANDO TPAG FOR INFORMADO COM 99',
  `valor` double(15,2) NOT NULL DEFAULT '0.00',
  `integracao_pagamento` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '2' COMMENT 'TIPO DE INTEGRACAO DO PROCESSO DE PAGAMENTO COM O SISTEMA DE AUTOMACAO DA EMPRESA',
  `cnpj_administradora_cartao` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CNPJ DA INSTITUICAO DE PAGAMENTO, ADQUIRENTE OU SUBADQUIRENTE',
  `operadora_cartao` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '99' COMMENT 'BANDEIRA DA OPERADORA DE CARTAO DE CREDITO E/OU DEBITO',
  `autorizacao_cartao` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NUMERO DA AUTORIZACAO DA TRANSACAO DA OPERACAO COM CARTAO DE CREDITO E/OU DEBITO',
  `troco` double(15,2) DEFAULT '0.00',
  `codigo_forma_pagamento` int DEFAULT NULL,
  `quantidade_parcelas` int DEFAULT '1',
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo_empresa`,`seguencia_pagamento`,`codigo_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='FORMAS DE PAGAMENTO DA NOTA FISCAL';

CREATE TABLE IF NOT EXISTS `nota_fiscal_processos_referenciados` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `identificador_processo` varchar(60) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'IDENTIFICADOR DO PROCESSO OU ATO CONCESSORIO',
  `origem_processo` enum('0','1','2','3','9') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'ORIGEM DO PROCESSO (0) SEFAZ, (1) JUSTICA FEDERAL, (2) JUSTICA ESTADUAL, (3) SECEX/RFB ou (9) OUTROS',
  `tipo_processo` varchar(2) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT '(8) TERMO DE ACORDO, (10) REGIME ESPECIAL, (12) AUTORIZACAO ESPECIFICA',
  PRIMARY KEY (`codigo_empresa`,`codigo_nota`,`identificador_processo`,`origem_processo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='PROCESSOS REFERENCIADOS A NOTA FISCAL';

CREATE TABLE IF NOT EXISTS `nota_fiscal_reboque_transportador` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `placa_reboque` varchar(7) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NUMERO DA PLACA DO REBOQUE (XXX1234)',
  `sigla_uf_reboque` varchar(2) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'SIGLA DO ESTADO DO REBOQUE',
  `rntc_antt` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'RNTC - REGISTRO NACIONAL DE TRANSPORTADOR DE CARGA (ANTT) DO REBOQUE DO TRANSPORTADOR',
  PRIMARY KEY (`codigo_empresa`,`placa_reboque`,`sigla_uf_reboque`,`codigo_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='REBOQUES DO TRANSPORTADOR DA NOTA FISCAL';


CREATE TABLE IF NOT EXISTS `nota_fiscal_referenciada` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `numero_item` int(3) unsigned zerofill NOT NULL DEFAULT '000' COMMENT 'NUMERO DO ITEM',
  `tipo_documento_referenciado` enum('0','1','2') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0' COMMENT 'TIPO DE DOCUMENTO REFERENCIADO (0) NF-e, (1) CT-e OU NFCE',
  `chave_nota` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'CHAVE DA NFe OU CTe',
  `data_emissao` timestamp NULL DEFAULT NULL COMMENT 'DATA DA EMISSAO DA NOTA DE TALAO FORMATO MM/AA',
  PRIMARY KEY (`codigo_empresa`,`codigo_nota`,`numero_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='NOTAS FISCAIS REFERENCIADAS NF-e, CT-e e NF';

CREATE TABLE IF NOT EXISTS `nota_fiscal_referenciada_produtor` (
  `codigo_nota` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_empresa` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'CODIGO DA EMPRESA',
  `numero_item` int(3) unsigned zerofill NOT NULL DEFAULT '000' COMMENT 'NUMERO DO ITEM',
  `sigla_estado_nota_produtor` varchar(2) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'SIGLA DO ESTADO DA NOTA FISCAL DE PRODUTOR',
  `modelo_nota_produtor` enum('01','04') COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '01' COMMENT 'MODELO DA NOTA DE PRODUTOR REFERENCIADA',
  `data_emissao_nota_produtor` date NOT NULL COMMENT 'DATA DA EMISSAO DA NOTA DE PRODUTOR FORMATO MM/AA',
  `numero_documento_emitente_nota_produtor` varchar(18) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'NUMERO DO DOCUMENTO DO EMITENTE DA NOTA FISCAL DE PRODUTOR REFERENCIADA',
  `inscricao_estadual_produtor` varchar(18) COLLATE utf8mb3_unicode_ci DEFAULT 'ISENTO' COMMENT 'INSCRICAO ESTADUAL DO EMITENTE DA NOTA DE PRODUTOR REFERENCIADA',
  `serie_nota_produtor` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'SERIE DA NOTA FISCAL DE PRODUTOR REFERENCIADA',
  `numero_nota_produtor` int NOT NULL DEFAULT '0' COMMENT 'NUMERO DA NOTA FISCAL DE PRODUTOR REFERENCIADA',
  PRIMARY KEY (`codigo_nota`,`codigo_empresa`,`numero_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='NOTAS FISCAIS REFERENCIADAS DE PRODUTOR';


CREATE TABLE IF NOT EXISTS `numero_serie` (
  `serie` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `numero_venda` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`serie`,`referencia_produto`,`codigo_empresa`),
  KEY `numero_serieFKempresa` (`codigo_empresa`) USING BTREE,
  KEY `numero_serieFKproduto` (`referencia_produto`) USING BTREE,
  CONSTRAINT `numero_serieFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `numero_serieFKproduto` FOREIGN KEY (`referencia_produto`) REFERENCES `produto` (`referencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `pdvs` (
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `numero` int NOT NULL DEFAULT '1',
  `chave` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `documento_fiscal` int DEFAULT NULL,
  `tipo_emissao` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `tempo_sincronizacao` smallint DEFAULT '30',
  `tema_principal` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'roxo',
  `controla_caixa` smallint DEFAULT '1' COMMENT '1 - sim, 0 - nao',
  `identifica_consumiddo` smallint DEFAULT '0' COMMENT '1 - sim,  0 - nao',
  `identifica_vendedor` smallint DEFAULT '0' COMMENT '1 - sim, 0 - nao',
  `usa_catraca` smallint DEFAULT '0' COMMENT '1 - sim, 0 - nao',
  `usa_comanda` smallint DEFAULT '0' COMMENT '1 - sim, 0 - nao',
  `imagem_produto_pesquisa` smallint DEFAULT '0' COMMENT '1 - sim, 0 - nao',
  `salva_ultima_pesquisa` smallint DEFAULT '0' COMMENT '1 - sim, 0 - nao',
  `valor_para_aviso_sangria` double(15,2) DEFAULT '0.00',
  `marca_impressora` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `valor_identifica_consumidor` double(15,2) DEFAULT '5000.00',
  `valor_maximo_quebra_caixa` double(15,2) DEFAULT '0.00',
  `balanca_porta_serial` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'COM1',
  `velocidade` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '9600',
  `paridade` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'NENHUM',
  `bits_dados` smallint DEFAULT '8',
  `bits_parada` smallint DEFAULT '2',
  `etiqueta_digito_verificador` smallint DEFAULT '2',
  `etiqueta_digito_inicial_codigo` smallint DEFAULT '2',
  `etiqueta_digito_final_codigo` smallint DEFAULT '2',
  `etiqueta_tipo` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'VALOR',
  `etiqueta_casas_decimais` smallint DEFAULT '2',
  `tef_numero_vias` smallint DEFAULT '2',
  `tef_tipo` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'discado',
  `previsualizar_cupom` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo_empresa`,`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS `pedido` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nome_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `logradouro_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_tabela_preco` int NOT NULL DEFAULT '0',
  `desconto` double(15,3) unsigned DEFAULT '0.000',
  `acrescimo` double(15,3) unsigned DEFAULT '0.000',
  `data_pedido` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_entrega` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo_desconto_acrescimo` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `status` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'LA' COMMENT 'LA -lancado, PG - pago,OR -orcamento, CA-cancelado',
  `total_bruto` double(18,3) NOT NULL DEFAULT '0.000',
  `total_liquido` double(18,3) NOT NULL DEFAULT '0.000',
  `numero_documento_fiscal` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_atualizacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status_entrega` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '' COMMENT 'AE - aguardando entrega, ER- entrega realizada,EP-entrega parcial',
  `nome_vendedor` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_pessoa` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `bairro_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `cidade_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `estado_pessoa` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `cep_pessoa` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `complemento_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_nota` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `codigo_venda` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`,`codigo_empresa`),
  KEY `codigo` (`codigo`) USING BTREE,
  KEY `codigo_pessoa` (`codigo_pessoa`) USING BTREE,
  KEY `data_pedido` (`data_pedido`) USING BTREE,
  KEY `numero_documento_fiscal` (`numero_documento_fiscal`) USING BTREE,
  KEY `pedidoFKempresa` (`codigo_empresa`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  CONSTRAINT `pedidoFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `pedido_forma_pagamento` (
  `codigo_pedido` int NOT NULL DEFAULT '0',
  `codigo_forma_pagamento` int NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `valor` double(15,2) unsigned DEFAULT '0.00',
  `parcelas` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo_pedido`,`codigo_forma_pagamento`,`codigo_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `pedido_forma_pagamento_parcelas` (
  `codigo_pedido` int NOT NULL DEFAULT '0',
  `codigo_forma_pagamento` int NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `numero_parcela` int NOT NULL DEFAULT '1',
  `valor_parcela` double(18,2) NOT NULL DEFAULT '0.00',
  `data_vencimento` date NOT NULL,
  PRIMARY KEY (`codigo_pedido`,`codigo_forma_pagamento`,`codigo_empresa`,`numero_parcela`),
  CONSTRAINT `pedido_forma_pagamento_parcelasFKpedido_forma_pagamento` FOREIGN KEY (`codigo_pedido`, `codigo_forma_pagamento`, `codigo_empresa`) REFERENCES `pedido_forma_pagamento` (`codigo_pedido`, `codigo_forma_pagamento`, `codigo_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `pedido_itens` (
  `codigo_pedido` int NOT NULL DEFAULT '0',
  `codigo_embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `numero_item` int NOT NULL,
  `codigo_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_unidade` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `descricao_item` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `quantidade` double(10,3) NOT NULL DEFAULT '0.000',
  `preco_unitario` double(15,2) NOT NULL DEFAULT '0.00',
  `total_bruto` double(18,2) NOT NULL DEFAULT '0.00',
  `total_liquido` double(18,2) NOT NULL DEFAULT '0.00',
  `entrega` smallint NOT NULL DEFAULT '0',
  `endereco_entrega` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `desconto` double(15,2) DEFAULT '0.00',
  `acrescimo` double(15,2) DEFAULT '0.00',
  `tipo_desconto_acrescimo` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_atualizacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1-lancado, 0-cancelado',
  `item_entregue` smallint NOT NULL DEFAULT '0' COMMENT '0-nao, 1-sim',
  `data_entrega` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `codigo_grade` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `preco_atacado` double(15,2) DEFAULT NULL,
  `quantidade_atacado` double(10,3) DEFAULT NULL,
  `bonificacao` smallint NOT NULL DEFAULT '0',
  `valor_desconto` double(18,2) DEFAULT '0.00',
  `valor_acrescimo` double(18,2) DEFAULT '0.00',
  PRIMARY KEY (`codigo_pedido`,`codigo_embalagem`,`codigo_empresa`,`numero_item`),
  KEY `codigo_embalagem` (`codigo_embalagem`) USING BTREE,
  KEY `codigo_pedido` (`codigo_pedido`) USING BTREE,
  KEY `data_lancamento` (`data_lancamento`) USING BTREE,
  KEY `pedido_itensFKempresa` (`codigo_empresa`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  CONSTRAINT `pedido_itensFKembalagem_empresa` FOREIGN KEY (`codigo_embalagem`) REFERENCES `embalagem_empresa` (`embalagem`),
  CONSTRAINT `pedido_itensFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `pedido_itensFKpedido` FOREIGN KEY (`codigo_pedido`) REFERENCES `pedido` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

 CREATE TABLE IF NOT EXISTS `plano_contas_categoria` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tipo` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'D' COMMENT 'A-ativos,P-passivos,D-despesas,R-receitas',
  `data_atualizacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `padrao` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `plano_contas` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_categoria` int NOT NULL DEFAULT '0',
  `data_atualizacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo`),
  KEY `placo_contasFKplano_contas_categoria` (`codigo_categoria`) USING BTREE,
  CONSTRAINT `placo_contasFKplano_contas_categoria` FOREIGN KEY (`codigo_categoria`) REFERENCES `plano_contas_categoria` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `referencia` (
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `referencia_auxiliar` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data_atualizacao` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`referencia_produto`,`referencia_auxiliar`),
  KEY `referencia_auxiliar` (`referencia_auxiliar`),
  KEY `referencia_produto` (`referencia_produto`),
  CONSTRAINT `referencia_ibfk_1` FOREIGN KEY (`referencia_produto`) REFERENCES `produto` (`referencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci COMMENT='tabela de referencias de produtos';

CREATE TABLE IF NOT EXISTS `relatorios` (
  `modulo` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `nome_relatorio` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `parametros` json NOT NULL,
  `padrao` smallint NOT NULL DEFAULT '0',
  `data_cadastro` date NOT NULL,
  `usuario` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`modulo`,`nome_relatorio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `relatorios_perfil` (
  `modulo` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `nome_relatorio` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `perfil` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`modulo`,`nome_relatorio`,`perfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `seguencia_nota_fiscal` (
  `serie_nota` int NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `ambiente` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1',
  `usuario` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `seguencia_nota` int NOT NULL DEFAULT '0',
  `versao` int DEFAULT '0',
  `codigo_nota` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'P',
  PRIMARY KEY (`serie_nota`,`codigo_empresa`,`ambiente`,`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `tabela_preco_embalagem_empresa` (
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_tabela_preco` int NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `preco_venda` double(18,2) unsigned NOT NULL DEFAULT '0.00',
  `comissao_venda` double(15,2) unsigned DEFAULT '0.00',
  `status` smallint NOT NULL DEFAULT '1',
  `principal` smallint NOT NULL DEFAULT '0',
  `tipo_comissao` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'V' COMMENT '(V)alor,(P)orcentagem',
  `preco_atacado` double(15,2) DEFAULT '0.00',
  `data_ultima_atualizacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario_ultima_atualizacao` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_email_atualizacao` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `padrao` smallint DEFAULT '0',
  `markup` double(10,2) DEFAULT '0.00',
  `data_cadastro` date NOT NULL,
  PRIMARY KEY (`embalagem`,`codigo_tabela_preco`,`codigo_empresa`),
  KEY `FK_tabela_preco_embalagem_empresa_empresa` (`codigo_empresa`),
  KEY `FK_tabela_preco_embalagem_empresa_produto` (`referencia_produto`),
  KEY `Index 4` (`principal`,`referencia_produto`,`embalagem`,`codigo_empresa`) USING BTREE,
  KEY `FK_tabela_preco_embalagem_empresa_tabela_preco` (`codigo_tabela_preco`),
  CONSTRAINT `FK_tabela_preco_embalagem_empresa_embalagem_empresa` FOREIGN KEY (`embalagem`) REFERENCES `embalagem_empresa` (`embalagem`),
  CONSTRAINT `FK_tabela_preco_embalagem_empresa_empresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `FK_tabela_preco_embalagem_empresa_produto` FOREIGN KEY (`referencia_produto`) REFERENCES `produto` (`referencia`),
  CONSTRAINT `FK_tabela_preco_embalagem_empresa_tabela_preco` FOREIGN KEY (`codigo_tabela_preco`) REFERENCES `tabela_preco` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `tabela_preco_perfil` (
  `codigo_tabela_preco` int NOT NULL,
  `perfil_usuario` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`codigo_tabela_preco`,`perfil_usuario`),
  CONSTRAINT `tabela_preco_perfilFKtabela_preco` FOREIGN KEY (`codigo_tabela_preco`) REFERENCES `tabela_preco` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `venda_itens` (
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `numero_item` int NOT NULL DEFAULT '0',
  `codigo_venda` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0',
  `codigo_embalagem` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `descricao_produto` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `fator_multiplicacao` double(10,3) NOT NULL DEFAULT '1.000',
  `quantidade_atacado` double(15,3) DEFAULT NULL,
  `quantidade` double(15,3) NOT NULL DEFAULT '1.000',
  `total_bruto` double(18,2) NOT NULL DEFAULT '0.00',
  `total_liquido` double(18,2) NOT NULL DEFAULT '0.00',
  `preco_unitario` double(18,2) NOT NULL DEFAULT '0.00',
  `preco_atacado` double(18,2) DEFAULT '0.00',
  `acrescimo` double(15,3) DEFAULT '0.000',
  `desconto` double(15,3) DEFAULT '0.000',
  `tipo_acrescimo_desconto` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'P - porcentagem, V - valor',
  `codigo_grade` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_lote` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status_item` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'N - normal, C - cancelado',
  `status_venda` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'N - normal, C - cancelado',
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `data_lancamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_cancelamento` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `usuario_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `usuario_venda` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `usuario_autorizacao_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `codigo_unidade` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `garantia` int DEFAULT '0',
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `validade` int DEFAULT NULL,
  `numero_venda` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `referencia_produto` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `codigo_nota` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `preco_custo` double(15,3) DEFAULT '0.000',
  `quantidade_devolvida` double(15,3) DEFAULT '0.000',
  `valor_comissao` double(18,2) DEFAULT '0.00',
  `codigo_tabela_preco` int DEFAULT NULL,
  PRIMARY KEY (`codigo_empresa`,`numero_item`,`codigo_venda`),
  KEY `venda_itensFKvendas` (`codigo_venda`),
  KEY `venda_itensFKembalagem` (`codigo_embalagem`),
  KEY `codigo_nota` (`codigo_nota`),
  KEY `codigo_nota_2` (`codigo_nota`),
  KEY `referencia_produto` (`referencia_produto`),
  KEY `status_item` (`status_item`),
  CONSTRAINT `venda_itensFKembalagem` FOREIGN KEY (`codigo_embalagem`) REFERENCES `embalagem_empresa` (`embalagem`),
  CONSTRAINT `venda_itensFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `venda_pagamento` (
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_venda` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_forma_pagamento` int NOT NULL DEFAULT '0',
  `valor_pago` double(18,2) NOT NULL DEFAULT '0.00',
  `valor_liquido` double(18,2) NOT NULL DEFAULT '0.00',
  `tipo_forma_pagamento` int NOT NULL DEFAULT '0',
  `descricao_forma_pagamento` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `data_pagamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'N' COMMENT 'N - normal, C - cancelado',
  `acrescimo` double(15,2) DEFAULT '0.00',
  `desconto` double(15,2) DEFAULT '0.00',
  `tipo_acrescimo_desconto` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'P - porcentagem, V - valor',
  `quantidade_parcelas` int DEFAULT '0',
  `usuario_pagamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `usuario_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_cancelamento` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_autorizacao_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nsu` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `autorizacao_tef` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `hora_tef` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_tef` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `comprovante_cliente_tef` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `comprovante_estabelecimento_tef` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `valor_tef` double(18,2) DEFAULT NULL,
  `taxa_cartao` double(15,2) DEFAULT NULL,
  `data_atualizacao` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `numero_venda` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_nota` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `baixa_automatica` smallint DEFAULT '0',
  `gera_financeiro` smallint DEFAULT '0',
  PRIMARY KEY (`codigo_empresa`,`codigo_venda`,`codigo_forma_pagamento`),
  KEY `venda_pagamentoFKforma_pagamento` (`codigo_forma_pagamento`),
  KEY `codigo_nota` (`codigo_nota`),
  CONSTRAINT `venda_pagamentoFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`),
  CONSTRAINT `venda_pagamentoFKforma_pagamento` FOREIGN KEY (`codigo_forma_pagamento`) REFERENCES `forma_pagamento` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS `vendas` (
  `codigo` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `numero_venda` varchar(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '0',
  `codigo_empresa` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `data_venda` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_atualizacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total_bruto` double(18,2) NOT NULL DEFAULT '0.00',
  `total_liquido` double(18,2) NOT NULL DEFAULT '0.00',
  `valor_desconto` double(15,2) DEFAULT '0.00',
  `valor_acrescimo` double(15,2) DEFAULT '0.00',
  `tipo_desconto_acrescimo` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'P' COMMENT 'P - porcentagem, V - valor',
  `status` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'N - normal, C - cancelada',
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `documento_fiscal` varchar(47) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_pedido` int DEFAULT NULL,
  `codigo_vendedor` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `codigo_pessoa` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT '',
  `cep_endereco_pessoa` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `logradouro_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_endereco_pessoa` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `complemento_endereco_pessoa` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `bairro_pessoa` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `municipio_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `uf_pessoa` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `email_pessoa` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data_emissao_cupom` timestamp NULL DEFAULT NULL,
  `status_migracao` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'N - normal, F - finalizado, A - atualizado, M - migrado',
  `razao_social_pessoa` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `chave_nota` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_nota` int DEFAULT NULL,
  `codigo_usuario` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
  `observacao` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `qrcode` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `numero_protocolo` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `tipo_emissao` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'NFE, NFCE, PEDIDO',
  `usuario_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `chave_cancelamento` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `chave_temporaria` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numero_nota_temporario` varchar(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `contingencia` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'S - sim, N - nao',
  `usuario_autorizacao_cancelamento` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `origem` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '' COMMENT 'Z-ZION,P-PDV',
  `serie_documento_fiscal` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '1',
  `codigo_nota` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status_devolucao` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'DP-Devolução Parcial,DT-Devolução Total',
  `total_devolucao` double(18,2) DEFAULT '0.00',
  `codigo_tabela_preco` int DEFAULT NULL,
  `inscricao_estadual_pessoa` varchar(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT 'INSC ESTADUAL OU RG',
  `numero_pdv` int DEFAULT NULL,
  `xml` longtext COLLATE utf8mb3_unicode_ci,
  `ambiente_emissao` varchar(1) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `rejeicao` text COLLATE utf8mb3_unicode_ci,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `numero_venda` (`numero_venda`),
  KEY `codigo_empresa_origem` (`codigo_empresa`,`origem`),
  KEY `data_venda` (`data_venda`),
  KEY `codigo_nota` (`codigo_nota`,`codigo_empresa`),
  KEY `status` (`status`),
  KEY `status_devolucao` (`status_devolucao`),
  CONSTRAINT `vendasFKempresa` FOREIGN KEY (`codigo_empresa`) REFERENCES `empresa` (`codigo_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

                
                CREATE TRIGGER `atualiza_reserva_estoque_exclusao`
                  AFTER DELETE ON `estoque_reservado`
                  FOR EACH ROW
                BEGIN
                  update embalagem_empresa set estoque_resevado=coalesce(estoque_resevado,0)-OLD.quantidade where referencia_produto=OLD.codigo_produto and codigo_empresa=OLD.codigo_empresa;
                  if OLD.codigo_grade is NOT null THEN
                    BEGIN
                        update
                           grade_produto_empresa gradPordEmp
                        set
                           gradPordEmp.estoque_reservado=coalesce(gradPordEmp.estoque_reservado,0)-OLD.quantidade
                        where
                            gradPordEmp.codigo=OLD.codigo_grade
                        and gradPordEmp.codigo_empresa=OLD.codigo_empresa;
                    END;
                  END IF;
                END;
                
                CREATE TRIGGER `atualiza_reserva_estoque_inclusao`
                  AFTER INSERT ON `estoque_reservado`
                  FOR EACH ROW
                BEGIN
                 update embalagem_empresa set estoque_resevado=coalesce(estoque_resevado,0)+NEW.quantidade where referencia_produto=NEW.codigo_produto and codigo_empresa=NEW.codigo_empresa;
                  if NEW.codigo_grade is NOT null THEN
                    BEGIN
                        update
                           grade_produto_empresa gradPordEmp
                        set
                           gradPordEmp.estoque_reservado=coalesce(gradPordEmp.estoque_reservado,0)+NEW.quantidade
                        where
                            gradPordEmp.codigo=NEW.codigo_grade
                        and gradPordEmp.codigo_empresa=NEW.codigo_empresa;
                    END;
                  END IF;
                END;
                
                CREATE TRIGGER `atualizar_estoque_produto_estoque_delete`
                  AFTER DELETE ON `movimentacao_estoque_itens`
                  FOR EACH ROW
                BEGIN
                  update embalagem_empresa emb set emb.estoque_atual=coalesce(emb.estoque_atual,0)-OLD.quantidade where emb.referencia_produto=OLD.referencia_produto and emb.codigo_empresa=OLD.codigo_empresa;
                 if OLD.codigo_grade is NOT null THEN
                    BEGIN
                        update grade_produto_empresa gradPordEmp set gradPordEmp.estoque_atual=coalesce(gradPordEmp.estoque_atual,0)-OLD.quantidade where gradPordEmp.codigo=OLD.codigo_grade and gradPordEmp.codigo_empresa=OLD.codigo_empresa;
                    END;
                  END IF;
                END;
               
                CREATE TRIGGER `atualizar_estoque_produto_insert`
                  AFTER INSERT ON `movimentacao_estoque_itens`
                  FOR EACH ROW
                BEGIN
                  DECLARE tipoMovimentacao varchar(13);
                  select tipoMovimentacao.tipo from tipo_movimentacao tipoMovimentacao where tipoMovimentacao.codigo=NEW.tipo_movimento into tipoMovimentacao;
                  update
                     embalagem_empresa emb
                  set
                     emb.estoque_atual=if(tipoMovimentacao = 'ENTRADA',coalesce(emb.estoque_atual,0)+NEW.quantidade,coalesce(emb.estoque_atual,0)-NEW.quantidade)
                  where
                     emb.referencia_produto=NEW.referencia_produto
                 and emb.codigo_empresa=NEW.codigo_empresa;
                 if NEW.codigo_grade is NOT null THEN
                    BEGIN
                        update grade_produto_empresa gradPordEmp set gradPordEmp.estoque_atual=if(tipoMovimentacao='ENTRADA',coalesce(gradPordEmp.estoque_atual,0)+NEW.quantidade,coalesce(gradPordEmp.estoque_atual,0)-NEW.quantidade) where gradPordEmp.codigo=NEW.codigo_grade and gradPordEmp.codigo_empresa=NEW.codigo_empresa;
                    END;
                  END IF;
                END;

CREATE TRIGGER `fluxo_caixa_after_insert` AFTER INSERT ON `fluxo_caixa` FOR EACH ROW BEGIN
  DECLARE codigoConta INT(11);

  SELECT formaPagamento.conta_bancaria
  INTO codigoConta
  FROM forma_pagamento formaPagamento
  WHERE formaPagamento.codigo_empresa = NEW.codigo_empresa
    AND formaPagamento.codigo = NEW.forma_pagamento;

  CASE
    WHEN NEW.tipo = 'E' THEN
      UPDATE conta_bancaria
      SET saldo_conta = COALESCE(saldo_conta, 0) + NEW.valor
      WHERE empresa = NEW.codigo_empresa AND codigo = codigoConta;
    WHEN NEW.tipo = 'S' THEN
      UPDATE conta_bancaria
      SET saldo_conta = COALESCE(saldo_conta, 0) - NEW.valor
      WHERE empresa = NEW.codigo_empresa AND codigo = codigoConta;
  END CASE;
END;
                
                INSERT INTO `grupo` VALUES (1,'DIVERSOS',NULL,1,CURRENT_TIMESTAMP(),NULL,NULL,1,NULL);

                INSERT INTO `subgrupo` VALUES (1,1,'DIVERSOS',1,NULL,CURRENT_TIMESTAMP(),NULL,NULL,NULL);

                INSERT INTO `unidade` VALUES ('CT','CARTELA',0,0),('CX','CAIXA',1,1),('FD','FARDO',1,1),('JG','JOGO',0,1),('KG','QUILO',1,1),('M2','METRO QUADRADO',0,1),('ML','METRO LINER',0,1),('MT','METRO',0,0),('PA','PACOTE',0,0),('PC','PEÇA',0,1),('UN','UNIDADE',0,1);

                INSERT INTO `tabela_preco` VALUES (20,'CONSUMIDOR',NULL,NULL,1,0.00,'P',1);

                INSERT INTO `empresa` VALUES (VALOR_CODIGO_DOCLIENTE,VALOR_CNPJ_DOCLIENTE,VALOR_RAZAO_SOCIAL_DOCLIENTE,VALOR_NOME_FANTASIA_DOCLIENTE,VALOR_REGIME_TRIBUTARIO,VALOR_INSCRICAO_ESTADUAL_DOCLIENTE,NULL,VALOR_INSCRICAO_MUNICIPAL_DOCLIENTE,VALOR_TELEFONE_DOCLIENTE,VALOR_LOGRADOURO_DOCLIENTE,VALOR_NUMERO_DOCLIENTE,VALOR_BAIRRO_DOCLIENTE,VALOR_CODIGO_MUNICIPIO_DOCLIENTE,VALOR_COMPLEMENTO_DOCLIENTE,VALOR_CEP_DOCLIENTE,VALOR_CNAE_DOCLIENTE,VALOR_GRUPO_DOCLIENTE,VALOR_CERTIFICADO_DIGITAL_DOCLIENTE,VALOR_SENHA_CERTIFICADO_DOCLIENTE,VALOR_CODIGO_CESTA_TRIBUTACAO_CLIENTE,VALOR_MATRIZ_DOCLIENTE,'','','','','','',20,0,0,0,1,0,0,1,1,1,5000.00,0.00,0.00,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),NULL,'NFCE','principal',2,1,0,0,0,1,VALOR_APELIDO_DOCLIENTE,NULL,30,30,30,0,NULL,VALOR_MUNICIPIO_DOCLIENTE,VALOR_ESTADO_DOCLIENTE,VALOR_UF_DOCLIENTE,VALOR_CODIGO_ESTADO_DOCLIENTE,1,'UN',1,VALOR_EMAIL_DOCLIENTE,VALOR_INIDICADOR_INSCRICAO_ESTADUAL_DOCLIENTE,0,0,0,'CIF',VALOR_CODIGO_PLANO_CONTRATADO_DOCLIENTE,1,0,0);

                INSERT INTO `centro_custo` VALUES (1,'ESCRITÓRIO');

                DROP PROCEDURE IF EXISTS inserir_cesta_tributacao;
                CREATE PROCEDURE inserir_cesta_tributacao()
                 BEGIN
                 DECLARE insertCestaTributacao text;
                SET @insertCestaTributacao = 'INSERT INTO `cestatributacao` VALUES ';
                IF VALOR_REGIME_TRIBUTARIO = '1' THEN
                  BEGIN
                    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(7,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADO COM PERMISSÃO DE CREDITO ','1','101','1',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,5.000,NULL,'53','999',0.000,'08',0.000,0.000,'08',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(8,'",VALOR_CODIGO_DOCLIENTE,"','NÃO TRIBUTADO PELO SIMPLES NACIONAL','1','400','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(10,'",VALOR_CODIGO_DOCLIENTE,"','SUBSTITUIÇÃO TRIBUTARIA','1','500','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'53','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(12,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADA COM PERMISSÃO DE CREDITO ICMS ST','1','201','3',0.000,NULL,0.000,'4',0.000,0.000,18.000,'',NULL,0.000,NULL,'53','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(13,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADA SEM PERMISSÃO DE CRÉDITO ICMS ST','1','202','3',0.000,NULL,0.000,'4',0.000,0.000,0.000,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                    SET @insertCestaTributacao = concat(@insertCestaTributacao, "(14,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADO SEM PERMISSÃO DE CREDITO ','1','102','1',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'08',0.000,0.000,'08',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00)");
                  END;
                ELSE
                  BEGIN
                   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(9,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADO 20,5% PELO REGIME NORMAL','3','00','3',0.000,20.500,0.000,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'50','999',0.000,'01',1.650,0.000,'01',7.600,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(15,'",VALOR_CODIGO_DOCLIENTE,"','TRIBUTADA E COM COBRANÇA DO ICMS POR SUBSTITUIÇÃO ','3','10','3',0.000,18.000,0.000,'4',0.000,0.000,18.000,'',NULL,NULL,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(16,'",VALOR_CODIGO_DOCLIENTE,"','ICMS COBRADO ANTERIORMENTE POR ST','3','60','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(17,'",VALOR_CODIGO_DOCLIENTE,"','COM REDUÇÃO DE BC E COBRANÇA DO ICMS ST','3','70','3',0.000,0.000,0.000,'4',0.000,0.000,18.000,'',NULL,NULL,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(18,'",VALOR_CODIGO_DOCLIENTE,"','NÃO TRIBUTADA','3','41','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'06',0.000,0.000,'06',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(24,'",VALOR_CODIGO_DOCLIENTE,"',' COM REDUÇÃO DE BASE DE CÁLCULO','3','20','3',15.000,18.000,0.000,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'',NULL,NULL,'08',0.000,0.000,'08',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00),");
                   SET @insertCestaTributacao = concat(@insertCestaTributacao, "(25,'",VALOR_CODIGO_DOCLIENTE,"','ISENTO','3','40','3',0.000,NULL,0.000,NULL,NULL,NULL,NULL,'',NULL,0.000,NULL,'99','999',0.000,'49',0.000,0.000,'49',0.000,0.000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.00,0.00)");
                  END;
                END IF;
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, ' ON DUPLICATE KEY UPDATE ');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'codigo=values(codigo),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'codigo_empresa=values(codigo_empresa),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'descricao=values(descricao),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'regime_tributario=values(regime_tributario),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cst=values(cst),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'modalidade_basecalculo_icms=values(modalidade_basecalculo_icms),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_basecalculo_icms=values(percentual_basecalculo_icms),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_icms_dentro_estado=values(aliquota_icms_dentro_estado),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_icms_fora_estado=values(aliquota_icms_fora_estado),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'modalidade_basecalculo_st=values(modalidade_basecalculo_st),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_margem_icms_st=values(percentual_margem_icms_st),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_reducao_basecalculo_icms_st=values(percentual_reducao_basecalculo_icms_st),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_icms_st=values(aliquota_icms_st),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'motivo_desoneracao_icms=values(motivo_desoneracao_icms),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_base_operacao_propria=values(percentual_base_operacao_propria),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_credito_nascional=values(aliquota_credito_nascional),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_diferimento=values(percentual_diferimento),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'situacao_tributaria_ipi=values(situacao_tributaria_ipi),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'enquadramento_ipi=values(enquadramento_ipi),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_ipi=values(aliquota_ipi),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'codigo_situacao_tributaria_pis=values(codigo_situacao_tributaria_pis),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_pis=values(aliquota_pis),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_pis_st=values(aliquota_pis_st),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cofins_situacao_tributaria=values(cofins_situacao_tributaria),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_cofins=values(aliquota_cofins),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_cofins_st=values(aliquota_cofins_st),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_imposto_servico=values(aliquota_imposto_servico),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'indicador_iss=values(indicador_iss),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'indicador_incentivo_fiscal=values(indicador_incentivo_fiscal),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_aliquota_combate_probreza=values(percentual_aliquota_combate_probreza),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_aliquota_produto_combate_probreza=values(percentual_aliquota_produto_combate_probreza),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_interestadual_envolvidas=values(aliquota_interestadual_envolvidas),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'percentual_icms_interestadual=values(percentual_icms_interestadual),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cfop_de_entrada=values(cfop_de_entrada),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'cfop_de_saida=values(cfop_de_saida),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'observacao_fiscal=values(observacao_fiscal),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aplicacao=values(aplicacao),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_fcp=values(aliquota_fcp),');
                  SET @insertCestaTributacao = concat(@insertCestaTributacao, 'aliquota_fcp_st=values(aliquota_fcp_st);');
                  PREPARE stmt FROM @insertCestaTributacao;
                  EXECUTE stmt;
                  DEALLOCATE PREPARE stmt;
                END;
                CALL inserir_cesta_tributacao();
                DROP PROCEDURE IF EXISTS inserir_cesta_tributacao;

                INSERT INTO `configuracao_documentos_fiscais`
                VALUES (1, VALOR_CODIGO_DOCLIENTE, 55, '0', '1', CURRENT_DATE(), 1, NULL, NULL, 'NFE', VALOR_EMAIL_DOCLIENTE),
                       (2, VALOR_CODIGO_DOCLIENTE, 65, '0', '1', CURRENT_DATE(), 1, '', '', 'NFCE', VALOR_EMAIL_DOCLIENTE);

                INSERT INTO `conta_bancaria` VALUES (1,VALOR_CODIGO_DOCLIENTE,NULL,NULL,NULL,'CAIXA PRINCIPAL',NULL,'',NULL,1,0);

                INSERT INTO `tipo_pagamento` VALUES (1,'DINHEIRO',0),(2,'CHEQUE',1),(3,'CARTÃO CRÉDITO',1),(4,'CARTÃO DÉBITO',0),(15,'BOLETO',1)
                            ,(17,'PIX',0),(18,'TRANSFERENCIA',0),(19,'VALE',0),(99,'CORRENTISTA',0)
                            ,(5, 'TEF CRÉDITO', 1)
                            ,(6, 'TEF DÉBITO', 0);

                INSERT INTO `forma_pagamento`
                  (`codigo`, `codigo_empresa`, `tipo_pagamento`, `conta_bancaria`, `descricao`, `desconto`, `acrescimo`, `taxa`, `valor_minimo`, `prazo_recebimento`, `pdv`, `administradora_cartao`, `data_cadastro`, `data_atualizacao`, `quantidade_parcelas`, `status`, `padrao`, `dias_liberacao_comissao`, `parcela_inicial_para_juros`, `gera_financeiro`, `baixa_automatica`)
                VALUES
                 (12, VALOR_CODIGO_DOCLIENTE, 1, 1, 'DINHEIRO', 20.00, 0.00, 0.00, 0.00, NULL, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
                ,(15, VALOR_CODIGO_DOCLIENTE, 17, 1, 'PIX', 10.00, NULL, NULL, NULL, NULL, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
                ,(16, VALOR_CODIGO_DOCLIENTE, 99, 1, 'CORRENTISTA', NULL, NULL, NULL, NULL, 30, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
                ,(17, VALOR_CODIGO_DOCLIENTE, 19, 1, 'VALE', NULL, NULL, NULL, NULL, NULL, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 0)
                ,(18, VALOR_CODIGO_DOCLIENTE, 3, 1, 'CARTÃO CRÉDITO', 0.00, 0.00, 0.00, NULL, 30, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 6, 1, 0, 0, 0, 0, 1)
                ,(19, VALOR_CODIGO_DOCLIENTE, 4, 1, 'CARTÃO DE DÉBITO', 0.00, 0.00, 0.00, 0.00, 0, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 1)
                ,(22, VALOR_CODIGO_DOCLIENTE, 15, 1, 'BOLETO', 0.00, 0.00, 0.00, NULL, 0, 1, NULL, CURRENT_DATE(), CURRENT_TIMESTAMP(), 12, 1, 0, 0, 0, 1, 1)
                ,(32, VALOR_CODIGO_DOCLIENTE, 17, 1, 'PIX MANUAL', 0.00, 0.00, 0.00, 0.00, 0, 1, NULL, CURRENT_DATE(), NULL, 0, 1, 0, 0, 0, 1, 1)
                ,(33, VALOR_CODIGO_DOCLIENTE, 5, 1, 'TEF CRÉDITO', 0.00, 0.00, 0.00, 0.00, 0, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 1, 1, 0, 0, 0, 0, 1)
                ,(34, VALOR_CODIGO_DOCLIENTE, 6, 1, 'TEF DÉBITO', 0.00, 0.00, 0.00, 0.00, 0, 1, '01027058000191', CURRENT_DATE(), CURRENT_TIMESTAMP(), 0, 1, 0, 0, 0, 0, 1);

                INSERT INTO `local_estoque` VALUES (1,VALOR_CODIGO_DOCLIENTE,'ESTOQUE PRINCIPAL',1,1);

                INSERT INTO `marca` VALUES (1,'OUTRAS');

                INSERT INTO `metas_vendedores` VALUES (VALOR_CODIGO_DOCLIENTE,VALOR_EMAIL_DOCLIENTE,0.00,0.00,CURRENT_TIMESTAMP(),VALOR_EMAIL_DOCLIENTE,'CIF');

                INSERT INTO `perfil_usuario`
                VALUES ('administrador',1,VALOR_EMAIL_DOCLIENTE,CURRENT_DATE(),CURRENT_DATE(),VALOR_EMAIL_DOCLIENTE);

                INSERT INTO `permissao` VALUES VALOR_PERMISSOES;

                INSERT INTO `plano_contas_categoria` VALUES
                (1,'DESPESAS ADMINISTRATIVAS','D',CURRENT_TIMESTAMP(),1),
                (2,'DESPESAS GERAIS','D',CURRENT_TIMESTAMP(),1),
                (3,'DESPESAS COM VENDAS','D',CURRENT_TIMESTAMP(),1),
                (4,'DESPESAS FINANCEIRAS','D',CURRENT_TIMESTAMP(),1),
                (5,'DESPESAS COM PESSOAL','D',CURRENT_TIMESTAMP(),1),
                (6,'DESPESAS NÃO OPERACIONAIS','D',CURRENT_TIMESTAMP(),1),
                (7,'RECEITA OPERACIONAL BRUTA','R',CURRENT_TIMESTAMP(),1),
                (8,'IMPOSTOS DE VENDA','D',CURRENT_TIMESTAMP(),1),
                (10,'OUTRAS SAÍDAS','D',CURRENT_TIMESTAMP(),1),
                (11,'DESPESAS COM COMPRAS','D',CURRENT_TIMESTAMP(),1);

                INSERT INTO `plano_contas` VALUES 
                     (18,'OUTRAS OPERAÇÕES',10,CURRENT_TIMESTAMP())
                    ,(20,'PAGAMENTO DE FORNECEDOR',11,CURRENT_TIMESTAMP())
                    ,(21,'RECEBIMENTO DE CLIENTES',11,CURRENT_TIMESTAMP())
                    ,(23,'VENDAS DO PDV',7,CURRENT_TIMESTAMP())
                    ,(24, 'VENDAS DE NOTA FISCAIS', 7, CURRENT_TIMESTAMP());

                INSERT INTO `seguencia_nota_fiscal` VALUES (1,VALOR_CODIGO_DOCLIENTE,'1',VALOR_EMAIL_DOCLIENTE,0,NULL,NULL,'P');

                INSERT INTO `tabela_preco_perfil` VALUES (20,'administrador');

                INSERT INTO `tipo_movimentacao` VALUES
                (3,'SAIDA','AJUSTE DE QUANTIDADE DE SAÍDA',1,0,'VENDA',CURRENT_DATE(),CURRENT_TIMESTAMP(),1),
                (4,'ENTRADA','DEVOLUCÃO DE CLIENTE BALCÃO',1,1,'VENDA',CURRENT_DATE(),CURRENT_TIMESTAMP(),1),
                (5,'ENTRADA','AJUSTE DE QUANTIDADE COM CUSTO',1,1,'CUSTO',CURRENT_DATE(),CURRENT_TIMESTAMP(),1),
                (6,'ENTRADA','AJUSTE DE QUANTIDADE DE ENTRADA',1,0,'VENDA',CURRENT_DATE(),NULL,1),
                (7,'ENTRADA','CANCELAMENTO DE SAIDA',1,0,'VENDA',CURRENT_DATE(),NULL,1),
                (8,'SAIDA','CANCELAMENTO DE ENTRADA',1,1,'VENDA',CURRENT_DATE(),NULL,1),
                (9,'ENTRADA','ENTRADA DE NOTA FISCAL',1,1,'CUSTO',CURRENT_DATE(),CURRENT_TIMESTAMP(),1),
                (10,'ENTRADA','COMPRA DE CLIENTE',1,1,'VENDA',CURRENT_DATE(),NULL,0),
                (11,'SAIDA','SAIDA DE NOTA FISCAL',1,1,'VENDA',CURRENT_DATE(),NULL,1),
                (12,'SAIDA','SAIDA DE VENDA',1,1,'VENDA',CURRENT_DATE(),NULL,1),
                (13,'ENTRADA','DEVOLUÇÃO DE VENDA',1,1,'VENDA',CURRENT_DATE(),NULL,0),
                (14,'TRANSFERENCIA','TRANSFERÊNCIA DE ESTOQUE',1,0,'CUSTO',CURRENT_DATE(),NULL,0);


                INSERT INTO `usuario` VALUES (1,VALOR_EMAIL_DOCLIENTE,'$2a$10$JCVPWzrjE3Lwn6orhK.oGOCRThyI39vsLrb.9/rcvaTCHD2jqPwgG','ADMINISTRADOR',NULL,CURRENT_DATE(),CURRENT_TIMESTAMP(),1,NULL,VALOR_IDENTIFICADOR_USUARIO_CLIENTE,0,NULL,VALOR_EMAIL_DOCLIENTE,0,1);

                INSERT INTO `usuario_empresa` VALUES (VALOR_EMAIL_DOCLIENTE,VALOR_CODIGO_DOCLIENTE,'administrador',1,0.0,0,1,CURRENT_TIMESTAMP(),VALOR_EMAIL_DOCLIENTE);

                INSERT INTO `usuario` VALUES (2,'pdv.autenticacao@zion.com',VALOR_SENHA_PDV,'PDV AUTENTICACAO',NULL,CURRENT_DATE(),CURRENT_TIMESTAMP(),1,NULL,'pdv.autenticacao@zion.com',0,NULL,VALOR_EMAIL_DOCLIENTE,1,0);
                INSERT INTO `usuario_empresa` VALUES ('pdv.autenticacao@zion.com',VALOR_CODIGO_DOCLIENTE,'administrador',1,0.0,0,1,CURRENT_TIMESTAMP(),VALOR_EMAIL_DOCLIENTE);
                
                INSERT INTO `usuario` VALUES (3,'loja@zoomtecnologia.com','$2y$10$K0oydkEwp9iiW.46g16rsed6EHnFUKtawp955hPVi0tlfxuie995q','LOJA',NULL,CURRENT_DATE(),CURRENT_TIMESTAMP(),1,NULL,'loja@zoomtecnologia.com',0,NULL,VALOR_EMAIL_DOCLIENTE,0,0);
                INSERT INTO `usuario_empresa` VALUES ('loja@zoomtecnologia.com',VALOR_CODIGO_DOCLIENTE,'administrador',1,0.1,0,1,CURRENT_TIMESTAMP(),VALOR_EMAIL_DOCLIENTE);
                
                INSERT INTO `usuario` VALUES (4,'suporte@zoomtecnologia.com','$2y$10$y/jUkzVpUyXIND5tQsMt6e8mr7Z3og6AWomI4ikIWeUlywbj.qsOy','Suporte',NULL,CURRENT_DATE(),CURRENT_TIMESTAMP(),1,NULL,'suporte@zoomtecnologia.com',0,NULL,VALOR_EMAIL_DOCLIENTE,1,0);
                INSERT INTO `usuario_empresa` VALUES ('suporte@zoomtecnologia.com',VALOR_CODIGO_DOCLIENTE,'administrador',1,0.1,0,1,CURRENT_TIMESTAMP(),VALOR_EMAIL_DOCLIENTE);

                INSERT INTO `pessoa` (`documento_identificacao`, `razao_social`, `nome_fantasia`, `tipo_pessoa`, `inscricao_estadual_rg`, `inscricao_municipal`, `data_nascimento_fundacao`, `estado_civil`, `tipo_sexo`, `regime_tributario`, `cnae`, `suframa`, `inscricao_estadual_st`, `funcionario`, `fornecedor`, `cliente`, `transportadora`, `produtor`, `fabricante`, `limite_credito`, `valor_pendente_conta_receber`, `valor_pendente_conta_pagar`, `correntista`, `observacao`, `status`, `data_ultima_atualizacao`, `usuario_ultima_atualizacao`, `indicador_ie`, `imagem`, `tabela_preco`, `data_cadastro`, `usuario_cadastro`, `membro`, `visitante`, `nacionalidade`, `representante`, `dia_vencimento`, `juros`, `dia_fechamento_fatura`, `bloquear_compras_apos_vencimento`, `total_compras`, `total_pago`, `identificador_imagem`) VALUES ('00000000000', 'CONSUMIDOR', NULL, 'F', NULL, NULL, NULL, 'O', 'O', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, 0, 0.00, 0.00, 0.00, 0, NULL, 1, '2024-04-16 00:00:00', 'loaj@zoomtecnologia.com', '1', NULL, 20, '2024-04-16 16:35:53', 'loaj@zoomtecnologia.com', 0, 0, 'Brasil', 0, 30, 0.00, 30, 0, 0.00, 0.00, NULL);
                VALOR_PDVS 