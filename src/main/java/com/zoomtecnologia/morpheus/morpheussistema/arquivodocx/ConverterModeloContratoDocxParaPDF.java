package com.zoomtecnologia.morpheus.morpheussistema.arquivodocx;

import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class ConverterModeloContratoDocxParaPDF {

    public byte[] converter(Consumer<XWPFRun> consumer) throws FileNotFoundException, IOException, InvalidFormatException {
        byte[] pdf;
        Path modeloContrato = Paths.get("modelo-contrato/NOVO CONTRATO.docx");
        InputStream fileInputStream = Files.newInputStream(modeloContrato);
        XWPFDocument template = new XWPFDocument(OPCPackage.open(fileInputStream));
        template.getParagraphs().forEach(paragrafo -> paragrafo.getRuns().forEach(consumer));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        template.write(byteArrayOutputStream);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ByteArrayOutputStream outputFile = new ByteArrayOutputStream();
        XWPFDocument document = new XWPFDocument(byteArrayInputStream);
        PdfOptions options = null;
        PdfConverter.getInstance().convert(document, outputFile, options);
        pdf = outputFile.toByteArray();
        template.close();
        document.close();
        byteArrayInputStream.close();
        byteArrayOutputStream.close();
        fileInputStream.close();
        return pdf;
    }

}
