/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.bancodedados;


import java.io.IOException;

public class ConectaBancoMysql extends ConectaBanco {
    private static ConectaBancoMysql conectaBancoMysql = null;

    public static ConectaBancoMysql getConectaBancoMysql() {
        conectaBancoMysql = new ConectaBancoMysql();
        return conectaBancoMysql;
    }

    public static void setConectaBancoMysql(ConectaBancoMysql conectaBancoMysql) {
        ConectaBancoMysql.conectaBancoMysql = conectaBancoMysql;
    }

    @Override
    public String criarCaminhoParaComunicaoComOBanco() throws IOException {
        this.setUsuario("zoomtecn_adm");
        this.setSenha("noiz@#$1030");
        String url = "jdbc:mysql://195.35.17.210:3306";
        return String.format("%s?useSSL=false&useUnicode=true&amp&characterEncoding=UTF-8&allowMultiQueries=true&allowPublicKeyRetrieval=true&connectTimeout=5000", url);
    }   
}
