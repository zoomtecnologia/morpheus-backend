package com.zoomtecnologia.morpheus.morpheussistema.bancodedados;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDVListaModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionAplicacoesContratada;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.Getter;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

public class GeraBancoDeDados {

    private final ConectaBanco conectaBanco;
    private final ResourceLoader resourceLoader;
    private final Cliente cliente;
    private final ZionPlanoContratado zionPlanoContratado;
    private final List<ZionAplicacoesContratada> aplicacoesContratadas;
    private final List<PDVListaModeloAPI> pdvs;

    public GeraBancoDeDados(
            ResourceLoader resourceLoader,
            Cliente cliente,
            ZionPlanoContratado zionPlanoContratado,
            List<ZionAplicacoesContratada> aplicacoesContratadas,
            List<PDVListaModeloAPI> pdvs) {
        this.conectaBanco = new ConectaBancoMysql();
        this.resourceLoader = resourceLoader;
        this.cliente = cliente;
        this.zionPlanoContratado = zionPlanoContratado;
        this.aplicacoesContratadas = aplicacoesContratadas;
        this.pdvs = pdvs;
    }

    public void gerarBanco() throws IOException, ClassNotFoundException, SQLException {
        this.conectaBanco.conectar();
        this.conectaBanco.executarSQL("SHOW DATABASES LIKE 'zoomtecn_zion_" + this.cliente.getCodigoGrupoEmpresa().getIdentificacaoBancoDados() + "'");
        final boolean next = this.conectaBanco.getResultSet().next();
        if (next) {
            final String nomeBancoDeDados = this.conectaBanco.getResultSet().getString(1);
            final boolean jaExisteBancoDeDados = nomeBancoDeDados != null && !nomeBancoDeDados.trim().isEmpty();
            if (jaExisteBancoDeDados) {
                this.adicionarNovaEmpresa();
                return;
            }
        }
        this.criarBancoDeDados();
    }

    private void criarBancoDeDados() throws IOException, SQLException {
        final Resource resource = this.resourceLoader.getResource("classpath:create_banco_dados.sql");
        try (InputStream createBancoDeDados = resource.getInputStream()) {
            final InputStreamReader inputStreamReader = new InputStreamReader(createBancoDeDados, StandardCharsets.UTF_8);
            final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            final String conteudoDoArquivo = bufferedReader.lines().collect(Collectors.joining());
            final String permissoes = this.criarSQLPermissoes();
            final String pdvsSQL = this.criarSQLPdvs();
            final SubstituicaoConteudo substituicaoConteudo = new SubstituicaoConteudo();
            substituicaoConteudo.setCliente(this.cliente);
            substituicaoConteudo.setPermissoes(permissoes);
            substituicaoConteudo.setConteudoDoArquivo(conteudoDoArquivo);
            substituicaoConteudo.setPdvs(pdvsSQL);
            substituicaoConteudo.setZionPlanoContratado(this.zionPlanoContratado);
            final String sql = this.substituirConteudo(substituicaoConteudo);
            this.executarSQL(sql);
        }
    }

    private void adicionarNovaEmpresa() throws SQLException, IOException {
        final String nomeBanco = "`zoomtecn_zion_" + this.cliente.getCodigoGrupoEmpresa().getIdentificacaoBancoDados() + "`";
        try (PreparedStatement prepareStatement = this.conectaBanco.getConnection().prepareStatement("use " + nomeBanco)) {
            prepareStatement.executeUpdate();
            this.conectaBanco.getConnection().commit();
            this.adicionarEmpresa();
            this.zionPlanoContratado.setEmpresaNova(true);
        } finally {
            this.conectaBanco.desconectar();
        }
    }

    private String substituirConteudo(SubstituicaoConteudo substituicaoConteudo) {
        final String emailNomeGrupoEmpresa = substituicaoConteudo.getCliente().getEmail() + ":"
                + substituicaoConteudo.getCliente().getCodigoGrupoEmpresa().getNome();
        final String identificador = Base64.getEncoder()
                .encodeToString(emailNomeGrupoEmpresa.getBytes(Charset.defaultCharset()));
        return substituicaoConteudo.conteudoDoArquivo.replaceAll("NOME_BANCO_DADOS", substituicaoConteudo.cliente.getCodigoGrupoEmpresa().getIdentificacaoBancoDados())
                .replaceAll("VALOR_CODIGO_DOCLIENTE", this.getCampo(substituicaoConteudo.zionPlanoContratado.getCodigoEmpresa()))
                .replaceAll("VALOR_CNPJ_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getCnpj()))
                .replaceAll("VALOR_RAZAO_SOCIAL_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getRazaoSocial()))
                .replaceAll("VALOR_NOME_FANTASIA_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getNomeFantasia()))
                .replaceAll("VALOR_INSCRICAO_ESTADUAL_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getInscricaoEstadual()))
                .replaceAll("VALOR_INSCRICAO_MUNICIPAL_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getInscricaoMunicipal()))
                .replaceAll("VALOR_TELEFONE_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getTelefone()))
                .replaceAll("VALOR_LOGRADOURO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getLogradouro()))
                .replaceAll("VALOR_NUMERO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getNumero()))
                .replaceAll("VALOR_BAIRRO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getBairro()))
                .replaceAll("VALOR_CODIGO_REGIME_TRIBUTARIO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getCodigoRegimeTributario()))
                .replaceAll("VALOR_CODIGO_MUNICIPIO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getCodigoMunicipio()))
                .replaceAll("VALOR_COMPLEMENTO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getComplemento()))
                .replaceAll("VALOR_CEP_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getCep()))
                .replaceAll("VALOR_CNAE_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getCnae()))
                .replaceAll("VALOR_GRUPO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getCodigoGrupoEmpresa().getCodigo()))
                .replaceAll("VALOR_CERTIFICADO_DIGITAL_DOCLIENTE", this.getCampo(""))
                .replaceAll("VALOR_SENHA_CERTIFICADO_DOCLIENTE", this.getCampo(""))
                .replaceAll("VALOR_MATRIZ_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.isMatriz()))
                .replaceAll("VALOR_APELIDO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getApelido()))
                .replaceAll("VALOR_MUNICIPIO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getCidade()))
                .replaceAll("VALOR_ESTADO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getNomeEstado()))
                .replaceAll("VALOR_UF_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getEstado()))
                .replaceAll("VALOR_CODIGO_ESTADO_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEndereco().getCodigoEstado()))
                .replaceAll("VALOR_EMAIL_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getEmail()))
                .replaceAll("VALOR_INIDICADOR_INSCRICAO_ESTADUAL_DOCLIENTE", this.getCampo(substituicaoConteudo.cliente.getIndicadorInscricaoEstadual()))
                .replaceAll("VALOR_CODIGO_PLANO_CONTRATADO_DOCLIENTE", this.getCampo(substituicaoConteudo.zionPlanoContratado.getCodigo()))
                .replaceAll("VALOR_PERMISSOES", substituicaoConteudo.getPermissoes())
                .replaceAll("VALOR_IDENTIFICADOR_USUARIO_CLIENTE", this.getCampo(identificador))
                .replaceAll("VALOR_CODIGO_CESTA_TRIBUTACAO_CLIENTE", this.getCampo(substituicaoConteudo.getCliente().getCodigoCestaTributacao()))
                .replaceAll("VALOR_PDVS", substituicaoConteudo.getPdvs())
                .replaceAll("VALOR_CODIGO_DO_EMPRESA_PRINCIPAL", substituicaoConteudo.getCodigoEmpresaPrincipal())
                .replace("VALOR_SENHA_PDV", this.getCampo(substituicaoConteudo.cliente.getSenhaPDV()))
                .replaceAll("VALOR_REGIME_TRIBUTARIO", this.getCampo(substituicaoConteudo.cliente.getCodigoRegimeTributario()));
    }

    private String getCampo(Object obj) {
        if (obj == null) {
            return "null";
        }
        if (obj instanceof Integer) {
            return obj.toString();
        }

        if (obj instanceof String) {
            return "'" + obj + "'";
        }

        if (obj instanceof Number) {
            return obj.toString();
        }

        if (obj instanceof Boolean) {
            return Boolean.parseBoolean(obj.toString()) ? "1" : "0";
        }
        return "'" + obj + "'";
    }

    private String criarSQLPermissoes() {
        return this.aplicacoesContratadas.stream().map(aplicacao -> {
            return "("
                    + "'" + UUID.randomUUID().toString() + "',"
                    + "'administrador',"
                    + "'" + this.zionPlanoContratado.getCodigoEmpresa() + "',"
                    + "'" + aplicacao.getCodigo() + "',"
                    + "'" + aplicacao.getModulo() + "',"
                    + "'" + aplicacao.getNome() + "',"
                    + "'" + aplicacao.getFuncao() + "',"
                    + "1"
                    + ")";
        }).collect(Collectors.joining(","));
    }

    private void adicionarEmpresa() throws IOException, SQLException {
        final String consultarUltimaEmpresa = "select codigo_empresa from empresa order by codigo_empresa limit 1";
        try (PreparedStatement preparedStatement = this.conectaBanco.getConnection().prepareStatement(consultarUltimaEmpresa)) {
            final ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            final String codigoEmpresaPrincipal = resultSet.getString("codigo_empresa");
            final Resource resource = this.resourceLoader.getResource("classpath:insert_dados_empresa.sql");
            try (InputStream createBancoDeDados = resource.getInputStream()) {
                final InputStreamReader inputStreamReader = new InputStreamReader(createBancoDeDados, StandardCharsets.UTF_8);
                final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                final String conteudoDoArquivo = bufferedReader.lines().collect(Collectors.joining());
                final String permissoes = this.criarSQLPermissoes();
                final String pdvsSQL = this.criarSQLPdvs();
                final SubstituicaoConteudo substituicaoConteudo = new SubstituicaoConteudo();
                substituicaoConteudo.setCliente(this.cliente);
                substituicaoConteudo.setPermissoes(permissoes);
                substituicaoConteudo.setPdvs(pdvsSQL);
                substituicaoConteudo.setConteudoDoArquivo(conteudoDoArquivo);
                substituicaoConteudo.setZionPlanoContratado(this.zionPlanoContratado);
                substituicaoConteudo.setCodigoEmpresaPrincipal(codigoEmpresaPrincipal);
                final String sql = this.substituirConteudo(substituicaoConteudo);
                this.executarSQL(sql);
            }
        }
    }

    private String criarSQLPdvs() {
        final String values = this.pdvs.stream().map(pdv -> {
            return "("
                    + "'" + this.zionPlanoContratado.getCodigoEmpresa() + "',"
                    + pdv.getNumeroPdv() + ","
                    + "'" + pdv.getChave() + "',"
                    + "null,"
                    + "'NFCE',"
                    + "30,"
                    + "'principal',"
                    + "1, "
                    + " 0, "
                    + " 0, "
                    + " 0, "
                    + " 0, "
                    + " 0, "
                    + " 0, "
                    + " 0.00, "
                    + "'ELGIN',"
                    + "5000.00, "
                    + "0.00, "
                    + "'COM1', "
                    + "'9600', "
                    + "'NENHUM', "
                    + "8, "
                    + "2, "
                    + "2, "
                    + "2, "
                    + "2, "
                    + "'VALOR',"
                    + "2,"
                    + "2,"
                    + "'discado'"
                    + ")";
        }).collect(Collectors.joining(","));
        String sql = "INSERT INTO `pdvs`"
                + " (`codigo_empresa`, `numero`, `chave`, `documento_fiscal`, `tipo_emissao`, `tempo_sincronizacao`, `tema_principal`, `controla_caixa`, `identifica_consumiddo`, `identifica_vendedor`, `usa_catraca`, `usa_comanda`, `imagem_produto_pesquisa`, `salva_ultima_pesquisa`, `valor_para_aviso_sangria`, `marca_impressora`, `valor_identifica_consumidor`, `valor_maximo_quebra_caixa`, `balanca_porta_serial`, `velocidade`, `paridade`, `bits_dados`, `bits_parada`, `etiqueta_digito_verificador`, `etiqueta_digito_inicial_codigo`, `etiqueta_digito_final_codigo`, `etiqueta_tipo`, `etiqueta_casas_decimais`, `tef_numero_vias`, `tef_tipo`) "
                + "VALUES "
                + values
                + " ON DUPLICATE KEY UPDATE "
                + "`codigo_empresa` = VALUES(`codigo_empresa`);";
        return this.pdvs.isEmpty() ? "" : sql;
    }

    private void executarSQL(final String sql) throws SQLException {
        try (PreparedStatement prepareStatement = this.conectaBanco.getConnection().prepareStatement(sql)) {
            prepareStatement.executeUpdate();
            this.conectaBanco.getConnection().commit();
        } finally {
            this.conectaBanco.desconectar();
        }
    }

    @Getter
    private static final class SubstituicaoConteudo {

        private Cliente cliente;
        private ZionPlanoContratado zionPlanoContratado;
        private String conteudoDoArquivo;
        private String permissoes;
        private String pdvs;
        private String codigoEmpresaPrincipal;

        public void setCliente(Cliente cliente) {
            this.cliente = cliente;
        }

        public void setZionPlanoContratado(ZionPlanoContratado zionPlanoContratado) {
            this.zionPlanoContratado = zionPlanoContratado;
        }

        public void setConteudoDoArquivo(String conteudoDoArquivo) {
            this.conteudoDoArquivo = conteudoDoArquivo;
        }

        public void setPermissoes(String permissoes) {
            this.permissoes = permissoes;
        }

        @Override
        public String toString() {
            return "SubstituicaoConteudo{" + "cliente=" + cliente + ", zionPlanoContratado=" + zionPlanoContratado + ", conteudoDoArquivo=" + conteudoDoArquivo + ", aplicacaoes=" + permissoes + '}';
        }

        private void setPdvs(String pdvs) {
            this.pdvs = pdvs;
        }

        public void setCodigoEmpresaPrincipal(String codigoEmpresaPrincipal) {
            this.codigoEmpresaPrincipal = codigoEmpresaPrincipal;
        }
    }
}
