package com.zoomtecnologia.morpheus.morpheussistema.bancodedados;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public abstract class ConectaBanco {

    private Connection connection;
    private ResultSet resultSet;
    private PreparedStatement statement;
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private String usuario = "";
    private String senha = "";

    public void conectar() throws ClassNotFoundException, SQLException, IOException {
        Class.forName(DRIVER);
        String path = criarCaminhoParaComunicaoComOBanco();
        DriverManager.setLoginTimeout(5);
        this.connection = DriverManager.getConnection(path, this.usuario, this.senha);
        this.connection.setAutoCommit(false);
    }

    public abstract String criarCaminhoParaComunicaoComOBanco() throws IOException;

    public void desconectar() throws SQLException {
        if (this.connection == null) {
            return;
        }
        if (this.statement != null) {
            this.statement.close();
            this.statement = null;
        }
        if (this.resultSet != null) {
            this.resultSet.close();
            this.resultSet = null;
        }
        this.connection.close();
        this.connection = null;
    }

    public void executarSQL(String sql) throws SQLException {
        if (connection == null) {
            return;
        }
        this.statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        this.resultSet = this.statement.executeQuery();
    }

    public Connection getConnection() {
        return connection;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public Statement getStatement() {
        return statement;
    }

    public String getDRIVER() {
        return DRIVER;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
