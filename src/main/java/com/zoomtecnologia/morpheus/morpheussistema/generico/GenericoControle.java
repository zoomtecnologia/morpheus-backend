package com.zoomtecnologia.morpheus.morpheussistema.generico;

import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin
public abstract class GenericoControle<T, K> {

    @Autowired
    private GenericoServico<T,K> genericoServico;

    @GetMapping
    public List<T> listarTodos(@RequestParam(value = "pesquisa", required = false, defaultValue = "") String pesquisa) {
        return genericoServico.listarTodos(pesquisa);
    }

    @GetMapping("{codigo}")
    public ResponseEntity<T> buscarPorChave(@PathVariable("codigo") K key) {
        T entidade = this.genericoServico.buscarPorCodigo(key);
        return ResponseEntity.ok(entidade);
    }

    @PostMapping
    public ResponseEntity<T> salvar(@RequestBody @Valid T entidade,
            UriComponentsBuilder uriComponentsBuilder) {
        T entidadeSalva = this.genericoServico.salvar(entidade);
        Map<String, Object> parametros = this.getParametros(entidade);
        URI uri = uriComponentsBuilder.path(this.getURI()).buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(entidadeSalva);
    }

    @PutMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterar(@RequestBody @Valid T entidade, @PathVariable("codigo") K key) {
        this.genericoServico.alterar(entidade, key);
    }

    @DeleteMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable("codigo") K key) {
        this.genericoServico.deletar(key);
    }
    
    public abstract Map<String, Object> getParametros(T entidade);

    public abstract String getURI();
}
