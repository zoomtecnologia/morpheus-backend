package com.zoomtecnologia.morpheus.morpheussistema.generico;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericoServico<T, K> {

    public List<T> listarTodos(String pesquisa);

    public Page<T> listarTodosPaginado(String pesquisa, Pageable pageable);

    public T buscarPorCodigo(K key);

    public T salvar(T entidade);

    public void alterar(T entidade, K key);

    public void deletar(K key);
}
