package com.zoomtecnologia.morpheus.morpheussistema.exception;

public class RespostaErroServidor {

    private String titulo;
    private String mensagemDesenvolvedor;
    private String status;

    public RespostaErroServidor(String titulo, String mensagemDesenvolvedor) {
        this.titulo = titulo;
        this.mensagemDesenvolvedor = mensagemDesenvolvedor;
    }

    public RespostaErroServidor() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getMensagemDesenvolvedor() {
        return mensagemDesenvolvedor;
    }

    public void setMensagemDesenvolvedor(String mensagemDesenvolvedor) {
        this.mensagemDesenvolvedor = mensagemDesenvolvedor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RespostaErroServidor{" + "titulo=" + titulo + ", mensagemDesenvolvedor=" + mensagemDesenvolvedor + ", status=" + status + '}';
    }

    
}
