package com.zoomtecnologia.morpheus.morpheussistema.exception;

public class RecursoNaoEncontradoException extends NegocioException{

    public RecursoNaoEncontradoException(String message, String mensagemDesenvolvedor) {
        super(message, mensagemDesenvolvedor);
    }

}
