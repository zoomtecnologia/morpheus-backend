package com.zoomtecnologia.morpheus.morpheussistema.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class MorpheusException extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        String mensagem = this.messageSource.getMessage("campo.desconhecido", null, LocaleContextHolder.getLocale());
        String mensagemDesevolvedor = ex.getCause().toString();
        return handleExceptionInternal(ex, Arrays.asList(new RespostaErroServidor(mensagem, mensagemDesevolvedor)), headers,
                HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex,
            WebRequest request) {
        String causaDaExecao = ExceptionUtils.getRootCauseMessage(ex);
        String mensagem = this.messageSource.getMessage("recurso.erro-integridade-dadados", null,
                LocaleContextHolder.getLocale());
        String mensagemDesevolvedor = causaDaExecao;
        if (causaDaExecao.contains("SQLIntegrityConstraintViolationException")) {
            mensagem = this.messageSource.getMessage("recurso.erro-integridade-nos-dados", null,
                    LocaleContextHolder.getLocale());
        }
        return handleExceptionInternal(ex, Arrays.asList(new RespostaErroServidor(mensagem, mensagemDesevolvedor)),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<Object> handleDataIntegrityViolationException(EntityNotFoundException ex,
            WebRequest request) {
        HttpStatus BAD_REQUEST = HttpStatus.BAD_REQUEST;
        String mensagem = this.messageSource.getMessage("recurso.principal-nao-cadastrado", null,
                LocaleContextHolder.getLocale());
        String mensagemDesevolvedor = ExceptionUtils.getRootCauseMessage(ex);
        RespostaErroServidor serverResponseError = new RespostaErroServidor(mensagem, mensagemDesevolvedor);
        serverResponseError.setStatus(String.valueOf(BAD_REQUEST.value()));
        return handleExceptionInternal(ex, Arrays.asList(), new HttpHeaders(), BAD_REQUEST, request);
    }

    @ExceptionHandler({EmptyResultDataAccessException.class})
    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex,
            WebRequest request) {
        HttpStatus NOT_FOUND = HttpStatus.NOT_FOUND;
        String mensagem = "Recurso "
                + this.messageSource.getMessage("recurso.nao-cadastrado", null, LocaleContextHolder.getLocale());
        String mensagemDesevolvedor = this.messageSource.getMessage("recurso.nao-cadastrado-deletar", null,
                LocaleContextHolder.getLocale());
        RespostaErroServidor serverResponseError = new RespostaErroServidor(mensagem, mensagemDesevolvedor);
        serverResponseError.setStatus(String.valueOf(NOT_FOUND.value()));
        return handleExceptionInternal(ex, Arrays.asList(serverResponseError), new HttpHeaders(), NOT_FOUND, request);
    }

    @ExceptionHandler({RecursoJaExisteException.class})
    public ResponseEntity<Object> handleRecursoJaExisteException(RecursoJaExisteException ex, WebRequest webRequest) {
        return preencherServerResponse(ex, HttpStatus.CONFLICT, webRequest);
    }

    @ExceptionHandler({NegocioException.class})
    public ResponseEntity<Object> handleNegocioException(NegocioException ex, WebRequest webRequest) {
        return preencherServerResponse(ex, HttpStatus.BAD_REQUEST, webRequest);
    }

    @ExceptionHandler({RecursoNaoEncontradoException.class})
    public ResponseEntity<Object> handleRecursoNaoEncontradoException(RecursoNaoEncontradoException ex, WebRequest webRequest) {
        return preencherServerResponse(ex, HttpStatus.NOT_FOUND, webRequest);
    }

    @ExceptionHandler({CannotCreateTransactionException.class})
    public ResponseEntity<Object> method(CannotCreateTransactionException cannotCreateTransactionException, WebRequest webRequest) {
        final HttpStatus INTERNAL_SERVER_ERROR = HttpStatus.INTERNAL_SERVER_ERROR;
        final String mensagem = this.messageSource.getMessage("erro-interno.bancodados", null, LocaleContextHolder.getLocale());
        final String mensagemDesevolvedor = this.messageSource.getMessage("erro-interno.bancodados-detalhes", null, LocaleContextHolder.getLocale());
        final RespostaErroServidor serverResponseError = new RespostaErroServidor(mensagem, mensagemDesevolvedor);
        serverResponseError.setStatus(String.valueOf(INTERNAL_SERVER_ERROR.value()));
        return handleExceptionInternal(cannotCreateTransactionException, serverResponseError, new HttpHeaders(), INTERNAL_SERVER_ERROR, webRequest);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<RespostaErroServidor> erros = this.criarListaDeErros(ex.getBindingResult());
        return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
    }

    private List<RespostaErroServidor> criarListaDeErros(BindingResult bindingResult) {
        List<RespostaErroServidor> erros = new ArrayList<>();
        String mensagem;
        String mensagemDesevolvedor;
        String statusHttp = String.valueOf(HttpStatus.BAD_REQUEST.value());
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            mensagem = this.messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
            mensagemDesevolvedor = fieldError.toString();
            RespostaErroServidor respostaErroServidor = new RespostaErroServidor(mensagem, mensagemDesevolvedor);
            respostaErroServidor.setStatus(statusHttp);
            erros.add(respostaErroServidor);
        }
        return erros;
    }

    private ResponseEntity<Object> preencherServerResponse(NegocioException ex, HttpStatus status,
            WebRequest webRequest) {
        RespostaErroServidor respostaErroServidor = new RespostaErroServidor();
        respostaErroServidor.setTitulo(ex.getMessage());
        respostaErroServidor.setMensagemDesenvolvedor(ex.getMensagemDesenvolvedor());
        respostaErroServidor.setStatus(String.valueOf(status.value()));
        return handleExceptionInternal(ex, respostaErroServidor, new HttpHeaders(), status, webRequest);
    }
}
