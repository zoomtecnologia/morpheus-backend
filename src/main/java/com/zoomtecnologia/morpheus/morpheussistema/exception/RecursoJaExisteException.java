package com.zoomtecnologia.morpheus.morpheussistema.exception;

public class RecursoJaExisteException extends NegocioException{

    public RecursoJaExisteException(String message, String mensagemDesenvolvedor) {
        super(message, mensagemDesenvolvedor);
    }
    
}
