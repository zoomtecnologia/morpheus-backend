    
package com.zoomtecnologia.morpheus.morpheussistema.exception;

import lombok.ToString;

@ToString
public class NegocioException extends RuntimeException {

    private final String mensagem;
    private final String mensagemDesenvolvedor;

    public NegocioException(String message,String mensagemDesenvolvedor) {
        super(message);
        this.mensagemDesenvolvedor = mensagemDesenvolvedor;
        this.mensagem = message;
    }

    public String getMensagemDesenvolvedor() {
        return mensagemDesenvolvedor;
    }

    public String getMensagem() {
        return mensagem;
    }
    

}
