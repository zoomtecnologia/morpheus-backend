/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.util;

import java.util.Random;

/**
 *
 * @author eudes
 */
public class GeradorCPF {

    public static String gerarCPF() {
        Random random = new Random();

        int num1 = random.nextInt(9) + 1;
        int num2 = random.nextInt(10);
        int num3 = random.nextInt(10);
        int num4 = random.nextInt(10);
        int num5 = random.nextInt(10);
        int num6 = random.nextInt(10);
        int num7 = random.nextInt(10);
        int num8 = random.nextInt(10);
        int num9 = random.nextInt(10);

        // Cálculo do primeiro dígito verificador
        int soma1 = num1 * 10 + num2 * 9 + num3 * 8 + num4 * 7 + num5 * 6 + num6 * 5 + num7 * 4 + num8 * 3 + num9 * 2;
        int digito1 = 11 - (soma1 % 11);
        if (digito1 >= 10) {
            digito1 = 0;
        }

        // Cálculo do segundo dígito verificador
        int soma2 = num1 * 11 + num2 * 10 + num3 * 9 + num4 * 8 + num5 * 7 + num6 * 6 + num7 * 5 + num8 * 4 + num9 * 3 + digito1 * 2;
        int digito2 = 11 - (soma2 % 11);
        if (digito2 >= 10) {
            digito2 = 0;
        }

        String cpf = "" + num1 + num2 + num3 + "." + num4 + num5 + num6 + "." + num7 + num8 + num9 + "-" + digito1 + digito2;
        return cpf.replaceAll("\\D", "");
    }

}
