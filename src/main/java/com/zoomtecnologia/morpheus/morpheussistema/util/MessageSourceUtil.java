package com.zoomtecnologia.morpheus.morpheussistema.util;

import com.zoomtecnologia.morpheus.morpheussistema.exception.NegocioException;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoJaExisteException;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MessageSourceUtil {

    @Autowired
    private MessageSource messageSource;
    
    public void recursoNaoExiste(String titulo,String mensagem) throws RecursoNaoEncontradoException{
         throw new RecursoNaoEncontradoException(this.pegarMensagem(titulo), this.pegarMensagem(mensagem));
    }
    
    public void recursoJaExiste(String titulo,String mensagem) throws RecursoJaExisteException{
         throw new RecursoJaExisteException(this.pegarMensagem(titulo), this.pegarMensagem(mensagem));
    }
        
    public void negocioException(String titulo,String mensagem) throws NegocioException{
        throw new NegocioException(this.pegarMensagem(titulo),this.pegarMensagem(mensagem));
    }    
    
    public String pegarMensagem(String mensagem) {
        return this.messageSource.getMessage(mensagem, null, LocaleContextHolder.getLocale());
    }

    
}
