package com.zoomtecnologia.morpheus.morpheussistema.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

public class ImageCrop {

    private final int width;
    private final int height;
    private File originFile;
    private InputStream originInputStream;
    private InputStream inputStream;

    public ImageCrop(File originFile, int width, int height) {
        this.originFile = originFile;
        this.width = width;
        this.height = height;
    }
    
    public ImageCrop(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void resize() throws IOException {
        BufferedImage imagem;
        if(this.originInputStream != null){
            imagem = ImageIO.read(this.originInputStream);
        }else{
            imagem = ImageIO.read(originFile);
        }
        BufferedImage new_img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = new_img.createGraphics();
        g.drawImage(imagem, 0, 0, width, height, null);
        g.dispose();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(new_img, "JPG", byteArrayOutputStream);
        this.inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public void setOriginFile(File originFile) {
        this.originFile = originFile;
    }

    public void setOriginInputStream(InputStream originInputStream) {
        this.originInputStream = originInputStream;
    }
    
    public InputStream getInputStream() {
        return inputStream;
    }

}
