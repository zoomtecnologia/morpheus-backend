package com.zoomtecnologia.morpheus.morpheussistema;

import com.zoomtecnologia.morpheus.morpheussistema.config.propriedades.MorpheusProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(MorpheusProperty.class)
public class MorpheusApplication {

    public static void main(String[] args) {
        SpringApplication.run(MorpheusApplication.class, args);
    }
}
