package com.zoomtecnologia.morpheus.morpheussistema.email;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaPagamentosModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.config.propriedades.MorpheusProperty;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoStatus;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.representante.Representante;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioRecuperacaoSenhaModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.util.FormatterUtil;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratado;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component
public class Mailer {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine thymeleaf;

    @Autowired
    private MorpheusProperty morpheusProperty;

    private static final String REMETENTE_ZOOM = "suporte@zoomtecnologia.com";

    public void enviarEmailDeSolicitacaoPagamento(ComissaoListaPagamentosModeloAPI solicitacao, List<ComissaoListaModeloAPI> comissoes) {
        String template = "email/solicitacao-pagamento";
        String remetente = REMETENTE_ZOOM;
        String assunto = solicitacao.comissaoPaga() ? "Solicitação Paga" : "Solicitação de Pagamento";
        Map<String, Object> variaveis = new HashMap<>();
        variaveis.put("solicitacao", solicitacao);
        variaveis.put("comissoes", comissoes);
        this.enviarEmail(remetente, Arrays.asList(solicitacao.getEmailRepresentante(), remetente), assunto, template, variaveis);
    }

    public void enviarEmailContrato(ZionPlanoContratado zionPlanoContratado, Cliente cliente) {
        MorpheusProperty.FrontEnd frontEnd = this.morpheusProperty.getFrontEnd();
        String template = "email/email-contrato";
        String remetente = REMETENTE_ZOOM;
        String assunto = "Aceite do contrato";
        Map<String, Object> variaveis = new HashMap<>();
        variaveis.put("link", frontEnd.getEndereco() + "/contrato/contrato-documento.html?codigoContrato=" + zionPlanoContratado.getCodigo());
        variaveis.put("razaoSocial", cliente.getRazaoSocial());
        variaveis.put("cnpj", FormatterUtil.definirMascara(cliente.getCnpj(), "##.###.###/####-##"));
        this.enviarEmail(remetente, Arrays.asList(cliente.getEmail(), remetente), assunto, template, variaveis);
    }

    public void enviarEmailContratoAceito(Cliente cliente) {
        String template = "email/email-contrato-aceito";
        String remetente = REMETENTE_ZOOM;
        String assunto = "Aceite do contrato confirmado";
        Map<String, Object> variaveis = new HashMap<>();
        variaveis.put("dataConfirmacao", cliente.getDataAceite().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        variaveis.put("razaoSocial", cliente.getRazaoSocial());
        variaveis.put("cnpj", FormatterUtil.definirMascara(cliente.getCnpj(), "##.###.###/####-##"));
        this.enviarEmail(remetente, Arrays.asList(cliente.getEmail(), REMETENTE_ZOOM), assunto, template, variaveis);
    }

    public void enviarEmailRevendedorCadastrado(Representante representante) {
        String template = "email/email-revendedor";
        String remetente = REMETENTE_ZOOM;
        String assunto = "Revendedor cadastrado";
        Map<String, Object> variaveis = new HashMap<>();
        variaveis.put("dataCadastro", representante.getDataCadastro().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        variaveis.put("nome", representante.getNome());
        variaveis.put("email", representante.getEmail());
        variaveis.put("celular", FormatterUtil.definirMascara(representante.getWhatsapp(), "(##) #####-####"));
        this.enviarEmail(remetente, Arrays.asList(remetente), assunto, template, variaveis);
    }

    public void enviarEmailConfirmacaoSenha(UsuarioRecuperacaoSenhaModeloAPI usuario) {
        MorpheusProperty.FrontEnd frontEnd = this.morpheusProperty.getFrontEnd();
        String template = "email/recuperacao-senha";
        String remetente = REMETENTE_ZOOM;
        String assunto = "Recuperação de senha";
        Map<String, Object> variaveis = new HashMap<>();
        variaveis.put("email", usuario.getEmail());
        variaveis.put("link", frontEnd.getEndereco() + "/usuario/recuperar-senha.html?chave=" + usuario.getRecuperandoSenha());
        this.enviarEmail(remetente, Arrays.asList(usuario.getEmail(), REMETENTE_ZOOM), assunto, template, variaveis);
    }

    public void enviarEmailAndamentoDoPedido(Pedido pedido) {
        String template = "email/pedido-status";
        String remetente = REMETENTE_ZOOM;
        String assunto = "Instalação do sistema, Andamento do pedido";
        Cliente cliente = pedido.getCliente();
        Representante representante = pedido.getRepresentante();
        Integer numeroPedido = pedido.getNumero();
        PedidoStatus pedidoStatus = pedido.getStatus();
        Map<String, Object> variaveis = new HashMap<>();
        variaveis.put("numeroPedido", String.format("%02d", numeroPedido));
        variaveis.put("razaoSocial", cliente.getRazaoSocial());
        variaveis.put("cnpj", FormatterUtil.definirMascara(cliente.getCnpj(), "##.###.###/####-##"));
        if (pedidoStatus.equals(PedidoStatus.INSTALACAO_MARCADA)) {
            String statusInstalacaoMarcada = "Instalação marcada: " + pedido.getDataMarcacaoInstalacao().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
            variaveis.put("statusPedido", statusInstalacaoMarcada);
        } else if (pedidoStatus.equals(PedidoStatus.INSTALACAO_FINALIZADA)) {
            String statusInstalacaoMarcada = "Instalação Finalizada: " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            variaveis.put("statusPedido", statusInstalacaoMarcada);
        }
        this.enviarEmail(remetente, Arrays.asList(cliente.getEmail(), representante.getEmail(), REMETENTE_ZOOM), assunto, template, variaveis);
    }

    public void enviarEmail(String remetente, List<String> destinatarios, String assunto, String template, Map<String, Object> variaveis) {
        Context context = new Context(new Locale("pt", "BR"));
        variaveis.entrySet().forEach(elemento -> context.setVariable(elemento.getKey(), elemento.getValue()));
        String mensagem = this.thymeleaf.process(template, context);
        this.enviarEmail(remetente, destinatarios, assunto, mensagem);
    }

    public void enviarEmail(String remetente, List<String> destinatarios, String assunto, String mensagem) {
        try {
            MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "UTF-8");
            helper.setFrom(remetente);
            helper.setTo(destinatarios.toArray(new String[destinatarios.size()]));
            helper.setSubject(assunto);
            helper.setText(mensagem, true);
            this.javaMailSender.send(mimeMessage);
            System.out.println("Enviado com sucesso!!");
        } catch (MessagingException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void enviarEmailAcessoUsuario(Cliente cliente) {
        String template = "email/email-primeiro-acesso";
        String remetente = REMETENTE_ZOOM;
        String assunto = "Dados de acesso ao ZION";
        Map<String, Object> variaveis = new HashMap<>();
        variaveis.put("razaoSocial", cliente.getRazaoSocial());
        variaveis.put("cnpj", FormatterUtil.definirMascara(cliente.getCnpj(), "##.###.###/####-##"));
        variaveis.put("grupo", cliente.getCodigoGrupoEmpresa().getNome());
        variaveis.put("usuario", cliente.getEmail());
        variaveis.put("senha", "123");
        this.enviarEmail(remetente, Arrays.asList(cliente.getEmail(), remetente), assunto, template, variaveis);
    }

}
