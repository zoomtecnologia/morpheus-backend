package com.zoomtecnologia.morpheus.morpheussistema.modulos.gerencianet;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gerencianet")
@AllArgsConstructor
public class GerencianetControle {

    private final GerencianetServico gerencianetServico;

    @GetMapping("/login")
    public String fazerLogin(@RequestParam String username, @RequestParam String password) {
        return gerencianetServico.fazerLogin(username, password);
    }

}
