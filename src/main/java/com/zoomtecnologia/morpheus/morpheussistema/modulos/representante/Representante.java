package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "representante")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Representante implements Serializable {
        
    @Id

	@Column(name = "documento")
	private String documento;


	@Column(name = "nome")
	private String nome;


	@Column(name = "nome_social")
	private String nomeSocial;


	@Column(name = "logradouro")
	private String logradouro;


	@Column(name = "numero")
	private String numero;


	@Column(name = "complemento")
	private String complemento;


	@Column(name = "bairro")
	private String bairro;


	@Column(name = "cidade")
	private String cidade;


	@Column(name = "estado")
	private String estado;


	@Column(name = "cep")
	private String cep;


	@Column(name = "nome_contato")
	private String nomeContato;


	@Column(name = "telefone")
	private String telefone;


	@Column(name = "celular")
	private String celular;


	@Column(name = "whatsapp")
	private String whatsapp;


	@Column(name = "email")
	private String email;


	@Column(name = "tipo")
	private String tipo;

@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)

	@Column(name = "data_cadastro")
	private LocalDate dataCadastro;


	@Column(name = "dia_pagamento_comissao")
	private Integer diaPagamentoComissao;


	@Column(name = "dias_tolerancia_pagamento_comissao")
	private Integer diasToleranciaPagamentoComissao;

@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)

	@Column(name = "data_ultima_atualizacao")
	private LocalDate dataUltimaAtualizacao;

@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)

	@Column(name = "data_ultimo_acesso")
	private LocalDate dataUltimoAcesso;


	@Column(name = "usuario_atualizacao")
	private String usuarioAtualizacao;


	@Column(name = "nivel")
	private String nivel;


	@Column(name = "status")
	private String status;


	@Column(name = "obs")
	private String obs;


	@Column(name = "banco")
	private String banco;


	@Column(name = "agencia")
	private String agencia;


	@Column(name = "conta")
	private String conta;


	@Column(name = "tipo_conta")
	private String tipoConta;


	@Column(name = "documento_pessoa_conta")
	private String documentoPessoaConta;


	@Column(name = "nome_pessoa_conta")
	private String nomePessoaConta;


	@Column(name = "chave_pix")
	private String chavePix;

@Lob

	@Column(name = "foto")
	private byte[] foto;


	@Column(name = "gera_receita")
	private Boolean geraReceita;


    
	@Column(name = "codigo_indicacao")
	private String codigoIndicacao;
    
    public Boolean getGeraReceita() {
        return geraReceita;
    }

    public void setGeraReceita(Boolean geraReceita) {
        this.geraReceita = geraReceita;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeSocial() {
        return nomeSocial;
    }

    public void setNomeSocial(String nomeSocial) {
        this.nomeSocial = nomeSocial;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNomeContato() {
        return nomeContato;
    }

    public void setNomeContato(String nomeContato) {
        this.nomeContato = nomeContato;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Integer getDiaPagamentoComissao() {
        return diaPagamentoComissao;
    }

    public void setDiaPagamentoComissao(Integer diaPagamentoComissao) {
        this.diaPagamentoComissao = diaPagamentoComissao;
    }

    public Integer getDiasToleranciaPagamentoComissao() {
        return diasToleranciaPagamentoComissao;
    }

    public void setDiasToleranciaPagamentoComissao(Integer diasToleranciaPagamentoComissao) {
        this.diasToleranciaPagamentoComissao = diasToleranciaPagamentoComissao;
    }

    public LocalDate getDataUltimaAtualizacao() {
        return dataUltimaAtualizacao;
    }

    public void setDataUltimaAtualizacao(LocalDate dataUltimaAtualizacao) {
        this.dataUltimaAtualizacao = dataUltimaAtualizacao;
    }

    public LocalDate getDataUltimoAcesso() {
        return dataUltimoAcesso;
    }

    public void setDataUltimoAcesso(LocalDate dataUltimoAcesso) {
        this.dataUltimoAcesso = dataUltimoAcesso;
    }

    public String getUsuarioAtualizacao() {
        return usuarioAtualizacao;
    }

    public void setUsuarioAtualizacao(String usuarioAtualizacao) {
        this.usuarioAtualizacao = usuarioAtualizacao;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(String tipoConta) {
        this.tipoConta = tipoConta;
    }

    public String getDocumentoPessoaConta() {
        return documentoPessoaConta;
    }

    public void setDocumentoPessoaConta(String documentoPessoaConta) {
        this.documentoPessoaConta = documentoPessoaConta;
    }

    public String getNomePessoaConta() {
        return nomePessoaConta;
    }

    public void setNomePessoaConta(String nomePessoaConta) {
        this.nomePessoaConta = nomePessoaConta;
    }

    public String getChavePix() {
        return chavePix;
    }

    public void setChavePix(String chavePix) {
        this.chavePix = chavePix;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getCodigoIndicacao() {
        return codigoIndicacao;
    }

    public void setCodigoIndicacao(String codigoIndicacao) {
        this.codigoIndicacao = codigoIndicacao;
    }

    public boolean naoGeraReceita() {
        return !this.geraReceita;
    }

    public boolean eSuporte() {
        return this.tipo.equals("S");
    }

    public boolean eComercial() {
        return this.tipo.equals("C");
    }

    public boolean naoEnviaContrato() {
        return this.eSuporte() && this.naoGeraReceita();
    }

    public boolean enviaContrato() {
        return !naoEnviaContrato();
    }

    public boolean representanteZoom() {
        return this.documento.equals("3055");
    }

    public String emailToHash() throws NoSuchAlgorithmException {
        final String emailPrefix = String.format("%s", this.email);
        final MessageDigest md = MessageDigest.getInstance("MD5");
        final byte[] hashBytes = md.digest(emailPrefix.getBytes());
        final String hashBase64 = Base64.getEncoder().encodeToString(hashBytes);
        final String hash = hashBase64.substring(0, 10);
        return hash;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.documento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Representante other = (Representante) obj;
        return Objects.equals(this.documento, other.documento);
    }

    @Override
    public String toString() {
        return "Representante{" + "documento=" + documento + ", nome=" + nome + ", nomeSocial=" + nomeSocial + ", logradouro=" + logradouro + ", numero=" + numero + ", complemento=" + complemento + ", bairro=" + bairro + ", cidade=" + cidade + ", estado=" + estado + ", cep=" + cep + ", nomeContato=" + nomeContato + ", telefone=" + telefone + ", celular=" + celular + ", whatsapp=" + whatsapp + ", email=" + email + ", tipo=" + tipo + ", dataCadastro=" + dataCadastro + ", diaPagamentoComissao=" + diaPagamentoComissao + ", diasToleranciaPagamentoComissao=" + diasToleranciaPagamentoComissao + ", dataUltimaAtualizacao=" + dataUltimaAtualizacao + ", dataUltimoAcesso=" + dataUltimoAcesso + ", usuarioAtualizacao=" + usuarioAtualizacao + ", nivel=" + nivel + ", status=" + status + ", obs=" + obs + ", banco=" + banco + ", agencia=" + agencia + ", conta=" + conta + ", tipoConta=" + tipoConta + ", documentoPessoaConta=" + documentoPessoaConta + ", nomePessoaConta=" + nomePessoaConta + ", chavePix=" + chavePix + ", foto=" + foto + ", geraReceita=" + geraReceita + '}';
    }
}
