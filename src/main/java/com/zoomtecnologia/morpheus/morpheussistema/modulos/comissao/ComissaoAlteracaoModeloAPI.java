package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import javax.validation.constraints.NotNull;

public class ComissaoAlteracaoModeloAPI {
    
    @NotNull
    private Double valor;
    
    private String obs;

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    @Override
    public String toString() {
        return "ComissaoAlteracaoModeloAPI{" + "valor=" + valor + ", obs=" + obs + '}';
    }

    
}
