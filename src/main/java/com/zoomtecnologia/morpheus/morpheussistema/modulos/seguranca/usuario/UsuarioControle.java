package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("usuarios")
@CrossOrigin("*")
public class UsuarioControle {

    @Autowired
    private UsuarioServico usuarioServico;

    @GetMapping
    public List<?> listarUsuarios(@RequestParam(value = "resumido", required = false, defaultValue = "false") boolean resumido) {
        if (resumido) {
            return this.usuarioServico.listarUsuariosResumido();
        }
        return this.usuarioServico.listarUsuarios();
    }

    @GetMapping("{codigo}")
    public ResponseEntity<?> buscarPorCodigo(@PathVariable("codigo") Integer codigo, @RequestParam(value = "resumido", required = false, defaultValue = "false") boolean resumido) {
        if (resumido) {
            return ResponseEntity.ok(this.usuarioServico.buscarUsuarioResumidoPorCodigo(codigo));
        }
        Usuario usuario = this.usuarioServico.buscarPorCodigo(codigo);
        return ResponseEntity.ok(usuario);
    }

    @GetMapping("{chave}/recuperar-senha")
    public ResponseEntity<Usuario> buscarUsuarioPorChaveRecuperarSenha(@PathVariable("chave") String chave) {
        Usuario usuario = this.usuarioServico.buscarUsuarioPorRecuperacaoSenha(chave);
        return ResponseEntity.ok(usuario);
    }

    @PostMapping
    public ResponseEntity<Usuario> salvarUsuario(@RequestBody @Valid UsuarioInclusaoModeloAPI usuario, UriComponentsBuilder uriComponentsBuilder) {
        Usuario usuarioSalvo = this.usuarioServico.salvarUsuario(usuario);
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codigo", usuarioSalvo.getCodigo());
        URI uri = uriComponentsBuilder.path("usuarios/{codigo}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(usuarioSalvo);
    }

    @PutMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarUsuario(@PathVariable("codigo") Integer codigo, @RequestBody @Valid Usuario usuario) {
        this.usuarioServico.alterarUsuario(codigo, usuario);
    }
    
    @DeleteMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluirUsuario(@PathVariable("codigo") Integer codigo) {
        this.usuarioServico.excluirUsuario(codigo);
    }    

    @PostMapping("login")
    public ResponseEntity<UsuarioModeloAPI> realizarLogin(@RequestBody @Valid UsuarioLoginModeloAPI usuarioLogin) {
        UsuarioModeloAPI usuario = this.usuarioServico.realizarLogin(usuarioLogin);
        return ResponseEntity.ok(usuario);
    }

    @PostMapping("verificar-email-cadastrado")
    public ResponseEntity<Boolean> verificarEmailCadastrado(@RequestBody @Valid UsuarioRecuperarSenha usuarioRecuperarSenha) {
        boolean emailCadastrado = this.usuarioServico.verificarEmailCadastrado(usuarioRecuperarSenha);
        return ResponseEntity.ok(emailCadastrado);
    }

    @PostMapping("enviar-email-redefinir-senha")
    public void enviarEmailRedefinirSenha(@RequestBody @Valid UsuarioRecuperarSenha usuarioRecuperarSenha) {
        this.usuarioServico.enviarEmailRecuperacaoSenha(usuarioRecuperarSenha);
    }

    @PostMapping("recuperar-senha")
    public ResponseEntity<UsuarioRecuperarSenha> recuperarSenha(@RequestBody @Valid UsuarioRecuperarSenha usuarioRecuperarSenha) {
        UsuarioRecuperarSenha recuperarSenha = this.usuarioServico.recuperarSenha(usuarioRecuperarSenha);
        return ResponseEntity.status(HttpStatus.CREATED).body(recuperarSenha);
    }

}
