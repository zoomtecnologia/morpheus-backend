package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ItensPedidoInclusaoModeloAPI extends  ItensPedidoGenericoModeloAPI{

}
