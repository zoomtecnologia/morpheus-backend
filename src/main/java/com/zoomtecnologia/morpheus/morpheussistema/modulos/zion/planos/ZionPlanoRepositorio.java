package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planos;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionPlanoRepositorio extends JpaRepository<ZionPlano, String> {

    @Query("SELECT plano.codigo AS codigo, " +
       "plano.nome AS nome, " +
       "plano.status AS status, " +
       "plano.valor AS valor, " +
       "plano.valorUsuarioAdicional AS valorUsuarioAdicional, " +
       "plano.valorPDVAdicional AS valorPDVAdicional, " +
       "plano.descricao AS descricao, " +
       "plano.tipoPlano AS tipoPlano, " +
       "plano.geraCobranca AS geraCobranca, " +
       "plano.dataCadastro AS dataCadastro, " +
       "plano.usuarios AS usuarios, " +
       "plano.pdvs AS pdvs, " +
       "plano.teste AS teste, " +
       "plano.diasParaTeste AS diasParaTeste, " +
       "zionPlanoAplicacoes.id.codigoAplicacao AS codigoAplicacao, " +
       "zionPlanoAplicacoes.id.moduloAplicacao AS codigoModulo " +
       "FROM ZionPlano plano " +
       "INNER JOIN ZionPlanoAplicacoes zionPlanoAplicacoes ON (zionPlanoAplicacoes.id.codigoPlano = plano.codigo) " +
       "WHERE (plano.nome LIKE %:pesquisa% OR plano.descricao LIKE %:pesquisa%) " +
       "AND plano.tipoPlano LIKE %:tipoPlano% " +
       "ORDER BY plano.dataCadastro")

    public List<ZionPlanoModeloResumido> listarPlanos(String pesquisa, String tipoPlano);

    @Query("select plano from ZionPlano plano where plano.codigo=:codigo")
    public Optional<ZionPlano> buscarPlano(String codigo);

}
