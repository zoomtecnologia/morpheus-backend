package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planos;

import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoes;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoesModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoesRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planoaplicacoes.ZionPlanoAplicacoesPadrao;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planoaplicacoes.ZionPlanoAplicacoesPadraoPK;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planoaplicacoes.ZionPlanoAplicacoesPadraoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planos.ZionPlanoPadrao;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planos.ZionPlanoPadraoRepositorio;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ZionPlanoServico {

    private final ZionPlanoRepositorio planoRepositorio;
    private final ZionPlanoAplicacoesRepositorio planoAplicacoesRepositorio;
    private final MessageSourceUtil messageSourceUtil;
    private final ZionPlanoPadraoRepositorio zionPlanoPadraoRepositorio;
    private final ZionPlanoAplicacoesPadraoRepositorio zionPlanoAplicacoesPadraoRepositorio;

    public Collection<ZionPlano> listarPlanos(String pesquisa, String tipoPlano) {
        List<ZionPlanoModeloResumido> zionPlanos = this.planoRepositorio.listarPlanos(pesquisa, tipoPlano);
        Map<String, ZionPlano> planosAgrupados = new HashMap<>();
        for (ZionPlanoModeloResumido zionPlanoModeloResumido : zionPlanos) {
            ZionPlano zionPlanoEncontrado = planosAgrupados.get(zionPlanoModeloResumido.getCodigo());
            if (zionPlanoEncontrado != null) {
                Set<ZionPlanoAplicacoesModeloAPI> aplicacoes = zionPlanoEncontrado.getAplicacoes();
                ZionAplicacaoModeloAPI zionAplicacaoModeloAPI = new ZionAplicacaoModeloAPI();
                zionAplicacaoModeloAPI.setZionPlanoModeloResumido(zionPlanoModeloResumido);
                aplicacoes.add(zionAplicacaoModeloAPI);
                continue;
            }
            zionPlanoEncontrado = new ZionPlano();
            zionPlanoEncontrado.setZionPlanoModeloResumido(zionPlanoModeloResumido);
            zionPlanoEncontrado.setAplicacoes(new HashSet<>());
            Set<ZionPlanoAplicacoesModeloAPI> aplicacoes = zionPlanoEncontrado.getAplicacoes();
            if (zionPlanoModeloResumido.getCodigoAplicacao() != null) {
                ZionAplicacaoModeloAPI zionAplicacaoModeloAPI = new ZionAplicacaoModeloAPI();
                zionAplicacaoModeloAPI.setZionPlanoModeloResumido(zionPlanoModeloResumido);
                aplicacoes.add(zionAplicacaoModeloAPI);
            }
            planosAgrupados.put(zionPlanoModeloResumido.getCodigo(), zionPlanoEncontrado);
        }
        return planosAgrupados.values().stream().sorted((ZionPlano o1, ZionPlano o2) -> {
            if (o1.getDataCadastro().isBefore(o2.getDataCadastro())) {
                return -1;
            }

            return 1;
        }).collect(Collectors.toList());
    }

    public Set<ZionPlanoAplicacoesModeloAPI> listasAplicacoesPorPlano(String codigoPlano) {
        return this.planoAplicacoesRepositorio.listarAplicacoesPorPlano(codigoPlano);
    }

    public ZionPlano buscarPlanoAplicacoes(String codigo) {
        final Optional<ZionPlano> zionPlanoOptional = this.planoRepositorio.buscarPlano(codigo);
        if (zionPlanoOptional.isEmpty()) {
            this.messageSourceUtil.recursoNaoExiste("zionplano.nao-existe", "zionplano.nao-existe-detalhes");
        }
        ZionPlano zionPlano = zionPlanoOptional.get();
        Set<ZionPlanoAplicacoesModeloAPI> aplicacoes = this.planoAplicacoesRepositorio.listarAplicacoesPorPlano(codigo);
        zionPlano.setAplicacoes(aplicacoes);
        return zionPlano;
    }

    @Transactional
    public ZionPlano salvar(ZionPlanoInclusaoModeloAPI plano) {
        final ZionPlano zionPlano = plano.converterParaZionPlano();
        zionPlano.setDataCadastro(LocalDateTime.now());
        zionPlano.setCodigo(UUID.randomUUID().toString());
        final boolean planoEncontrado = this.planoRepositorio.existsById(zionPlano.getCodigo());
        if (planoEncontrado) {
            this.messageSourceUtil.recursoJaExiste("zionplano.ja-existe", "zionplano.ja-existe-detalhes");
        }
        final List<ZionPlanoAplicacoes> aplicacoes = plano.converterParaZionPlanoAplicacoes(zionPlano.getCodigo());
        if (aplicacoes.isEmpty()) {
            this.messageSourceUtil.negocioException("zionplano.nao-existe-aplicacoes", "zionplano.nao-existe-aplicacoes-detalhes");
        }
        final ZionPlano zionPlanoSalvo = this.planoRepositorio.save(zionPlano);
        this.planoAplicacoesRepositorio.salvar(aplicacoes);

        ZionPlanoPadrao zionPlanoPadrao = new ZionPlanoPadrao();
        BeanUtils.copyProperties(zionPlanoSalvo, zionPlanoPadrao);
        zionPlanoPadraoRepositorio.save(zionPlanoPadrao);
        List<ZionPlanoAplicacoesPadrao> aplicacoesPadraos = aplicacoes.stream().map(aplicacao -> {
            ZionPlanoAplicacoesPadrao zionPlanoAplicacoesPadrao = new ZionPlanoAplicacoesPadrao();
            zionPlanoAplicacoesPadrao.setId(new ZionPlanoAplicacoesPadraoPK());
            BeanUtils.copyProperties(aplicacao.getId(), zionPlanoAplicacoesPadrao.getId());
            return zionPlanoAplicacoesPadrao;
        }).collect(Collectors.toList());
        this.zionPlanoAplicacoesPadraoRepositorio.salvar(aplicacoesPadraos);
        return zionPlanoSalvo;
    }

    @Transactional
    public void alterarPlano(String codigoPlano, ZionPlanoInclusaoModeloAPI zionPlanoInclusaoModeloAPI) {
        final ZionPlano zionPlano = zionPlanoInclusaoModeloAPI.converterParaZionPlano();
        final ZionPlano zionPlanoEncontrado = this.buscarPlano(codigoPlano);
        BeanUtils.copyProperties(zionPlano, zionPlanoEncontrado, "codigo", "dataCadastro");
        this.planoRepositorio.save(zionPlanoEncontrado);
        this.planoAplicacoesRepositorio.excluir(codigoPlano);
        List<ZionPlanoAplicacoes> aplicacoes = zionPlanoInclusaoModeloAPI.converterParaZionPlanoAplicacoes(codigoPlano);
        this.planoAplicacoesRepositorio.salvar(aplicacoes);
        this.zionPlanoAplicacoesPadraoRepositorio.excluir(codigoPlano);
        ZionPlanoPadrao zionPlanoPadrao = new ZionPlanoPadrao();
        BeanUtils.copyProperties(zionPlanoEncontrado, zionPlanoPadrao);
        zionPlanoPadraoRepositorio.save(zionPlanoPadrao);
        List<ZionPlanoAplicacoesPadrao> aplicacoesPadraos = aplicacoes.stream().map(aplicacao -> {
            ZionPlanoAplicacoesPadrao zionPlanoAplicacoesPadrao = new ZionPlanoAplicacoesPadrao();
            zionPlanoAplicacoesPadrao.setId(new ZionPlanoAplicacoesPadraoPK());
            BeanUtils.copyProperties(aplicacao.getId(), zionPlanoAplicacoesPadrao.getId());
            return zionPlanoAplicacoesPadrao;
        }).collect(Collectors.toList());
        this.zionPlanoAplicacoesPadraoRepositorio.salvar(aplicacoesPadraos);
    }

    @Transactional
    public void excluirPlano(String codigoPlano) {
        ZionPlano zionPlano = this.buscarPlano(codigoPlano);
        this.planoAplicacoesRepositorio.excluir(codigoPlano);
        this.planoRepositorio.delete(zionPlano);
        this.zionPlanoAplicacoesPadraoRepositorio.excluir(codigoPlano);
        this.zionPlanoPadraoRepositorio.deleteById(codigoPlano);
    }

    private ZionPlano buscarPlano(String codigo) {
        final Optional<ZionPlano> zionPlanoOptional = this.planoRepositorio.buscarPlano(codigo);
        if (zionPlanoOptional.isEmpty()) {
            this.messageSourceUtil.recursoNaoExiste("zionplano.nao-existe", "zionplano.nao-existe-detalhes");
        }
        return zionPlanoOptional.get();
    }

}
