package com.zoomtecnologia.morpheus.morpheussistema.modulos.pix;

import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("payload-pix")
@CrossOrigin
public class PayloadPixControle {

    @PostMapping
    public ResponseEntity<String> gerarPayloadPIX(@RequestBody @Valid PayloadPixModeloAPI payloadPixModeloAPI) {
        String codigoSolicitacao = payloadPixModeloAPI.getCodigoSolicitacao().split("-")[2];
        PayloadPix payload = new PayloadPix()
                .setChavePix(payloadPixModeloAPI.getChavePix().toLowerCase())
                .setCidadeTitular(payloadPixModeloAPI.getCidadeTitular())
                .setDescricaoPagamento("Zoom tecnologia ***"+codigoSolicitacao)
                .setTitularConta(payloadPixModeloAPI.getNomeTitular())
                .setTxId(codigoSolicitacao)
                .setValorTransacao(payloadPixModeloAPI.getTotalSolicitacao().doubleValue());
        return ResponseEntity.status(HttpStatus.CREATED).body(payload.getPayloadPIX());
    }

}
