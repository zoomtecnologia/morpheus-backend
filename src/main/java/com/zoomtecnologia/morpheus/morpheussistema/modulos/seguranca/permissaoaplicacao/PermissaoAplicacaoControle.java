package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("permissao-aplicaoes")
@CrossOrigin
public class PermissaoAplicacaoControle {
    
    @Autowired
    private PermissaoAplicacaoServico permissaoAplicacaoServico;
    
    @GetMapping("{codigo}/permissao")
    public List<PermissaoAplicacaoResumida> listarAplicacoes(@PathVariable("codigo") Integer codigo) {
        return this.permissaoAplicacaoServico.listarAplicacoesPorPermissao(codigo);
    }
    
    @PostMapping
    public List<PermissaoAplicacao> salvarPermissaoAplicacao(@RequestBody @Valid List<PermissaoAplicacaoInclusaoModeloAPI> permissoes) {
        List<PermissaoAplicacao> permissoesSalvas = this.permissaoAplicacaoServico.salvarPermissaoAplicacaos(permissoes);
        return permissoesSalvas;
    }
    
    
    
}
