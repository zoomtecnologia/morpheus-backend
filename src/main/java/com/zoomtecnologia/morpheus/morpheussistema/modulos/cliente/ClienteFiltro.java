package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

public class ClienteFiltro {
    
    private String cnpj;

    private String regimeTributario;

    private String seguimento;

    private String contingencia;

    private Integer diaVencimento;

    private String statusAtividade;

    private String statusPagamento;

    private String codigoRepresentante;

    private String statusPedido;

    private Integer codigoGrupoEmpresa;

    private String aceiteContrato;
    
    public boolean temFiltro() {
        return (this.cnpj != null && !this.cnpj.isEmpty())
                || (this.regimeTributario != null && !this.regimeTributario.isEmpty())
                || (this.seguimento != null && !this.seguimento.isEmpty())
                || (this.codigoRepresentante != null && !this.codigoRepresentante.isEmpty())
                || (this.statusPagamento != null && !this.statusPagamento.isEmpty())
                || (this.statusAtividade != null && !this.statusAtividade.isEmpty())
                || (this.aceiteContrato != null && !this.aceiteContrato.isEmpty())
                || (this.contingencia != null && !this.contingencia.isEmpty())
                || (this.statusPedido != null && !this.statusPedido.isEmpty())
                || (this.diaVencimento != null && this.diaVencimento != 0)
                || (this.codigoGrupoEmpresa != null && this.codigoGrupoEmpresa != 0);
    }

    public boolean naoTemFiltro() {
        return !temFiltro();
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRegimeTributario() {
        return regimeTributario;
    }

    public void setRegimeTributario(String regimeTributario) {
        this.regimeTributario = regimeTributario;
    }

    public String getSeguimento() {
        return seguimento;
    }

    public void setSeguimento(String seguimento) {
        this.seguimento = seguimento;
    }

    public String getContingencia() {
        return contingencia;
    }

    public void setContingencia(String contingencia) {
        this.contingencia = contingencia;
    }

    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(Integer diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    public String getStatusAtividade() {
        return statusAtividade;
    }

    public void setStatusAtividade(String statusAtividade) {
        this.statusAtividade = statusAtividade;
    }

    public String getStatusPagamento() {
        return statusPagamento;
    }

    public void setStatusPagamento(String statusPagamento) {
        this.statusPagamento = statusPagamento;
    }

    public String getCodigoRepresentante() {
        return codigoRepresentante;
    }

    public void setCodigoRepresentante(String codigoRepresentante) {
        this.codigoRepresentante = codigoRepresentante;
    }

    public String getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }

    public Integer getCodigoGrupoEmpresa() {
        return codigoGrupoEmpresa;
    }

    public void setCodigoGrupoEmpresa(Integer codigoGrupoEmpresa) {
        this.codigoGrupoEmpresa = codigoGrupoEmpresa;
    }

    public String getAceiteContrato() {
        return aceiteContrato;
    }

    public void setAceiteContrato(String aceiteContrato) {
        this.aceiteContrato = aceiteContrato;
    }
    
}
