package com.zoomtecnologia.morpheus.morpheussistema.modulos.item;

import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoJaExisteException;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import com.zoomtecnologia.morpheus.morpheussistema.generico.GenericoServico;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ItemServico implements GenericoServico<Item, Integer> {

    @Autowired
    private ItemRepositorio itemRepositorio;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    @Override
    public List<Item> listarTodos(String pesquisa) {
        return this.itemRepositorio.findByDescricaoContainingIgnoreCase(pesquisa);
    }

    @Override
    public Page<Item> listarTodosPaginado(String pesquisa, Pageable pageable) {
        return this.itemRepositorio.findByDescricaoContainingIgnoreCase(pesquisa, pageable);
    }

    @Override
    public Item buscarPorCodigo(Integer codigo) {
        Optional<Item> itemOptional = this.itemRepositorio.findById(codigo);
        if (!itemOptional.isPresent()) {
            this.itemNaoEncontrado();
        }
        return itemOptional.get();
    }

    @Override
    public Item salvar(Item entidade) {
        this.verificaMesmaDescricao(entidade);
        return this.itemRepositorio.save(entidade);
    }

    @Override
    public void alterar(Item entidade, Integer key) {
        Item itemEncontrado = this.buscarPorCodigo(key);
        BeanUtils.copyProperties(entidade, itemEncontrado,"codigo");
        this.itemRepositorio.save(itemEncontrado);
    }

    @Override
    public void deletar(Integer key) {
        this.itemRepositorio.deleteById(key);
    }

    private void itemNaoEncontrado() throws RecursoNaoEncontradoException {
        this.messageSourceUtil.recursoNaoExiste("item.recurso-nao-encontrado", "item.recurso-nao-encontrado-detalhes");
    }

    private void itemJaExiste() throws RecursoJaExisteException {
        this.messageSourceUtil.recursoJaExiste("item.recurso-ja-existe", "item.recurso-ja-existe-detalhes");
    }

    private void verificaMesmaDescricao(Item entidade) throws RecursoJaExisteException {
        Optional<Item> itemOptional = this.itemRepositorio.findByDescricao(entidade.getDescricao());
        if (itemOptional.isPresent()) {
            this.itemJaExiste();
        }
    }

}
