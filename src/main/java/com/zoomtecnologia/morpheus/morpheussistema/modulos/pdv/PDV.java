/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.generico.NaoAtualizaAtributo;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "pdv")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
@NoArgsConstructor
public class PDV implements Serializable {

    @EmbeddedId
    private PDVPK id;

    @NotBlank
    @Column(name = "chave")
    private String chave;

    @NaoAtualizaAtributo
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)

    @Column(name = "data_cadastrado")
    private LocalDateTime dataCadastrado;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)

    @Column(name = "data_atualizacao")
    private LocalDateTime dataAtualizacao;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)

    @Column(name = "ultimo_acesso")
    private LocalDateTime ultimoAcesso;    

    public PDV(int numeroPdv, String cnpj, String codigoRepresentante, String chave) {
        this.id = new PDVPK();
        this.id.setNumeroPdv(numeroPdv);
        this.id.setCnpj(cnpj);
        this.id.setCodigoRepresentante(codigoRepresentante);
        this.chave = chave;
        this.dataCadastrado = LocalDateTime.now();
    }
    
    public ChaveDesserializacao chaveAcesso(){
        final ChaveAcesso chaveAcesso = new ChaveAcesso(this.chave);
        final ChaveDesserializacao desserializar = chaveAcesso.desserializar();
        return desserializar;
    }

}
