package com.zoomtecnologia.morpheus.morpheussistema.modulos.generico;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;

public abstract class SalvamentoGenericoEntidades<T> {

    @PersistenceContext
    private EntityManager entityManager;
    private final Map<String, CacheSql> insertsTables = new HashMap<>();

    public void saveEntities(List<T> entidades) throws ClassNotFoundException, 
            InstantiationException, IllegalAccessException, 
            NoSuchFieldException, IllegalArgumentException, 
            InvocationTargetException, NoSuchMethodException, 
            SecurityException {
        if (entidades == null || entidades.isEmpty()) {
            return;
        }
        final T primeiroRegistro = entidades.get(0);
        final Class<?> classe = primeiroRegistro.getClass();
        final Table anotacaoTable = classe.getAnnotation(Table.class);
        final String nomeTabela = anotacaoTable.name();
        CacheSql cache = insertsTables.get(nomeTabela);
        if (cache == null) {
            final Field[] declaredFields = classe.getDeclaredFields();
            final Set<Campo> nomeCampos = new HashSet<>();
            this.preenchendoCamposCabecalhoInsert(declaredFields, nomeCampos);
            final List<String> colunasDaTabela = nomeCampos.stream().map(Campo::getColunaTabela).collect(Collectors.toList());
            final String colunas = String.join(",", colunasDaTabela);
            final String insert = "insert into " + nomeTabela + " (" + colunas + ") values ";
            final String atualizaEntidadeCasoJaExista = this.atualizarEntidadeCasoJaExista(nomeCampos);
            cache = new CacheSql(insert, atualizaEntidadeCasoJaExista, nomeCampos);
            this.insertsTables.put(nomeTabela, cache);
        }
        final StringBuilder sqlValues = new StringBuilder();
        for (final T entidade : entidades) {
            sqlValues.append(",(").append(this.preencherSQL(entidade, cache.getNomeCampos())).append(")");
        }
        String sql = cache.getInsert().concat(sqlValues.toString().replaceFirst(",", "")).concat(cache.getAtualizacao());
        if (cache.getAtualizacao().isBlank()) {
            sql = sql.replace("insert", "replace");
        }
        this.entityManager.createNativeQuery(sql).executeUpdate();
    }

    private void preenchendoCamposCabecalhoInsert(final Field[] declaredFields, final Set<Campo> nomeCampos) {
        for (final Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            this.preenchendoCamposChaveComposta(declaredField, nomeCampos);
            final Column column = declaredField.getAnnotation(Column.class);
            final JoinColumn joinColumn = declaredField.getAnnotation(JoinColumn.class);
            final NaoAtualizaAtributo naoAtualizaAtributo = declaredField.getAnnotation(NaoAtualizaAtributo.class);
            final boolean naoAtualiza = naoAtualizaAtributo == null || naoAtualizaAtributo.atualiza();
            if (column != null) {
                nomeCampos.add(new Campo(null, declaredField.getName(), column.name(), naoAtualiza));
                continue;
            }
            if (joinColumn != null) {
                nomeCampos.add(new Campo(null, declaredField.getName(), joinColumn.name(), naoAtualiza));
            }
        }
    }

    private void preenchendoCamposChaveComposta(final Field declaredField, final Set<Campo> nomeCampos) {
        final EmbeddedId annotation = declaredField.getAnnotation(EmbeddedId.class);
        if (annotation == null) {
            return;
        }
        final Class<?> aClass = declaredField.getType();
        for (Field idField : aClass.getDeclaredFields()) {
            final Column column = idField.getAnnotation(Column.class);
            final JoinColumn joinColumn = idField.getAnnotation(JoinColumn.class);
            if (column != null) {
                nomeCampos.add(new Campo(declaredField.getName(), idField.getName(), column.name(), false));
                continue;
            }
            if (joinColumn != null) {
                nomeCampos.add(new Campo(declaredField.getName(), idField.getName(), joinColumn.name(), false));
            }
        }
    }

    private String preencherSQL(T entidade, final Set<Campo> nomeCampos) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String sql = "";
        for (Campo campo : nomeCampos) {
            if (campo.atributoChave == null) {
                final Field declaredField = entidade.getClass().getDeclaredField(campo.atributo);
                declaredField.setAccessible(true);
                final Object value = declaredField.get(entidade);
                sql += this.getCampoObject(value);
                continue;
            }
            final Field declaredField = entidade.getClass().getDeclaredField(campo.atributoChave);
            declaredField.setAccessible(true);
            final Object get = declaredField.get(entidade);
            final Field idDeclaredField = get.getClass().getDeclaredField(campo.atributo);
            idDeclaredField.setAccessible(true);
            final Object value = idDeclaredField.get(get);
            sql += this.getCampoObject(value);
        }
        return sql.replaceFirst(",", "");
    }

    private String atualizarEntidadeCasoJaExista(final Set<Campo> nomeCampos) {
        final String sql = " ON DUPLICATE KEY UPDATE ";
        final String camposParaAtualizar = nomeCampos.stream()
                .filter(campo -> campo.atualizaCampo)
                .map(Campo::getColunaTabela)
                .map(coluna -> coluna + "= values(" + coluna + ")")
                .collect(Collectors.joining(","));
        if (camposParaAtualizar.isBlank()) {
            return "";
        }
        return sql + camposParaAtualizar;
    }

    private String getCampoObject(Object campo) {
        if (campo != null && campo instanceof Boolean) {
            final Boolean campoBoolean = (Boolean) campo;
            return "," + (campoBoolean ? 1 : 0) + "";
        }
        if (campo != null && campo instanceof Integer) {
            final Integer campoInteger = (Integer) campo;
            return "," + campoInteger + "";
        }
        if (campo != null && campo instanceof OffsetDateTime) {
            final OffsetDateTime campoOffsetDateTime = (OffsetDateTime) campo;
            return ",'" + campoOffsetDateTime.toLocalDateTime() + "'";
        }
        return campo == null ? ",null" : ",'" + campo + "'";
    }

    private final class Campo {

        private final String atributoChave;
        private final String atributo;
        private final String colunaTabela;
        private final boolean atualizaCampo;

        public Campo(String atributoChave, String atributo, String colunaTabela, boolean atualizaCampo) {
            this.atributoChave = atributoChave;
            this.atributo = atributo;
            this.colunaTabela = colunaTabela;
            this.atualizaCampo = atualizaCampo;
        }

        public String getColunaTabela() {
            return colunaTabela;
        }

    }

    private final class CacheSql {

        private final String insert;
        private final String atualizacao;
        private final Set<Campo> nomeCampos;

        public CacheSql(String insert, String atualizacao, Set<Campo> nomeCampos) {
            this.insert = insert;
            this.atualizacao = atualizacao;
            this.nomeCampos = nomeCampos;
        }

        public String getInsert() {
            return insert;
        }

        public Set<Campo> getNomeCampos() {
            return nomeCampos;
        }

        public String getAtualizacao() {
            return atualizacao;
        }

    }
}
