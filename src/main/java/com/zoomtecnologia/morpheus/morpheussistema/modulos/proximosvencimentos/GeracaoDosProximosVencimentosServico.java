package com.zoomtecnologia.morpheus.morpheussistema.modulos.proximosvencimentos;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDVServico;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planocontratado.ZionPlanoContratadoPadraoServico;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class GeracaoDosProximosVencimentosServico {

    private final ClienteServico clienteServico;
    private final PDVServico pdvServico;
    private final ZionPlanoContratadoPadraoServico zionPlanoContratadoPadraoServico;

    @Scheduled(cron = "0 0 0 * * ?")
    public void gerarProximosVencimentos() {
        final List<Integer> diasVencimento = this.clienteServico.listarDiasVencimentos();
        final LocalDate dataAtual = LocalDate.now();
        final int diaAtual = dataAtual.getDayOfMonth();
        final Integer diaVencimentoMaisProximo = diasVencimento.stream()
                .filter(dia -> dia <= diaAtual)
                .max(Integer::compareTo)
                .orElse(diaAtual);
        final List<Cliente> clientes = this.clienteServico
                .listarClientesComDiaVencimentoMenorOuIgualAoInformado(diaVencimentoMaisProximo);
        for (Cliente cliente : clientes) {
            this.atualizarInformacoes(cliente);
        }
    }

    private void atualizarInformacoes(Cliente cliente) {
        cliente.setDataUltimaValidacao(LocalDate.now());
        this.pdvServico.salvarPdvsMorpheusEZion(cliente, cliente.getQuantidadePDVs());
        this.clienteServico.alterarCliente(cliente);
        this.zionPlanoContratadoPadraoServico.atualizarProximoVencimento(cliente.getDataProximoVencimento(), cliente.getCnpj());
    }

}
