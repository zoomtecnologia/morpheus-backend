package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "permissao_aplicacao")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissaoAplicacao implements Serializable {

    @EmbeddedId    
    
	@Column(name = "permissaoAplicacaoPK")
	private PermissaoAplicacaoPK permissaoAplicacaoPK;

    
	@Column(name = "status")
	private Boolean status;

    
	@Column(name = "acessar")
	private Boolean acessar;

    
	@Column(name = "incluir")
	private Boolean incluir;

    
	@Column(name = "alterar")
	private Boolean alterar;

    
	@Column(name = "excluir")
	private Boolean excluir;

    public PermissaoAplicacao() {
    }

    public PermissaoAplicacao(PermissaoAplicacaoPK permissaoAplicacaoPK) {
        this.permissaoAplicacaoPK = permissaoAplicacaoPK;
    }

    public PermissaoAplicacaoPK getPermissaoAplicacaoPK() {
        return permissaoAplicacaoPK;
    }

    public void setPermissaoAplicacaoPK(PermissaoAplicacaoPK permissaoAplicacaoPK) {
        this.permissaoAplicacaoPK = permissaoAplicacaoPK;
    }

    public Boolean getStatus() {
        return status;
    }

    public Boolean getAcessar() {
        return acessar;
    }

    public Boolean getIncluir() {
        return incluir;
    }

    public Boolean getAlterar() {
        return alterar;
    }

    public Boolean getExcluir() {
        return excluir;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setAcessar(Boolean acessar) {
        this.acessar = acessar;
    }

    public void setIncluir(Boolean incluir) {
        this.incluir = incluir;
    }

    public void setAlterar(Boolean alterar) {
        this.alterar = alterar;
    }

    public void setExcluir(Boolean excluir) {
        this.excluir = excluir;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.permissaoAplicacaoPK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PermissaoAplicacao other = (PermissaoAplicacao) obj;
        if (!Objects.equals(this.permissaoAplicacaoPK, other.permissaoAplicacaoPK)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PermissaoAplicacao{" + "permissaoAplicacaoPK=" + permissaoAplicacaoPK + '}';
    }

}
