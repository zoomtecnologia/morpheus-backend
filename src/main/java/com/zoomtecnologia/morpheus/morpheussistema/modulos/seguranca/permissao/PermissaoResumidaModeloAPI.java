package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao;

public interface PermissaoResumidaModeloAPI {
    
    String getCodigo();
    
    String getNome();
    
}
