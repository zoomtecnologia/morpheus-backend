package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.aplicacao;

import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("zion/aplicacoes")
@CrossOrigin("*")
@AllArgsConstructor
public class ZionAplicacaoControle {

    private final ZionAplicacaoServico aplicacaoServico;
    

    @GetMapping
    public List<ZionAplicacao> listarAplicacoes(
            @RequestParam(
                    value = "pesquisa",
                    required = false, defaultValue = "") String pesquisa) {
        return this.aplicacaoServico.listarAplicacoes(pesquisa);
    }

    @GetMapping("{codigo}/modulo/{modulo}")
    public ResponseEntity<ZionAplicacao> buscarUmaAplicacao(
            @PathVariable("codigo") String codigo,
            @PathVariable("modulo") String modulo) {
        final ZionAplicacao zionAplicacaoEncontrada = this.aplicacaoServico.buscarAplicacao(codigo, modulo);
        return ResponseEntity.ok(zionAplicacaoEncontrada);
    }

    @PostMapping
    public ResponseEntity<ZionAplicacao> salvar(@RequestBody @Valid ZionAplicacao aplicacao) {
        final ZionAplicacao aplicacaoSalva = this.aplicacaoServico.salvar(aplicacao);
        return ResponseEntity.status(HttpStatus.CREATED).body(aplicacaoSalva);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarAplicacao(@RequestBody @Valid ZionAplicacao aplicacao) {
        this.aplicacaoServico.alterarAplicacao(aplicacao);
    }

}
