/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.generico.SalvamentoGenericoEntidades;
import org.springframework.stereotype.Component;

@Component
public class PDVRepositorioSalvamentoBach extends SalvamentoGenericoEntidades<PDV>{
    
}
