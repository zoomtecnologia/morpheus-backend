package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import java.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

public final class PedidoRetornoModeloAPI {

    private final Integer numero;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private final LocalDate data;
    private final String documentoRepresentante;
    private final String nomeRepresentante;
    private final String cnpjCliente;
    private final String razaoSocialCliente;
    private final PedidoStatus statusPedido;
    private final Integer tipoComissao;
    private final Double total;
    private final Double valorMensalidade;
    private final Boolean geraReceita;

    public PedidoRetornoModeloAPI(Integer numero, LocalDate data, String documentoRepresentante, String nomeRepresentante, String cnpjCliente, String razaoSocialCliente, PedidoStatus statusPedido, Integer tipoComissao, Double total, Double valorMensalidade, Boolean geraReceita) {
        this.numero = numero;
        this.data = data;
        this.documentoRepresentante = documentoRepresentante;
        this.nomeRepresentante = nomeRepresentante;
        this.cnpjCliente = cnpjCliente;
        this.razaoSocialCliente = razaoSocialCliente;
        this.statusPedido = statusPedido;
        this.tipoComissao = tipoComissao;
        this.total = total;
        this.valorMensalidade = valorMensalidade;
        this.geraReceita = geraReceita;
    }

    public Integer getNumero() {
        return numero;
    }

    public LocalDate getData() {
        return data;
    }

    public String getDocumentoRepresentante() {
        return documentoRepresentante;
    }

    public String getNomeRepresentante() {
        return nomeRepresentante;
    }

    public String getCnpjCliente() {
        return cnpjCliente;
    }

    public String getRazaoSocialCliente() {
        return razaoSocialCliente;
    }

    public PedidoStatus getStatusPedido() {
        return statusPedido;
    }

    public Integer getTipoComissao() {
        return tipoComissao;
    }

    public Double getTotal() {
        return total;
    }

    public Double getValorMensalidade() {
        return valorMensalidade;
    }

    public Boolean getGeraReceita() {
        return geraReceita;
    }

    @Override
    public String toString() {
        return "PedidoRetornoModeloAPI{" + "numero=" + numero + ", data=" + data + ", documentoRepresentante=" + documentoRepresentante + ", nomeRepresentante=" + nomeRepresentante + ", cnpjCliente=" + cnpjCliente + ", razaoSocialCliente=" + razaoSocialCliente + ", statusPedido=" + statusPedido + ", tipoComissao=" + tipoComissao + ", total=" + total + ", valorMensalidade=" + valorMensalidade + ", geraReceita=" + geraReceita + '}';
    }
        
}
