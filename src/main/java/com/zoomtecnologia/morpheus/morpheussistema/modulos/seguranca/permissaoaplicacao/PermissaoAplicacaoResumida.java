package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao;

public final class PermissaoAplicacaoResumida {

    private final Boolean status;

    private final Integer codigo;

    private final String nome;

    private final String descricao;

    private final Boolean acessar;

    private final Boolean incluir;

    private final Boolean alterar;

    private final Boolean excluir;

    public PermissaoAplicacaoResumida(Boolean status, Integer codigo, String nome, String descricao, Boolean acessar, Boolean incluir, Boolean alterar, Boolean excluir) {
        this.status = status;
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.acessar = acessar;
        this.incluir = incluir;
        this.alterar = alterar;
        this.excluir = excluir;
    }

    public Boolean getStatus() {
        return status;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }

    public Boolean getAcessar() {
        return acessar;
    }

    public Boolean getIncluir() {
        return incluir;
    }

    public Boolean getAlterar() {
        return alterar;
    }

    public Boolean getExcluir() {
        return excluir;
    }

    @Override
    public String toString() {
        return "PermissaoAplicacaoResumida{" + "status=" + status + ", codigo=" + codigo + ", nome=" + nome + ", descricao=" + descricao + ", acessar=" + acessar + ", incluir=" + incluir + ", alterar=" + alterar + ", excluir=" + excluir + '}';
    }

}
