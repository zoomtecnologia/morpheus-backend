package com.zoomtecnologia.morpheus.morpheussistema.modulos.tipocomissao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoComissaoRepositorio extends JpaRepository<TipoComissao, Integer>{

    public List<TipoComissao> findByNomeContainingIgnoreCase(String pesquisa);
    
    public Page<TipoComissao> findByNomeContainingIgnoreCase(Pageable pageable, String pesquisa);
    
    public Optional<TipoComissao> findByNome(String nome);
}
