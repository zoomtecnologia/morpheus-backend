package com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoJaExisteException;
import com.zoomtecnologia.morpheus.morpheussistema.generico.GenericoServico;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class GrupoEmpresaServico implements GenericoServico<GrupoEmpresa, String> {

    @Autowired
    private GrupoEmpresaRepositorio grupoEmpresaRepositorio;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    public List<GrupoEmpresa> listarTodos(String pesquisa, String representante) {
        return this.grupoEmpresaRepositorio.findByNomeContainingIgnoreCaseAndCodigoRepresentante(pesquisa, representante);
    }

    @Override
    public List<GrupoEmpresa> listarTodos(String pesquisa) {
        return this.grupoEmpresaRepositorio.findByNomeContainingIgnoreCase(pesquisa);
    }

    public Optional<GrupoEmpresa> verificarGrupoEmpresaExistePorCodigo(String codigoGrupo) {
        return this.grupoEmpresaRepositorio.findById(codigoGrupo);
    }

    @Override
    public GrupoEmpresa buscarPorCodigo(String codigo) {
        Optional<GrupoEmpresa> grupoEmpresaOptional = this.grupoEmpresaRepositorio.findById(codigo);
        if (!grupoEmpresaOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("grupo-empresa.nao-existe", "grupo-empresa.nao-existe-detalhe");
        }
        return grupoEmpresaOptional.get();
    }

    @Override
    public GrupoEmpresa salvar(GrupoEmpresa grupoEmpresa) {
        this.verificarGrupoEmpresaExiste(grupoEmpresa);
        grupoEmpresa.setCodigo(UUID.randomUUID().toString());
        grupoEmpresa.setIdentificacaoBancoDados(UUID.randomUUID().toString());
        return this.grupoEmpresaRepositorio.save(grupoEmpresa);
    }

    @Override
    public void alterar(GrupoEmpresa grupoEmpresa, String codigo) {
        GrupoEmpresa grupoEmpresaEncontrado = this.buscarPorCodigo(codigo);
        BeanUtils.copyProperties(grupoEmpresa, grupoEmpresaEncontrado, "codigo", "identificacaoBancoDados");
        this.grupoEmpresaRepositorio.save(grupoEmpresaEncontrado);
    }

    @Override
    public void deletar(String codigo) {
        this.grupoEmpresaRepositorio.deleteById(codigo);
    }

    @Override
    public Page<GrupoEmpresa> listarTodosPaginado(String pesquisa, Pageable pageable) {
        return this.grupoEmpresaRepositorio.findByNomeContainingIgnoreCase(pesquisa, pageable);
    }

    public void criarNovoGrupoDeEmpresasPorCliente(Cliente cliente) {
        final GrupoEmpresa grupoEmpresa = new GrupoEmpresa();
        grupoEmpresa.setCodigo(UUID.randomUUID().toString());
        grupoEmpresa.setNome(cliente.getCnpj());
        grupoEmpresa.setCodigoRepresentante(cliente.getCodigoRepresentante());
        grupoEmpresa.setIdentificacaoBancoDados(UUID.randomUUID().toString());
        Optional<GrupoEmpresa> grupoEmpresaOptional = this.grupoEmpresaRepositorio.buscarPorNome(grupoEmpresa.getNome());
        if (grupoEmpresaOptional.isPresent()) {
            cliente.setCodigoGrupoEmpresa(grupoEmpresaOptional.get());
            return;
        }
        GrupoEmpresa grupoEmpresaSalvo = this.grupoEmpresaRepositorio.save(grupoEmpresa);
        cliente.setCodigoGrupoEmpresa(grupoEmpresaSalvo);
    }

    public void excluirGrupoEmpresaPorCliente(String cnpj) {
        Optional<GrupoEmpresa> grupoEmpresaOptional = this.grupoEmpresaRepositorio.buscarPorNome(cnpj);
        GrupoEmpresa grupoEmpresaEncontrado = grupoEmpresaOptional.get();
        this.grupoEmpresaRepositorio.delete(grupoEmpresaEncontrado);
    }

    private void verificarGrupoEmpresaExiste(GrupoEmpresa grupoEmpresa) throws RecursoJaExisteException {
        Optional<GrupoEmpresa> optionalGrupoEmpresa = this.grupoEmpresaRepositorio.buscarPorNome(grupoEmpresa.getNome());
        if (optionalGrupoEmpresa.isPresent()) {
            this.messageSourceUtil.recursoJaExiste("grupo-empresa.ja-existe", "grupo-empresa.ja-existe-detalhes");
        }
    }

}
