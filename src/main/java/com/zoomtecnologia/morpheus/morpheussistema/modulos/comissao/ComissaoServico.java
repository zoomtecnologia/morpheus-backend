package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoStatus;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.email.Mailer;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.representante.Representante;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioResumidoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.tipocomissao.TipoComissaoServico;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ComissaoServico {

    private final ComissaoRepositorio comissaoRepositorio;
    private final TipoComissaoServico tipoComissaoServico;
    private final UsuarioServico usuarioServico;
    private final MessageSourceUtil messageSourceUtil;
    private final ModelMapper modelMapper;
    private final Mailer mailer;

    public Page<Comissao> listarTodasPaginado(Pageable pageable, String pesquisa) {
        return this.comissaoRepositorio.listarTodasPaginado(pageable, pesquisa);
    }

    public List<ComissaoListaModeloAPI> listarComissoes(FiltroComissao filtroComissao) {
        String representante = filtroComissao.getRepresentante();
        String status = filtroComissao.getStatus();
        if (!filtroComissao.isPeriodo()) {
            this.adicionarDataCasoNaoSejaSelecionada(filtroComissao);
            return this.comissaoRepositorio.listarComissaoes(representante, status, filtroComissao.getDataInicial(), filtroComissao.getDataFinal());
        }
        LocalDate dataInformada = filtroComissao.isData() ? LocalDate.now() : null;
        List<String> listaStatus = status == null ? Arrays.asList("PENDENTE", "PAGO", "LIBERADO", "SOLICITADO") : Arrays.asList(status.split(","));
        ComissaoRepositorioQueryImpl.FiltroComissaoQuery filtroComissaoQuery = new ComissaoRepositorioQueryImpl.FiltroComissaoQuery();
        filtroComissaoQuery.setCodigoCliente(filtroComissao.getCliente());
        filtroComissaoQuery.setCodigoRepresentante(representante);
        filtroComissaoQuery.setCodigoSolicitacao(filtroComissao.getCodigoSolicitacao());
        filtroComissaoQuery.setStatus(listaStatus);
        filtroComissaoQuery.setDataVencimento(dataInformada);
        filtroComissaoQuery.setDataInicial(filtroComissao.getDataInicial());
        filtroComissaoQuery.setDataFinal(filtroComissao.getDataFinal());
        return this.comissaoRepositorio.listarComissaoes(filtroComissaoQuery);
    }

    public List<ComissaoListaPagamentosModeloAPI> listarSolicitacoesPagamento(FiltroComissao filtroComissao) {
        return this.comissaoRepositorio.listarPagamentosComissao(filtroComissao.getRepresentante(), filtroComissao.getStatus(), filtroComissao.getNomeRepresentante());
    }

    public Comissao buscarPorCodigo(Integer codigoComissao) {
        Optional<Comissao> comissaoOptional = this.comissaoRepositorio.findById(codigoComissao);
        if (!comissaoOptional.isPresent()) {
            this.comissaoNaoEncontrada();
        }
        return comissaoOptional.get();
    }

    public ComissaoTotaisModeloAPI consultarTotalComissaoPorRepresentante(String representante, LocalDate dataInicial, LocalDate dataFinal) {
        dataInicial = dataInicial == null ? LocalDate.now().with(TemporalAdjusters.firstDayOfYear()) : dataInicial;
        dataFinal = dataFinal == null ? LocalDate.now().with(TemporalAdjusters.lastDayOfYear()) : dataFinal;
        final ComissaoTotaisModeloAPI comissao = this.comissaoRepositorio.listarTotalDeComissoes(representante, dataInicial, dataFinal);
        if (comissao == null) {
            this.comissaoNaoEncontrada();
        }
        return comissao;
    }

    public ComissaoListaPagamentosModeloAPI buscarSolocitacaoComissaoPorCodigo(String codigoSolicitacao) {
        ComissaoListaPagamentosModeloAPI comissao = this.comissaoRepositorio.buscarSolicitacao(codigoSolicitacao);
        return comissao;
    }

    public void alterarStatus(List<ComissaoAlteracaoStatusModeloAPI> comissoesModeloAPI) {
        List<Integer> comissoes = comissoesModeloAPI.stream().map(ComissaoAlteracaoStatusModeloAPI::getCodigo).collect(Collectors.toList());
        String status = comissoesModeloAPI.get(0).getStatus();
        this.comissaoRepositorio.atualizarStatus(comissoes, status);
    }

    public void alterarComissao(ComissaoAlteracaoModeloAPI comissaoAlteracaoModeloAPI, Integer codigoComissao) {
        Comissao comissaoEncontrada = this.buscarPorCodigo(codigoComissao);
        BeanUtils.copyProperties(comissaoAlteracaoModeloAPI, comissaoEncontrada);
        this.comissaoRepositorio.save(comissaoEncontrada);
    }

    public Comissao solicitarComissoes(List<Integer> comissoesAPI) {
        this.comissaoRepositorio.socilitarLiberacao(comissoesAPI, "SOLICITADO", LocalDate.now());
        Comissao comissao = this.buscarPorCodigo(comissoesAPI.get(0));
        return comissao;
    }

    public void lancarComissao(Pedido pedido) {
        final PedidoStatus status = pedido.getStatus();
        if (this.verificarSeNaoPodeLancarComissao(status)) {
            return;
        }

        final String tipoComissao = "INSTALACAO";
        if (status.equals(PedidoStatus.INSTALACAO_PAGA)) {
            final Comissao comissaoInstalacao = this.preencherComissaoInstalacao(pedido);
            this.comissaoRepositorio.save(comissaoInstalacao);
            final UsuarioResumidoModeloAPI usuarioAtivo = this.usuarioServico
                    .buscarUsuarioAtivoPorRepresentante(pedido.getRepresentante().getDocumento())
                    .get();
            final String permissaoAfiliado = "AFILIADOS";
            if (usuarioAtivo.getPermissao().getNome().equals(permissaoAfiliado)) {
                return;
            }
            this.gerarParcelasMensalidade(pedido);
            return;
        }
        if (status.equals(PedidoStatus.COMISSAO_LIBERADA)) {
            this.adicionarDataDeLiberacaoNaComissaoDaInstalacao(pedido, tipoComissao);
            return;
        }
        this.adicionarDataDePagamentoNaComissaoDaInstalacao(pedido, tipoComissao);
    }

    public void realizarPagamentoDeSolicitacao(String codigoSolicitacao) {
        this.comissaoRepositorio.realizarPagamentoSolicitacao(codigoSolicitacao, LocalDate.now());
    }

    public void alterarDataProximoVencimento(Cliente cliente) {
        Integer diaVencimento = cliente.getDiaVencimento();
        LocalDate dataDoProximoVencimento = this.gerarDataDoProximoVencimento(diaVencimento, LocalDate.now());
        String codigoRepresentante = cliente.getCodigoRepresentante();
        String codigoCliente = cliente.getCnpj();
        this.comissaoRepositorio.alterarDataProximoVencimento(dataDoProximoVencimento, codigoCliente, codigoRepresentante);
    }

    public void salvarComissaoManutencao(ComissaoManutencaoInclusaoModeloAPI comissaoManutencaoInclusaoModeloAPI) {
        List<Comissao> comissoes = new ArrayList<>();
        long faltaEssaQuantidadeMesesParaDezembro = comissaoManutencaoInclusaoModeloAPI.getQuantidadeParcela();
        for (int i = 1; i <= faltaEssaQuantidadeMesesParaDezembro; i++) {
            Comissao comissao = this.modelMapper.map(comissaoManutencaoInclusaoModeloAPI, Comissao.class);
            comissao.setValorInstalacao(0.0);
            Pedido pedido = this.converterComissaoManutencaoEmPedido(comissaoManutencaoInclusaoModeloAPI);
            if (comissao.getTipo().equals("INSTALACAO")) {
                comissao.setValorInstalacao(comissao.getValorMensalidade());
                comissao.setValorMensalidade(0.0);
                pedido.setTotal(comissao.getValorInstalacao());
                comissao.setValor(this.tipoComissaoServico.calcularComissaoInstalacao(pedido));
            } else {
                pedido.setValorMensalidade(comissao.getValorMensalidade());
                comissao.setValor(this.tipoComissaoServico.calcularComissaoMensalidade(pedido));
            }
            comissao.setStatus("PENDENTE");
            if (i == 1) {
                comissoes.add(comissao);
                continue;
            }
            LocalDate dataMensalidadeMaisTrintaDias = comissao.getDataMensalidade().plusDays(30);
            int diaDoMesParaGerarOVencimento = comissaoManutencaoInclusaoModeloAPI.getDataMensalidade().getDayOfMonth();
            if (diaDoMesParaGerarOVencimento > dataMensalidadeMaisTrintaDias.lengthOfMonth()) {
                diaDoMesParaGerarOVencimento = dataMensalidadeMaisTrintaDias.lengthOfMonth();
            }
            comissao.setDataMensalidade(dataMensalidadeMaisTrintaDias.withDayOfMonth(diaDoMesParaGerarOVencimento));
            comissaoManutencaoInclusaoModeloAPI.setDataMensalidade(comissao.getDataMensalidade());
            comissoes.add(comissao);
        }
        this.comissaoRepositorio.saveAll(comissoes);
    }

    public void gerarParcelasMensalidade(Pedido pedido) {
        final List<Comissao> comissoes = new ArrayList<>();
        LocalDate proximaMensalidade = LocalDate.now();
        final long faltaEssaQuantidadeMesesParaDezembro = proximaMensalidade.until(proximaMensalidade.withMonth(12), ChronoUnit.MONTHS);
        for (int i = 0; i < faltaEssaQuantidadeMesesParaDezembro; i++) {
            final Comissao comissaoMensalidade = this.preencherComissaoMensalidade(pedido, proximaMensalidade);
            if (proximaMensalidade != comissaoMensalidade.getDataMensalidade()) {
                proximaMensalidade = comissaoMensalidade.getDataMensalidade();
            }
            comissoes.add(comissaoMensalidade);
        }
        this.comissaoRepositorio.saveAll(comissoes);
    }

    public void enviarEmailSolicitacaoPagamento(String codigoSolicitacao) {
        FiltroComissao filtroComissao = new FiltroComissao();
        filtroComissao.setPeriodo(true);
        filtroComissao.setData(false);
        filtroComissao.setCodigoSolicitacao(codigoSolicitacao);
        ComissaoListaPagamentosModeloAPI solicitacao = this.comissaoRepositorio.buscarSolicitacao(codigoSolicitacao);
        List<ComissaoListaModeloAPI> comissoes = this.listarComissoes(filtroComissao);
        this.mailer.enviarEmailDeSolicitacaoPagamento(solicitacao, comissoes);
    }

    public boolean existsByDataMensalidadeAndCodigoClienteCnpj(LocalDate dataMensalidade, String cnpjCliente) {
        return this.comissaoRepositorio.existsByDataMensalidadeAndCodigoCliente_Cnpj(dataMensalidade, cnpjCliente);
    }

    public void atualizarStatusComissaoPorDataMensalidade(String status, String cnpjCliente, LocalDate parse) {
        this.comissaoRepositorio.atualizarStatusComissaoPorDataMensalidade(status, cnpjCliente, parse);
    }

    public List<String> listarPessoasComMensalidadesEmAberto() {
        return this.comissaoRepositorio.listarPessoasComMensalidadesEmAberto();
    }

    public void bloquearClienteComPagamentosVencidos() {
        this.comissaoRepositorio.bloquearClienteComPagamentosVencidos();
    }

    public void ativarClienteComPagamentosQuitados() {
        this.comissaoRepositorio.ativarClienteComPagamentosQuitados();
    }

    private Pedido converterComissaoManutencaoEmPedido(ComissaoManutencaoInclusaoModeloAPI comissaoManutencaoInclusaoModeloAPI) {
        Pedido pedido = new Pedido();
        Representante representante = new Representante();
        representante.setDocumento(comissaoManutencaoInclusaoModeloAPI.getPedido().getDocumentoRepresentante());
        representante.setGeraReceita(comissaoManutencaoInclusaoModeloAPI.getPedido().getGeraReceita());
        pedido.setRepresentante(representante);
        pedido.setTipoComissao(comissaoManutencaoInclusaoModeloAPI.getPedido().getTipoComissao());
        return pedido;
    }

    private void adicionarDataDeLiberacaoNaComissaoDaInstalacao(Pedido pedido, String tipoComissao) throws RecursoNaoEncontradoException {
        Comissao comissaoInstalacaoEncontrada = this.buscarComissao(pedido, tipoComissao);
        comissaoInstalacaoEncontrada.setDataLiberacao(LocalDate.now());
        comissaoInstalacaoEncontrada.setStatus("LIBERADO");
        this.comissaoRepositorio.save(comissaoInstalacaoEncontrada);
    }

    private void adicionarDataDePagamentoNaComissaoDaInstalacao(Pedido pedido, String tipoComissao) throws RecursoNaoEncontradoException {
        Comissao comissaoInstalacaoEncontrada = this.buscarComissao(pedido, tipoComissao);
        comissaoInstalacaoEncontrada.setDataPagamento(LocalDate.now());
        comissaoInstalacaoEncontrada.setStatus("PAGO");
        this.comissaoRepositorio.save(comissaoInstalacaoEncontrada);
    }

    private Comissao buscarComissao(Pedido pedido, String tipoComissao) {
        String codigoRepresentante = pedido.getRepresentante().getDocumento();
        String codigoCliente = pedido.getCliente().getCnpj();
        Optional<Comissao> comissaoOptional = this.comissaoRepositorio.buscarComissaoPorTipo(tipoComissao, codigoRepresentante, codigoCliente);
        if (!comissaoOptional.isPresent()) {
            this.comissaoNaoEncontrada();
        }
        return comissaoOptional.get();
    }

    private Comissao preencherComissaoInstalacao(Pedido pedido) {
        Comissao comissao = new Comissao();
        comissao.setCodigoCliente(pedido.getCliente());
        comissao.setCodigoRepresentante(pedido.getRepresentante());
        comissao.setTipo("INSTALACAO");
        comissao.setStatus("PENDENTE");
        if (comissao.getDataMensalidade() == null) {
            comissao.setDataMensalidade(LocalDate.now().plusDays(15));
        }
        comissao.setValor(this.tipoComissaoServico.calcularComissaoInstalacao(pedido));
        comissao.setValorMensalidade(pedido.getValorMensalidade());
        comissao.setValorInstalacao(pedido.getTotal());
        return comissao;
    }

    private Comissao preencherComissaoMensalidade(Pedido pedido, LocalDate dataMensalidade) {
        Comissao comissao = new Comissao();
        comissao.setCodigoCliente(pedido.getCliente());
        comissao.setCodigoRepresentante(pedido.getRepresentante());
        comissao.setTipo("MENSALIDADE");
        comissao.setStatus("PENDENTE");
        Integer diaVencimento = pedido.getCliente().getDiaVencimento();
        comissao.setDataMensalidade(this.gerarDataDoProximoVencimento(diaVencimento, dataMensalidade));
        comissao.setValor(this.tipoComissaoServico.calcularComissaoMensalidade(pedido));
        comissao.setValorMensalidade(pedido.getValorMensalidade());
        comissao.setValorInstalacao(pedido.getTotal());
        return comissao;
    }

    private LocalDate gerarDataDoProximoVencimento(Integer diaVencimento, LocalDate dataMensalidade) {
        if (diaVencimento == 0) {
            diaVencimento = 5;
        }
        LocalDate dataDoProximoVencimento = dataMensalidade.plusDays(30).withDayOfMonth(diaVencimento);
        return dataDoProximoVencimento;
    }

    private void comissaoNaoEncontrada() throws RecursoNaoEncontradoException {
        this.messageSourceUtil.recursoNaoExiste("comissao.recurso-nao-encontrado", "comissao.recurso-nao-encontrado-detalhes");
    }

    private void adicionarDataCasoNaoSejaSelecionada(FiltroComissao filtroComissao) {
        if (filtroComissao.getDataInicial() == null) {
            LocalDate dataInicial = LocalDate.now().with(TemporalAdjusters.firstDayOfYear());
            filtroComissao.setDataInicial(dataInicial);
        }
        if (filtroComissao.getDataFinal() == null) {
            LocalDate dataFinal = LocalDate.now().with(TemporalAdjusters.lastDayOfYear());
            filtroComissao.setDataFinal(dataFinal);
        }
    }

    private boolean verificarSeNaoPodeLancarComissao(PedidoStatus status) {
        return status.equals(PedidoStatus.INSTALACAO_MARCADA) || status.equals(PedidoStatus.CLIENTE_ACEITOU)
                || status.equals(PedidoStatus.FINALIZADO) || status.equals(PedidoStatus.LANCADO)
                || status.equals(PedidoStatus.CANCELADO) || status.equals(PedidoStatus.ABERTO)
                || status.equals(PedidoStatus.INSTALACAO_FINALIZADA);
    }
}
