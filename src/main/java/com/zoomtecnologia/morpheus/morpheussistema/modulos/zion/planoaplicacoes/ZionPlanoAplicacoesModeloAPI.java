package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes;

public interface ZionPlanoAplicacoesModeloAPI {

    String getCodigoAplicacao();

    String getModuloAplicacao();

    String getNome();

}
