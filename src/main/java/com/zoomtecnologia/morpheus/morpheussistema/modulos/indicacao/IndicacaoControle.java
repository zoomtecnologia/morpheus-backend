package com.zoomtecnologia.morpheus.morpheussistema.modulos.indicacao;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("indicacoes")
@CrossOrigin("*")
@AllArgsConstructor
public class IndicacaoControle {

    private final IndicacaoServico indicacaoServico;

    @GetMapping
    public Page<IndicacaoModeloAPI> listarIndicacoes(
            @RequestParam(value = "pesquisa", required = false, defaultValue = "") String pesquisa,
            Pageable pageable) {
        return this.indicacaoServico.listarIndicacoes(pesquisa, pageable);
    }

    @GetMapping("{identificador}")
    public ResponseEntity<Indicacao> buscarIndicacao(@PathVariable String identificador) {
        final Indicacao indicacao = this.indicacaoServico.buscarIndicacao(identificador);
        return ResponseEntity.ok(indicacao);
    }

    @PostMapping
    public ResponseEntity<Indicacao> salvarInidicacao(
            @RequestBody @Valid IndicacaoInclusaoModeloAPI indicacaoInclusaoModeloAPI,
            UriComponentsBuilder uriComponentsBuilder) {
        final Indicacao indicacaoSalva = this.indicacaoServico.salvarIndicacao(indicacaoInclusaoModeloAPI);
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("identificador", indicacaoSalva.getIdentificador());
        URI uri = uriComponentsBuilder.path("/indicacoes/{identificador}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(indicacaoSalva);
    }

}
