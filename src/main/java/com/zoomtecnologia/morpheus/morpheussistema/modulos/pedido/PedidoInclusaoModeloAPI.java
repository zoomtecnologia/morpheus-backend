package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PedidoInclusaoModeloAPI {

    @Size(max = 14)
    @NotBlank
    private String documentoRepresentante;

    @NotNull
    private ClientePedidoModeloAPI cliente;

    @NotNull
    private Integer tipoComissao;

    @NotNull
    private Double valorMensalidade;
    
    @NotNull
    private Double total;
    
    private String obs;

    public String getDocumentoRepresentante() {
        return documentoRepresentante;
    }

    public void setDocumentoRepresentante(String documentoRepresentante) {
        this.documentoRepresentante = documentoRepresentante;
    }

    public ClientePedidoModeloAPI getCliente() {
        return cliente;
    }

    public void setCliente(ClientePedidoModeloAPI cliente) {
        this.cliente = cliente;
    }

    public Integer getTipoComissao() {
        return tipoComissao;
    }

    public void setTipoComissao(Integer tipoComissao) {
        this.tipoComissao = tipoComissao;
    }

    public Double getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(Double valorMensalidade) {
        this.valorMensalidade = valorMensalidade;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    @Override
    public String toString() {
        return "PedidoInclusaoModeloAPI{" + "documentoRepresentante=" + documentoRepresentante + ", cliente=" + cliente + ", tipoComissao=" + tipoComissao + ", valorMensalidade=" + valorMensalidade + ", total=" + total + ", obs=" + obs + '}';
    }
    
    
}
