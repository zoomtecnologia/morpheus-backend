/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


public class UsuarioRecuperarSenha {
    
    @NotBlank
    @Email
    private String email;
    
    private String senha;

    private String confirmaSenha;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getConfirmaSenha() {
        return confirmaSenha;
    }

    public void setConfirmaSenha(String confirmaSenha) {
        this.confirmaSenha = confirmaSenha;
    }

    @Override
    public String toString() {
        return "UsuarioRecuperarSenha{" + "email=" + email + ", senha=" + senha + ", confirmaSenha=" + confirmaSenha + '}';
    }
    
}
