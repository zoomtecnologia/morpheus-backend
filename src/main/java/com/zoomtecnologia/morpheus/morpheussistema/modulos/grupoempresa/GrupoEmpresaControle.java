package com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa;

import com.zoomtecnologia.morpheus.morpheussistema.generico.GenericoControle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("grupo-empresas")
public class GrupoEmpresaControle extends GenericoControle<GrupoEmpresa, String> {

    @Autowired
    private GrupoEmpresaServico grupoEmpresaServico;
    
    @Override
    public Map<String, Object> getParametros(GrupoEmpresa grupoEmpresa) {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codigo", grupoEmpresa.getCodigo());
        return parametros;
    }

    @Override
    public String getURI() {
        return "grupo-empresas/{codigo}";
    }
    

    @GetMapping("/todos")
    public List<GrupoEmpresa> listarTodos(@RequestParam(value = "pesquisa", required = false, defaultValue = "") String pesquisa, @RequestParam("representante") String representante) {
        return this.grupoEmpresaServico.listarTodos(pesquisa, representante);
    }

}
