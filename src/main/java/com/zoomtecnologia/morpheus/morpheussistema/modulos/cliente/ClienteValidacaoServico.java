package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.ChaveDesserializacao;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDV;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDVServico;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ClienteValidacaoServico {

    private final PDVServico pdvServico;
    private final ClienteServico clienteServico;
    public ClienteValidacao validarChavePDV(String cnpj, int numeroPDV) {
        final Cliente cliente = this.clienteServico.buscarClientePorCNPJ(cnpj);
        final PDV pdv = this.pdvServico.buscarPDV(cnpj, cliente.getCodigoRepresentante(), numeroPDV);
        final ClienteValidacao clienteValidacao = new ClienteValidacao();
        clienteValidacao.setClientePDV(cliente, pdv);
        return clienteValidacao;
    }

}
