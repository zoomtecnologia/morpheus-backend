package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa.GrupoEmpresa;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class PDVServico {

    private final PDVRepositorio pdvRepositorio;
    private final MessageSourceUtil messageSourceUtil;

    public List<PDVListaModeloAPI> listarPDVs(String cnpj, String codigoRepresentante) {
        return this.pdvRepositorio.listarPDVs(cnpj, codigoRepresentante);
    }

    public PDV buscarPDV(String cnpj, String codigoRepresentante, int numeroPDV) {
        final Optional<PDV> pdvOptional = this.pdvRepositorio.buscarPDV(cnpj, codigoRepresentante, numeroPDV);
        if (pdvOptional.isEmpty()) {
            this.messageSourceUtil.recursoNaoExiste("pdv.nao-existe", "pdv.nao-existe-detalhes");
        }
        return pdvOptional.get();
    }


    public void salvarPdvs(Cliente cliente, int quantidadeDePDVs) {
        if (quantidadeDePDVs == 0) return;
        final List<PDV> pdvs = new ArrayList<>(quantidadeDePDVs);
        for (int i = 1; i <= quantidadeDePDVs; i++) {
            final String chavePDV = cliente.gerarChaveComNumeroCaixa(i);
            final PDV pdv = new PDV(
                    i,
                    cliente.getCnpj(),
                    cliente.getCodigoRepresentante(),
                    chavePDV);
            pdvs.add(pdv);
        }
        this.pdvRepositorio.salvarPDVs(pdvs);
    }

    public void salvarPdvsMorpheusEZion(Cliente cliente, int quantidadeDePDVs) {
        if (quantidadeDePDVs == 0) return;
        final List<PDV> pdvs = new ArrayList<>(quantidadeDePDVs);
        for (int i = 1; i <= quantidadeDePDVs; i++) {
            final String chavePDV = cliente.gerarChaveComNumeroCaixa(i);
            final PDV pdv = new PDV(
                    i,
                    cliente.getCnpj(),
                    cliente.getCodigoRepresentante(),
                    chavePDV);
            pdvs.add(pdv);
        }
        this.pdvRepositorio.salvarPDVs(pdvs);
        this.pdvRepositorio.atualizarPDVsNoZion(pdvs, cliente);
    }

    public void excluirPDVs(String cnpj, String codigoRepresentante) {
        this.pdvRepositorio.excluirPDVPorCliente(cnpj, codigoRepresentante);
    }

    @Transactional
    public void atualizarPDV(PDV pdv, Cliente cliente) {
        this.pdvRepositorio.save(pdv);
        this.pdvRepositorio.atualizarPDVsNoZion(List.of(pdv), cliente);
    }


}
