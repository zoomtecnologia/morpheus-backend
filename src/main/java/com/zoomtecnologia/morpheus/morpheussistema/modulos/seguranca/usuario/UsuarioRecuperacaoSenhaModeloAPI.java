package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

public final class UsuarioRecuperacaoSenhaModeloAPI {

    private final String email;

    private final String status;

    private String recuperandoSenha;

    public UsuarioRecuperacaoSenhaModeloAPI(String email, String status) {
        this.email = email;
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public String getRecuperandoSenha() {
        return recuperandoSenha;
    }

    public void setRecuperandoSenha(String recuperandoSenha) {
        this.recuperandoSenha = recuperandoSenha;
    }

    @Override
    public String toString() {
        return "UsuarioRecuperacaoSenhaModeloAPI{" + "email=" + email + ", status=" + status + ", recuperandoSenha=" + recuperandoSenha + '}';
    }

}
