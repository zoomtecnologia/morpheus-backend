package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public abstract class RepresentanteGenericoModeloAPI {

    @Size(max = 50)
    @NotBlank
    private String nome;

    @Size(max = 50)
    private String nomeSocial;

    @Size(max = 60)
    @NotBlank
    private String logradouro;

    @Size(max = 10)
    @NotBlank
    private String numero;

    @Size(max = 60)
    private String complemento;

    @Size(max = 60)
    @NotBlank
    private String bairro;

    @Size(max = 60)
    @NotBlank
    private String cidade;

    @Size(max = 2)
    @NotBlank
    private String estado;

    @Size(max = 8)
    @NotBlank
    private String cep;

    @Size(max = 50)
    private String nomeContato;

    @Size(max = 13)
    private String telefone;

    @Size(max = 13)
    @NotBlank
    private String celular;

    @Size(max = 13)
    private String whatsapp;

    @Size(max = 120)
    @NotBlank
    private String email;

    @Size(max = 1)
    @NotBlank
    private String tipo;

    @NotNull
    private Integer diaPagamentoComissao;

    @NotNull
    private Integer diasToleranciaPagamentoComissao;

    @Size(max = 20)
    @NotBlank
    private String usuarioAtualizacao;

    @Size(max = 1)
    @NotBlank
    private String nivel;

    private String obs;

    @Size(max = 20)
    @NotBlank
    private String banco;

    @Size(max = 15)
    @NotBlank
    private String agencia;

    @Size(max = 15)
    @NotBlank
    private String conta;

    @Size(max = 3)
    @NotBlank
    private String tipoConta;

    @Size(max = 14)
    @NotBlank
    private String documentoPessoaConta;

    @Size(max = 60)
    @NotBlank
    private String nomePessoaConta;

    @Size(max = 120)
    private String chavePix;
    
    private Boolean geraReceita;

    public Boolean getGeraReceita() {
        return geraReceita;
    }

    public void setGeraReceita(Boolean geraReceita) {
        this.geraReceita = geraReceita;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeSocial() {
        return nomeSocial;
    }

    public void setNomeSocial(String nomeSocial) {
        this.nomeSocial = nomeSocial;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNomeContato() {
        return nomeContato;
    }

    public void setNomeContato(String nomeContato) {
        this.nomeContato = nomeContato;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getDiaPagamentoComissao() {
        return diaPagamentoComissao;
    }

    public void setDiaPagamentoComissao(Integer diaPagamentoComissao) {
        this.diaPagamentoComissao = diaPagamentoComissao;
    }

    public Integer getDiasToleranciaPagamentoComissao() {
        return diasToleranciaPagamentoComissao;
    }

    public void setDiasToleranciaPagamentoComissao(Integer diasToleranciaPagamentoComissao) {
        this.diasToleranciaPagamentoComissao = diasToleranciaPagamentoComissao;
    }

    public String getUsuarioAtualizacao() {
        return usuarioAtualizacao;
    }

    public void setUsuarioAtualizacao(String usuarioAtualizacao) {
        this.usuarioAtualizacao = usuarioAtualizacao;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(String tipoConta) {
        this.tipoConta = tipoConta;
    }

    public String getDocumentoPessoaConta() {
        return documentoPessoaConta;
    }

    public void setDocumentoPessoaConta(String documentoPessoaConta) {
        this.documentoPessoaConta = documentoPessoaConta;
    }

    public String getNomePessoaConta() {
        return nomePessoaConta;
    }

    public void setNomePessoaConta(String nomePessoaConta) {
        this.nomePessoaConta = nomePessoaConta;
    }

    public String getChavePix() {
        return chavePix;
    }

    public void setChavePix(String chavePix) {
        this.chavePix = chavePix;
    }

    @Override
    public String toString() {
        return "RepresentanteGenericoModeloAPI{" + "nome=" + nome + ", nomeSocial=" + nomeSocial + ", logradouro=" + logradouro + ", numero=" + numero + ", complemento=" + complemento + ", bairro=" + bairro + ", cidade=" + cidade + ", estado=" + estado + ", cep=" + cep + ", nomeContato=" + nomeContato + ", telefone=" + telefone + ", celular=" + celular + ", whatsapp=" + whatsapp + ", email=" + email + ", tipo=" + tipo + ", diaPagamentoComissao=" + diaPagamentoComissao + ", diasToleranciaPagamentoComissao=" + diasToleranciaPagamentoComissao + ", usuarioAtualizacao=" + usuarioAtualizacao + ", nivel=" + nivel + ", obs=" + obs + ", banco=" + banco + ", agencia=" + agencia + ", conta=" + conta + ", tipoConta=" + tipoConta + ", documentoPessoaConta=" + documentoPessoaConta + ", nomePessoaConta=" + nomePessoaConta + ", chavePix=" + chavePix + ", geraReceita=" + geraReceita + '}';
    }
}
