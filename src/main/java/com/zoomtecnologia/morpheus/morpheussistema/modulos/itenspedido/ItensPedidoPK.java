package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.item.Item;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Embeddable
public class ItensPedidoPK implements Serializable {
    
    @Column(name="pedido")
    private Integer pedido;
    
    @ManyToOne
    @JoinColumn(name="codigo_item", nullable = false)
    private Item item;

    public Integer getPedido() {
        return pedido;
    }

    public void setPedido(Integer pedido) {
        this.pedido = pedido;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.pedido);
        hash = 23 * hash + Objects.hashCode(this.item);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItensPedidoPK other = (ItensPedidoPK) obj;
        if (!Objects.equals(this.pedido, other.pedido)) {
            return false;
        }
        return Objects.equals(this.item, other.item);
    }

    @Override
    public String toString() {
        return "ItensPedidoPK{" + "pedido=" + pedido + ", item=" + item + '}';
    }

    
}
