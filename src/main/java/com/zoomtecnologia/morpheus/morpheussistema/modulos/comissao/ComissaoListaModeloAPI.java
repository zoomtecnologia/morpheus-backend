package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import com.fasterxml.jackson.annotation.JsonIgnore;

public final class ComissaoListaModeloAPI {
    
    private final Integer codigo;    
    private final String razaoSocial;
    private final String tipo;
    private final String data;
    private final Double valor;
    private final Double valorMensalidade;
    private final Double valorInstalacao;
    private final String status;

    public ComissaoListaModeloAPI(Integer codigo, String razaoSocial, String tipo, String data, Double valor, Double valorMensalidade, Double valorInstalacao, String status) {
        this.codigo = codigo;
        this.razaoSocial = razaoSocial;
        this.tipo = tipo;
        this.data = data;
        this.valor = valor;
        this.valorMensalidade = valorMensalidade;
        this.valorInstalacao = valorInstalacao;
        this.status = status;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public String getTipo() {
        return tipo;
    }

    public String getData() {
        return data;
    }

    public Double getValor() {
        return valor;
    }

    public Double getValorMensalidade() {
        return valorMensalidade;
    }

    public Double getValorInstalacao() {
        return valorInstalacao;
    }

    public String getStatus() {
        return status;
    }
    
    @JsonIgnore
    public double getValorReferente(){
        return "MENSALIDADE".equals(this.tipo) ? this.valorMensalidade : this.valorInstalacao;
    }

    @Override
    public String toString() {
        return "ComissaoListaModeloAPI{" + "codigo=" + codigo + ", razaoSocial=" + razaoSocial + ", tipo=" + tipo + ", data=" + data + ", valor=" + valor + ", valorMensalidade=" + valorMensalidade + ", valorInstalacao=" + valorInstalacao + ", status=" + status + '}';
    }

    

}
