package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("permissoes")
@CrossOrigin
public class PermissaoControle {

    @Autowired
    private PermissaoServico permissaoServico;
    
    @GetMapping
    public List<?> listarPermissoes(@RequestParam(value = "resumo",required = false,defaultValue = "false") boolean resumida) {
        if(resumida){
            return this.permissaoServico.listarPermissaoResumida();
        }
        return this.permissaoServico.listarPermissoes();
    }

    @GetMapping("{codigo}")
    public ResponseEntity<Permissao> buscarPermissaoPorCodigo(@PathVariable("codigo") Integer codigo) {
        return ResponseEntity.ok(this.permissaoServico.buscarPermissaoPorCodigo(codigo));
    }

    @PostMapping
    public ResponseEntity<Permissao> salvarPermissao(@RequestBody @Valid Permissao permissao, UriComponentsBuilder uriComponentsBuilder) {
        Permissao permissaoSalva = this.permissaoServico.salvarPermissao(permissao);
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codigo", permissaoSalva.getCodigo());
        URI uri = uriComponentsBuilder.path("/permissoes/{codigo}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(permissaoSalva);
    }

    @PutMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarPermissao(@PathVariable("codigo") Integer codigo,@RequestBody @Valid Permissao permissao) {
        this.permissaoServico.alterarPermissao(codigo, permissao);
    }

    @DeleteMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable("codigo") Integer codigo) {
        this.permissaoServico.excluir(codigo);
    }

}
