package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ComissaoAlteracaoStatusModeloAPI {

    @NotNull
    private Integer codigo;
    
    @NotBlank
    private String Status;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    @Override
    public String toString() {
        return "ComissaoAlteracaoStatusModeloAPI{" + "codigo=" + codigo + ", Status=" + Status + '}';
    }
    
    
}
