package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@AllArgsConstructor
public class ClienteRetornoModeloAPI {

    private final String cnpj;
    private final String razaoSocial;
    private final String nomeFantasia;
    private final String celular;
    private final String email;
    private final String aceiteContrato;
    private final Integer diaVencimento;
    private final LocalDate dataCadastro;
    private final LocalDate dataProximoVencimento;
    private final String statusAtividade;
    private final String statusPagamento;
    private final String statusPedido;
    private final String logradouro;
    private final String numero;
    private final String bairro;
    private final String cidade;
    private final String estado;
    private final String cep;    
    private final String documentoRepresentante;    
    private final String nomeRepresentante;
    private final TipoRetaguarda retaguarda;
}
