package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class ClienteRepositorioQueryImpl implements ClienteRepositorioQuery {

    @PersistenceContext
    private EntityManager entityManager;
    private StringBuilder sql;

    @Override
    public Page<ClienteRetornoModeloAPI> listarClientes(String pesquisa, String representante, Pageable pageable) {
        final TypedQuery<ClienteRetornoModeloAPI> query = this.entityManager.createQuery(this.montarSqlListarClientes(), ClienteRetornoModeloAPI.class);
        query.setParameter("pesquisa", "%" + pesquisa + "%");
        query.setParameter("representante", "%" + representante + "%");
        this.adicionarRestricoesDePaginacao(query, pageable);
        final List<ClienteRetornoModeloAPI> clientes = query.getResultList();
        long quantidadeClientes = clientes.size();
        if (quantidadeClientes >= pageable.getPageSize()) {
            quantidadeClientes = this.getQuantidadeClientes(pesquisa, representante);
        }
        return new PageImpl(clientes, pageable, quantidadeClientes);
    }

    @Override
    public Page<ClienteRetornoModeloAPI> filtrarClientes(ClienteFiltro clienteFiltro, Pageable pageable) {
        this.sql = null;
        this.sql = new StringBuilder();
        this.sql
                .append("select ")
                .append(ClienteRepositorio.INSTANCIA_CLIENTE_RETORNO_MODELO)
                .append("(")
                .append("cliente.cnpj,")
                .append("cliente.razaoSocial,")
                .append("cliente.nomeFantasia,")
                .append("cliente.celular,")
                .append("cliente.email,")
                .append("cliente.aceiteContrato,")
                .append("cliente.diaVencimento,")
                .append("cliente.dataCadastro,")
                .append("cliente.dataProximoVencimento,")
                .append("cliente.statusAtividade,")
                .append("cliente.statusPagamento,")
                .append("cliente.statusPedido,")
                .append("cliente.endereco.logradouro,")
                .append("cliente.endereco.numero,")
                .append("cliente.endereco.bairro,")
                .append("cliente.endereco.cidade,")
                .append("cliente.endereco.estado,")
                .append("cliente.endereco.cep,")
                .append("r.documento,")
                .append("r.nome,")
                .append("cliente.retaguarda")
                .append(")")
                .append(" from Cliente cliente left outer join Representante r on(r.documento=cliente.codigoRepresentante) ")
                .append(" where ");
        this.adicionarCondicao(clienteFiltro);
        final TypedQuery<ClienteRetornoModeloAPI> typeQuery = this.entityManager.createQuery(this.sql.toString(),
                ClienteRetornoModeloAPI.class);
        this.adicionarRestricoesDePaginacao(typeQuery, pageable);
        final List<ClienteRetornoModeloAPI> clientes = typeQuery.getResultList();
        final long quantidadeClientePorPaginacao = this.getElementSize(clienteFiltro);
        return new PageImpl<>(clientes, pageable, quantidadeClientePorPaginacao);
    }

    private Long getElementSize(ClienteFiltro clienteFiltro) {
        this.sql = null;
        this.sql = new StringBuilder();
        this.sql.append(
                "select count(*) from Cliente cliente left outer join Representante r on(r.documento=cliente.codigoRepresentante) ")
                .append(" where ");
        this.adicionarCondicao(clienteFiltro);
        TypedQuery<Long> query = this.entityManager.createQuery(this.sql.toString(), Long.class);
        return query.getSingleResult();
    }

    private Long getQuantidadeClientes(String pesquisa, String representante) {
        final TypedQuery<Long> query = this.entityManager.createQuery(this.montarSqlContagemClientes(), Long.class);
        query.setParameter("pesquisa", "%" + pesquisa + "%");
        query.setParameter("representante", "%" + representante + "%");
        return query.getSingleResult();
    }

    private void adicionarRestricoesDePaginacao(TypedQuery<ClienteRetornoModeloAPI> query, Pageable pageable) {
        int paginaAtual = pageable.getPageNumber();
        int totalDeRegistroPorPagina = pageable.getPageSize();
        int primeiroRegistroDaPagina = paginaAtual * totalDeRegistroPorPagina;
        query.setFirstResult(primeiroRegistroDaPagina);
        query.setMaxResults(totalDeRegistroPorPagina);
    }

    private void adicionarCondicao(ClienteFiltro clienteFiltro) {
        int count = 0;
        if (Objects.nonNull(clienteFiltro.getCnpj())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(").append("cnpj")
                    .append(") like '%").append(clienteFiltro.getCnpj()).append("%'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getRegimeTributario())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("regime_tributario").append(") = '").append(clienteFiltro.getRegimeTributario())
                    .append("'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getSeguimento())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(").append("seguimento").append(") = '").append(clienteFiltro.getSeguimento())
                    .append("'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getContingencia())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("contingencia").append(") = '").append(clienteFiltro.getContingencia()).append("'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getDiaVencimento())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("dia_vencimento").append(") = '").append(clienteFiltro.getDiaVencimento()).append("'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getStatusAtividade())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("status_atividade").append(") = '").append(clienteFiltro.getStatusAtividade()).append("'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getStatusPagamento())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("status_pagamento").append(") = '").append(clienteFiltro.getStatusPagamento()).append("'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getCodigoRepresentante())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("codigo_representante").append(") = '").append(clienteFiltro.getCodigoRepresentante()).append("'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getStatusPedido())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("status_pedido").append(") like '").append(clienteFiltro.getStatusPedido()).append("%'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getCodigoGrupoEmpresa())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("codigo_grupo_empresa").append(") like '").append(clienteFiltro.getCodigoGrupoEmpresa())
                    .append("%'");
            count++;
        }
        if (Objects.nonNull(clienteFiltro.getAceiteContrato())) {
            this.adicionarCriterio(count);
            this.sql.append("upper(")
                    .append("aceite_contrato").append(") like '").append(clienteFiltro.getAceiteContrato())
                    .append("%'");
            count++;
        }
    }

    private void adicionarCriterio(int count) {
        if (count > 0) {
            this.sql.append(" and ");
        }
    }

    private String montarSqlContagemClientes() {
        final String sql = "select "
                + "count(cliente.cnpj) "
                + "from Cliente cliente "
                + "left outer join "
                + " Representante r on(r.documento=cliente.codigoRepresentante) "
                + "where "
                + "(cliente.cnpj like :pesquisa "
                + "or cliente.razaoSocial like :pesquisa "
                + "or cliente.nomeFantasia like :pesquisa "
                + "or cliente.seguimento like :pesquisa "
                + "or cliente.celular like :pesquisa "
                + ") "
                + "and cliente.codigoRepresentante like :representante";
        return sql;
    }

    private String montarSqlListarClientes() {
        final String sql = "select " + ClienteRepositorio.INSTANCIA_CLIENTE_RETORNO_MODELO + "("
                + "cliente.cnpj,"
                + "cliente.razaoSocial,"
                + "cliente.nomeFantasia,"
                + "cliente.celular,"
                + "cliente.email,"
                + "cliente.aceiteContrato,"
                + "cliente.diaVencimento,"
                + "cliente.dataCadastro,"
                + "cliente.dataProximoVencimento,"
                + "cliente.statusAtividade,"
                + "cliente.statusPagamento,"
                + "cliente.statusPedido,"
                + "cliente.endereco.logradouro,"
                + "cliente.endereco.numero,"
                + "cliente.endereco.bairro,"
                + "cliente.endereco.cidade,"
                + "cliente.endereco.estado,"
                + "cliente.endereco.cep,"
                + "r.documento,"
                + "r.nome,"
                + "cliente.retaguarda"
                + ") "
                + "from Cliente cliente "
                + "left outer join "
                + " Representante r on(r.documento=cliente.codigoRepresentante) "
                + "where "
                + "(cliente.cnpj like :pesquisa "
                + "or cliente.razaoSocial like :pesquisa "
                + "or cliente.nomeFantasia like :pesquisa "
                + "or cliente.seguimento like :pesquisa "
                + "or cliente.celular like :pesquisa "
                + ") "
                + "and cliente.codigoRepresentante like :representante";
        return sql;
    }

}
