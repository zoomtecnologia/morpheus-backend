package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.aplicacao;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("aplicacoes")
@CrossOrigin
public class AplicacaoControle {

    @Autowired
    private AplicacaoServico aplicacaoServico;

    @GetMapping
    public List<Aplicacao> listarAplicacoes() {
        return this.aplicacaoServico.listarAplicacoes();
    }

    @GetMapping("{codigo}")
    public ResponseEntity<Aplicacao> buscarAplicacaoPorCodigo(@PathVariable("codigo") Integer codigo) {
        Aplicacao aplicacao = this.aplicacaoServico.buscarAplicacaoPorCodigo(codigo);
        return ResponseEntity.ok(aplicacao);
    }

    @PostMapping
    public ResponseEntity<Aplicacao> salvarAplicacao(@RequestBody @Valid AplicacaoModeloAPI aplicacao, UriComponentsBuilder uriComponentsBuilder) {
        Aplicacao aplicacaoSalva = this.aplicacaoServico.salvarAplicacao(aplicacao);
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codigo", aplicacaoSalva.getCodigo());
        URI uri = uriComponentsBuilder.path("/aplicacoes/{codigo}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(aplicacaoSalva);
    }

    @PutMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarAplicacao(@PathVariable("codigo") Integer codigo,@RequestBody @Valid AplicacaoModeloAPI aplicacaoInclusaoModeloAPI) {
        this.aplicacaoServico.alterarAplicacao(codigo,aplicacaoInclusaoModeloAPI);
    }
    
    @DeleteMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable("codigo") Integer codigo) {
        this.aplicacaoServico.excluirAplicacao(codigo);
    }
    
    
}
