package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("pedidos")
@CrossOrigin
public class PedidoControle {

    @Autowired
    private PedidoServico pedidoServico;

    @GetMapping
    public Page<PedidoRetornoModeloAPI> listarTodosPaginado(Pageable pageable,
            @RequestParam(value = "pesquisa", required = false, defaultValue = "") String conteudoPesquisa) {
        return this.pedidoServico.listarPedidosPaginado(conteudoPesquisa, pageable);
    }

    @GetMapping("{numero}")
    public ResponseEntity<Pedido> buscarPorNumero(@PathVariable("numero") Integer numeroPedido) {
        Pedido pedidoEncontrado = this.pedidoServico.buscarPedidoPorNumero(numeroPedido);
        return ResponseEntity.ok(pedidoEncontrado);
    }

    @GetMapping("status-pedido")
    public List<PedidoStatusModeloAPI> listarStatusPedido() {
        return this.pedidoServico.listarStatusPedidos();
    }

    @GetMapping("cliente/{cnpj}")
    public ResponseEntity<PedidoRetornoModeloAPI> buscarPorPedidoPorCliente(@PathVariable("cnpj") String cnpj) {
        return ResponseEntity.ok(this.pedidoServico.buscarPedidoResumidoPorCliente(cnpj));
    }

    @GetMapping(path = "{numero}/contrato", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> gerarContratoCliente(@PathVariable("numero") Integer numero) {
        return ResponseEntity.ok(this.pedidoServico.gerarContratoPorNumeroPedido(numero));
    }

    @GetMapping(path = "{cnpj}/visualizar-contrato-cliente", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> visualizarContratoCliente(@PathVariable("cnpj") String cnpj) {
        return ResponseEntity.ok(this.pedidoServico.gerarContratoPorCliente(cnpj));
    }

    @PostMapping
    public ResponseEntity<Pedido> salvar(@RequestBody @Valid PedidoInclusaoModeloAPI pedidoInclusaoModeloAPI,
            UriComponentsBuilder uriComponentsBuilder) {
        Pedido pedidoSalvo = this.pedidoServico.salvarPedido(pedidoInclusaoModeloAPI);
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("numero", pedidoSalvo.getNumero());
        URI uri = uriComponentsBuilder.path("pedidos/{numero}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(pedidoSalvo);
    }

    @PutMapping("{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Pedido> alterar(@PathVariable("numero") Integer numeroPedido,
            @RequestBody @Valid PedidoAlteracaoModeloAPI pedidoAlteracaoModeloAPI) {
        Pedido pedido = this.pedidoServico.alterarPedido(numeroPedido, pedidoAlteracaoModeloAPI);
        return ResponseEntity.ok(pedido);
    }

    @PutMapping("{numero}/status-pedido")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarStatusPedido(@PathVariable("numero") Integer numeroPedido,
            @RequestBody String status) {
        PedidoStatus pedidoStatus = PedidoStatus.valueOf(status);
        this.pedidoServico.alterarStatusPedido(pedidoStatus, numeroPedido);
    }

    @PutMapping("{cliente}/status-pedido-por-cliente")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarStatusPedido(@PathVariable("cliente") String cliente,
            @RequestBody String status) {
        PedidoStatus pedidoStatus = PedidoStatus.valueOf(status);
        this.pedidoServico.alterarStatusPedido(pedidoStatus, cliente);
    }

    @PostMapping("{numeroPedido}/enviar-contrato")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enviarContratoClientePorPedido(@PathVariable("numeroPedido") Integer numeroPedido) {
        this.pedidoServico.enviarContratoPorNumeroPedido(numeroPedido);
    }

    @PostMapping("{cnpj}/reenviar-contrato")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void reenviarContrato(@PathVariable("cnpj") String cnpj, @RequestBody @Valid EmailContratoModeloAPI emailContratoModeloAPI) {
        this.pedidoServico.reenviarContrato(cnpj, emailContratoModeloAPI);
    }

    @PostMapping("{numeroPedido}/enviar-email-status-pedido")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enviarEmailDeMudancaDeStatusPedidoParaCliente(@PathVariable("numeroPedido") Integer numeroPedido) {
        this.pedidoServico.enviarEmailDeMudancaDeStatusPedidoParaCliente(numeroPedido);
    }

}
