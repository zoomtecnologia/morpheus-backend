package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface UsuarioResumidoModeloAPI {

    Integer getCodigo();

    String getNome();

    String getEmail();

    String getStatus();

    String getSenha();

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate getDataCadastro();

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate getDataRecuepracaoSenha();

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate getDataAcesso();

    PermissaoResumida getPermissao();

    RepresentanteResumido getRepresentante();

    interface PermissaoResumida {

        Integer getCodigo();
        
        String getNome();
    }

    interface RepresentanteResumido {

        String getDocumento();

        String getNome();
    }
}
