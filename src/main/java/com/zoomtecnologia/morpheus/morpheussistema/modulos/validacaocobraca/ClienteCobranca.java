package com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca;

public class ClienteCobranca {

    private String name;
    private String email;
    EnderecoClienteCobranca address;
    private String document;
    private String phone_number;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public EnderecoClienteCobranca getAddress() {
        return address;
    }

    public String getDocument() {
        return document;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(EnderecoClienteCobranca address) {
        this.address = address;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @Override
    public String toString() {
        return "ClienteCobranca{" + "name=" + name + ", email=" + email + ", address=" + address + ", document=" + document + ", phone_number=" + phone_number + '}';
    }

}
