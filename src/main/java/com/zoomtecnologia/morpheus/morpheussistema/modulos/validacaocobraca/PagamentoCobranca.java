package com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca;

public class PagamentoCobranca {

    private CarneCobranca carnet;
    private String method;
    private BoletoBancarioCobranca banking_billet;

    public CarneCobranca getCarnet() {
        return carnet;
    }

    public String getMethod() {
        return method;
    }

    public BoletoBancarioCobranca getBanking_billet() {
        return banking_billet;
    }

    public void setCarnet(CarneCobranca carnet) {
        this.carnet = carnet;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setBanking_billet(BoletoBancarioCobranca banking_billet) {
        this.banking_billet = banking_billet;
    }

    @Override
    public String toString() {
        return "PagamentoCobranca{" + "carnet=" + carnet + ", method=" + method + ", banking_billet=" + banking_billet + '}';
    }

}
