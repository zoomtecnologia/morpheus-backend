package com.zoomtecnologia.morpheus.morpheussistema.modulos.pix;

import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PayloadPixModeloAPI {

    @NotBlank
    private String chavePix;
    @NotBlank
    private String nomeTitular;
    @NotBlank
    private String cidadeTitular;
    @NotNull
    private BigDecimal totalSolicitacao;
    @NotBlank
    private String codigoSolicitacao;

    public String getChavePix() {
        return chavePix;
    }

    public String getNomeTitular() {
        return nomeTitular;
    }

    public String getCidadeTitular() {
        return cidadeTitular;
    }

    public BigDecimal getTotalSolicitacao() {
        return totalSolicitacao;
    }

    public String getCodigoSolicitacao() {
        return codigoSolicitacao;
    }

    public void setChavePix(String chavePix) {
        this.chavePix = chavePix;
    }

    public void setNomeTitular(String nomeTitular) {
        this.nomeTitular = nomeTitular;
    }

    public void setCidadeTitular(String cidadeTitular) {
        this.cidadeTitular = cidadeTitular;
    }

    public void setTotalSolicitacao(BigDecimal totalSolicitacao) {
        this.totalSolicitacao = totalSolicitacao;
    }

    public void setCodigoSolicitacao(String codigoSolicitacao) {
        this.codigoSolicitacao = codigoSolicitacao;
    }

    @Override
    public String toString() {
        return "PayloadPixModeloAPI{" + "chavePix=" + chavePix + ", nomeTitular=" + nomeTitular + ", cidadeTitular=" + cidadeTitular + ", totalSolicitacao=" + totalSolicitacao + ", codigoSolicitacao=" + codigoSolicitacao + '}';
    }
}
