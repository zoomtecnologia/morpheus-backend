package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ZionPlanosContratacaoRepositorioQueryImpl implements ZionPlanosContratacaoRepositorioQuery{

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public void salvar(List<ZionPlanosContratacao> planosContratacao) {
        if (planosContratacao == null || planosContratacao.isEmpty()) {
            return;
        }
        final String cabecalhoInsert = "insert into zion_planos_contratacao(codigo_contratacao,codigo_plano,status,data_cadastro,usuario_contratacao) values ";
        String conteudo = "";
        for (ZionPlanosContratacao zionPlanoContratacao : planosContratacao) {
            conteudo += ",(";
            conteudo += "'" + zionPlanoContratacao.getId().getCodigoContratacao() + "',";
            conteudo += "'" + zionPlanoContratacao.getId().getCodigoPlano() + "',";
            conteudo += "" + (zionPlanoContratacao.isStatus()?1:0) + " ,";
            conteudo += "'" + zionPlanoContratacao.getDataCadastro() + "',";
            conteudo += "'" + zionPlanoContratacao.getUsuarioContratacao() + "'";
            conteudo += ")";
        }
        final String atualizacao = " ON DUPLICATE KEY UPDATE codigo_plano = values(codigo_plano), status = values(status);";
        final String sql = cabecalhoInsert + conteudo.replaceFirst(",", "") + atualizacao;
        this.entityManager.createNativeQuery(sql).executeUpdate();
    }
    
}
