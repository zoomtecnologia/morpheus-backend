package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao;


public interface ZionPlanoPrincipal {
    public Integer getDiasParaTeste();
    public Boolean getTeste();
    public Boolean getGeraCobranca();
}
