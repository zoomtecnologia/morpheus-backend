package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import java.io.IOException;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("representantes")
@CrossOrigin
public class RepresentanteControle {

    @Autowired
    private RepresentanteServico representanteServico;

    @GetMapping
    public Page<RepresentanteRetornoModeloAPI> listarRepresentantesPaginado(Pageable pageable,
            @RequestParam(value = "pesquisa", required = false, defaultValue = "") String pesquisa) {
        return this.representanteServico.litarTodosPaginado(pesquisa, pageable);
    }

    @GetMapping("/todos")
    public List<?> listarTodos(@RequestParam(value = "resumido", required = false, defaultValue = "false") boolean resumido) {
        if (resumido) {
            return this.representanteServico.listarRepresentantesResumido();
        }
        return this.representanteServico.listarTodos();
    }

    @GetMapping("{documento}")
    public ResponseEntity<Representante> buscarPorDocumento(@PathVariable("documento") String documento) {
        Representante representanteEncontrado = representanteServico.buscarRepresentantePorDocumento(documento);
        return ResponseEntity.ok(representanteEncontrado);
    }

    @PostMapping
    public ResponseEntity<Representante> salvar(@RequestBody @Valid RepresentanteInclusaoModeloAPI representanteInclusaoModeloAPI, UriComponentsBuilder uriComponentsBuilder) throws NoSuchAlgorithmException {
        Representante representanteSalvo = this.representanteServico.salvarRepresentante(representanteInclusaoModeloAPI);
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("documento", representanteSalvo.getDocumento());
        URI uri = uriComponentsBuilder.path("representante/{documento}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(representanteSalvo);
    }

    @PostMapping("revendedores")
    public ResponseEntity<Representante> salvarRevendedor(@RequestBody @Valid RevendedorModeloAPI revendedorModeloAPI, UriComponentsBuilder uriComponentsBuilder) throws NoSuchAlgorithmException {
        final Representante representanteSalvo = this.representanteServico.salvarRevendedor(revendedorModeloAPI);
        final Map<String, Object> parametros = new HashMap<>();
        parametros.put("documento", representanteSalvo.getDocumento());
        final URI uri = uriComponentsBuilder.path("representante/{documento}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(representanteSalvo);
    }

    @PutMapping("{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterar(@RequestBody @Valid RepresentanteAlteracaoModeloAPI representanteAlteracaoModeloAPI, @PathVariable("documento") String documento) {
        this.representanteServico.alterarRepresentante(documento, representanteAlteracaoModeloAPI);
    }

    @DeleteMapping("{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("documento") String documento) {
        this.representanteServico.deletarRepresentante(documento);
    }

    @PostMapping("upload-imagem")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizarFoto(@RequestPart("imagem") MultipartFile multipartFile,
            @RequestParam("documento") String documento) {
        try {
            this.representanteServico.atualizarFoto(documento, multipartFile.getInputStream());
        } catch (IOException ex) {
            System.out.println("ex = " + ex.getMessage());
        }
    }

    @PutMapping("{representante}/bloquear")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void bloquearRepresentante(@PathVariable("representante") String representante, @RequestBody String status) {
        this.representanteServico.bloquearRepresentante(representante, status);
    }

}
