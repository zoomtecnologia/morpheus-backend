package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "zion_planos_aplicacao")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
public class ZionPlanoAplicacoes implements Serializable{
    
    @EmbeddedId
    
	@Column(name = "id")
	private ZionPlanoAplicacoesPK id;
}
