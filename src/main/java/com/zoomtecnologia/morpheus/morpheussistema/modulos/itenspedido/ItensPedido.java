package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "itens_pedido")
public class ItensPedido implements Serializable {

    @EmbeddedId
    
	@Column(name = "itensPedidoPK")
	private ItensPedidoPK itensPedidoPK;
    
    
	@Column(name = "valor")
	private Double valor;

    
	@Column(name = "quantidade")
	private Integer quantidade;
    
    
	@Column(name = "status")
	private String status;

    public ItensPedidoPK getItensPedidoPK() {
        return itensPedidoPK;
    }

    public void setItensPedidoPK(ItensPedidoPK itensPedidoPK) {
        this.itensPedidoPK = itensPedidoPK;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.itensPedidoPK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItensPedido other = (ItensPedido) obj;
        return Objects.equals(this.itensPedidoPK, other.itensPedidoPK);
    }

    @Override
    public String toString() {
        return "ItensPedido{" + "itensPedidoPK=" + itensPedidoPK + ", valor=" + valor + ", quantidade=" + quantidade + ", status=" + status + '}';
    }
    
    
}
