/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDV;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author eudes
 */
@Getter
@Setter
@ToString
public class ClienteValidacao {

    private String cnpj;
    private String codigoRepresentante;
    private String chave;
    private String statusPagamento;
    private String contingencia;
    private String status;
    private LocalDate dataProximoVencimento;
    private LocalDate dataUltimaValidacao;
    private int prazoMaximoProximoVencimento;
    private int diaPagamento;

    public boolean naoUsaContingencia() {
        return this.contingencia != null && this.contingencia.equals("N");
    }

    public boolean usaContingencia() {
        return !this.naoUsaContingencia();
    }

    public void setClientePDV(Cliente cliente, PDV pdv) {
        this.cnpj = cliente.getCnpj();
        this.codigoRepresentante = cliente.getCodigoRepresentante();
        this.statusPagamento = cliente.getStatusPagamento();
        this.contingencia = cliente.getContingencia();
        this.status = cliente.getStatusAtividade();
        this.prazoMaximoProximoVencimento = cliente.getPrazoMaximo();
        this.diaPagamento = cliente.getDiaVencimento();
        this.chave = pdv.getChave();
        this.dataProximoVencimento = cliente.getDataProximoVencimento();
        this.dataUltimaValidacao = cliente.getDataUltimaValidacao();
    }

}
