package com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "grupo_empresa")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"codigo"})
@AllArgsConstructor
@NoArgsConstructor
public class GrupoEmpresa implements Serializable {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    
	@Column(name = "codigo")
	private String codigo;

    @Size(max = 50)
    @NotBlank
    
	@Column(name = "nome")
	private String nome;

    
	@Column(name = "codigo_representante")
	private String codigoRepresentante;

    
	@Column(name = "identificacao_banco_dados")
	private String identificacaoBancoDados;

    public GrupoEmpresa(String codigo) {
        this.codigo = codigo;
    }

    public GrupoEmpresa(String codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

}
