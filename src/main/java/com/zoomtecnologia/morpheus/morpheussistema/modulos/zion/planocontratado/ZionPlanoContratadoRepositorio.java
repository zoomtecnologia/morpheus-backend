package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionPlanoContratadoRepositorio extends JpaRepository<ZionPlanoContratado, String> {

    @Query(value="select * from zion_plano_contratado where documento_identificacao_empresa like %:cliente% "
            + "order by data_fim_contrato desc,status desc"
    ,nativeQuery = true)
    public Page<ZionPlanoContratado> listarContratosPorCliente(String cliente, Pageable pageable);
    
    @Query(value = "SELECT "
            + "aplicacao.codigo AS codigo,"
            + "aplicacao.modulo AS modulo,"
            + "aplicacao.nome AS nome,"
            + "aplicacao.permissoes AS funcao,"
            + "aplicacao.status AS status "
            + "FROM "
            + "zion_plano_contratado contratado "
            + "inner JOIN "
            + "zion_planos_contratacao contratacao ON(contratacao.codigo_contratacao=contratado.codigo) "
            + "inner JOIN "
            + "zion_planos_aplicacao planoAplicacao ON(planoAplicacao.codigo_plano=contratacao.codigo_plano) "
            + "INNER JOIN "
            + "zion_aplicacoes aplicacao ON(aplicacao.codigo=planoAplicacao.codigo_aplicacao AND aplicacao.modulo=planoAplicacao.codigo_modulo) "
            + "WHERE "
            + "contratado.codigo=:codigoContrato", nativeQuery = true)
    public List<ZionAplicacoesContratada> listarAplicacoesContratadas(String codigoContrato);

    @Query("select contrato from ZionPlanoContratado contrato where contrato.documentoIdentificacaoEmpresa=:documentoIdentificacao and contrato.status=true")
    public Optional<ZionPlanoContratado> buscarContratoPorCliente(String documentoIdentificacao);

    @Query(value = "select codigo_empresa   from zion_plano_contratado  where grupo_empresa=:grupoEmpresa and status=1 ORDER by codigo_empresa DESC LIMIT 1", nativeQuery = true)
    public Optional<String> buscarGrupoEmpresa(String grupoEmpresa);

    @Query(value="SELECT "
            + "  cliente.cnpj as cnpj"
            + "  ,cliente.razao_social as razaoSocial"
            + "  ,planoContratado.implantacao as implantacao"
            + "  ,planoContratado.mensalidade as mensalidade"
            + "  ,planoContratado.status as status"
            + "  ,planoContratado.data_inicio_contrato as dataInicioContrato"
            + "  ,cliente.dia_vencimento as diaVencimento"
            + "  ,aplicacoes.modulo as modulo"
            + "  ,aplicacoes.nome as nomeAplicacao"
            + " FROM"
            + "  zion_plano_contratado as planoContratado"
            + " inner join "
            + "  cliente as cliente on(cliente.cnpj=planoContratado.documento_identificacao_empresa)"
            + " inner join "
            + "  zion_planos_contratacao as planoContratacao on(planoContratacao.codigo_contratacao=planoContratado.codigo)"
            + " inner join "
            + "  zion_planos_aplicacao as planos on(planos.codigo_plano=planoContratacao.codigo_plano)"
            + " inner join "
            + "  zion_aplicacoes as aplicacoes on(aplicacoes.codigo=planos.codigo_aplicacao and aplicacoes.modulo=planos.codigo_modulo)"
            + " where"
            + "  planoContratado.codigo=:codigoContrato"
            + " and planoContratado.status=1", nativeQuery = true)
    public List<ZionPlanoContratadoResumido> buscarContrato(String codigoContrato);

    @Modifying
    @Query("update ZionPlanoContratado zionPlanoContratado "
            + "set "
            + "zionPlanoContratado.dataCancelamento=:dataCancelamento, "
            + "zionPlanoContratado.status=false "
            + "where "
            + "zionPlanoContratado.codigo=:codigoContrato")
    public void cancelarContrato(String codigoContrato, LocalDate dataCancelamento);    

}
