package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratadoServico;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planocontratado.ZionPlanoContratadoPadraoServico;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("clientes")
@CrossOrigin("*")
@AllArgsConstructor
public class ClienteControle {

    private final ClienteServico clienteServico;
    private final ZionPlanoContratadoServico zionPlanoContratadoServico;
    private final ZionPlanoContratadoPadraoServico zionPlanoContratadoPadraoServico;

    @GetMapping
    public Page<?> listarTodos(
            Pageable pageable,
            @RequestParam(value = "pesquisa", required = false, defaultValue = "") String conteudoPesquisa,
            @RequestParam(value = "representante") String representante,
            ClienteFiltro clienteFiltro
    ) {
        return this.clienteServico.listarClientes(conteudoPesquisa, representante, pageable, clienteFiltro);
    }

    @GetMapping("seguimentos")
    public List<String> listarSeguimentos() {
        return this.clienteServico.listrarSeguimentos();
    }

    @GetMapping("{cnpj}")
    public ResponseEntity<Cliente> busarPorChave(@PathVariable("cnpj") String numeroCnpj) {
        Cliente clienteEncontrado = this.clienteServico.buscarClientePorCNPJ(numeroCnpj);
        return ResponseEntity.ok(clienteEncontrado);
    }

    @PostMapping
    public ResponseEntity<Cliente> salvar(@RequestBody @Valid ClienteInclusaoModeloAPI clienteInclusaoModeloAPI,
                                          UriComponentsBuilder uriComponentsBuilder) {
        Cliente clienteSalvo = this.clienteServico.salvarCliente(clienteInclusaoModeloAPI);
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("cnpj", clienteSalvo.getCnpj());
        URI uri = uriComponentsBuilder.path("clientes/{cnpj}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(clienteSalvo);
    }

    @PutMapping("{cnpj}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterar(@PathVariable("cnpj") String numeroCnpj, @RequestBody @Valid ClienteAlteracaoModeloAPI clienteAlteracaoModeloAPI) {
        this.clienteServico.alterarCliente(numeroCnpj, clienteAlteracaoModeloAPI);
    }

    @DeleteMapping("{cnpj}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable("cnpj") String numeroCnpj) {
        this.clienteServico.deletarCliente(numeroCnpj);
    }

    @PutMapping("{cnpj}/status-pagamento")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarStatusPagamento(@PathVariable("cnpj") String numeroCnpj, @RequestBody String statusPagamento) {
        this.clienteServico.alterarStatusPagamento(numeroCnpj, statusPagamento);
        this.zionPlanoContratadoServico.alterarStatusPagamento(numeroCnpj, statusPagamento);
        this.zionPlanoContratadoPadraoServico.alterarStatusPagamento(numeroCnpj, statusPagamento);
    }

    @PutMapping("{cnpj}/aceite")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void acreitarContrato(@PathVariable("cnpj") String numeroCnpj) {
        this.clienteServico.aceitarContrato(numeroCnpj);
    }

    @PutMapping("{representante}/bloquear-por-representante")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void bloquearClientePorRepresentante(@PathVariable("representante") String representante, @RequestBody String status) {
        this.clienteServico.bloquearClientePorRepresentante(representante, status);
    }

    @PostMapping("{cnpj}/gerar-senha")
    public ResponseEntity<Cliente> gerarSenha(@PathVariable("cnpj") String cnpj) {
        Cliente cliente = this.clienteServico.gerarSenhaCliente(cnpj);
        return ResponseEntity.ok(cliente);
    }

    @PostMapping("{cnpj}/enviar-email-contrato-aceito")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enviarEmailContratoAceito(@PathVariable("cnpj") String cnpj) {
        this.clienteServico.enviarEmailContratoAceito(cnpj);
    }

}
