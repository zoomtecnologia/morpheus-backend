package com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca;

public class CobrancaModeloAPI {

    private String razaoSocial;
    private String cnpj;
    private String mes;
    private String statusPagamento;

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getMes() {
        return mes;
    }

    public String getStatusPagamento() {
        return statusPagamento;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public void setStatusPagamento(String statusPagamento) {
        this.statusPagamento = statusPagamento;
    }

    @Override
    public String toString() {
        return "CobrancaModeloAPI{" + "razaoSocial=" + razaoSocial + ", cnpj=" + cnpj + ", mes=" + mes + ", statusPagamento=" + statusPagamento + '}';
    }
    
}
