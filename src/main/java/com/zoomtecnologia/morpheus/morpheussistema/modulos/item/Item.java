package com.zoomtecnologia.morpheus.morpheussistema.modulos.item;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;

@Entity
@Table(name = "item")
public class Item {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    
	@Column(name = "codigo")
	private Integer codigo;
    
    @Size(max = 50)
    @NotBlank
    
	@Column(name = "descricao")
	private String descricao;
    
    @NotNull
    
	@Column(name = "valor")
	private Double valor;
    
    @NotNull
    
	@Column(name = "valor_minimo")
	private Double valorMinimo;
    
    @NotBlank
    
	@Column(name = "status")
	private String status;
    
    @NotNull
    
	@Column(name = "item_unico")
	private Boolean itemUnico;
    
    @NotNull
    
	@Column(name = "opcional")
	private Boolean opcional;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(Double valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getItemUnico() {
        return itemUnico;
    }

    public void setItemUnico(Boolean itemUnico) {
        this.itemUnico = itemUnico;
    }

    public Boolean getOpcional() {
        return opcional;
    }

    public void setOpcional(Boolean opcional) {
        this.opcional = opcional;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        return Objects.equals(this.codigo, other.codigo);
    }

    @Override
    public String toString() {
        return "Item{" + "codigo=" + codigo + ", descricao=" + descricao + ", valor=" + valor + ", valorMinimo=" + valorMinimo + ", status=" + status + ", itemUnico=" + itemUnico + ", opcional=" + opcional + '}';
    }
    
    
    
}
