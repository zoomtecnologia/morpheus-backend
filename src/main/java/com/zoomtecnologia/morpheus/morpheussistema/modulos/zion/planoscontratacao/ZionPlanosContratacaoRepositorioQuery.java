package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao;

import java.util.List;

public interface ZionPlanosContratacaoRepositorioQuery {
     public void salvar(List<ZionPlanosContratacao> planosContratacao);
}
