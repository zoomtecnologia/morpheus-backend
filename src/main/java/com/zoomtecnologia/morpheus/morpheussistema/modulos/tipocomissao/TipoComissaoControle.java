package com.zoomtecnologia.morpheus.morpheussistema.modulos.tipocomissao;

import com.zoomtecnologia.morpheus.morpheussistema.generico.GenericoControle;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("tipo-comissoes")
public class TipoComissaoControle extends GenericoControle<TipoComissao, Integer>{

    @Override
    public Map<String, Object> getParametros(TipoComissao entidade) {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codigo", entidade.getCodigo());
        return parametros;
    }

    @Override
    public String getURI() {
        return "/tipo-comissoes/{codigo}";
    }

}
