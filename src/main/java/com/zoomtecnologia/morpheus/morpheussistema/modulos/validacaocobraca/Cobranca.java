package com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca;

public class Cobranca {

    private float total;
    private String status;
    PagamentoCobranca Payment;
    ClienteCobranca customer;
    private float charge_id;
    private String expire_at;
    private String created_at;
    private float paid_value;
    private String charge_method;

    public float getTotal() {
        return total;
    }

    public String getStatus() {
        return status;
    }

    public PagamentoCobranca getPayment() {
        return Payment;
    }

    public ClienteCobranca getCustomer() {
        return customer;
    }

    public float getCharge_id() {
        return charge_id;
    }

    public String getExpire_at() {
        return expire_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public float getPaid_value() {
        return paid_value;
    }

    public String getCharge_method() {
        return charge_method;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPayment(PagamentoCobranca Payment) {
        this.Payment = Payment;
    }

    public void setCustomer(ClienteCobranca customer) {
        this.customer = customer;
    }

    public void setCharge_id(float charge_id) {
        this.charge_id = charge_id;
    }

    public void setExpire_at(String expire_at) {
        this.expire_at = expire_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setPaid_value(float paid_value) {
        this.paid_value = paid_value;
    }

    public void setCharge_method(String charge_method) {
        this.charge_method = charge_method;
    }

    public boolean eCobrancaDeCarne() {
        return this.getCharge_method().equals("carnet");
    }

    public Cobranca converterCarneSemHoraNaDataExpiracao() {
        PagamentoCobranca pagamentoCobranca = this.getPayment();
        CarneCobranca carne = pagamentoCobranca.getCarnet();
        carne.setExpire_at(carne.getExpire_at().substring(0, 10));
        return this;
    }

    @Override
    public String toString() {
        return "Cobranca{" + "total=" + total + ", status=" + status + ", Payment=" + Payment + ", customer=" + customer + ", charge_id=" + charge_id + ", expire_at=" + expire_at + ", created_at=" + created_at + ", paid_value=" + paid_value + ", charge_method=" + charge_method + '}';
    }

}
