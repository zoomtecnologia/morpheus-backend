package com.zoomtecnologia.morpheus.morpheussistema.modulos.indicacao;

import java.time.OffsetDateTime;

public interface IndicacaoModeloAPI {

    String getCodigoIndicacao();

    String getEmailCliente();

    String getNomeCliente();

    String getTelefone();

    OffsetDateTime getData();

    String getIdentificador();
    
    String getRepresentante();

}
