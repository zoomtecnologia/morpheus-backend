package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao;

import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PermissaoServico {

    private final PermissaoRepositorio permissaoRepositorio;
    private final MessageSourceUtil messageSourceUtil;

    public List<Permissao> listarPermissoes() {
        return this.permissaoRepositorio.findAll();
    }

    public List<PermissaoResumidaModeloAPI> listarPermissaoResumida() {
        return this.permissaoRepositorio.listarPermissoesResumidas();
    }

    public Permissao buscarPermissaoPorCodigo(Integer codigo) {
        final Optional<Permissao> permissaoOptional = this.permissaoRepositorio.findById(codigo);
        if (!permissaoOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("permissao.nao-existe", "permissao.nao-existe");
        }
        return permissaoOptional.get();
    }

    public Permissao buscarPermissaoAfiliado() {
        final Optional<Permissao> permissaoOptional = this.permissaoRepositorio.buscarPermissaoAfiliado();
        if (!permissaoOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("permissao.nao-existe", "permissao.nao-existe");
        }
        return permissaoOptional.get();
    }

    public Permissao salvarPermissao(Permissao permissao) {
        return this.permissaoRepositorio.save(permissao);
    }

    public void alterarPermissao(Integer codigo, Permissao permissao) {
        Permissao permissaoEncontrada = this.buscarPermissaoPorCodigo(codigo);
        BeanUtils.copyProperties(permissao, permissaoEncontrada, "codigo");
        this.permissaoRepositorio.save(permissaoEncontrada);
    }

    public void excluir(Integer codigo) {
        this.permissaoRepositorio.deleteById(codigo);
    }

}
