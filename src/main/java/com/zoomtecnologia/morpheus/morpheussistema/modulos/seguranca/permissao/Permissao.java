package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao.PermissaoAplicacao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "permissao")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Permissao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    
	@Column(name = "codigo")
	private Integer codigo;

    
	@Column(name = "nome")
	private String nome;

    
	@Column(name = "representante")
	private Boolean representante;
    
    @OneToMany(mappedBy = "permissaoAplicacaoPK.permissao")
    private List<PermissaoAplicacao> aplicacoes = new ArrayList<>();

    public List<PermissaoAplicacao> getAplicacoes() {
        return aplicacoes;
    }

    public void setAplicacoes(List<PermissaoAplicacao> aplicacoes) {
        this.aplicacoes = aplicacoes;
    }

    public Permissao(Integer codigo) {
        this.codigo = codigo;
    }

    public Permissao() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getRepresentante() {
        return representante;
    }

    public void setRepresentante(Boolean representante) {
        this.representante = representante;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Permissao other = (Permissao) obj;
        return Objects.equals(this.codigo, other.codigo);
    }

    @Override
    public String toString() {
        return "Permissao{" + "codigo=" + codigo + ", nome=" + nome + ", representante=" + representante + '}';
    }

}
