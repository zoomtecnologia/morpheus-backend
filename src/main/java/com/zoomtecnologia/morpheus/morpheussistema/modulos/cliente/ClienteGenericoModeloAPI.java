package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

public abstract class ClienteGenericoModeloAPI {

    @Size(max = 14)
    private String inscricaoEstadual;

    @Size(max = 120)
    @NotBlank
    private String razaoSocial;

    @Size(max = 120)
    private String nomeFantasia;

    @Size(max = 20)
    @NotBlank
    private String regimeTributario;

    @Valid
    @NotNull
    private EnderecoCliente endereco;

    @Size(max = 50)
    private String nomeContato;

    @Size(max = 13)
    private String telefone;

    @Size(max = 13)
    @NotBlank
    private String celular;

    @Size(max = 13)
    private String whatsapp;

    @Size(max = 120)
    @NotBlank
    @Email
    private String email;

    @Size(max = 50)
    private String seguimento;

    private String obs;

    @Size(max = 1)
    private String seTef;

    @Size(max = 1)
    @NotBlank
    private String contingencia;

    @Size(max = 1)
    @NotBlank
    private String onSaiph;

    private Integer diaVencimento;

    private Integer prazoMaximo;

    @Size(max = 1)
    private String statusPagamento;

    @Size(max = 14)
    @NotBlank
    private String codigoRepresentante;

    private String codigoGrupoEmpresa;

    @Size(max = 1)
    private String aceiteContrato;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataAceite;

    @NotBlank
    @Size(max = 50)
    private String nomePessoaResponsavel;

    @NotBlank
    @Size(max = 20)
    private String cpfPessoaResponsavel;

    private boolean matriz;

    @Size(max = 60)
    private String apelido;

    @Size(max = 7)
    private String cnae;

    @Size(max = 7)
    private String indicadorInscricaoEstadual;

    @Size(max = 36)
    private String identificadorAPIRetaguarda;
    
    private TipoRetaguarda retaguarda;

    public boolean isMatriz() {
        return matriz;
    }

    public void setMatriz(boolean matriz) {
        this.matriz = matriz;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getCnae() {
        return cnae;
    }

    public void setCnae(String cnae) {
        this.cnae = cnae;
    }

    public String getNomePessoaResponsavel() {
        return nomePessoaResponsavel;
    }

    public void setNomePessoaResponsavel(String nomePessoaResponsavel) {
        this.nomePessoaResponsavel = nomePessoaResponsavel;
    }

    public String getCpfPessoaResponsavel() {
        return cpfPessoaResponsavel;
    }

    public void setCpfPessoaResponsavel(String cpfPessoaResponsavel) {
        this.cpfPessoaResponsavel = cpfPessoaResponsavel;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getRegimeTributario() {
        return regimeTributario;
    }

    public void setRegimeTributario(String regimeTributario) {
        this.regimeTributario = regimeTributario;
    }

    public EnderecoCliente getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoCliente endereco) {
        this.endereco = endereco;
    }

    public String getNomeContato() {
        return nomeContato;
    }

    public void setNomeContato(String nomeContato) {
        this.nomeContato = nomeContato;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSeguimento() {
        return seguimento;
    }

    public void setSeguimento(String seguimento) {
        this.seguimento = seguimento;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getSeTef() {
        return seTef;
    }

    public void setSeTef(String seTef) {
        this.seTef = seTef;
    }

    public String getContingencia() {
        return contingencia;
    }

    public void setContingencia(String contingencia) {
        this.contingencia = contingencia;
    }

    public String getOnSaiph() {
        return onSaiph;
    }

    public void setOnSaiph(String onSaiph) {
        this.onSaiph = onSaiph;
    }

    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(Integer diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    public Integer getPrazoMaximo() {
        return prazoMaximo;
    }

    public void setPrazoMaximo(Integer prazoMaximo) {
        this.prazoMaximo = prazoMaximo;
    }

    public String getStatusPagamento() {
        return statusPagamento;
    }

    public void setStatusPagamento(String statusPagamento) {
        this.statusPagamento = statusPagamento;
    }

    public String getCodigoRepresentante() {
        return codigoRepresentante;
    }

    public void setCodigoRepresentante(String codigoRepresentante) {
        this.codigoRepresentante = codigoRepresentante;
    }

    public String getCodigoGrupoEmpresa() {
        return codigoGrupoEmpresa;
    }

    public void setCodigoGrupoEmpresa(String codigoGrupoEmpresa) {
        this.codigoGrupoEmpresa = codigoGrupoEmpresa;
    }

    public String getAceiteContrato() {
        return aceiteContrato;
    }

    public void setAceiteContrato(String aceiteContrato) {
        this.aceiteContrato = aceiteContrato;
    }

    public LocalDate getDataAceite() {
        return dataAceite;
    }

    public void setDataAceite(LocalDate dataAceite) {
        this.dataAceite = dataAceite;
    }

    public String getIndicadorInscricaoEstadual() {
        return indicadorInscricaoEstadual;
    }

    public void setIndicadorInscricaoEstadual(String indicadorInscricaoEstadual) {
        this.indicadorInscricaoEstadual = indicadorInscricaoEstadual;
    }

    public String getIdentificadorAPIRetaguarda() {
        return identificadorAPIRetaguarda;
    }

    public void setIdentificadorAPIRetaguarda(String identificadorAPIRetaguarda) {
        this.identificadorAPIRetaguarda = identificadorAPIRetaguarda;
    }

    public TipoRetaguarda getRetaguarda() {
        return retaguarda;
    }

    public void setRetaguarda(TipoRetaguarda retaguarda) {
        this.retaguarda = retaguarda;
    }

    @Override
    public String toString() {
        return "ClienteGenericoModeloAPI{" + "inscricaoEstadual=" + inscricaoEstadual + ", razaoSocial=" + razaoSocial + ", nomeFantasia=" + nomeFantasia + ", regimeTributario=" + regimeTributario + ", endereco=" + endereco + ", nomeContato=" + nomeContato + ", telefone=" + telefone + ", celular=" + celular + ", whatsapp=" + whatsapp + ", email=" + email + ", seguimento=" + seguimento + ", obs=" + obs + ", seTef=" + seTef + ", contingencia=" + contingencia + ", onSaiph=" + onSaiph + ", diaVencimento=" + diaVencimento + ", prazoMaximo=" + prazoMaximo + ", statusPagamento=" + statusPagamento + ", codigoRepresentante=" + codigoRepresentante + ", codigoGrupoEmpresa=" + codigoGrupoEmpresa + ", aceiteContrato=" + aceiteContrato + ", dataAceite=" + dataAceite + ", nomePessoaResponsavel=" + nomePessoaResponsavel + ", cpfPessoaResponsavel=" + cpfPessoaResponsavel + ", matriz=" + matriz + ", apelido=" + apelido + ", cnae=" + cnae + ", indicadorInscricaoEstadual=" + indicadorInscricaoEstadual + ", identificadorAPIRetaguarda=" + identificadorAPIRetaguarda + ", retaguarda=" + retaguarda + '}';
    }
    
}
