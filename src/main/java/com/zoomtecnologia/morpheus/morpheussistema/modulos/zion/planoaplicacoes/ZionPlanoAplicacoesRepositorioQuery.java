package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes;

import java.util.List;

public interface ZionPlanoAplicacoesRepositorioQuery {
    public void salvar(List<ZionPlanoAplicacoes> aplicacoes );
}
