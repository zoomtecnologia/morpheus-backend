package com.zoomtecnologia.morpheus.morpheussistema.modulos.indicacao;

import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoJaExisteException;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.representante.Representante;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.representante.RepresentanteServico;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class IndicacaoServico {

    private final IndicacaoRepositorio indicacaoRepositorio;
    private final RepresentanteServico representanteServico;
    private final MessageSourceUtil messageSourceUtil;

    public Page<IndicacaoModeloAPI> listarIndicacoes(String pesquisa, Pageable pageable) {
        return this.indicacaoRepositorio.listarIndicacoes(pesquisa, pageable);
    }

    public Indicacao buscarIndicacao(String identificador) {
        final Optional<Indicacao> indicacaoOptional = this.indicacaoRepositorio.buscarIndicacao(identificador);
        if (!indicacaoOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("indicacao.nao-encontrada", "indicacao.nao-encontrada");
        }
        return indicacaoOptional.get();
    }

    public Indicacao salvarIndicacao(IndicacaoInclusaoModeloAPI indicacaoInclusaoModeloAPI) {
        final Indicacao indicacao = indicacaoInclusaoModeloAPI.converterIndicacaoModeloAPI();
        final String codigoIndicacao = indicacaoInclusaoModeloAPI.getCodigoIndicacao();
        if (codigoIndicacao == null || codigoIndicacao.trim().isEmpty()) {
            final Representante representante = this.representanteServico.buscarRepresentantePorDocumento("3055");
            indicacao.getId().setCodigoIndicacao(representante.getCodigoIndicacao());
        }
        if (codigoIndicacao != null && !codigoIndicacao.trim().isEmpty()) {
            this.validarSeJaExisteUmaIndicacao(indicacao);
        }
        indicacao.setData(OffsetDateTime.now());
        indicacao.setIdentificador(UUID.randomUUID().toString());
        return this.indicacaoRepositorio.save(indicacao);
    }

    private void validarSeJaExisteUmaIndicacao(final Indicacao indicacao) throws RecursoJaExisteException {
        final boolean existe = this.indicacaoRepositorio.existsById(indicacao.getId());
        if (existe) {
            this.messageSourceUtil.recursoJaExiste("indicacao.ja-existe", "indicacao.ja-existe");
        }
    }

}
