package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("clientes/validacao")
@CrossOrigin("*")
@AllArgsConstructor
public class ClienteValidacaoControler {

    private final ClienteValidacaoServico clienteValidacaoServico;

    @PutMapping
    public ResponseEntity<ClienteValidacao> validarChavePDV(@RequestParam("cnpj") String cnpj, 
            @RequestParam("numeroPDV") int numeroPDV) {
        final ClienteValidacao clienteValidacao = this.clienteValidacaoServico.validarChavePDV(cnpj, numeroPDV);
        return ResponseEntity.ok(clienteValidacao);
    }

}
