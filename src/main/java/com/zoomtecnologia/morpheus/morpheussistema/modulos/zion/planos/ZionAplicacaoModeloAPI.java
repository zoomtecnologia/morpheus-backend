package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planos;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoesModeloAPI;
import java.util.Objects;

public class ZionAplicacaoModeloAPI implements ZionPlanoAplicacoesModeloAPI {

    private String codigoAplicacao;

    private String moduloAplicacao;

    private String nome;

    @Override
    public String getCodigoAplicacao() {
        return this.codigoAplicacao;
    }

    @Override
    public String getModuloAplicacao() {
        return this.moduloAplicacao;
    }

    @Override
    public String getNome() {
        return this.nome;
    }

    public void setCodigoAplicacao(String codigoAplicacao) {
        this.codigoAplicacao = codigoAplicacao;
    }

    public void setModuloAplicacao(String moduloAplicacao) {
        this.moduloAplicacao = moduloAplicacao;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.codigoAplicacao);
        hash = 61 * hash + Objects.hashCode(this.moduloAplicacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ZionAplicacaoModeloAPI other = (ZionAplicacaoModeloAPI) obj;
        if (!Objects.equals(this.codigoAplicacao, other.codigoAplicacao)) {
            return false;
        }
        return Objects.equals(this.moduloAplicacao, other.moduloAplicacao);
    }

    public void setZionPlanoModeloResumido(ZionPlanoModeloResumido zionPlanoModeloResumido) {
        this.setCodigoAplicacao(zionPlanoModeloResumido.getCodigoAplicacao());
        this.setModuloAplicacao(zionPlanoModeloResumido.getCodigoModulo());

    }

}
