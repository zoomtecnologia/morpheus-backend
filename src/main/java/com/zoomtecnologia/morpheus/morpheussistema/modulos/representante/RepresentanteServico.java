package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import com.zoomtecnologia.morpheus.morpheussistema.email.Mailer;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao.PermissaoServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioServico;
import com.zoomtecnologia.morpheus.morpheussistema.util.GeradorCPF;
import com.zoomtecnologia.morpheus.morpheussistema.util.ImageCrop;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import lombok.AllArgsConstructor;
import org.apache.commons.compress.utils.IOUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class RepresentanteServico {

    private final RepresentanteRepositorio representanteRepositorio;
    private final UsuarioServico usuarioServico;
    private final PermissaoServico permissaoServico;
    private final Mailer mailer;
    private final ModelMapper modelMapper;
    private final MessageSourceUtil messageSourceUtil;

    public Page<RepresentanteRetornoModeloAPI> litarTodosPaginado(String pesquisa, Pageable pageable) {
        return this.representanteRepositorio.listarRepresentantes(pesquisa, pageable);
    }

    public List<RepresentanteRetornoModeloAPI> listarTodos() {
        return this.representanteRepositorio.listarRepresentantes();
    }

    public List<RepresentanteResumidoModeloAPI> listarRepresentantesResumido() {
        return this.representanteRepositorio.listarRepresentanteResumido();
    }

    public Representante buscarRepresentantePorDocumento(String documento) {
        Optional<Representante> representanteOptional = this.representanteRepositorio.findById(documento);
        if (!representanteOptional.isPresent()) {
            this.representanteNaoEncontrado();
        }
        return representanteOptional.get();
    }

    public Representante salvarRepresentante(RepresentanteInclusaoModeloAPI representanteInclusaoModeloAPI) throws NoSuchAlgorithmException {
        if (this.representanteRepositorio.existsById(representanteInclusaoModeloAPI.getDocumento())) {
            this.messageSourceUtil.recursoJaExiste("representante.recurso-ja-existe", "representante.recurso-ja-existe-detalhes");
        }
        Representante representante = this.modelMapper.map(representanteInclusaoModeloAPI, Representante.class);
        representante.setDataCadastro(LocalDate.now());
        representante.setCodigoIndicacao(representante.emailToHash());
        representante.setStatus("A");
        return this.representanteRepositorio.save(representante);
    }

    public void alterarRepresentante(String documento, RepresentanteAlteracaoModeloAPI representanteAlteracaoModeloAPI) {
        Representante representanteEncontrado = this.buscarRepresentantePorDocumento(documento);
        BeanUtils.copyProperties(representanteAlteracaoModeloAPI, representanteEncontrado);
        representanteEncontrado.setDataUltimaAtualizacao(LocalDate.now());
        this.representanteRepositorio.save(representanteEncontrado);
    }

    @Transactional
    public Representante salvarRevendedor(RevendedorModeloAPI revendedorModeloAPI) throws NoSuchAlgorithmException {
        final Representante representante = new Representante();
        representante.setNome(revendedorModeloAPI.getNome());
        representante.setEmail(revendedorModeloAPI.getEmail());
        representante.setChavePix(revendedorModeloAPI.getChavePIX());
        representante.setWhatsapp(revendedorModeloAPI.getWhatsApp().replaceAll("\\D", ""));
        representante.setCelular(revendedorModeloAPI.getWhatsApp().replaceAll("\\D", ""));
        representante.setDocumento(GeradorCPF.gerarCPF());
        representante.setDataCadastro(LocalDate.now());
        representante.setDiaPagamentoComissao(15);
        final String[] nomes = representante.getNome().split(" ");
        if (nomes.length > 1) {
            representante.setNomeSocial(nomes[0].trim() + " " + nomes[1].trim());
        }
        representante.setCodigoIndicacao(representante.emailToHash());
        representante.setTelefone("");
        representante.setStatus("A");
        representante.setDiasToleranciaPagamentoComissao(5);
        representante.setGeraReceita(Boolean.TRUE);
        representante.setTipo("C");
        UsuarioInclusaoModeloAPI usuario = new UsuarioInclusaoModeloAPI();
        usuario.setDocumentoRepresentante(representante.getDocumento());
        usuario.setEmail(representante.getEmail());
        usuario.setNome(representante.getNome());
        usuario.setSenha(revendedorModeloAPI.getSenha());
        final Integer parceiroComercial = this.permissaoServico.buscarPermissaoAfiliado().getCodigo();
        usuario.setPermissao(parceiroComercial);
        this.representanteRepositorio.save(representante);
        this.usuarioServico.salvarUsuario(usuario);
        this.mailer.enviarEmailRevendedorCadastrado(representante);
        return representante;
    }

    public void deletarRepresentante(String documento) {
        this.representanteRepositorio.deleteById(documento);
    }

    public void atualizarFoto(String documento, InputStream foto) throws IOException {
        Representante representanteEncontrado = this.buscarRepresentantePorDocumento(documento);
        ImageCrop imageCrop = new ImageCrop(240, 400);
        imageCrop.setOriginInputStream(foto);
        imageCrop.resize();
        InputStream targetInputStream = imageCrop.getInputStream();
        representanteEncontrado.setFoto(IOUtils.toByteArray(targetInputStream));
        this.representanteRepositorio.save(representanteEncontrado);
    }

    public void bloquearRepresentante(String codigoRepresentante, String status) {
        this.representanteRepositorio.bloquearRepresentanteDesseRepresentante(codigoRepresentante, status);
    }

    private void representanteNaoEncontrado() throws RecursoNaoEncontradoException {
        this.messageSourceUtil.recursoNaoExiste("representante.recurso-nao-encontrado", "representante.recurso-nao-encontrado-detalhes");
    }

}
