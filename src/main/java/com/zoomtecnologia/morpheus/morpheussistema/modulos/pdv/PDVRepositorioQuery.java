/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;

import java.util.List;

/**
 *
 * @author eudes
 */
public interface PDVRepositorioQuery {
    
    public void salvarPDVs(List<PDV> pdvs);
    public void atualizarPDVsNoZion(List<PDV> pdvs, Cliente cliente);
}
