package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

public enum PedidoStatus {
    SEM_PEDIDO("SEM PEDIDO"), ABERTO("ABERTO"), LANCADO("LANCADO"), FINALIZADO("FINALIZADO"), CLIENTE_ACEITOU("CLIENTE ACEITOU"),
    INSTALACAO_MARCADA("INSTALACAO MARCADA"), INSTALACAO_FINALIZADA("INSTALAÇÃO FINALIZADA"), INSTALACAO_PAGA("INSTALACAO PAGA"),
    COMISSAO_LIBERADA("COMISSAO LIBERADA"), COMISSAO_PAGA("COMISSAO PAGA"), CANCELADO("CANCELADO");

    private final String status;

    private PedidoStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Converter(autoApply = true)
    public static class Mapeador implements AttributeConverter<PedidoStatus, String> {

        @Override
        public String convertToDatabaseColumn(PedidoStatus pedidoStatus) {
            return pedidoStatus.getStatus();
        }

        @Override
        public PedidoStatus convertToEntityAttribute(String status) {
            switch (status) {
                case "LANCADO":
                    return LANCADO;
                case "ABERTO":
                    return ABERTO;
                case "CLIENTE ACEITOU":
                    return CLIENTE_ACEITOU;

                case "FINALIZADO":
                    return FINALIZADO;

                case "INSTALACAO MARCADA":
                    return INSTALACAO_MARCADA;

                case "INSTALAÇÃO FINALIZADA":
                    return INSTALACAO_FINALIZADA;

                case "INSTALACAO PAGA":
                    return INSTALACAO_PAGA;

                case "COMISSAO LIBERADA":
                    return COMISSAO_LIBERADA;

                case "COMISSAO PAGA":
                    return COMISSAO_PAGA;

                default:
                    return SEM_PEDIDO;
            }
        }
    }
}
