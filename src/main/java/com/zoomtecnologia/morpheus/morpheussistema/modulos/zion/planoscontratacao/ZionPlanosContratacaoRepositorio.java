package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionPlanosContratacaoRepositorio extends JpaRepository<ZionPlanosContratacao, ZionPlanosContratacaoPK>, ZionPlanosContratacaoRepositorioQuery {

    @Query("select plano.diasParaTeste as diasParaTeste, plano.teste as teste, plano.geraCobranca as geraCobranca"
            + " from ZionPlanosContratacao zionPlanosContratacao"
            + " inner join "
            + " ZionPlano plano on(plano.codigo=zionPlanosContratacao.id.codigoPlano and plano.tipoPlano='P')"
            + " where zionPlanosContratacao.id.codigoContratacao=:codigoPlanoContratado")
    public ZionPlanoPrincipal buscarPlanoPrincipalPorContrato(String codigoPlanoContratado);

}
