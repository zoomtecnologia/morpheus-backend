package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RevendedorModeloAPI {

    @NotBlank
    @Size(max = 50)
    private String nome;

    @NotBlank
    @Size(max = 150)
    private String email;

    @NotBlank
    private String senha;

    @NotBlank
    @Size(max = 14)
    private String whatsApp;

    @NotBlank
    @Size(max = 50)
    private String chavePIX;

}
