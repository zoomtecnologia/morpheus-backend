package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.representante.Representante;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "comissao")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Comissao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "codigo")
	private Integer codigo;

    @ManyToOne
    @JoinColumn(name = "codigo_representante")
    private Representante codigoRepresentante;

    @ManyToOne
    @JoinColumn(name = "codigo_cliente")
    private Cliente codigoCliente;

    @Column(name = "tipo")
	private String tipo;

    @Column(name = "valor")
	private Double valor;

    @Column(name = "valor_mensalidade")
	private Double valorMensalidade;

    @Column(name = "valor_instalacao")
	private Double valorInstalacao;

    @Column(name = "data_mensalidade")
	private LocalDate dataMensalidade;

    @Column(name = "data_liberacao")
	private LocalDate dataLiberacao;

    @Column(name = "data_solicitacao")
	private LocalDate dataSolicitacao;

    @Column(name = "data_pagamento")
	private LocalDate dataPagamento;

    @Column(name = "status")
	private String status;

    @Column(name = "codigo_solicitacao")
	private String codigoSolicitacao;

    @Column(name = "obs")
	private String obs;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Representante getCodigoRepresentante() {
        return codigoRepresentante;
    }

    public void setCodigoRepresentante(Representante codigoRepresentante) {
        this.codigoRepresentante = codigoRepresentante;
    }

    public Cliente getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Cliente codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public LocalDate getDataMensalidade() {
        return dataMensalidade;
    }

    public void setDataMensalidade(LocalDate dataMensalidade) {
        this.dataMensalidade = dataMensalidade;
    }

    public LocalDate getDataLiberacao() {
        return dataLiberacao;
    }

    public void setDataLiberacao(LocalDate dataLiberacao) {
        this.dataLiberacao = dataLiberacao;
    }

    public LocalDate getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(LocalDate dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public LocalDate getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(LocalDate dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCodigoSolicitacao() {
        return codigoSolicitacao;
    }

    public void setCodigoSolicitacao(String codigoSolicitacao) {
        this.codigoSolicitacao = codigoSolicitacao;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Double getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(Double valorMensalidade) {
        this.valorMensalidade = valorMensalidade;
    }

    public void setValorInstalacao(Double valorInstalacao) {
        this.valorInstalacao = valorInstalacao;
    }

    public Double getValorInstalacao() {
        return valorInstalacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comissao other = (Comissao) obj;
        return Objects.equals(this.codigo, other.codigo);
    }

    @Override
    public String toString() {
        return "Comissao{" + "codigo=" + codigo + ", codigoRepresentante=" + codigoRepresentante + ", codigoCliente=" + codigoCliente + ", tipo=" + tipo + ", valor=" + valor + ", valorMensalidade=" + valorMensalidade + ", valorInstalacao=" + valorInstalacao + ", dataMensalidade=" + dataMensalidade + ", dataLiberacao=" + dataLiberacao + ", dataSolicitacao=" + dataSolicitacao + ", dataPagamento=" + dataPagamento + ", status=" + status + ", codigoSolicitacao=" + codigoSolicitacao + ", obs=" + obs + '}';
    }

}
