/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import com.zaxxer.hikari.HikariDataSource;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa.GrupoEmpresa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.util.List;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Transactional;

public class PDVRepositorioQueryImpl implements PDVRepositorioQuery {

    @Autowired
    private PDVRepositorioSalvamentoBach salvamentoBach;
    private final Logger logger = LoggerFactory.getLogger(PDVRepositorioQueryImpl.class);

    @Transactional
    @Override
    public void salvarPDVs(List<PDV> pdvs) {
        try {
            this.salvamentoBach.saveEntities(pdvs);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchFieldException
                 | IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
                 SecurityException ex) {
            this.logger.error(ex.getMessage(), ex);
        }
    }

    @Override
    public void atualizarPDVsNoZion(List<PDV> pdvs, Cliente cliente) {
        if (pdvs == null || pdvs.isEmpty()) {
            return;
        }
        final GrupoEmpresa grupoEmpresa = cliente.getCodigoGrupoEmpresa();
        final DataSource dataSource = this.newDataSourceBuilderNuvem(grupoEmpresa.getIdentificacaoBancoDados());
        try {
            final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            for (PDV pdv : pdvs) {
                final List<String> codigoEmpresa = this.getCodigoEmpresa(jdbcTemplate, cliente.getCnpj());
                jdbcTemplate.execute("update pdvs set chave='" + pdv.getChave() + "' where codigo_empresa='" + codigoEmpresa.get(0) + "' and numero=" + pdv.getId().getNumeroPdv());
            }
        } catch (Exception ex) {
            this.logger.error("Erro ao atualizar PDVs no Zion, exception: " + ex.getMessage());
        } finally {
            try {
                DataSourceUtils.releaseConnection(DataSourceUtils.getConnection(dataSource), dataSource);
            } catch (Exception ex) {
                this.logger.error("Erro ao fechar conexão com o banco de dados: zoomtecn_zion_" + grupoEmpresa.getIdentificacaoBancoDados() + " exception: " + ex.getMessage());
            }
        }
    }

    private List<String> getCodigoEmpresa(final JdbcTemplate jdbcTemplate, String cnpjCliente) {
        return jdbcTemplate.query("select codigo_empresa as codigoEmpresa from empresa where cnpj='" + cnpjCliente + "'", (ResultSet rs, int rowNum) -> rs.getString("codigoEmpresa"));
    }

    private DataSource newDataSourceBuilderNuvem(String identificador) {
        final HikariDataSource dataSource = DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .url("jdbc:mysql://195.35.17.210:3306/zoomtecn_zion_" + identificador + "?useSSL=false&zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&allowMultiQueries=true&allowPublicKeyRetrieval=true")
                .username("zoomtecn_adm")
                .password("noiz@#$1030")
                .driverClassName("com.mysql.cj.jdbc.Driver").
                build();
        dataSource.setMaximumPoolSize(1);
        dataSource.setMinimumIdle(1);
        dataSource.setConnectionTimeout(30000); // Tempo máximo de espera para obter uma conexão, em milissegundos
        dataSource.setIdleTimeout(600000);  // Tempo máximo que uma conexão pode ficar ociosa, em milissegundos
        dataSource.setMaxLifetime(540000);
        return dataSource;
    }
}
