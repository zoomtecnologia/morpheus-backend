package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ItensPedidoAlteracaoModeloAPI extends ItensPedidoGenericoModeloAPI {

}
