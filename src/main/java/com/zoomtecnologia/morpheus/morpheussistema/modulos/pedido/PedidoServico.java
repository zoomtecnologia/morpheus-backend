package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoServico;
import com.zoomtecnologia.morpheus.morpheussistema.email.Mailer;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoJaExisteException;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PedidoServico {

    private final PedidoRepositorio pedidoRepositorio;
    private final ModelMapper modelMapper;
    private final MessageSourceUtil messageSourceUtil;
    private final ComissaoServico comissaoServico;
    private final PedidoClienteStatus pedidoClienteStatus;
    private final Mailer mailer;

    public Page<PedidoRetornoModeloAPI> listarPedidosPaginado(String pesquisaConteudo, Pageable pageable) {
        return this.pedidoRepositorio.listarTodos(pesquisaConteudo, pageable);
    }

    public List<PedidoStatusModeloAPI> listarStatusPedidos() {
        return this.pedidoRepositorio.listarStatusPedido();
    }

    public Pedido buscarPedidoPorNumero(Integer numeroPedido) {
        Optional<Pedido> pedidoOptional = this.pedidoRepositorio.findById(numeroPedido);
        if (!pedidoOptional.isPresent()) {
            this.pedidoNaoEncontrado();
        }
        return pedidoOptional.get();
    }

    public PedidoRetornoModeloAPI buscarPedidoResumidoPorCliente(String cnpj) {
        Optional<PedidoRetornoModeloAPI> pedidoOptional = this.pedidoRepositorio.buscarPedidoResumidoPorCliente(cnpj);
        if (!pedidoOptional.isPresent()) {
            this.pedidoNaoEncontrado();
        }
        return pedidoOptional.get();
    }

    public Pedido buscarPedidoPorCliente(String cnpj) {
        Optional<Pedido> pedidoOptional = this.pedidoRepositorio.buscarPedidoPorCliente(cnpj);
        if (!pedidoOptional.isPresent()) {
            this.pedidoNaoEncontrado();
        }
        return pedidoOptional.get();
    }

    public Pedido salvarPedido(PedidoInclusaoModeloAPI pedidoInclusaoModeloAPI) {
        Pedido pedido = this.modelMapper.map(pedidoInclusaoModeloAPI, Pedido.class);
        Cliente cliente = pedido.getCliente();
        this.verificarSeClienteJaTemPedido(cliente.getCnpj());
        pedido.setData(LocalDate.now());
        pedido.setStatus(PedidoStatus.ABERTO);
        pedido.setEmail(cliente.getEmail());
        pedido.atualizarStatusPedidoCliente(this.pedidoClienteStatus);
        cliente.setStatusPedido(pedido.getStatus().getStatus());
        return this.pedidoRepositorio.save(pedido);
    }

    public Pedido alterarPedido(Integer numeroPedido, PedidoAlteracaoModeloAPI pedidoAlteracaoModeloAPI) {
        Pedido pedidoEncontrado = buscarPedidoPorNumero(numeroPedido);
        if (this.verificarSePedidoTemDataDeInstalacao(pedidoAlteracaoModeloAPI)) {
            this.messageSourceUtil.negocioException("pedido.recurso-pedido-data-instalacao", "pedido.recurso-pedido-data-instalacao-detalhes");
        }
        BeanUtils.copyProperties(pedidoAlteracaoModeloAPI, pedidoEncontrado);
        pedidoEncontrado.atualizarStatusPedidoCliente(this.pedidoClienteStatus);
        this.pedidoRepositorio.save(pedidoEncontrado);
        return pedidoEncontrado;
    }

    public void alterarStatusPedido(PedidoStatus status, Integer numero) {
        Pedido pedidoEncontrado = this.buscarPedidoPorNumero(numero);
        this.alterarStatusPedido(pedidoEncontrado, status);
    }

    public void alterarStatusPedido(PedidoStatus status, String cliente) {
        Optional<Pedido> pedidoOptional = this.pedidoRepositorio.buscarPedidoPorCliente(cliente);
        if (!pedidoOptional.isPresent()) {
            this.pedidoNaoEncontrado();
        }
        this.alterarStatusPedido(pedidoOptional.get(), status);
    }

    public byte[] gerarContratoPorCliente(String cnpj) {
        Pedido pedido = this.buscarPedidoPorCliente(cnpj);
        return this.gerarContrato(pedido);
    }

    public byte[] gerarContratoPorNumeroPedido(Integer numero) {
        Pedido pedido = this.buscarPedidoPorNumero(numero);
        return this.gerarContrato(pedido);
    }

    public void enviarContratoPorNumeroPedido(Integer numero) {
        Pedido pedido = this.buscarPedidoPorNumero(numero);
        boolean contratoPendente = pedido.getCliente().getAceiteContrato() == null;
        boolean pedidoLancado = pedido.getStatus().equals(PedidoStatus.LANCADO);
        boolean podeEnviarContrato = pedidoLancado && contratoPendente;
        if (podeEnviarContrato) {
            this.enviarContratoPorEmail(pedido);
            return;
        }
        this.messageSourceUtil.negocioException("contrato-email", "contrato-email");
    }

    public void reenviarContrato(String cnpj, EmailContratoModeloAPI emailContratoModeloAPI) {
        Pedido pedido = this.buscarPedidoPorCliente(cnpj);
        pedido.setEmail(emailContratoModeloAPI.getEmail());
        this.enviarContratoPorEmail(pedido);
    }

    private void enviarContratoPorEmail(Pedido pedido) {
        this.mailer.enviarEmailContrato(null,null);
    }

    public void enviarEmailDeMudancaDeStatusPedidoParaCliente(Integer numeroPedido) {
        Pedido pedido = this.buscarPedidoPorNumero(numeroPedido);
        if (pedido.getStatus().equals(PedidoStatus.INSTALACAO_MARCADA) || pedido.getStatus().equals(PedidoStatus.INSTALACAO_FINALIZADA)) {
            this.mailer.enviarEmailAndamentoDoPedido(pedido);
        }
    }

    @Scheduled(cron = "1 0 0 * * *")
    public void atualizarContratoDeClientes() {
        List<Pedido> pedidos = this.pedidoRepositorio.listarClientesComContratoVencendoNoDia();
        pedidos.forEach(pedido -> {
            this.gerarNovoContrato(pedido);
            this.pedidoClienteStatus.atualizarDataContrato(pedido);
        });
    }

    private byte[] gerarContrato(Pedido pedido) {
        try {
            return pedido.gerarContrato();
        } catch (IOException | InvalidFormatException ex) {
            return new byte[]{};
        }
    }

    private void gerarNovoContrato(Pedido pedido) {
        try {
            pedido.gerarNovoContrato();
        } catch (IOException | InvalidFormatException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void alterarStatusPedido(Pedido pedidoEncontrado, PedidoStatus status) {
        if (this.verificarSeNaoPodeCancelarPedido(pedidoEncontrado, status)) {
            this.messageSourceUtil.negocioException("pedido.recurso-cancelamento-pedido", "pedido.recurso-cancelamento-pedido-detalhes");
        }
        pedidoEncontrado.setStatus(status);
        this.pedidoRepositorio.alterarStatus(pedidoEncontrado.getNumero(), status);
        pedidoEncontrado.atualizarStatusPedidoCliente(this.pedidoClienteStatus);
        this.comissaoServico.lancarComissao(pedidoEncontrado);
    }

    private Boolean verificarSeClienteJaTemPedido(String cnpj) {
        boolean verificarPedidoDeCliente = this.pedidoRepositorio.countByCliente_Cnpj(cnpj) > 0;
        if (verificarPedidoDeCliente) {
            this.pedidoJaExiste();
        }
        return verificarPedidoDeCliente;
    }

    private boolean verificarSePedidoTemDataDeInstalacao(PedidoAlteracaoModeloAPI pedidoAlteracaoModeloAPI) {
        return pedidoAlteracaoModeloAPI.getStatus().equals(PedidoStatus.INSTALACAO_MARCADA) && pedidoAlteracaoModeloAPI.getDataMarcacaoInstalacao() == null;
    }

    private boolean verificarSeNaoPodeCancelarPedido(Pedido pedido, PedidoStatus status) {
        return pedido.getStatus().equals(PedidoStatus.COMISSAO_PAGA) && status.equals(PedidoStatus.CANCELADO);
    }

    private void pedidoNaoEncontrado() throws RecursoNaoEncontradoException {
        this.messageSourceUtil.recursoNaoExiste("pedido.recurso-nao-encontrado", "pedido.recurso-nao-encontrado-detalhes");
    }

    private void pedidoJaExiste() throws RecursoJaExisteException {
        this.messageSourceUtil.recursoJaExiste("pedido.recurso-ja-existe", "pedido.recurso-ja-existe-detalhes");
    }

}
