package com.zoomtecnologia.morpheus.morpheussistema.modulos.tipocomissao;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoJaExisteException;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import com.zoomtecnologia.morpheus.morpheussistema.generico.GenericoServico;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TipoComissaoServico implements GenericoServico<TipoComissao, Integer> {

    @Autowired
    private TipoComissaoRepositorio tipoComissaoRepositorio;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    @Override
    public List<TipoComissao> listarTodos(String pesquisa) {
        return this.tipoComissaoRepositorio.findByNomeContainingIgnoreCase(pesquisa);
    }

    @Override
    public Page<TipoComissao> listarTodosPaginado(String pesquisa, Pageable pageable) {
        return this.tipoComissaoRepositorio.findByNomeContainingIgnoreCase(pageable, pesquisa);
    }

    @Override
    public TipoComissao buscarPorCodigo(Integer key) {
        Optional<TipoComissao> tipoComissaoOptional = this.tipoComissaoRepositorio.findById(key);
        if (!tipoComissaoOptional.isPresent()) {
            this.tipoComissaoNaoExiste();
        }
        return tipoComissaoOptional.get();
    }

    @Override
    public TipoComissao salvar(TipoComissao tipoComissao) {
        this.buscarPorNome(tipoComissao);
        tipoComissao.setDataCadastro(LocalDate.now());
        tipoComissao.setStatus("A");
        return this.tipoComissaoRepositorio.save(tipoComissao);
    }

    @Override
    public void alterar(TipoComissao tipoComissao, Integer codigo) {
        TipoComissao tipoComissaoEncontrada = this.buscarPorCodigo(codigo);
        BeanUtils.copyProperties(tipoComissao, tipoComissaoEncontrada, "codigo");
        this.tipoComissaoRepositorio.save(tipoComissaoEncontrada);
    }

    @Override
    public void deletar(Integer codigo) {
        this.tipoComissaoRepositorio.deleteById(codigo);
    }

    public double calcularComissaoMensalidade(Pedido pedido) {
        if (this.verificarSeNaoGerarReceita(pedido)) {
            return 0;
        }
        Integer tipoComissao = pedido.getTipoComissao();
        TipoComissao tipoComissaoEncontrada = this.buscarPorCodigo(tipoComissao);
        Double valorMensalidade = pedido.getValorMensalidade();
        Double porcentagemRepresentante = tipoComissaoEncontrada.getPorcentagemRepresentante();
        Double valorComissao = (valorMensalidade * (porcentagemRepresentante / 100));
        return valorComissao;
    }

    public double calcularComissaoInstalacao(Pedido pedido) {
        if (this.verificarSeNaoGerarReceita(pedido)) {
            return 0;
        }
        Integer tipoComissao = pedido.getTipoComissao();
        TipoComissao tipoComissaoEncontrada = this.buscarPorCodigo(tipoComissao);
        Double valorTotal = pedido.getTotal();
        double valorDaInstalacaoZoom = tipoComissaoEncontrada.getValorSemComissao(); // 200
        double valorMinimoInstalacao = tipoComissaoEncontrada.getValorMinimo(); // 400
        if (Objects.equals(valorTotal, valorDaInstalacaoZoom)) {
            return 0;
        }
        if (valorTotal >= valorMinimoInstalacao) {
            double valorDaComissao = valorTotal / 2;
            return valorDaComissao;
        }
        double valorDaComissao = valorTotal - tipoComissaoEncontrada.getValorSemComissao();
        return valorDaComissao;
    }

    private boolean verificarSeNaoGerarReceita(Pedido pedido) {
        Boolean geraReceita = pedido.getRepresentante().getGeraReceita();
        return geraReceita == null || !geraReceita;
    }

    private void tipoComissaoNaoExiste() throws RecursoNaoEncontradoException {
        this.messageSourceUtil.recursoNaoExiste("tipo-comissao.nao-existe", "tipo-comissao.nao-existe-detalhes");
    }

    public void tipoComissaoJaExiste() throws RecursoJaExisteException {
        this.messageSourceUtil.recursoJaExiste("tipo-comissao.ja-existe", "tipo-comissao.ja-existe-detalhes");
    }

    private void buscarPorNome(TipoComissao tipoComissao) {
        Optional<TipoComissao> tipoComissaoOptional = this.tipoComissaoRepositorio.findByNome(tipoComissao.getNome());
        if (tipoComissaoOptional.isPresent()) {
            this.tipoComissaoJaExiste();
        }
    }
}
