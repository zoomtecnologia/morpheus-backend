package com.zoomtecnologia.morpheus.morpheussistema.modulos.indicacao;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IndicacaoRepositorio extends JpaRepository<Indicacao, IndicacaoPK> {
    
    @Query("select indicacao from Indicacao indicacao where indicacao.identificador=:identificador")
    public Optional<Indicacao> buscarIndicacao(String identificador);
    
    @Query("select "
            + " indicacao.id.codigoIndicacao as codigoIndicacao,"
            + " indicacao.id.emailCliente as emailCliente,"
            + " indicacao.nomeCliente as nomeCliente,"
            + " indicacao.telefone as telefone,"
            + " indicacao.data as data,"
            + " indicacao.identificador as identificador, "
            + " representante.nome as representante "
            + "from "
            + "Indicacao indicacao "
            + "inner join "
            + "Representante representante on(representante.codigoIndicacao=indicacao.id.codigoIndicacao) "
            + "where "
            + " indicacao.nomeCliente like %:pesquisa% "
            + "or indicacao.telefone like %:pesquisa% "
            + "or indicacao.id.emailCliente like %:pesquisa% "
            + "or indicacao.id.codigoIndicacao like %:pesquisa% "
            + "or representante.nome like %:pesquisa% "
    )
    public Page<IndicacaoModeloAPI> listarIndicacoes(String pesquisa,Pageable pageable);
    
}
