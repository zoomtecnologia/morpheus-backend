package com.zoomtecnologia.morpheus.morpheussistema.modulos.indicacao;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@ToString
public class IndicacaoPK implements Serializable {

    @Column(name="codigo_indicacao")
    private String codigoIndicacao;

    @Column(name="email_cliente")
    private String emailCliente;

}
