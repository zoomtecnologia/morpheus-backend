package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planos;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoesModeloAPI;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("zion/planos")
@CrossOrigin("*")
@AllArgsConstructor
public class ZionPlanoControle {

    private final ZionPlanoServico planoServico;

    @GetMapping
    public Collection<ZionPlano> listarPlanos(
            @RequestParam(
                    value = "pesquisa",
                    required = false, defaultValue = "") String pesquisa, 
            @RequestParam(value = "tipoPlano") String tipoPlano) {
        return this.planoServico.listarPlanos(pesquisa,tipoPlano);
    }
    
    @GetMapping("{codigo}/aplicacoes")
    public Set<ZionPlanoAplicacoesModeloAPI> listarAplicacoesPorPlano(@PathVariable("codigo")String codigo) {
        return this.planoServico.listasAplicacoesPorPlano(codigo);
    }

    @GetMapping("{codigo}")
    public ResponseEntity<ZionPlano> buscarUmPlano(
            @PathVariable("codigo") String codigo) {
        final ZionPlano zionPlanoEncontrado = this.planoServico.buscarPlanoAplicacoes(codigo);
        return ResponseEntity.ok(zionPlanoEncontrado);
    }

    @PostMapping
    public ResponseEntity<ZionPlano> salvar(@RequestBody @Valid ZionPlanoInclusaoModeloAPI zionPlano, 
            UriComponentsBuilder uriComponentsBuilder) {
        final ZionPlano planoSalvo = this.planoServico.salvar(zionPlano);
        final Map<String, Object> parametros = new HashMap<>();
        parametros.put("codigo", planoSalvo.getCodigo());
        final URI uri = uriComponentsBuilder.path("planos/{codigo}").buildAndExpand(parametros).toUri();
        return ResponseEntity.created(uri).body(planoSalvo);
    }

    @PutMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarPlano(@PathVariable("codigo") String codigo, @RequestBody @Valid ZionPlanoInclusaoModeloAPI plano) {
        this.planoServico.alterarPlano(codigo,plano);
    }
    
    @DeleteMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarPlano(@PathVariable("codigo") String codigo) {
        this.planoServico.excluirPlano(codigo);
    }
    
    

}
