package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

public final class ItensPedidoRetornoModeloAPI {

    private final Integer numeroPedido;
    private final Integer codigoItem;
    private final String descricao;
    private final Double valor;
    private final Double valorMinimo;
    private final Boolean itemUnico;
    private final Integer quantidade;
    private final String status;

    public ItensPedidoRetornoModeloAPI(Integer numeroPedido, Integer codigoItem, String descricao, Double valor, Double valorMinimo, Boolean itemUnico, Integer quantidade, String status) {
        this.numeroPedido = numeroPedido;
        this.codigoItem = codigoItem;
        this.descricao = descricao;
        this.valor = valor;
        this.valorMinimo = valorMinimo;
        this.itemUnico = itemUnico;
        this.quantidade = quantidade;
        this.status = status;
    }

    public Integer getNumeroPedido() {
        return numeroPedido;
    }

    public Integer getCodigoItem() {
        return codigoItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public Double getValor() {
        return valor;
    }

    public Double getValorMinimo() {
        return valorMinimo;
    }

    public Boolean getItemUnico() {
        return itemUnico;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "ItensPedidoRetornoModeloAPI{" + "numeroPedido=" + numeroPedido + ", codigoItem=" + codigoItem + ", descricao=" + descricao + ", valor=" + valor + ", valorMinimo=" + valorMinimo + ", itemUnico=" + itemUnico + ", quantidade=" + quantidade + ", status=" + status + '}';
    }
    
    
    
}
