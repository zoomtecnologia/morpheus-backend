package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.aplicacao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionAplicacaoRepositorio extends JpaRepository<ZionAplicacao, ZionAplicacaoPK> {

    @Query("select aplicacao from ZionAplicacao aplicacao where aplicacao.nome like %:pesquisa% or aplicacao.id.codigo like %:pesquisa% or aplicacao.id.modulo like %:pesquisa% order by aplicacao.nome")
    public List<ZionAplicacao> listarAplicacoes(String pesquisa);

    @Query("select aplicacao from ZionAplicacao aplicacao where aplicacao.id.codigo=:codigo and aplicacao.id.modulo=:modulo")
    public Optional<ZionAplicacao> buscarAplicacao(String codigo, String modulo);

}
