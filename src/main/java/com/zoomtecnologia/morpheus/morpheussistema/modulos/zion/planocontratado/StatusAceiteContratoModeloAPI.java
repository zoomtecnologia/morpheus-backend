package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class StatusAceiteContratoModeloAPI {

    @NotBlank
    @Size(max = 1)
    private String status;

    public boolean contratoAceito (){
        return this.status.equals("A");
    }
    
    public boolean contratoNaoAceito (){
        return !this.contratoAceito();
    }
    
    
}
