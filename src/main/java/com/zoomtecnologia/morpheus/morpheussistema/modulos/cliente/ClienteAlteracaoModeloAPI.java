package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ClienteAlteracaoModeloAPI extends ClienteGenericoModeloAPI {

    @NotBlank
    @Size(max = 1)
    private String statusAtividade;
    private int quantidadePDVs;

}
