package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import java.io.Serializable;
import javax.persistence.Column;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Embeddable
public class EnderecoCliente implements Serializable {

    @Size(max = 60)
    @NotBlank
    @Column(name="logradouro")
    private String logradouro;

    @Size(max = 10)
    @NotBlank
    @Column(name="numero")
    private String numero;

    @Size(max = 60)
    @NotBlank
    @Column(name="bairro")
    private String bairro;

    @Size(max = 60)
    @NotBlank
    @Column(name="cidade")
    private String cidade;

    @Size(max = 2)
    @NotBlank
    @Column(name="estado")
    private String estado;

    @Size(max = 60)
    @Column(name="complemento")
    private String complemento;

    @Size(max = 8)
    @NotBlank
    @Column(name="cep")
    private String cep;
    
    @Size(max = 7)
    @NotBlank
    @Column(name="codigo_municipio")
    private String codigoMunicipio;
    
    @Size(max = 50)
    @NotBlank
    @Column(name="nome_estado")
    private String nomeEstado;
    
    @Size(max = 2)
    @NotBlank
    @Column(name="codigo_estado")
    private String codigoEstado;

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getNomeEstado() {
        return nomeEstado;
    }

    public void setNomeEstado(String nomeEstado) {
        this.nomeEstado = nomeEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    @Override
    public String toString() {
        return "EnderecoCliente{" + "logradouro=" + logradouro + ", numero=" + numero + ", bairro=" + bairro + ", cidade=" + cidade + ", estado=" + estado + ", complemento=" + complemento + ", cep=" + cep + ", codigoMunicipio=" + codigoMunicipio + ", nomeEstado=" + nomeEstado + ", codigoEstado=" + codigoEstado + '}';
    }
    
}
