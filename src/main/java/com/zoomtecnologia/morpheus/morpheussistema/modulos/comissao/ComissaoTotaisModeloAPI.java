package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

public final class ComissaoTotaisModeloAPI {
    
    private final Double totalLiberado;
    private final Double totalPendente;
    private final Double totalPago;
    private final String nomeRepresentante;

    public ComissaoTotaisModeloAPI(Double totalLiberado, Double totalPendente, Double totalPago, String nomeRepresentante) {
        this.totalLiberado = totalLiberado;
        this.totalPendente = totalPendente;
        this.totalPago = totalPago;
        this.nomeRepresentante = nomeRepresentante;
    }

    public Double getTotalLiberado() {
        return totalLiberado;
    }

    public Double getTotalPendente() {
        return totalPendente;
    }

    public Double getTotalPago() {
        return totalPago;
    }

    public String getNomeRepresentante() {
        return nomeRepresentante;
    }

    @Override
    public String toString() {
        return "ComissaoTotaisModeloAPI{" + "totalLiberado=" + totalLiberado + ", totalPendente=" + totalPendente + ", totalPago=" + totalPago + ", nomeRepresentante=" + nomeRepresentante + '}';
    }
    
    
}
