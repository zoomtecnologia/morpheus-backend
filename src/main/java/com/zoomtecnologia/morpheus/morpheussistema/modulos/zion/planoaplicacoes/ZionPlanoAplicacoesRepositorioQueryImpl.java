package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ZionPlanoAplicacoesRepositorioQueryImpl implements ZionPlanoAplicacoesRepositorioQuery {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void salvar(List<ZionPlanoAplicacoes> aplicacoes) {
        if (aplicacoes == null || aplicacoes.isEmpty()) {
            return;
        }
        final String cabecalhoInsert = "insert into zion_planos_aplicacao(codigo_plano,codigo_aplicacao,codigo_modulo) values ";
        String conteudo = "";
        for (ZionPlanoAplicacoes zionPlanoAplicacoes : aplicacoes) {
            conteudo += ",(";
            conteudo += "'" + zionPlanoAplicacoes.getId().getCodigoPlano() + "',";
            conteudo += "'" + zionPlanoAplicacoes.getId().getCodigoAplicacao() + "',";
            conteudo += "'" + zionPlanoAplicacoes.getId().getModuloAplicacao() + "'";
            conteudo += ")";
        }
        final String atualizacao = " ON DUPLICATE KEY UPDATE codigo_aplicacao = values(codigo_aplicacao), codigo_modulo = values(codigo_modulo);";
        final String sql = cabecalhoInsert + conteudo.replaceFirst(",", "") + atualizacao;
        this.entityManager.createNativeQuery(sql).executeUpdate();
    }

}
