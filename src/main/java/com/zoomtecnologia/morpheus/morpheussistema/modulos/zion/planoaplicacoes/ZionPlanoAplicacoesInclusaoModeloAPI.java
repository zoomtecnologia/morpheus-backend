package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ZionPlanoAplicacoesInclusaoModeloAPI {

    @NotBlank
    @Size(max = 36)
    private String codigoAplicacao;

    @NotBlank
    @Size(max = 36)
    private String codigoModulo;

    @NotNull
    private StatusPlanoAplicacoes status;

    public boolean isAtivo() {
        return this.status.equals(StatusPlanoAplicacoes.ATIVO);
    }

    public boolean isInativo() {
        return this.status.equals(StatusPlanoAplicacoes.INATIVO);
    }
}
