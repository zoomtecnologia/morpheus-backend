package com.zoomtecnologia.morpheus.morpheussistema.modulos.item;

import com.zoomtecnologia.morpheus.morpheussistema.generico.GenericoControle;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("itens")
@CrossOrigin
public class ItemControle extends GenericoControle<Item, Integer> {

    @Override
    public Map<String, Object> getParametros(Item entidade) {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("codigo", entidade.getCodigo());
        return parametros;
    }

    @Override
    public String getURI() {
        return "/itens/{codigo}";
    }

}
