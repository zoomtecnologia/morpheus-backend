package com.zoomtecnologia.morpheus.morpheussistema.modulos.tipocomissao;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "tipo_comissao")
public class TipoComissao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    
	@Column(name = "codigo")
	private Integer codigo;

    @Size(max = 20)
    @NotBlank
    
	@Column(name = "nome")
	private String nome;

    @NotNull
    
	@Column(name = "porcentagem_representante")
	private Double porcentagemRepresentante;

    @NotNull
    
	@Column(name = "porcentagem_zoom")
	private Double porcentagemZoom;

    @NotNull
    
	@Column(name = "valor_minimo")
	private Double valorMinimo;
    
    @NotNull
    
	@Column(name = "valor_sem_comissao")
	private Double valorSemComissao;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro;
    
    @NotNull
    @Size(max = 1)
    
	@Column(name = "status")
	private String status;
  

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getPorcentagemRepresentante() {
        return porcentagemRepresentante;
    }

    public void setPorcentagemRepresentante(Double porcentagemRepresentante) {
        this.porcentagemRepresentante = porcentagemRepresentante;
    }

    public Double getPorcentagemZoom() {
        return porcentagemZoom;
    }

    public void setPorcentagemZoom(Double porcentagemZoom) {
        this.porcentagemZoom = porcentagemZoom;
    }

    public Double getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(Double valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public Double getValorSemComissao() {
        return valorSemComissao;
    }

    public void setValorSemComissao(Double valorSemComissao) {
        this.valorSemComissao = valorSemComissao;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

  
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoComissao other = (TipoComissao) obj;
        return Objects.equals(this.codigo, other.codigo);
    }
  
    
}
