package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ZionPlanoContratadoModeloAPI {

    private String cnpj;

    private String razaoSocial;

    private Double implantacao;

    private Double mensalidade;

    private Integer diaVencimento;

    private String dataInicioContrato;

    private boolean status;

    private Set<ZionPlanoContratacaoAplicacoes> aplicacoes;

    public ZionPlanoContratadoModeloAPI() {

    }

    public ZionPlanoContratadoModeloAPI(List<ZionPlanoContratadoResumido> zionPlanoContratadoResumido) {
        ZionPlanoContratadoResumido dadosContrato = zionPlanoContratadoResumido.get(0);
        this.cnpj = dadosContrato.getCnpj();
        this.razaoSocial = dadosContrato.getRazaoSocial();
        this.implantacao = dadosContrato.getImplantacao();
        this.mensalidade = dadosContrato.getMensalidade();
        this.status = dadosContrato.getStatus() == 1;
        this.diaVencimento = dadosContrato.getDiaVencimento();
        this.dataInicioContrato = dadosContrato.getDataInicioContrato();
        this.aplicacoes = new HashSet<>();
        for (ZionPlanoContratadoResumido aplicacao : zionPlanoContratadoResumido) {
            ZionPlanoContratacaoAplicacoes zionPlanoContratacaoAplicacoes = new ZionPlanoContratacaoAplicacoes();
            zionPlanoContratacaoAplicacoes.setModulo(aplicacao.getModulo());
            zionPlanoContratacaoAplicacoes.setNomeAplicacao(aplicacao.getNomeAplicacao());
            this.aplicacoes.add(zionPlanoContratacaoAplicacoes);
        }
    }

    public static class ZionPlanoContratacaoAplicacoes {

        private String modulo;
        private String nomeAplicacao;

        public String getModulo() {
            return modulo;
        }

        public void setModulo(String modulo) {
            this.modulo = modulo;
        }

        public String getNomeAplicacao() {
            return nomeAplicacao;
        }

        public void setNomeAplicacao(String nomeAplicacao) {
            this.nomeAplicacao = nomeAplicacao;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 61 * hash + Objects.hashCode(this.modulo);
            hash = 61 * hash + Objects.hashCode(this.nomeAplicacao);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ZionPlanoContratacaoAplicacoes other = (ZionPlanoContratacaoAplicacoes) obj;
            if (!Objects.equals(this.modulo, other.modulo)) {
                return false;
            }
            return Objects.equals(this.nomeAplicacao, other.nomeAplicacao);
        }

    }
}
