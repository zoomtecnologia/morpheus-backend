package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PedidoRepositorio extends JpaRepository<Pedido, Integer> {

    static final String COLUNAS = "new com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoRetornoModeloAPI("
            + "pedido.numero,pedido.data,pedido.representante.documento,pedido.representante.nome,"
            + "pedido.cliente.cnpj,pedido.cliente.razaoSocial,pedido.status,pedido.tipoComissao,"
            + "pedido.total,pedido.valorMensalidade,pedido.representante.geraReceita"
            + ")";

    @Query("select " + COLUNAS + " from Pedido pedido where "
            + "pedido.representante.documento like %:pesquisa% or pedido.representante.nome like %:pesquisa% or "
            + "pedido.cliente.cnpj like %:pesquisa% or pedido.cliente.razaoSocial like %:pesquisa%")
    public Page<PedidoRetornoModeloAPI> listarTodos(@Param("pesquisa") String pesquisaConteudo, Pageable pageable);

    @Query("select pedido from Pedido pedido join fetch pedido.cliente cliente where cliente.dataFimVigenciaContrato=current_date")
    public List<Pedido> listarClientesComContratoVencendoNoDia();

    @Query("select " + COLUNAS + " from Pedido pedido where pedido.cliente.cnpj =:cnpj")
    public Optional<PedidoRetornoModeloAPI> buscarPedidoResumidoPorCliente(@Param("cnpj") String cnpj);

    @Query("select pedido from Pedido pedido join fetch pedido.cliente cliente join fetch pedido.representante where pedido.cliente.cnpj=:cnpj")
    public Optional<Pedido> buscarPedidoPorCliente(@Param("cnpj") String cnpj);

    @Query(value = "select new com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoStatusModeloAPI(pedido.status,pedido.status) from Pedido pedido group by pedido.status")
    public List<PedidoStatusModeloAPI> listarStatusPedido();

    @Transactional
    @Modifying
    @Query("update Pedido pedido set pedido.status=:status where pedido.numero=:numero")
    public void alterarStatus(@Param("numero") Integer numeroPedido, @Param("status") PedidoStatus statusPedido);

    @Transactional
    @Modifying
    @Query("update Pedido pedido set pedido.status=:status where pedido.cliente.cnpj=:cnpj")
    public void alterarStatusPedidoPorCliente(@Param("cnpj") String cnpj, @Param("status") PedidoStatus statusPedido);

    public int countByCliente_Cnpj(@Param("cnpj") String cnpjCliente);

}
