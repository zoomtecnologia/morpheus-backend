package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;

@Repository
public interface RepresentanteRepositorio extends JpaRepository<Representante, String> {

    @Query("select representante from Representante representante where representante.documento like %:pesquisa% or "
            + "representante.nome like %:pesquisa% or representante.nomeSocial like %:pesquisa% ")
    public Page<RepresentanteRetornoModeloAPI> listarRepresentantes(@Param("pesquisa") String pesquisa, Pageable pageable);

    @Query("select representante from Representante representante ")
    public List<RepresentanteResumidoModeloAPI> listarRepresentanteResumido();

    @Query("select representante from Representante representante order by representante.nome")
    public List<RepresentanteRetornoModeloAPI> listarRepresentantes();

    @Transactional
    @Modifying
    @Query("update Representante set status=:status where documento=:documento")
    public void bloquearRepresentanteDesseRepresentante(@Param("documento") String documento, @Param("status") String status);
}
