package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PedidoClienteStatus {

    @Autowired
    private ClienteServico clienteServico;

    public void alterarStatusPedidoCliente(Pedido pedido) {
        this.clienteServico.alterarStatusPedido(pedido.getCliente().getCnpj(), pedido.getStatus().getStatus());
    }
    
    public void atualizarDataContrato(Pedido pedido) {
        Cliente cliente = pedido.getCliente();
        clienteServico.aceitarContrato(cliente.getCnpj());
    }
    

}
