package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import java.time.LocalDateTime;
import javax.persistence.Convert;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

public class PedidoAlteracaoModeloAPI {

    @NotNull
    private Integer tipoComissao;

    @NotNull
    private Double valorMensalidade;
    
    @NotNull
    private Double total;

    @NotNull
    @Convert(converter = PedidoStatus.Mapeador.class)
    private PedidoStatus status;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dataMarcacaoInstalacao;

    @Email
    private String email;
    
    private String obs;

    public Integer getTipoComissao() {
        return tipoComissao;
    }

    public void setTipoComissao(Integer tipoComissao) {
        this.tipoComissao = tipoComissao;
    }

    public Double getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(Double valorMensalidade) {
        this.valorMensalidade = valorMensalidade;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public PedidoStatus getStatus() {
        return status;
    }

    public void setStatus(PedidoStatus status) {
        this.status = status;
    }

    public LocalDateTime getDataMarcacaoInstalacao() {
        return dataMarcacaoInstalacao;
    }

    public void setDataMarcacaoInstalacao(LocalDateTime dataMarcacaoInstalacao) {
        this.dataMarcacaoInstalacao = dataMarcacaoInstalacao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    @Override
    public String toString() {
        return "PedidoAlteracaoModeloAPI{" + "tipoComissao=" + tipoComissao + ", valorMensalidade=" + valorMensalidade + ", total=" + total + ", status=" + status + ", dataMarcacaoInstalacao=" + dataMarcacaoInstalacao + ", email=" + email + ", obs=" + obs + '}';
    }

    
}
