package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ZionPlanoAplicacoesPK implements Serializable{

    @Column(name="codigo_plano")
    private String codigoPlano;
    
    @Column(name="codigo_aplicacao")
    private String codigoAplicacao;

    @Column(name="codigo_modulo")
    private String moduloAplicacao;
}
