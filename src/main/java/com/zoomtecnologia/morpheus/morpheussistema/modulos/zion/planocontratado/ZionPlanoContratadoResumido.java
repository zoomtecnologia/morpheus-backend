package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

public interface ZionPlanoContratadoResumido {

    String getCnpj();

    String getRazaoSocial();

    Double getImplantacao();

    Double getMensalidade();

    Integer getDiaVencimento();

    String getDataInicioContrato();

    String getModulo();
    
    Integer getStatus();

    String getNomeAplicacao();

}
