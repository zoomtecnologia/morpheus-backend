package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import com.zoomtecnologia.morpheus.morpheussistema.email.Mailer;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao.Permissao;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServico {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    @Autowired
    private Mailer mailer;

    @Autowired
    private ModelMapper modelMapper;

    public List<UsuarioModeloAPI> listarUsuarios() {
       return this.usuarioRepositorio.listarUsuarios();
    }

    public List<UsuarioResumidoModeloAPI> listarUsuariosResumido() {
        return this.usuarioRepositorio.listarUsuariosResumidos();
    }

    public Usuario buscarPorCodigo(Integer codigo) {
        Optional<Usuario> usuarioOptional = this.usuarioRepositorio.findById(codigo);
        if (!usuarioOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("usuario.nao-existe", "usuario.nao-existe");
        }
        return usuarioOptional.get();
    }

    public UsuarioResumidoModeloAPI buscarUsuarioResumidoPorCodigo(Integer codigo) {
        Optional<UsuarioResumidoModeloAPI> usuarioOptional = this.usuarioRepositorio.buscarUsuarioResumido(codigo);
        if (!usuarioOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("usuario.nao-existe", "usuario.nao-existe");
        }
        return usuarioOptional.get();
    }

    public Usuario buscarUsuarioPorRecuperacaoSenha(String chave) {
        Optional<Usuario> usuarioOptional = this.usuarioRepositorio.buscarUsuarioPorChaveRecuperacaoSenha(chave);
        if (!usuarioOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("usuario.nao-existe", "usuario.nao-existe");
        }
        return usuarioOptional.get();
    }

    public Usuario salvarUsuario(UsuarioInclusaoModeloAPI usuarioInclusaoModeloAPI) {
        int quantidadeUsuarioComEsseEmail = this.usuarioRepositorio.countByEmail(usuarioInclusaoModeloAPI.getEmail());
        if (quantidadeUsuarioComEsseEmail >= 1) {
            this.messageSourceUtil.recursoJaExiste("usuario.ja-existe", "usuario.ja-existe");
        }
        Usuario usuario = this.modelMapper.map(usuarioInclusaoModeloAPI, Usuario.class);
        usuario.setPermissao(new Permissao(usuarioInclusaoModeloAPI.getPermissao()));
        usuario.getRepresentante().setDocumento(usuarioInclusaoModeloAPI.getDocumentoRepresentante());
        usuario.setStatus("A");
        usuario.setDataCadastro(LocalDate.now());
        Usuario usuarioSalvo = this.usuarioRepositorio.save(usuario);
        return usuarioSalvo;
    }

    public void alterarUsuario(Integer codigo, Usuario usuario) {
        Usuario usuarioEncontrado = this.buscarPorCodigo(codigo);
        usuarioEncontrado.setNome(usuario.getNome());
        usuarioEncontrado.setStatus(usuario.getStatus());
        usuarioEncontrado.setRepresentante(usuario.getRepresentante());
        usuarioEncontrado.setSenha(usuario.getSenha());
        this.usuarioRepositorio.save(usuarioEncontrado);
    }

    public void excluirUsuario(Integer codigo) {
        this.usuarioRepositorio.deleteById(codigo);
    }

    public UsuarioModeloAPI realizarLogin(UsuarioLoginModeloAPI usuarioLogin) {
        boolean usuarioNaoCadastrado = this.usuarioRepositorio.countByEmail(usuarioLogin.getEmail()) == 0;
        if(usuarioNaoCadastrado){
            this.messageSourceUtil.recursoNaoExiste("usuario.nao-existe", "usuario.nao-existe");            
        }
        boolean existeUsuario = this.usuarioRepositorio.countByEmailAndSenha(usuarioLogin.getEmail(), usuarioLogin.getSenha()) >= 1;
        if (!existeUsuario) {
            this.messageSourceUtil.recursoNaoExiste("usuario.senha-incorreta", "usuario.senha-incorreta");
        }
        UsuarioModeloAPI usuarioLogado = this.usuarioRepositorio.buscarPorEmail(usuarioLogin.getEmail()).get();
        if (usuarioLogado.getStatus().equals("I")) {
            this.messageSourceUtil.recursoNaoExiste("usuario.inativo", "usuario.inativo");
        }
        if(usuarioLogado.getRepresentante().getStatus().equals("B")){
            this.messageSourceUtil.negocioException("usuario.representante-bloqueado", "usuario.representante-bloqueado-detalhes");
        }
        this.usuarioRepositorio.atualizarDataDeAcesso(usuarioLogado.getEmail());
        return usuarioLogado;
    }

    public boolean verificarEmailCadastrado(UsuarioRecuperarSenha usuarioRecuperarSenha) {
        Optional<UsuarioRecuperacaoSenhaModeloAPI> usuarioOptional = this.usuarioRepositorio.buscarUsuarioParaRecurarSenha(usuarioRecuperarSenha.getEmail());
        if (!usuarioOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("usuario.email-nao-existe", "usuario.email-nao-existe-detalhes");
        }
        UsuarioRecuperacaoSenhaModeloAPI usuario = usuarioOptional.get();
        if (usuario.getStatus().equals("I")) {
            this.messageSourceUtil.negocioException("usuario.inativo", "usuario.inativo");
        }
        return true;
    }

    public void enviarEmailRecuperacaoSenha(UsuarioRecuperarSenha usuarioRecuperarSenha) {
        Optional<UsuarioRecuperacaoSenhaModeloAPI> usuarioOptional = this.usuarioRepositorio.buscarUsuarioParaRecurarSenha(usuarioRecuperarSenha.getEmail());
        if (!usuarioOptional.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("usuario.email-nao-existe", "usuario.email-nao-existe-detalhes");
        }
        UsuarioRecuperacaoSenhaModeloAPI usuario = usuarioOptional.get();
        Usuario usuarioGerarChaveDeRecuperacao = new Usuario();
        usuarioGerarChaveDeRecuperacao.gerarCodigoRecuperacaoSenha();
        usuario.setRecuperandoSenha(usuarioGerarChaveDeRecuperacao.getRecuperandoSenha());
        this.mailer.enviarEmailConfirmacaoSenha(usuario);
        this.usuarioRepositorio.atualizarRecuperacaoSenha(usuario.getRecuperandoSenha(), usuario.getEmail());
    }

    public UsuarioRecuperarSenha recuperarSenha(UsuarioRecuperarSenha usuarioRecuperarSenha) {
        int quantidadeUsuarioComEsseEmail = this.usuarioRepositorio.countByEmail(usuarioRecuperarSenha.getEmail());
        boolean usuarioNaoExiste = quantidadeUsuarioComEsseEmail == 0;
        if (usuarioNaoExiste) {
            this.messageSourceUtil.recursoNaoExiste("usuario.email-nao-existe", "usuario.email-nao-existe-detalhes");
        }
        if (!usuarioRecuperarSenha.getSenha().equalsIgnoreCase(usuarioRecuperarSenha.getConfirmaSenha())) {
            this.messageSourceUtil.recursoNaoExiste("usuario.senhas-incorretas", "usuario.senhas-incorretas");
        }
        this.usuarioRepositorio.mudarSenha(null, usuarioRecuperarSenha.getEmail(), usuarioRecuperarSenha.getSenha());
        return usuarioRecuperarSenha;
    }

    public Optional<UsuarioResumidoModeloAPI> buscarUsuarioAtivoPorRepresentante(String documento) {
        return this.usuarioRepositorio.buscarUsuarioAtivoPorRepresentante(documento);
    }

}
