package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


public class RepresentanteInclusaoModeloAPI extends RepresentanteGenericoModeloAPI{

    @Size(max = 14)
    @NotBlank
    private String documento;

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    @Override
    public String toString() {
        return "RepresentanteInclusaoModeloAPI{" + "documento=" + documento + '}';
    }

    
}
