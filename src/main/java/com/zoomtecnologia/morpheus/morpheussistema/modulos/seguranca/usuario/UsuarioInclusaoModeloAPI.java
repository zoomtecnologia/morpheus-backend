package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UsuarioInclusaoModeloAPI {

    @NotBlank
    private String nome;
    @NotBlank
    private String email;
    @NotBlank
    private String senha;
    @NotBlank
    private String documentoRepresentante;
    @NotNull
    private Integer permissao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentoRepresentante() {
        return documentoRepresentante;
    }

    public void setDocumentoRepresentante(String documentoRepresentante) {
        this.documentoRepresentante = documentoRepresentante;
    }

    public Integer getPermissao() {
        return permissao;
    }

    public void setPermissao(Integer permissao) {
        this.permissao = permissao;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public String toString() {
        return "UsuarioInclusaoModeloAPI{" + "nome=" + nome + ", email=" + email + ", senha=" + senha + ", documentoRepresentante=" + documentoRepresentante + ", permissao=" + permissao + '}';
    }
}
