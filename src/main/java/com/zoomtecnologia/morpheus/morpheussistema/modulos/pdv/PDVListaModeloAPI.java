/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import java.time.LocalDateTime;

/**
 *
 * @author eudes
 */
public interface PDVListaModeloAPI {

    int getNumeroPdv();

    String getChave();

    LocalDateTime getDataCadastro();
    
    LocalDateTime getUltimoAcesso();

}
