package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.br.CNPJ;

@Getter
@Setter
@ToString
public class ClienteInclusaoModeloAPI extends ClienteGenericoModeloAPI {

    @Size(max = 14)
    @NotBlank
    @CNPJ
    private String cnpj;
    
    private int quantidadePDVs;    

}
