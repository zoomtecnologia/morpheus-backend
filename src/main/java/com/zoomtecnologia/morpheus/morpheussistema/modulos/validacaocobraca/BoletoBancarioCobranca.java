package com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca;

public class BoletoBancarioCobranca {

    private String paid_at;
    private String expire_at;
    private float paid_value;

    public String getPaid_at() {
        return paid_at;
    }

    public void setPaid_at(String paid_at) {
        this.paid_at = paid_at;
    }

    public String getExpire_at() {
        return expire_at;
    }

    public void setExpire_at(String expire_at) {
        this.expire_at = expire_at;
    }

    public float getPaid_value() {
        return paid_value;
    }

    public void setPaid_value(float paid_value) {
        this.paid_value = paid_value;
    }

    @Override
    public String toString() {
        return "BoletoBancarioCobranca{" + "paid_at=" + paid_at + ", expire_at=" + expire_at + ", paid_value=" + paid_value + '}';
    }
    
}
