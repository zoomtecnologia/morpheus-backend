package com.zoomtecnologia.morpheus.morpheussistema.modulos.indicacao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IndicacaoInclusaoModeloAPI {

    @Size(max = 10)
    private String codigoIndicacao;

    @NotBlank
    @Size(max = 150)
    private String emailCliente;

    @NotBlank
    @Size(max = 60)
    private String nomeCliente;

    @NotBlank
    @Size(max = 14)
    private String telefone;

    public Indicacao converterIndicacaoModeloAPI() {
        final Indicacao indicacao = new Indicacao();
        final IndicacaoPK id = new IndicacaoPK();
        id.setCodigoIndicacao(this.codigoIndicacao);
        id.setEmailCliente(this.emailCliente.trim());       
        indicacao.setId(id);
        indicacao.setNomeCliente(this.nomeCliente.trim());
        indicacao.setTelefone(this.telefone);
        return indicacao;
    }

}
