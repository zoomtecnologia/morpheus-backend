package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao.ZionPlanoPrincipal;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "zion_plano_contratado")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"codigo"})
public class ZionPlanoContratado implements Serializable {

    @Id

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "codigo_empresa")
    private String codigoEmpresa;

    @Column(name = "grupo_empresa")
    private String grupoEmpresa;

    @Column(name = "documento_identificacao_empresa")
    private String documentoIdentificacaoEmpresa;

    @Column(name = "data_inicio_contrato")
    private LocalDate dataInicioContrato;

    @Column(name = "data_fim_contrato")
    private LocalDate dataFimContrato;

    @Column(name = "quantidade_usuarios")
    private int quantidadeUsuarios;

    @Column(name = "status")
    private boolean status;

    @Column(name = "status_pagamento")
    private String statusPagamento;

    @Column(name = "data_proximo_vencimento")
    private LocalDate dataProximoVencimento;

    @Column(name = "data_cancelamento")
    private LocalDate dataCancelamento;

    @Column(name = "dias_tolerancia")
    private int diasTolerancia;

    @Column(name = "pdvs")
    private Integer pdvs;

    @Column(name = "implantacao")
    private Double implantacao;

    @Column(name = "mensalidade")
    private double mensalidade;

    @Column(name = "adicional")
    private double adicional;

    @Column(name = "desconto")
    private double desconto;

    @Transient
    private boolean empresaNova;

    public void definirPeriodoContrato(ZionPlanoPrincipal zionPlanoPrincipal) {
        this.dataInicioContrato = LocalDate.now();
        if (zionPlanoPrincipal.getTeste()) {
            Integer diasParaTeste = zionPlanoPrincipal.getDiasParaTeste();
            this.dataFimContrato = dataInicioContrato.plusDays(diasParaTeste == null ? 30 : diasParaTeste);
            return;
        }
        int periodoIdeterminado = 20;
        this.dataFimContrato = dataInicioContrato.plusYears(periodoIdeterminado);
    }

    public boolean temPDV() {
        return this.pdvs != null && this.pdvs > 0;
    }
}
