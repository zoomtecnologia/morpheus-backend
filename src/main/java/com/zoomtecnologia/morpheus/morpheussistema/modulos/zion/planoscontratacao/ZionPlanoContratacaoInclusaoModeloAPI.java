package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ZionPlanoContratacaoInclusaoModeloAPI {

    @Size(max = 36)
    @NotBlank
    private String codigoPlano;

    @Size(max =150)
    @NotBlank
    private String usuario;

}
