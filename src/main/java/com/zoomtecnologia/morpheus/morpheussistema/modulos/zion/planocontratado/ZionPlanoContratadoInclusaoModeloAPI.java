package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao.ZionPlanoContratacaoInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao.ZionPlanosContratacao;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ZionPlanoContratadoInclusaoModeloAPI {

    private int quantidadeUsuarios;

    private int pdvs;

    private Double implantacao;

    private Double mensalidade;

    private double adicional;

    private double desconto;

    @NotBlank
    @Size(max = 20)
    private String documentoIdentificacaoCliente;

    @NotNull
    @Valid
    private List<ZionPlanoContratacaoInclusaoModeloAPI> planos;

    public ZionPlanoContratado converterParaZionPlanoContratado() {
        ZionPlanoContratado zionPlanoContratado = new ZionPlanoContratado();
        zionPlanoContratado.setQuantidadeUsuarios(this.quantidadeUsuarios);
        zionPlanoContratado.setPdvs(this.pdvs);
        zionPlanoContratado.setImplantacao(this.implantacao);
        zionPlanoContratado.setMensalidade(this.mensalidade);
        zionPlanoContratado.setAdicional(this.adicional);
        zionPlanoContratado.setDesconto(this.desconto);
        zionPlanoContratado.setDocumentoIdentificacaoEmpresa(this.documentoIdentificacaoCliente);
        return zionPlanoContratado;
    }

    public List<ZionPlanosContratacao> converterParaZionPlanosContratacao(String codigoContratacao) {
        return this.planos.stream().map(planoInclusao -> {
            ZionPlanosContratacao zionPlanosContratacao = new ZionPlanosContratacao();
            zionPlanosContratacao.setPlanoInclusao(planoInclusao, codigoContratacao);
            return zionPlanosContratacao;
        }).collect(Collectors.toList());
    }

}
