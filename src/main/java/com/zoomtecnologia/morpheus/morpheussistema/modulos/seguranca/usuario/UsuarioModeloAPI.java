package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.aplicacao.Aplicacao;
import java.util.List;

public interface UsuarioModeloAPI {

    Integer getCodigo();

    String getNome();

    String getEmail();

    RepresentanteResumido getRepresentante();

    String getStatus();

    PermissaoResumida getPermissao();

    interface RepresentanteResumido {

        String getDocumento();

        String getNome();

        String getNivel();

        String getTipo();
        
        String getStatus();
        
        String getCodigoIndicacao();
    }

    interface PermissaoResumida {
        
        Integer getCodigo();

        String getNome();

        Boolean getRepresentante();

        List<PermissaoAplicacoesResumida> getAplicacoes();

        interface PermissaoAplicacoesResumida {

            @JsonProperty("permissaoAplicacao")
            PermissaoAplicacoesPKResumida getPermissaoAplicacaoPK();

            Boolean getStatus();

            Boolean getAcessar();

            Boolean getIncluir();

            Boolean getAlterar();

            Boolean getExcluir();

            interface PermissaoAplicacoesPKResumida {

                Aplicacao getAplicacao();
            }
        }
    }

}
