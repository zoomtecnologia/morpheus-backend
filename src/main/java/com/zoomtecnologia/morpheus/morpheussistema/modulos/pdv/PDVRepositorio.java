/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PDVRepositorio extends JpaRepository<PDV, Integer>,PDVRepositorioQuery{
    
    @Query("select pdv.chave as chave,pdv.dataCadastrado as dataCadastro, pdv.id.numeroPdv as numeroPdv, pdv.ultimoAcesso as ultimoAcesso  from PDV pdv where pdv.id.cnpj=:cnpj and pdv.id.codigoRepresentante=:codigoRepresentante")
    public List<PDVListaModeloAPI> listarPDVs(String cnpj,String codigoRepresentante);
    
    @Modifying
    @Query("delete from PDV pdv where pdv.id.cnpj=:cnpj and pdv.id.codigoRepresentante=:codigoRepresentante")
    public void excluirPDVPorCliente(String cnpj,String codigoRepresentante);

    @Query("select pdv from PDV pdv where pdv.id.cnpj=:cnpj and pdv.id.codigoRepresentante=:codigoRepresentante and pdv.id.numeroPdv=:numeroPDV")
    public Optional<PDV> buscarPDV(String cnpj, String codigoRepresentante, int numeroPDV);
    
}
