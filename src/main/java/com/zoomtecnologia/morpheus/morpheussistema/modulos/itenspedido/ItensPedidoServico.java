package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoJaExisteException;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.item.Item;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.item.ItemServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoStatus;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItensPedidoServico {

    @Autowired
    private ItensPedidoRepositorio itensPedidoRepositorio;

    @Autowired
    private ItemServico itemServico;

    @Autowired
    private PedidoServico pedidoServico;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    @Autowired
    private ModelMapper modelMapper;

    public List<ItensPedidoRetornoModeloAPI> listarItensPedido(Integer numeroPedido) {
        return this.itensPedidoRepositorio.listarItensPorNumeroPedido(numeroPedido);
    }

    public ItensPedidoRetornoModeloAPI buscarItensPedido(Integer numeroPedido, Integer codigoItem) {
        Optional<ItensPedidoRetornoModeloAPI> itensPedido = this.itensPedidoRepositorio.buscarItensPedidoPorCodigo(numeroPedido, codigoItem);
        if (!itensPedido.isPresent()) {
            itemPedidoNaoEncontrado();
        }
        return itensPedido.get();
    }

    public List<ItensPedido> salvarItemPedido(Integer numeroPedido, List<ItensPedidoInclusaoModeloAPI> itensPedidoInclusaoModeloAPI) {
        this.itensPedidoRepositorio.salvarItensPedido(numeroPedido, itensPedidoInclusaoModeloAPI);
        return itensPedidoInclusaoModeloAPI.stream()
                .map(itemPedido -> this.modelMapper.map(itemPedido, ItensPedido.class))
                .collect(Collectors.toList());
    }

    public void alterarItemPedido(Integer numeroPedido,List<ItensPedidoAlteracaoModeloAPI> itensPedidoAlteracaoModeloAPI) {        
        this.itensPedidoRepositorio.salvarItensPedido(numeroPedido, itensPedidoAlteracaoModeloAPI);
    }

    public void deletarItemPedido(Integer numeroPedido, Integer codigoItem) {
        ItensPedidoPK itensPedidoPK = this.pegarChave(numeroPedido, codigoItem);
        if (this.itensPedidoRepositorio.count() == 0) {
            this.pedidoServico.alterarStatusPedido(PedidoStatus.ABERTO, numeroPedido);
        }
        this.itensPedidoRepositorio.deleteById(itensPedidoPK);
    }

    private void verificarItemAntesDaInclusaoOuAlteracao(int codigoItem, ItensPedidoGenericoModeloAPI itensPedidoGenericoModeloAPI) throws RecursoJaExisteException {
        Item itemEncontrado = this.itemServico.buscarPorCodigo(codigoItem);
        if (itemEncontrado.getItemUnico() && itensPedidoGenericoModeloAPI.getQuantidade() > 1) {
            messageSourceUtil.negocioException("item-pedido.recurso-quantidade-invalida", "item-pedido.recurso-quantidade-invalida-detalhes");
        }
        if (itensPedidoGenericoModeloAPI.getValor() < itemEncontrado.getValor()) {
            messageSourceUtil.negocioException("item-pedido.recurso-valor-invalido", "item-pedido.recurso-valor-invalido-detalhes");
        }
    }

    private ItensPedidoPK pegarChave(Integer numeroPedido, Integer codigoItem) {
        ItensPedidoPK itensPedidoPK = new ItensPedidoPK();
        itensPedidoPK.setPedido(numeroPedido);
        Item item = new Item();
        item.setCodigo(codigoItem);
        itensPedidoPK.setItem(item);
        return itensPedidoPK;
    }

    private void itemPedidoNaoEncontrado() throws RecursoNaoEncontradoException {
        this.messageSourceUtil.recursoNaoExiste("item-pedido.recurso-nao-encontrado", "item-pedido.recurso-nao-encontrado-detalhes");
    }
}
