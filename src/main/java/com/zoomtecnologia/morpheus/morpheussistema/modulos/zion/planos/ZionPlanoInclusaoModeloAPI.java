package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planos;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoes;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoesInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoesPK;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ZionPlanoInclusaoModeloAPI {

    @Size(max = 50)
    @NotBlank
    private String nome;

    private boolean status;

    private double valor;
    
    private Double valorUsuarioAdicional;
    
    private Double valorPDVAdicional;

    @NotBlank
    private String descricao;

    @Size(max = 1)
    @NotBlank
    private String tipoPlano;

    private boolean geraCobranca;
    
    private int usuarios;
    
    private int pdvs;
    
    private Integer diasParaTeste;
    
    private Boolean teste;

    @NotNull
    @Valid
    private List<ZionPlanoAplicacoesInclusaoModeloAPI> aplicacoes;

    public ZionPlano converterParaZionPlano() {
        final ZionPlano zionPlano = new ZionPlano();
        zionPlano.setNome(this.nome);
        zionPlano.setStatus(this.status);
        zionPlano.setValor(this.valor);
        zionPlano.setValorUsuarioAdicional(this.valorUsuarioAdicional);
        zionPlano.setValorPDVAdicional(this.valorPDVAdicional);
        zionPlano.setDescricao(this.descricao);
        zionPlano.setTipoPlano(this.tipoPlano);
        zionPlano.setGeraCobranca(this.geraCobranca);
        zionPlano.setTeste(this.teste);
        zionPlano.setDiasParaTeste(this.diasParaTeste);
        zionPlano.setUsuarios(this.usuarios);
        zionPlano.setPdvs(this.pdvs);
        return zionPlano;
    }
    
    public List<ZionPlanoAplicacoes> converterParaZionPlanoAplicacoes(String codigoPlano) {
        return this.aplicacoes.stream()
                .filter(ZionPlanoAplicacoesInclusaoModeloAPI::isAtivo)
                .map(aplicacao -> {
                    final ZionPlanoAplicacoes zionPlanoAplicacoes = new ZionPlanoAplicacoes();
                    zionPlanoAplicacoes.setId(new ZionPlanoAplicacoesPK(
                            codigoPlano,
                            aplicacao.getCodigoAplicacao(),
                            aplicacao.getCodigoModulo()
                    ));
                    return zionPlanoAplicacoes;
                }).collect(Collectors.toList());
    }

}
