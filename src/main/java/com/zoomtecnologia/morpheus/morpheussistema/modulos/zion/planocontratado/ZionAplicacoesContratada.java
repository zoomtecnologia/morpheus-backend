package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

public interface ZionAplicacoesContratada {
    String getCodigo();
    String getModulo();
    String getNome();
    String getFuncao();
    int getStatus();
}
