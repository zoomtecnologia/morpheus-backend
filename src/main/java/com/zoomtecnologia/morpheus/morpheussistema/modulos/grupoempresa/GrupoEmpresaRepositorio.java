package com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GrupoEmpresaRepositorio extends JpaRepository<GrupoEmpresa, String> {

    @Query("select grupo from GrupoEmpresa grupo where grupo.nome=:nome")
    public Optional<GrupoEmpresa> buscarPorNome(@Param("nome") String nome);

    public List<GrupoEmpresa> findByNomeContainingIgnoreCaseAndCodigoRepresentante(String pesquisa, String representante);

    public List<GrupoEmpresa> findByNomeContainingIgnoreCase(String pesquisa);

    public Page<GrupoEmpresa> findByNomeContainingIgnoreCase(String pesquisa, Pageable pageable);



}
