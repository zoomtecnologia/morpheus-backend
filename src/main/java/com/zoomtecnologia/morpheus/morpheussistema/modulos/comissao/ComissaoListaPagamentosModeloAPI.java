package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import com.fasterxml.jackson.annotation.JsonIgnore;

public final class ComissaoListaPagamentosModeloAPI {

    private final String dataSolicitacao;
    private final String dataPagamento;
    private final String codigoSolicitacao;
    private final Double totalSolicitacao;
    private final String status;
    private final String nomeRepresentante;
    private final String emailRepresentante;
    private final String chavePix;
    private final String cidade;
    private final String documentoRepresentante;

    public ComissaoListaPagamentosModeloAPI(String dataSolicitacao, String dataPagamento, String codigoSolicitacao, Double totalSolicitacao, String status, String nomeRepresentante, String emailRepresentante, String chavePix, String cidade, String documentoRepresentante) {
        this.dataSolicitacao = dataSolicitacao;
        this.dataPagamento = dataPagamento;
        this.codigoSolicitacao = codigoSolicitacao;
        this.totalSolicitacao = totalSolicitacao;
        this.status = status;
        this.nomeRepresentante = nomeRepresentante;
        this.emailRepresentante = emailRepresentante;
        this.chavePix = chavePix;
        this.cidade = cidade;
        this.documentoRepresentante = documentoRepresentante;
    }

    public String getDataSolicitacao() {
        return dataSolicitacao;
    }

    public String getDataPagamento() {
        return dataPagamento;
    }

    public String getCodigoSolicitacao() {
        return codigoSolicitacao;
    }

    public Double getTotalSolicitacao() {
        return totalSolicitacao;
    }

    public String getStatus() {
        return status;
    }

    public String getNomeRepresentante() {
        return nomeRepresentante;
    }

    public String getEmailRepresentante() {
        return emailRepresentante;
    }

    @JsonIgnore
    public boolean comissaoPaga() {
        return this.dataPagamento != null;
    }

    public String getChavePix() {
        return chavePix;
    }

    public String getCidade() {
        return cidade;
    }

    public String getDocumentoRepresentante() {
        return documentoRepresentante;
    }

    @Override
    public String toString() {
        return "ComissaoListaPagamentosModeloAPI{" + "dataSolicitacao=" + dataSolicitacao + ", dataPagamento=" + dataPagamento + ", codigoSolicitacao=" + codigoSolicitacao + ", totalSolicitacao=" + totalSolicitacao + ", status=" + status + ", nomeRepresentante=" + nomeRepresentante + ", emailRepresentante=" + emailRepresentante + ", chavePix=" + chavePix + ", cidade=" + cidade + ", documentoRepresentante=" + documentoRepresentante + '}';
    }

}
