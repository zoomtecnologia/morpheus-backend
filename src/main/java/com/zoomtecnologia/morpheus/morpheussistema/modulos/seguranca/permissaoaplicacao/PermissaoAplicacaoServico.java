package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao;

import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissaoAplicacaoServico {

    @Autowired
    private PermissaoAplicacaoRepositorio permissaoAplicacaoRepositorio;
    
    @Autowired
    private ModelMapper modelMapper;

    public List<PermissaoAplicacaoResumida> listarAplicacoesPorPermissao(Integer codigo) {
        return this.permissaoAplicacaoRepositorio.listarAplicacoesPorPermissao(codigo);
    }
    
    public List<PermissaoAplicacao> salvarPermissaoAplicacaos(List<PermissaoAplicacaoInclusaoModeloAPI> permissaoAplicacaoInclusaoModeloAPIs) {
        List<PermissaoAplicacao> permissoes = this.converterPermissaoAplicacao(permissaoAplicacaoInclusaoModeloAPIs);
        return this.permissaoAplicacaoRepositorio.saveAll(permissoes);
    }
    
    private List<PermissaoAplicacao> converterPermissaoAplicacao(List<PermissaoAplicacaoInclusaoModeloAPI> permissaoAplicacaoInclusaoModeloAPIs) {
        return permissaoAplicacaoInclusaoModeloAPIs.stream().map(this::converterParaPermissaoAplicacao).collect(Collectors.toList());
    }
    
    private PermissaoAplicacao converterParaPermissaoAplicacao(PermissaoAplicacaoInclusaoModeloAPI inclusaoModeloAPI) {
        PermissaoAplicacaoPK permissaoPK = this.modelMapper.map(inclusaoModeloAPI, PermissaoAplicacaoPK.class);
        PermissaoAplicacao permissao = this.modelMapper.map(inclusaoModeloAPI, PermissaoAplicacao.class);
        permissao.setPermissaoAplicacaoPK(permissaoPK);
        return permissao;
    }    

}
