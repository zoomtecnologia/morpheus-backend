package com.zoomtecnologia.morpheus.morpheussistema.modulos.item;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ItemRepositorio extends JpaRepository<Item, Integer>{
 
   Optional<Item> findByDescricao(String descricao);
   List<Item> findByDescricaoContainingIgnoreCase(String descricao);
   Page<Item> findByDescricaoContainingIgnoreCase(String descricao, Pageable pageable);
}
