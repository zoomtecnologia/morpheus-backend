package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ItensPedidoRepositorioQueryImpl implements ItensPedidoRepositorioQuery {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void salvarItensPedido(Integer numeroPedido, List<? extends ItensPedidoGenericoModeloAPI> itensPedidoInclusaoModeloAPI) {
       this.executarSql(numeroPedido, itensPedidoInclusaoModeloAPI);
    }

    private void executarSql(Integer numeroPedido,List<? extends ItensPedidoGenericoModeloAPI> itensPedidoGenericoModeloAPI) {
        StringBuilder sqlItensPedido = new StringBuilder();
        sqlItensPedido.append("replace into itens_pedido (numero_pedido,codigo_item,valor,quantidade,status) ");
        int indece = 1;
        for (ItensPedidoGenericoModeloAPI itemPedido : itensPedidoGenericoModeloAPI) {
            if (indece == 1) {
                sqlItensPedido.append(" values(")
                        .append(numeroPedido).append(",").append(itemPedido.getCodigoItem()).append(",")
                        .append(itemPedido.getValor()).append(",").append(itemPedido.getQuantidade()).append(",")
                        .append("'").append(itemPedido.getStatus()).append("')");
            } else {
                sqlItensPedido.append(" ,(")
                        .append(numeroPedido).append(",").append(itemPedido.getCodigoItem()).append(",")
                        .append(itemPedido.getValor()).append(",").append(itemPedido.getQuantidade()).append(",")
                        .append("'").append(itemPedido.getStatus()).append("')");
            }
            indece++;
        }
        System.out.println("Desenvolvedor: "+sqlItensPedido);
        this.entityManager.createNativeQuery(sqlItensPedido.toString()).executeUpdate();
    }
    
    
}
