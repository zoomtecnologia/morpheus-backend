package com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoManutencaoInclusaoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoRetornoModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoStatus;
import com.zoomtecnologia.morpheus.morpheussistema.util.FormatterUtil;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidacaoCobrancaServico {

    @Autowired
    private ComissaoServico comissaoServico;

    @Autowired
    private PedidoRepositorio pedidoRepositorio;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    private final List<CobrancaModeloAPI> cobrancasProcessadas;
    private boolean naoExistePedidosParaOsClientesDoArquivo;
    private boolean naoExistePedidosFinalizadosNemPagados;

    public ValidacaoCobrancaServico() {
        this.cobrancasProcessadas = new ArrayList<>();
    }

    public List<CobrancaModeloAPI> realizarValidacaoCobrancas(InputStream inputStream) {
        try {
            this.cobrancasProcessadas.clear();
            Cobranca[] cobrancas = this.lerArquivoDeCobranca(inputStream);
            List<Cobranca> cobrancasDeCarnes = this.filtrarApenasCobrancasDeCarnes(cobrancas);
            Map<String, List<Cobranca>> agruparPorClientes = this.agruparPorCliente(cobrancasDeCarnes);
            agruparPorClientes.entrySet().forEach(this::pesquisandoCobrancaPorCliente);
            if (naoExistePedidosParaOsClientesDoArquivo) {
                this.messageSourceUtil.negocioException("pedido.cliente-sem-pedido", "pedido.cliente-sem-pedido");
            }
            if (naoExistePedidosFinalizadosNemPagados) {
                this.messageSourceUtil.negocioException("pedido.cliente-sem-pedido-pagos", "pedido.cliente-sem-pedido-pagos");
            }
            this.comissaoServico.bloquearClienteComPagamentosVencidos();
            this.comissaoServico.ativarClienteComPagamentosQuitados();
        } catch (IOException ex) {
            this.messageSourceUtil.negocioException("cobrancas.nao-existe-cobrancas", "cobrancas.nao-existe-cobrancas-detalhes");
        }
        return this.cobrancasProcessadas;
    }

    private void adicionarCobrancasProcessadas(Cobranca cobranca) {
        LocalDate dataVencimento = LocalDate.parse(cobranca.getPayment().getCarnet().getExpire_at().substring(0, 10));
        String statusPagamento = "Pago";
        String mes = String.format("%02d", dataVencimento.getMonthValue());
        String parcelaMensal = "Parcela do mês " + mes + " foi paga";
        if (cobranca.getStatus().equals("unpaid")) {
            statusPagamento = "Inadimplente";
            parcelaMensal = "Parcela do mês " + mes + " está em aberto";
        } else if (cobranca.getStatus().equals("canceled")) {
            statusPagamento = "Cancelado";
            parcelaMensal = "Parcela do mês " + mes + " foi cancelada";
        }
        CobrancaModeloAPI cobrancaModeloAPI = new CobrancaModeloAPI();
        cobrancaModeloAPI.setCnpj(cobranca.getCustomer().getDocument());
        cobrancaModeloAPI.setRazaoSocial(cobranca.getCustomer().getName());
        cobrancaModeloAPI.setStatusPagamento(statusPagamento);
        cobrancaModeloAPI.setMes(parcelaMensal);
        this.cobrancasProcessadas.add(cobrancaModeloAPI);
    }

    private Cobranca[] lerArquivoDeCobranca(InputStream inputStream) throws IOException {
        return new ObjectMapper().readValue(inputStream, Cobranca[].class);
    }

    private void pesquisandoCobrancaPorCliente(Map.Entry<String, List<Cobranca>> entry) {
        String cnpjCliente = entry.getKey();
        Optional<Pedido> pedidoExiste = this.pedidoRepositorio.buscarPedidoPorCliente(cnpjCliente);
        boolean pedidoNaoExiste = pedidoExiste.isEmpty();
        if (pedidoNaoExiste) {
            naoExistePedidosParaOsClientesDoArquivo = true;
            return;
        }
        Pedido pedido = pedidoExiste.get();
        PedidoStatus statusPedido = pedido.getStatus();
        boolean pedidoNaoFoiPagoNemFinalizado = !statusPedido.equals(PedidoStatus.INSTALACAO_PAGA) && !statusPedido.equals(PedidoStatus.FINALIZADO);
        if (pedidoNaoFoiPagoNemFinalizado) {
            naoExistePedidosFinalizadosNemPagados = true;
            naoExistePedidosParaOsClientesDoArquivo = false;
            return;
        }
        naoExistePedidosParaOsClientesDoArquivo = false;
        naoExistePedidosFinalizadosNemPagados = false;
        List<Cobranca> cobrancasPorCliente = entry.getValue();
        cobrancasPorCliente.forEach(cobranca -> this.pesquisarCobrancaCliente(cobranca, pedido));
    }

    private void pesquisarCobrancaCliente(Cobranca cobranca, Pedido pedido) {
        this.adicionarCobrancasProcessadas(cobranca);
        Cliente cliente = pedido.getCliente();
        String dataExpiracaoPagamento = cobranca.getPayment().getCarnet().getExpire_at();
        LocalDate dataMensalidade = LocalDate.parse(dataExpiracaoPagamento);
        boolean comissao = this.comissaoServico.existsByDataMensalidadeAndCodigoClienteCnpj(dataMensalidade, cliente.getCnpj());
        if (!comissao) {
            this.salvarComissao(dataMensalidade, cobranca, pedido);
            return;
        }
        if (this.verificarSFoiPagoECancelado(cobranca)) {
            this.adicionarCobrancaComoPagaECancelada(cobranca);
        }
    }

    public Pedido buscarPedidoPorCliente(String cnpjCliente) {
        return this.pedidoRepositorio.buscarPedidoPorCliente(cnpjCliente).get();
    }

    private Map<String, List<Cobranca>> agruparPorCliente(List<Cobranca> cobrancasDeCarnes) {
        return cobrancasDeCarnes.stream().collect(Collectors.groupingBy(cobranca -> cobranca.getCustomer().getDocument()));
    }

    private List<Cobranca> filtrarApenasCobrancasDeCarnes(Cobranca[] cobrancas) {
        return Arrays.stream(cobrancas)
                .filter(Cobranca::eCobrancaDeCarne)
                .map(Cobranca::converterCarneSemHoraNaDataExpiracao)
                .collect(Collectors.toList());
    }

    private boolean verificarSFoiPagoECancelado(Cobranca cobranca) {
        return this.verificarSeFoiPago(cobranca) || this.verificarSeFoiCancelada(cobranca);
    }

    private boolean verificarSeFoiPago(Cobranca cobranca) {
        return cobranca.getStatus().equals("paid");
    }

    private boolean verificarSeFoiCancelada(Cobranca cobranca) {
        return cobranca.getStatus().equals("canceled");
    }

    private void adicionarCobrancaComoPagaECancelada(Cobranca cobranca) {
        String statusCobranca = "PAGO";
        if (this.verificarSeFoiCancelada(cobranca)) {
            statusCobranca = "CANCELADO";
        }
        this.atualizarMensalidadePorDataMensalidade(cobranca, statusCobranca);
    }

    private void atualizarMensalidadePorDataMensalidade(Cobranca cobranca, String status) {
        String dataExpiracaoPagamento = cobranca.getPayment().getCarnet().getExpire_at();
        String cnpjCliente = cobranca.getCustomer().getDocument();
        this.comissaoServico.atualizarStatusComissaoPorDataMensalidade(status, cnpjCliente, LocalDate.parse(dataExpiracaoPagamento));
    }

    private void salvarComissao(LocalDate dataMensalidade, Cobranca cobranca, Pedido pedido) {
        Cliente cliente = pedido.getCliente();
        ComissaoManutencaoInclusaoModeloAPI comissaoManutencaoInclusaoModeloAPI = new ComissaoManutencaoInclusaoModeloAPI();
        comissaoManutencaoInclusaoModeloAPI.setCodigoClienteCnpj(cliente.getCnpj());
        comissaoManutencaoInclusaoModeloAPI.setCodigoRepresentanteDocumento(cliente.getCodigoRepresentante());
        comissaoManutencaoInclusaoModeloAPI.setDataMensalidade(dataMensalidade);
        comissaoManutencaoInclusaoModeloAPI.setQuantidadeParcela(1);
        comissaoManutencaoInclusaoModeloAPI.setTipo("MENSALIDADE");
        float valorTotalCobranca = cobranca.getTotal() / 100;
        comissaoManutencaoInclusaoModeloAPI.setValorMensalidade(FormatterUtil.arredondarValor(2, valorTotalCobranca));
        comissaoManutencaoInclusaoModeloAPI.setPedido(this.preencherPedidoRetornoModeloAPI(pedido));
        this.comissaoServico.salvarComissaoManutencao(comissaoManutencaoInclusaoModeloAPI);
    }

    private PedidoRetornoModeloAPI preencherPedidoRetornoModeloAPI(Pedido pedido) {
        return new PedidoRetornoModeloAPI(
                pedido.getNumero(),
                pedido.getData(),
                pedido.getRepresentante().getDocumento(),
                pedido.getRepresentante().getNome(),
                pedido.getCliente().getCnpj(),
                pedido.getCliente().getRazaoSocial(),
                pedido.getStatus(),
                pedido.getTipoComissao(),
                pedido.getTotal(),
                pedido.getValorMensalidade(),
                pedido.getRepresentante().getGeraReceita()
        );
    }

}
