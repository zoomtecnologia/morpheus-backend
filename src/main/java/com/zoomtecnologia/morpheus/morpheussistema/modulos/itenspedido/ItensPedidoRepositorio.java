package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ItensPedidoRepositorio extends JpaRepository<ItensPedido, ItensPedidoPK>, ItensPedidoRepositorioQuery {

    static final String COLUNAS
            = "new com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido.ItensPedidoRetornoModeloAPI("
            + "itens.itensPedidoPK.pedido, "
            + "itens.itensPedidoPK.item.codigo, "
            + "itens.itensPedidoPK.item.descricao, "
            + "itens.valor, "
            + "itens.itensPedidoPK.item.valorMinimo, "
            + "itens.itensPedidoPK.item.itemUnico, "
            + "itens.quantidade,"
            + "itens.status"
            + ")";

    @Query("select " + ItensPedidoRepositorio.COLUNAS + " from ItensPedido itens where itens.itensPedidoPK.pedido =:numero")
    public List<ItensPedidoRetornoModeloAPI> listarItensPorNumeroPedido(@Param("numero") Integer numero);

    @Query("select " + ItensPedidoRepositorio.COLUNAS + " from ItensPedido itens where itens.itensPedidoPK.pedido =:numeroPedido and itens.itensPedidoPK.item.codigo=:numeroItem")
    public Optional<ItensPedidoRetornoModeloAPI> buscarItensPedidoPorCodigo(@Param("numeroPedido") Integer numero, @Param("numeroItem") Integer codigoItem);
}
