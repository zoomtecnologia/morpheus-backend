package com.zoomtecnologia.morpheus.morpheussistema.modulos.pix;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class PayloadPix {

    private final DecimalFormat decimal = new DecimalFormat("#####0.00");

    public PayloadPix() {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        this.decimal.setDecimalFormatSymbols(decimalFormatSymbols);
    }

    private final static String ID_PAYLOAD_FORMAT_INDICATOR = "00";
    private final static String ID_MERCHANT_ACCOUNT_INFORMATION = "26";
    private final static String ID_MERCHANT_ACCOUNT_INFORMATION_GUI = "00";
    private final static String ID_MERCHANT_ACCOUNT_INFORMATION_KEY = "01";
    private final static String ID_MERCHANT_ACCOUNT_INFORMATION_DESCRIPTION = "02";
    private final static String ID_MERCHANT_CATEGORY_CODE = "52";
    private final static String ID_TRANSACTION_CURRENCY = "53";
    private final static String ID_TRANSACTION_AMOUNT = "54";
    private final static String ID_COUNTRY_CODE = "58";
    private final static String ID_MERCHANT_NAME = "59";
    private final static String ID_MERCHANT_CITY = "60";
    private final static String ID_ADDITIONAL_DATA_FIELD_TEMPLATE = "62";
    private final static String ID_ADDITIONAL_DATA_FIELD_TEMPLATE_TXID = "05";
    private final static String ID_CRC16 = "63";

    private String chavePix;
    private String descricaoPagamento;
    private String titularConta;
    private String cidadeTitularConta;
    private String txId;
    private String valorTransacao;

    public PayloadPix setChavePix(String chavePix) {
        this.chavePix = chavePix;
        return this;
    }

    public PayloadPix setDescricaoPagamento(String descricaoPagamento) {
        this.descricaoPagamento = descricaoPagamento;
        return this;
    }

    public PayloadPix setTitularConta(String titularConta) {
        this.titularConta = titularConta;
        return this;
    }

    public PayloadPix setCidadeTitular(String cidadeTitularConta) {
        this.cidadeTitularConta = cidadeTitularConta;
        return this;
    }

    public PayloadPix setTxId(String txId) {
        this.txId = txId;
        return this;
    }

    public PayloadPix setValorTransacao(double valorTransacao) {
        this.valorTransacao = this.decimal.format(valorTransacao);
        return this;
    }

    private String getValue(String id, String valor) {
        String tamanho = String.format("%02d", valor.length());
        return id + tamanho + valor;
    }

    private String getMerchantAccountInformation() {
        String gui = this.getValue(ID_MERCHANT_ACCOUNT_INFORMATION_GUI, "br.gov.bcb.pix");
        String pixChave = this.getValue(ID_MERCHANT_ACCOUNT_INFORMATION_KEY, this.chavePix);
        String descricao = "";
        if (this.descricaoPagamento != null && !this.descricaoPagamento.isEmpty()) {
            descricao = this.getValue(ID_MERCHANT_ACCOUNT_INFORMATION_DESCRIPTION, this.descricaoPagamento);
        }
        return this.getValue(ID_MERCHANT_ACCOUNT_INFORMATION, gui + pixChave + descricao);
    }

    private String getAdditionalDataFieldTemplate() {
        String implementacaoTxId = this.getValue(ID_ADDITIONAL_DATA_FIELD_TEMPLATE_TXID, this.txId);
        return this.getValue(ID_ADDITIONAL_DATA_FIELD_TEMPLATE, implementacaoTxId);
    }

    public String getPayloadPIX() {
        String payloadPIX = this.getValue(ID_PAYLOAD_FORMAT_INDICATOR, "01");
        payloadPIX += this.getMerchantAccountInformation();
        payloadPIX += this.getValue(ID_MERCHANT_CATEGORY_CODE, "0000");
        payloadPIX += this.getValue(ID_TRANSACTION_CURRENCY, "986");
        payloadPIX += this.getValue(ID_TRANSACTION_AMOUNT, this.valorTransacao);
        payloadPIX += this.getValue(ID_COUNTRY_CODE, "BR");
        payloadPIX += this.getValue(ID_MERCHANT_NAME, this.titularConta);
        payloadPIX += this.getValue(ID_MERCHANT_CITY, this.cidadeTitularConta);
        payloadPIX += this.getAdditionalDataFieldTemplate();
        return payloadPIX + this.getCRC16(payloadPIX);
    }

    /**
     * Método responsável por calcular o valor da hash de validação do código
     * pix
     *
     * @return string
     */
    private String getCRC16(String payload) {
        //ADICIONA DADOS GERAIS NO PAYLOAD
        payload += ID_CRC16 + "04";
        //DADOS DEFINIDOS PELO BACEN
        int polinomio = 0x1021;
        int resultado = 0xFFFF;
        //CHECKSUM
        int lengthPayload = payload.length();
        if (lengthPayload > 0) {
            for (int offset = 0; offset < lengthPayload; offset++) {
                resultado ^= ((int) payload.charAt(offset) << 8);
                for (int bitwise = 0; bitwise < 8; bitwise++) {
                    int check = (resultado <<= 1) & 0x10000;
                    if (check != 0) {
                        resultado ^= polinomio;
                    }
                    resultado &= 0xFFFF;
                }
            }
        }
        //RETORNA CÓDIGO CRC16 DE 4 CARACTERES
        return ID_CRC16 + "04" + String.valueOf(Integer.toHexString(resultado)).toUpperCase();
    }

}
