package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario, Integer> {
    
    @Query("select distinct u from Usuario u join fetch u.representante representante join fetch u.permissao permissao order by u.nome")
    List<UsuarioModeloAPI> listarUsuarios();
    
    @Query("select distinct u from Usuario u join fetch u.representante representante join fetch u.permissao permissao order by u.nome")
    List<UsuarioResumidoModeloAPI> listarUsuariosResumidos();

    @Query("select u from Usuario u join fetch u.representante representante join fetch u.permissao permissao where u.email=:email")
    Optional<UsuarioModeloAPI> buscarPorEmail(String email);

    @Query("select u from Usuario u join fetch u.representante representante join fetch u.permissao permissao where u.recuperandoSenha=:recuperandoSenha")
    Optional<Usuario> buscarUsuarioPorChaveRecuperacaoSenha(String recuperandoSenha);

    @Query("select new com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioRecuperacaoSenhaModeloAPI(u.email,u.status) "
            + "from Usuario u where u.email=:email")
    Optional<UsuarioRecuperacaoSenhaModeloAPI> buscarUsuarioParaRecurarSenha(String email);

    @Query("select usuario from Usuario usuario left join fetch usuario.representante representante left join fetch usuario.permissao permissao where usuario.codigo=:codigo")
    Optional<UsuarioResumidoModeloAPI> buscarUsuarioResumido(Integer codigo);
    
    @Query("select usuario from Usuario usuario left join fetch usuario.representante representante left join fetch usuario.permissao permissao where representante.documento=:documento")
    Optional<UsuarioResumidoModeloAPI> buscarUsuarioAtivoPorRepresentante(String documento);        

    int countByEmail(String email);

    int countByEmailAndSenha(String email, String senha);

    @Transactional
    @Modifying
    @Query("update Usuario set recuperandoSenha=:chaveRecuperacaoSenha where email=:email")
    void atualizarRecuperacaoSenha(String chaveRecuperacaoSenha, String email);

    @Transactional
    @Modifying
    @Query("update Usuario set recuperandoSenha=:chaveRecuperacaoSenha,senha=:senha,dataRecuepracaoSenha=current_date where email=:email")
    void mudarSenha(String chaveRecuperacaoSenha, String email, String senha);
    
    @Transactional
    @Modifying
    @Query("update Usuario set dataAcesso=current_date where email=:email")
    void atualizarDataDeAcesso(String email);

}
