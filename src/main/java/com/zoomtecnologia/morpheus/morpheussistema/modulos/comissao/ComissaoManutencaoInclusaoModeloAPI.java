package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.PedidoRetornoModeloAPI;
import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ComissaoManutencaoInclusaoModeloAPI {
  
    @NotBlank
    @Size(max = 14)
    private String codigoClienteCnpj;

    @NotBlank
    @Size(max = 14)
    private String codigoRepresentanteDocumento;

    @Size(max = 15)
    private String tipo;
    
    private PedidoRetornoModeloAPI pedido;

    private double valorMensalidade;

    private LocalDate dataMensalidade;
    
    private int quantidadeParcela;        

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(double valorMensalidade) {
        this.valorMensalidade = valorMensalidade;
    }

    public LocalDate getDataMensalidade() {
        return dataMensalidade;
    }

    public void setDataMensalidade(LocalDate dataMensalidade) {
        this.dataMensalidade = dataMensalidade;
    }
   
    public String getCodigoClienteCnpj() {
        return codigoClienteCnpj;
    }

    public void setCodigoClienteCnpj(String codigoClienteCnpj) {
        this.codigoClienteCnpj = codigoClienteCnpj;
    }

    public String getCodigoRepresentanteDocumento() {
        return codigoRepresentanteDocumento;
    }

    public void setCodigoRepresentanteDocumento(String codigoRepresentanteDocumento) {
        this.codigoRepresentanteDocumento = codigoRepresentanteDocumento;
    }

    public int getQuantidadeParcela() {
        return quantidadeParcela;
    }

    public void setQuantidadeParcela(int quantidadeParcela) {
        this.quantidadeParcela = quantidadeParcela;
    }

    public PedidoRetornoModeloAPI getPedido() {
        return pedido;
    }

    public void setPedido(PedidoRetornoModeloAPI pedido) {
        this.pedido = pedido;
    }
    
    @Override
    public String toString() {
        return "ComissaoManutencaoInclusaoModeloAPI{" + "codigoClienteCnpj=" + codigoClienteCnpj + ", codigoRepresentanteDocumento=" + codigoRepresentanteDocumento + ", tipo=" + tipo + ", valorMensalidade=" + valorMensalidade + ", dataMensalidade=" + dataMensalidade + '}';
    }
}
