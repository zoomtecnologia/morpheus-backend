package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.br.CNPJ;

public class ClientePedidoModeloAPI {

    @NotBlank
    @Size(max = 14)
    @CNPJ
    private String cnpj;

    @Size(max = 120)
    @NotBlank
    @Email
    private String email;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ClientePedidoModeloAPI{" + "cnpj=" + cnpj + ", email=" + email + '}';
    }
    
    
}
