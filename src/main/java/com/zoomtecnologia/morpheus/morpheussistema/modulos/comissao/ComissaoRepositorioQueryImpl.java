package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public class ComissaoRepositorioQueryImpl implements ComissaoRepositorioQuery {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ComissaoListaModeloAPI> listarComissaoes(FiltroComissaoQuery filtroComissaoQuery) {
        LocalDate data = filtroComissaoQuery.getDataVencimento();
        String codigoSolicitacao = filtroComissaoQuery.getCodigoSolicitacao();
        String codigoCliente = filtroComissaoQuery.getCodigoCliente();
        String codigoRepresentante = filtroComissaoQuery.getCodigoRepresentante();
        List<String> status = filtroComissaoQuery.getStatus();
        String jpql = "select new com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaModeloAPI("
                + "comissao.codigo"
                + ",comissao.codigoCliente.razaoSocial"
                + ",comissao.tipo"
                + ",function('DATE_FORMAT',comissao.dataMensalidade,'%d/%m/%Y')"
                + ",comissao.valor"
                + ",comissao.valorMensalidade"
                + ",comissao.valorInstalacao"
                + ",comissao.status"
                + ") from Comissao comissao "
                + "where comissao.status in (:status) ";
        boolean temData = data != null;
        boolean temCodigoSolicitacao = codigoSolicitacao != null && !codigoSolicitacao.trim().isEmpty();
        boolean temCliente = codigoCliente != null && !codigoCliente.trim().isEmpty();
        boolean temRepresentante = codigoRepresentante != null && !codigoRepresentante.trim().isEmpty();
        if (temData) {
            jpql += " and (:data > comissao.dataMensalidade or comissao.dataMensalidade is null) ";
        }
        if (temCodigoSolicitacao) {
            jpql += " and codigoSolicitacao=:codigoSolicitacao ";
        }
        if (temCliente) {
            jpql += " and comissao.codigoCliente.cnpj=:codigoCliente ";
        }
        if (temRepresentante) {
            jpql += " and comissao.codigoRepresentante.documento =:codigoRepresentante ";
        }
        jpql += " order by comissao.dataMensalidade ";
        TypedQuery<ComissaoListaModeloAPI> createQuery = this.entityManager.createQuery(jpql, ComissaoListaModeloAPI.class);
        if (temData) {
            createQuery.setParameter("data", data);
        }
        if (temCodigoSolicitacao) {
            createQuery.setParameter("codigoSolicitacao", codigoSolicitacao);
        }
        if (temCliente) {
            createQuery.setParameter("codigoCliente", codigoCliente);
        }
        if (temRepresentante) {
            createQuery.setParameter("codigoRepresentante", codigoRepresentante);
        }
        createQuery.setParameter("status", status);
        List<ComissaoListaModeloAPI> comissoes = createQuery.getResultList();
        return comissoes;
    }

    public static class FiltroComissaoQuery {

        private String codigoSolicitacao;
        private String codigoCliente;
        private String codigoRepresentante;
        private List<String> status;
        private LocalDate dataVencimento;
        private LocalDate dataInicial;
        private LocalDate dataFinal;

        public String getCodigoSolicitacao() {
            return codigoSolicitacao;
        }

        public String getCodigoCliente() {
            return codigoCliente;
        }

        public String getCodigoRepresentante() {
            return codigoRepresentante;
        }

        public List<String> getStatus() {
            return status;
        }

        public LocalDate getDataVencimento() {
            return dataVencimento;
        }

        public LocalDate getDataInicial() {
            return dataInicial;
        }

        public LocalDate getDataFinal() {
            return dataFinal;
        }

        public void setCodigoSolicitacao(String codigoSolicitacao) {
            this.codigoSolicitacao = codigoSolicitacao;
        }

        public void setCodigoCliente(String codigoCliente) {
            this.codigoCliente = codigoCliente;
        }

        public void setCodigoRepresentante(String codigoRepresentante) {
            this.codigoRepresentante = codigoRepresentante;
        }

        public void setStatus(List<String> status) {
            this.status = status;
        }

        public void setDataVencimento(LocalDate dataVencimento) {
            this.dataVencimento = dataVencimento;
        }

        public void setDataInicial(LocalDate dataInicial) {
            this.dataInicial = dataInicial;
        }

        public void setDataFinal(LocalDate dataFinal) {
            this.dataFinal = dataFinal;
        }

        @Override
        public String toString() {
            return "FiltroComissaoQuery{" + "codigoSolicitacao=" + codigoSolicitacao + ", codigoCliente=" + codigoCliente + ", codigoRepresentante=" + codigoRepresentante + ", status=" + status + ", dataVencimento=" + dataVencimento + ", dataInicial=" + dataInicial + ", dataFinal=" + dataFinal + '}';
        }
    }

}
