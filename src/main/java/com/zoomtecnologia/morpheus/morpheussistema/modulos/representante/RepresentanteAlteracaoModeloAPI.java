package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class RepresentanteAlteracaoModeloAPI extends RepresentanteGenericoModeloAPI {

    @Size(max = 1)
    @NotBlank
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RepresentanteAlteracaoModeloAPI{" + "status=" + status + '}';
    }
    
    
}
