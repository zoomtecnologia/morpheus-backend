package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissaoAplicacaoRepositorio extends JpaRepository<PermissaoAplicacao, PermissaoAplicacaoPK> {

    @Query("select new com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao.PermissaoAplicacaoResumida("
            + "permissao.status,"
            + "permissao.permissaoAplicacaoPK.aplicacao.codigo,"
            + "permissao.permissaoAplicacaoPK.aplicacao.nome,"
            + "permissao.permissaoAplicacaoPK.aplicacao.descricao,"
            + "permissao.acessar,"
            + "permissao.incluir,"
            + "permissao.alterar,"
            + "permissao.excluir"
            + ") from PermissaoAplicacao permissao  where permissao.permissaoAplicacaoPK.permissao.codigo =:codigo ")
    public List<PermissaoAplicacaoResumida> listarAplicacoesPorPermissao(Integer codigo);
    
}
