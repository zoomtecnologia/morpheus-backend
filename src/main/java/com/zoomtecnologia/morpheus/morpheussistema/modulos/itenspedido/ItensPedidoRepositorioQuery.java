package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import java.util.List;

public interface ItensPedidoRepositorioQuery {
    
    void salvarItensPedido(Integer numeroPedido, List<? extends ItensPedidoGenericoModeloAPI> itensPedidoInclusaoModeloAPI);
    
}
