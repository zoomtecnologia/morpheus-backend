package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "zion_planos_contratacao")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ZionPlanosContratacao implements Serializable {

    @EmbeddedId
    
	@Column(name = "id")
	private ZionPlanosContratacaoPK id;

    
	@Column(name = "status")
	private boolean status;

    
	@Column(name = "data_cadastro")
	private LocalDateTime dataCadastro;

    
	@Column(name = "usuario_contratacao")
	private String usuarioContratacao;

    public void setPlanoInclusao(ZionPlanoContratacaoInclusaoModeloAPI planoInclusao, String codigoContratacao) {
        this.id = new ZionPlanosContratacaoPK();
        this.id.setCodigoContratacao(codigoContratacao);
        this.id.setCodigoPlano(planoInclusao.getCodigoPlano());
        this.setId(this.id);
        this.setDataCadastro(LocalDateTime.now());
        this.setStatus(true);
        this.setUsuarioContratacao(planoInclusao.getUsuario());
    }

}
