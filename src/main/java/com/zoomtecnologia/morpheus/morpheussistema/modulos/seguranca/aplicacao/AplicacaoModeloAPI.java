package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.aplicacao;

import javax.validation.constraints.NotBlank;


public class AplicacaoModeloAPI {

    @NotBlank
    private String nome;
    
    @NotBlank
    private String descricao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return "AplicacaoModeloAPI{" + "nome=" + nome + ", descricao=" + descricao + '}';
    }
    
}
