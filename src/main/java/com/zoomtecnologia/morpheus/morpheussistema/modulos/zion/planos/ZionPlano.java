package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planos;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoaplicacoes.ZionPlanoAplicacoesModeloAPI;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "zion_planos")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"codigo"})
public class ZionPlano implements Serializable {

    @Id
    
	@Column(name = "codigo")
	private String codigo;

    
	@Column(name = "nome")
	private String nome;

    
	@Column(name = "status")
	private boolean status;

    
	@Column(name = "valor")
	private double valor;

    
	@Column(name = "valor_usuario_adicional")
	private Double valorUsuarioAdicional;

    
	@Column(name = "valor_pdv_adicional")
	private Double valorPDVAdicional;

    
	@Column(name = "descricao")
	private String descricao;

    
	@Column(name = "tipo_plano")
	private String tipoPlano;

    
	@Column(name = "gera_cobranca")
	private boolean geraCobranca;

    
	@Column(name = "data_cadastro")
	private LocalDateTime dataCadastro;

    
	@Column(name = "usuarios")
	private int usuarios;
	
        @Column(name = "pdvs")
	private int pdvs;

    
	@Column(name = "teste")
	private Boolean teste;

    
	@Column(name = "dias_para_teste")
	private Integer diasParaTeste;

    @Transient
    
	@Column(name = "aplicacoes")
	private Set<ZionPlanoAplicacoesModeloAPI> aplicacoes;

    public void setZionPlanoModeloResumido(ZionPlanoModeloResumido zionPlanoModeloResumido) {
        this.codigo = zionPlanoModeloResumido.getCodigo();
        this.nome = zionPlanoModeloResumido.getNome();
        this.status = zionPlanoModeloResumido.getStatus();
        this.valor = zionPlanoModeloResumido.getValor();
        this.valorUsuarioAdicional = zionPlanoModeloResumido.getValorUsuarioAdicional();
        this.valorPDVAdicional = zionPlanoModeloResumido.getValorPDVAdicional();
        this.descricao = zionPlanoModeloResumido.getDescricao();
        this.tipoPlano = zionPlanoModeloResumido.getTipoPlano();
        this.geraCobranca = zionPlanoModeloResumido.getGeraCobranca();
        this.dataCadastro = zionPlanoModeloResumido.getDataCadastro();
        this.usuarios = zionPlanoModeloResumido.getUsuarios();
        this.pdvs = zionPlanoModeloResumido.getPdvs();
        this.teste = zionPlanoModeloResumido.getTeste();
        this.diasParaTeste = zionPlanoModeloResumido.getDiasParaTeste();
    }

}
