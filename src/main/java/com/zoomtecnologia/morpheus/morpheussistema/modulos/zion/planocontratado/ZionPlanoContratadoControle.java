package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

import javax.validation.Valid;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/zion/contratos")
@CrossOrigin("*")
@AllArgsConstructor
public class ZionPlanoContratadoControle {
    private final ZionPlanoContratadoServico zionPlanoContratadoServico;

    @GetMapping
    public Page<ZionPlanoContratado> listarContratos(
            @RequestParam(value="cliente",required = false,defaultValue = "") String cliente, Pageable pageable) {
        return this.zionPlanoContratadoServico.listarContratos(cliente,pageable);
    }

    @GetMapping("{codigoContrato}")
    public ResponseEntity<ZionPlanoContratadoModeloAPI> buscarDadosContrato(@PathVariable("codigoContrato") String codigoContrato) {
        ZionPlanoContratadoModeloAPI zionPlanoContratadoModeloAPI = this.zionPlanoContratadoServico.buscarDadosContrato(codigoContrato);
        return ResponseEntity.ok(zionPlanoContratadoModeloAPI);
    }

    @PostMapping
    public ResponseEntity<ZionPlanoContratado> gerarContrato(@RequestBody @Valid ZionPlanoContratadoInclusaoModeloAPI zionPlanoContratadoInclusaoModeloAPI) {
        ZionPlanoContratado zionPlanoContratado = this.zionPlanoContratadoServico.gerarContrato(zionPlanoContratadoInclusaoModeloAPI);
        return ResponseEntity.status(HttpStatus.CREATED).body(zionPlanoContratado);
    }

    @PutMapping("aceite-contrato/{codigoContrato}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void aceitarContrato(@PathVariable("codigoContrato")  String codigoContrato,
                                @RequestBody @Valid StatusAceiteContratoModeloAPI statusAceiteContratoModeloAPI) {
        this.zionPlanoContratadoServico.aceitarContrato(
                codigoContrato,
                statusAceiteContratoModeloAPI);
    }

    @DeleteMapping("{codigoContrato}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelarContrato(@PathVariable String codigoContrato) {
        this.zionPlanoContratadoServico.cancelarContrato(codigoContrato);
    }
}
