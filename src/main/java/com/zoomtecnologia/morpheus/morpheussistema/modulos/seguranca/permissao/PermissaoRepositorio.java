package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissaoRepositorio extends JpaRepository<Permissao, Integer> {

    @Query("select permissao from Permissao permissao ")
    List<PermissaoResumidaModeloAPI> listarPermissoesResumidas();

    @Query("select permissao from Permissao permissao where permissao.nome='AFILIADOS'")
    Optional<Permissao> buscarPermissaoAfiliado();

}
