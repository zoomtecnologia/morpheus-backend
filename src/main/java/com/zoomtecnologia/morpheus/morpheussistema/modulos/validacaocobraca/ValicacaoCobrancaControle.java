package com.zoomtecnologia.morpheus.morpheussistema.modulos.validacaocobraca;

import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("validacao-cobranca")
@CrossOrigin
public class ValicacaoCobrancaControle {

    @Autowired
    private ValidacaoCobrancaServico validacaoCobrancaServico;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    @PostMapping("upload-cobrancas")
    public List<CobrancaModeloAPI> quitaCobrancas(@RequestPart("cobrancas") MultipartFile multipartFile) {
        try {
            return this.validacaoCobrancaServico.realizarValidacaoCobrancas(multipartFile.getInputStream());
        } catch (IOException ex) {
            this.messageSourceUtil.negocioException("cobrancas.nao-existe-cobrancas", "cobrancas.nao-existe-cobrancas-detalhes");
        }
        return List.of();
    }

}
