package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

public interface RepresentanteResumidoModeloAPI {
    
    String getDocumento();
    String getNome();
    
}
