package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zoomtecnologia.morpheus.morpheussistema.arquivodocx.ConverterModeloContratoDocxParaPDF;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa.GrupoEmpresa;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido.Pedido;
import com.zoomtecnologia.morpheus.morpheussistema.util.CurrencyWriter;
import com.zoomtecnologia.morpheus.morpheussistema.util.FormatterUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Base64;
import java.util.Locale;
import java.util.Objects;

@Entity
@Table(name = "cliente")
@JsonInclude(JsonInclude.Include.NON_NULL)
@DynamicUpdate
public class Cliente implements Serializable {

    @Id
    @Column(name = "cnpj")
    private String cnpj;

    @Column(name = "inscricao_estadual")
    private String inscricaoEstadual;

    @Column(name = "inscricao_municipal")
    private String inscricaoMunicipal;

    @Column(name = "razao_social")
    private String razaoSocial;

    @Column(name = "nome_fantasia")
    private String nomeFantasia;

    @Column(name = "regime_tributario")
    private String regimeTributario;

    @Embedded

    @Column(name = "endereco")
    private EnderecoCliente endereco;

    @Column(name = "nome_contato")
    private String nomeContato;

    @Column(name = "telefone")
    private String telefone;

    @Column(name = "celular")
    private String celular;

    @Column(name = "whatsapp")
    private String whatsapp;

    @Column(name = "email")
    private String email;

    @Column(name = "seguimento")
    private String seguimento;

    @Column(name = "obs")
    private String obs;

    @Column(name = "se_tef")
    private String seTef;

    @Column(name = "contingencia")
    private String contingencia;

    @Column(name = "on_saiph")
    private String onSaiph;

    @Column(name = "dia_vencimento")
    private Integer diaVencimento;

    @Column(name = "prazo_maximo")
    private Integer prazoMaximo;

    @Column(name = "matriz")
    private boolean matriz;

    @Column(name = "apelido")
    private String apelido;

    @Column(name = "cnae")
    private String cnae;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")

    @Column(name = "data_cadastro")
    private LocalDate dataCadastro;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")

    @Column(name = "data_ultimo_acesso")
    private LocalDate dataUltimoAcesso;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")

    @Column(name = "data_ultima_validacao")
    private LocalDate dataUltimaValidacao;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")

    @Column(name = "data_proximo_vencimento")
    private LocalDate dataProximoVencimento;

    @Column(name = "status_atividade")
    private String statusAtividade;

    @Column(name = "atualizou_sistema")
    private String atualizouSistema;

    @Column(name = "status_pagamento")
    private String statusPagamento;

    @Column(name = "codigo_representante")
    private String codigoRepresentante;

    @Column(name = "status_pedido")
    private String statusPedido;

    @ManyToOne
    @JoinColumn(name = "codigo_grupo_empresa", referencedColumnName = "codigo")

    private GrupoEmpresa codigoGrupoEmpresa;

    @Column(name = "chave_saiph")
    private String chaveSaiph;

    @Column(name = "aceite_contrato")
    private String aceiteContrato;

    @Column(name = "contrato")
    private String contrato;

    @Column(name = "nome_pessoa_responsavel")
    private String nomePessoaResponsavel;

    @Column(name = "cpf_pessoa_responsavel")
    private String cpfPessoaResponsavel;

    @Column(name = "indicador_ie")
    private String indicadorInscricaoEstadual;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")

    @Column(name = "data_aceite")
    private LocalDate dataAceite;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")

    @Column(name = "data_fim_vigencia_contrato")
    private LocalDate dataFimVigenciaContrato;

    @Column(name = "quantidade_pdv")
    private int quantidadePDVs;

    @Column(name = "identificador_api_retaguarda")
    private String identificadorAPIRetaguarda;

    @Column(name = "retaguarda")
    @Enumerated(EnumType.STRING)
    private TipoRetaguarda retaguarda;

    @Transient
    private String senhaPDV;

    public TipoRetaguarda getRetaguarda() {
        return retaguarda;
    }

    public void setRetaguarda(TipoRetaguarda retaguarda) {
        this.retaguarda = retaguarda;
    }

    public String getIdentificadorAPIRetaguarda() {
        return identificadorAPIRetaguarda;
    }

    public void setIdentificadorAPIRetaguarda(String identificadorAPIRetaguarda) {
        this.identificadorAPIRetaguarda = identificadorAPIRetaguarda;
    }

    public int getQuantidadePDVs() {
        return quantidadePDVs;
    }

    public void setQuantidadePDVs(int quantidadePDVs) {
        this.quantidadePDVs = quantidadePDVs;
    }

    public LocalDate getDataFimVigenciaContrato() {
        return dataFimVigenciaContrato;
    }

    public void setDataFimVigenciaContrato(LocalDate dataFimVirgenciaContrato) {
        this.dataFimVigenciaContrato = dataFimVirgenciaContrato;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getRegimeTributario() {
        return regimeTributario;
    }

    public void setRegimeTributario(String regimeTributario) {
        this.regimeTributario = regimeTributario;
    }

    public EnderecoCliente getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoCliente endereco) {
        this.endereco = endereco;
    }

    public String getNomeContato() {
        return nomeContato;
    }

    public void setNomeContato(String nomeContato) {
        this.nomeContato = nomeContato;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSeguimento() {
        return seguimento;
    }

    public void setSeguimento(String seguimento) {
        this.seguimento = seguimento;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getSeTef() {
        return seTef;
    }

    public void setSeTef(String seTef) {
        this.seTef = seTef;
    }

    public String getContingencia() {
        return contingencia;
    }

    public void setContingencia(String contingencia) {
        this.contingencia = contingencia;
    }

    public String getOnSaiph() {
        return onSaiph;
    }

    public void setOnSaiph(String onSaiph) {
        this.onSaiph = onSaiph;
    }

    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(Integer diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    public Integer getPrazoMaximo() {
        return prazoMaximo;
    }

    public void setPrazoMaximo(Integer prazoMaximo) {
        this.prazoMaximo = prazoMaximo;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public LocalDate getDataUltimoAcesso() {
        return dataUltimoAcesso;
    }

    public void setDataUltimoAcesso(LocalDate dataUltimoAcesso) {
        this.dataUltimoAcesso = dataUltimoAcesso;
    }

    public LocalDate getDataUltimaValidacao() {
        return dataUltimaValidacao;
    }

    public void setDataUltimaValidacao(LocalDate dataUltimaValidacao) {
        this.dataUltimaValidacao = dataUltimaValidacao;
    }

    public LocalDate getDataProximoVencimento() {
        return dataProximoVencimento;
    }

    public void setDataProximoVencimento(LocalDate dataProximoVencimento) {
        this.dataProximoVencimento = dataProximoVencimento;
    }

    public String getStatusAtividade() {
        return statusAtividade;
    }

    public void setStatusAtividade(String statusAtividade) {
        this.statusAtividade = statusAtividade;
    }

    public String getAtualizouSistema() {
        return atualizouSistema;
    }

    public void setAtualizouSistema(String atualizouSistema) {
        this.atualizouSistema = atualizouSistema;
    }

    public String getStatusPagamento() {
        return statusPagamento;
    }

    public void setStatusPagamento(String statusPagamento) {
        this.statusPagamento = statusPagamento;
    }

    public String getCodigoRepresentante() {
        return codigoRepresentante;
    }

    public void setCodigoRepresentante(String codigoRepresentante) {
        this.codigoRepresentante = codigoRepresentante;
    }

    public GrupoEmpresa getCodigoGrupoEmpresa() {
        return codigoGrupoEmpresa;
    }

    public void setCodigoGrupoEmpresa(GrupoEmpresa codigoGrupoEmpresa) {
        this.codigoGrupoEmpresa = codigoGrupoEmpresa;
    }

    public String getChaveSaiph() {
        return chaveSaiph;
    }

    public void setChaveSaiph(String chaveSaiph) {
        this.chaveSaiph = chaveSaiph;
    }

    public String getAceiteContrato() {
        return aceiteContrato;
    }

    public void setAceiteContrato(String aceiteContrato) {
        this.aceiteContrato = aceiteContrato;
    }

    public LocalDate getDataAceite() {
        return dataAceite;
    }

    public void setDataAceite(LocalDate dataAceite) {
        this.dataAceite = dataAceite;
    }

    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }

    public String getStatusPedido() {
        return statusPedido;
    }

    public String getNomePessoaResponsavel() {
        return nomePessoaResponsavel;
    }

    public void setNomePessoaResponsavel(String nomePessoaResponsavel) {
        this.nomePessoaResponsavel = nomePessoaResponsavel;
    }

    public String getCpfPessoaResponsavel() {
        return cpfPessoaResponsavel;
    }

    public void setCpfPessoaResponsavel(String cpfPessoaResponsavel) {
        this.cpfPessoaResponsavel = cpfPessoaResponsavel;
    }

    public String getInscricaoMunicipal() {
        return inscricaoMunicipal;
    }

    public void setInscricaoMunicipal(String inscricaoMunicipal) {
        this.inscricaoMunicipal = inscricaoMunicipal;
    }

    public boolean isMatriz() {
        return matriz;
    }

    public void setMatriz(boolean matriz) {
        this.matriz = matriz;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getCnae() {
        return cnae;
    }

    public void setCnae(String cnae) {
        this.cnae = cnae;
    }

    public void setIndicadorInscricaoEstadual(String indicadorInscricaoEstadual) {
        this.indicadorInscricaoEstadual = indicadorInscricaoEstadual;
    }

    public String getIndicadorInscricaoEstadual() {
        return indicadorInscricaoEstadual;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.cnpj);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        return Objects.equals(this.cnpj, other.cnpj);
    }

    @Override
    public String toString() {
        return "Cliente{" + "cnpj=" + cnpj + ", inscricaoEstadual=" + inscricaoEstadual + ", inscricaoMunicipal=" + inscricaoMunicipal + ", razaoSocial=" + razaoSocial + ", nomeFantasia=" + nomeFantasia + ", regimeTributario=" + regimeTributario + ", endereco=" + endereco + ", nomeContato=" + nomeContato + ", telefone=" + telefone + ", celular=" + celular + ", whatsapp=" + whatsapp + ", email=" + email + ", seguimento=" + seguimento + ", obs=" + obs + ", seTef=" + seTef + ", contingencia=" + contingencia + ", onSaiph=" + onSaiph + ", diaVencimento=" + diaVencimento + ", prazoMaximo=" + prazoMaximo + ", matriz=" + matriz + ", apelido=" + apelido + ", cnae=" + cnae + ", dataCadastro=" + dataCadastro + ", dataUltimoAcesso=" + dataUltimoAcesso + ", dataUltimaValidacao=" + dataUltimaValidacao + ", dataProximoVencimento=" + dataProximoVencimento + ", statusAtividade=" + statusAtividade + ", atualizouSistema=" + atualizouSistema + ", statusPagamento=" + statusPagamento + ", codigoRepresentante=" + codigoRepresentante + ", statusPedido=" + statusPedido + ", codigoGrupoEmpresa=" + codigoGrupoEmpresa + ", chaveSaiph=" + chaveSaiph + ", aceiteContrato=" + aceiteContrato + ", contrato=" + contrato + ", nomePessoaResponsavel=" + nomePessoaResponsavel + ", cpfPessoaResponsavel=" + cpfPessoaResponsavel + ", indicadorInscricaoEstadual=" + indicadorInscricaoEstadual + ", dataAceite=" + dataAceite + ", dataFimVigenciaContrato=" + dataFimVigenciaContrato + ", quantidadePDVs=" + quantidadePDVs + ", identificadorAPIRetaguarda=" + identificadorAPIRetaguarda + '}';
    }

    public void gerarChave() {
        final LocalDate dataAtual = LocalDate.now();
        this.dataProximoVencimento = dataAtual.plusDays(this.prazoMaximo).withDayOfMonth(this.diaVencimento);
        final String dataDoProximoVencimento = this.dataProximoVencimento.toString();
        final long dataVencimento = (Long.parseLong(dataDoProximoVencimento.replace("-", ""))) * 653;
        final String montandoChave = ((Long.parseLong(this.cnpj)) * 653) + "." + ((Long.parseLong(this.codigoRepresentante)) * 653) + "." + dataVencimento;
        this.chaveSaiph = Base64.getEncoder().encodeToString(montandoChave.getBytes());
        this.dataUltimaValidacao = dataAtual;
    }

    public String gerarChaveComNumeroCaixa(int numeroCaixa) {
        final LocalDate dataAtual = LocalDate.now();
        this.dataProximoVencimento = dataAtual.plusDays(this.prazoMaximo).withDayOfMonth(this.diaVencimento);
        final String dataDoProximoVencimento = this.dataProximoVencimento.toString();
        final long dataVencimento = (Long.parseLong(dataDoProximoVencimento.replace("-", ""))) * 653;
        final String montandoChave = this.montarChavePDV(numeroCaixa, dataVencimento);
        return Base64.getEncoder().encodeToString(montandoChave.getBytes());
    }

    public byte[] gerarContrato(Pedido pedido) throws IOException, FileNotFoundException, InvalidFormatException {
        Path caminhoContrato = Paths.get("contratos");
        Path pastaContratoCliente = Paths.get(pedido.getCliente().getRazaoSocial());
        Path arquivoContratoCliente = Paths.get(pastaContratoCliente.toString() + ".pdf");
        Path caminhoCompletoContrato = caminhoContrato.resolve(pastaContratoCliente).resolve(arquivoContratoCliente);
        Files.createDirectories(caminhoContrato);
        Files.createDirectories(caminhoContrato.resolve(pastaContratoCliente));
        ConverterModeloContratoDocxParaPDF converterDocxParaPDF = new ConverterModeloContratoDocxParaPDF();
        byte[] contratoPDF = converterDocxParaPDF.converter(elemento -> this.substituirInformacaoDoContrarto(elemento, pedido));
        Files.write(caminhoCompletoContrato, contratoPDF);
        return contratoPDF;
    }

    public void gerarNovoContrato(Pedido pedido) throws IOException, FileNotFoundException, InvalidFormatException {
        Path caminhoContrato = Paths.get("contratos");
        Path pastaContratoCliente = Paths.get(pedido.getCliente().getRazaoSocial());
        Path arquivoContratoCliente = Paths.get(pastaContratoCliente.toString() + ".pdf");
        Path caminhoCompletoContrato = caminhoContrato.resolve(pastaContratoCliente).resolve(arquivoContratoCliente);
        Files.createDirectories(caminhoContrato);
        Files.createDirectories(caminhoContrato.resolve(pastaContratoCliente));
        ConverterModeloContratoDocxParaPDF converterDocxParaPDF = new ConverterModeloContratoDocxParaPDF();
        byte[] contratoPDF = converterDocxParaPDF.converter(elemento -> this.substituirInformacaoDoContrarto(elemento, pedido));
        Files.write(caminhoCompletoContrato, contratoPDF);
    }

    public boolean contratoAceito() {
        return this.aceiteContrato != null && this.aceiteContrato.equals("A");
    }

    @JsonIgnore
    public boolean isSimplesNacional() {
        return this.regimeTributario != null && this.regimeTributario.equals("SIMPLES_NACIONAL");
    }

    @JsonIgnore
    public boolean isLucroReal() {
        return this.regimeTributario != null && this.regimeTributario.equals("REGIME_NORMAL");
    }

    @JsonIgnore
    public boolean isLucroPresumido() {
        return this.regimeTributario != null && this.regimeTributario.equals("LUCRO_PRESUMITO");
    }

    @JsonIgnore
    public String getCodigoRegimeTributario() {
        return this.isSimplesNacional() ? "1"
                : this.isLucroReal() ? "3" : "2";
    }

    public void aceitarContrato() {
        this.aceiteContrato = "A";
        this.dataAceite = LocalDate.now();
    }

    @JsonIgnore
    public boolean isGrupoEmpresa() {
        return this.codigoGrupoEmpresa != null && this.codigoGrupoEmpresa.getCodigo() != null;
    }

    @JsonIgnore
    public boolean isNotGrupoEmpresa() {
        return !this.isGrupoEmpresa();
    }

    public void removerAceite() {
        this.aceiteContrato = null;
        this.dataAceite = null;
    }

    @JsonIgnore
    @Transient
    public int getCodigoCestaTributacao() {
        return this.isSimplesNacional() ? 7 : 9;
    }

    public String getSenhaPDV() {
        return this.senhaPDV;
    }

    public void gerarSenhaPDV(PasswordEncoder passwordEncoder) {
        this.senhaPDV = passwordEncoder.encode(this.codigoGrupoEmpresa.getCodigo());
    }

    @Transient
    @JsonIgnore
    public boolean isIdentificadorRetaguarda() {
        return this.identificadorAPIRetaguarda != null && !this.identificadorAPIRetaguarda.trim().isEmpty();
    }

    public boolean isPago() {
        return this.statusPagamento.equals("P");
    }

    private String montarChavePDV(int numeroCaixa, long dataVencimento) {
        String montandoChave = ((Long.parseLong(this.cnpj)) * 653) + "." + ((Long.parseLong(this.codigoRepresentante)) * 653) + "." + dataVencimento + "." + numeroCaixa;
        if (this.retaguarda.equals(TipoRetaguarda.ZION)) {
            final String identificador = this.codigoGrupoEmpresa.getNome()
                    + ":" + this.codigoGrupoEmpresa.getCodigo()
                    + ":" + this.codigoGrupoEmpresa.getIdentificacaoBancoDados();
            montandoChave += "." + identificador;
        } else {
            if (this.isIdentificadorRetaguarda()) {
                montandoChave += "." + this.identificadorAPIRetaguarda;
            }
        }
        return montandoChave;
    }

    private void substituirInformacaoDoContrarto(XWPFRun elemento, Pedido pedido) {
        if (elemento.getText(0) == null) {
            return;
        }
        String texto = elemento.getText(0);
        if (texto.trim().isEmpty()) {
            return;
        }
        CurrencyWriter currencyWriter = CurrencyWriter.getInstance();
        String textoAlterado = texto;
        Double valorMensalidade = pedido.getValorMensalidade();
        Double valorInstalacao = pedido.getTotal();
        String diaPorExtenso = "";
        if (texto.contains("razaoSocial")) {
            textoAlterado = texto.replace("razaoSocial", this.razaoSocial);
        }
        if (texto.contains("enderecoCliente")) {
            textoAlterado = texto.replace("enderecoCliente", this.endereco.getLogradouro());
        }
        if (texto.contains("numeroEndereco")) {
            textoAlterado = texto.replace("numeroEndereco", this.endereco.getNumero());
        }
        if (texto.contains("bairroCliente")) {
            textoAlterado = texto.replace("bairroCliente", this.endereco.getBairro());
        }
        if (texto.contains("cidadeCliente")) {
            textoAlterado = texto.replace("cidadeCliente", this.endereco.getCidade());
        }
        if (texto.contains("estadoCliente")) {
            textoAlterado = texto.replace("estadoCliente", this.endereco.getEstado());
        }
        if (texto.contains("cepCliente")) {
            textoAlterado = texto.replace("cepCliente", FormatterUtil.definirMascara(this.endereco.getCep(), "#####-###"));
        }
        if (texto.contains("cnpjCliente")) {
            textoAlterado = texto.replace("cnpjCliente", FormatterUtil.definirMascara(this.cnpj, "##.###.###/####-##"));
        }
        if (texto.contains("inscricaoCliente")) {
            textoAlterado = texto.replace("inscricaoCliente", this.inscricaoEstadual);
        }
        if (texto.contains("nomeDoCliente")) {
            textoAlterado = texto.replace("nomeDoCliente", this.nomePessoaResponsavel == null ? "" : this.nomePessoaResponsavel);
        }
        if (texto.contains("cpfCliente")) {
            textoAlterado = texto.replace("cpfCliente", this.cpfPessoaResponsavel == null ? "" : FormatterUtil.definirMascara(this.cpfPessoaResponsavel, "###.###.###-##"));
        }
        if (texto.contains("valorMensalidade")) {
            textoAlterado = texto.replace("valorMensalidade", FormatterUtil.getValorFormatado(valorMensalidade));
        }
        if (texto.contains("valorPorExtenso")) {
            textoAlterado = texto.replace("valorPorExtenso", currencyWriter.write(BigDecimal.valueOf(valorMensalidade)).toUpperCase());
        }
        if (texto.contains("diaDePagamento")) {
            textoAlterado = texto.replace("diaDePagamento", String.format("%02d", this.diaVencimento));
            diaPorExtenso = currencyWriter.write(BigDecimal.valueOf(this.diaVencimento)).toUpperCase().replace("REAIS", "");
        }
        if (texto.contains("diaDePagamentoPorExtenso")) {
            textoAlterado = texto.replace("diaDePagamentoPorExtenso", diaPorExtenso);
        }
        if (texto.contains("valorDaInstalacao")) {
            textoAlterado = texto.replace("valorDaInstalacao", FormatterUtil.getValorFormatado(valorInstalacao));
        }
        if (texto.contains("valorDaInstalacaoPorExtenso")) {
            textoAlterado = texto.replace("valorDaInstalacaoPorExtenso", currencyWriter.write(BigDecimal.valueOf(valorInstalacao)).toUpperCase());
        }
        if (texto.contains("dataDoContrato")) {
            LocalDate dataContrato = this.contratoAceito() ? this.dataAceite : LocalDate.now();
            String dataContratoFormatada = dataContrato.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(Locale.forLanguageTag("pt")));
            int indeceVirgula = dataContratoFormatada.indexOf(",") + 1;
            String dataContratoSemODiaDaSemana = dataContratoFormatada.substring(indeceVirgula).trim();
            textoAlterado = texto.replace("dataDoContrato", dataContratoSemODiaDaSemana);
        }
        elemento.setText(textoAlterado, 0);
    }
}
