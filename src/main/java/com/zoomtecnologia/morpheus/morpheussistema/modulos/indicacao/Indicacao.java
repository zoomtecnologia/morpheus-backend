package com.zoomtecnologia.morpheus.morpheussistema.modulos.indicacao;

import java.io.Serializable;
import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "indicacoes")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
public class Indicacao implements Serializable {

    @EmbeddedId
    
	@Column(name = "id")
	private IndicacaoPK id;

    
	@Column(name = "nome_cliente")
	private String nomeCliente;

    
	@Column(name = "telefone")
	private String telefone;

    
	@Column(name = "data")
	private OffsetDateTime data;

    
	@Column(name = "identificador")
	private String identificador;

}
