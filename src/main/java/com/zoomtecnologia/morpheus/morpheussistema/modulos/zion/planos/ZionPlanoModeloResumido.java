package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planos;

import java.time.LocalDateTime;

public interface ZionPlanoModeloResumido {

  
     String getCodigo();

     String getNome();

     boolean getStatus();

     double getValor();
    
     Double getValorUsuarioAdicional();

     Double getValorPDVAdicional();

     String getDescricao();
    
     String getTipoPlano();

     boolean getGeraCobranca();
    
     LocalDateTime getDataCadastro();
    
     int getUsuarios();
     
     int getPdvs();
    
     Boolean getTeste();
    
     Integer getDiasParaTeste();
     
     String getCodigoAplicacao();
     
     String getCodigoModulo();          
}
