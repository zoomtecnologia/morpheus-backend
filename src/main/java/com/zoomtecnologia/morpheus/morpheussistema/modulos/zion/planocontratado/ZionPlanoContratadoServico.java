package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado;

import com.zoomtecnologia.morpheus.morpheussistema.bancodedados.GeraBancoDeDados;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.ClienteServico;
import com.zoomtecnologia.morpheus.morpheussistema.email.Mailer;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDVListaModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDVServico;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao.ZionPlanoPrincipal;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao.ZionPlanosContratacao;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao.ZionPlanosContratacaoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planocontratado.ZionPlanoContratadoPadrao;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planocontratado.ZionPlanoContratadoPadraoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planoscontratacao.ZionPlanosContratacaoPadrao;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planoscontratacao.ZionPlanosContratacaoPadraoPK;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.planoscontratacao.ZionPlanosContratacaoPadraoRepositorio;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ZionPlanoContratadoServico {

    private final ZionPlanoContratadoRepositorio zionPlanoContratadoRepositorio;
    private final ZionPlanosContratacaoRepositorio zionPlanosContratacaoRepositorio;
    private final ClienteServico clienteServico;
    private final Mailer mailer;
    private final MessageSourceUtil messageSourceUtil;
    private final ResourceLoader resourceLoader;
    private final ZionPlanoContratadoPadraoRepositorio zionPlanoContratadoPadraoRepositorio;
    private final ZionPlanosContratacaoPadraoRepositorio zionPlanosContratacaoPadraoRepositorio;
    private final PDVServico pdvServico;
    private final PasswordEncoder passwordEncoder;

    public Page<ZionPlanoContratado> listarContratos(String cliente, Pageable pageable) {
        return this.zionPlanoContratadoRepositorio.listarContratosPorCliente(cliente, pageable);
    }

    public ZionPlanoContratado buscarContrato(String codigo) {
        final Optional<ZionPlanoContratado> planoContratadoOptional = this.zionPlanoContratadoRepositorio.findById(codigo);
        if (planoContratadoOptional.isEmpty()) {
            this.messageSourceUtil.recursoNaoExiste("zionplanocontratado.nao-existe", "zionplanocontratado.nao-existe-detalhes");
        }
        return planoContratadoOptional.get();
    }

    public ZionPlanoContratadoModeloAPI buscarDadosContrato(String codigoContrato) {
        List<ZionPlanoContratadoResumido> zionPlanoContratadoResumido = this.zionPlanoContratadoRepositorio.buscarContrato(codigoContrato);
        if (zionPlanoContratadoResumido.isEmpty()) {
            this.messageSourceUtil.recursoNaoExiste("zionplanocontratado.nao-existe", "zionplanocontratado.nao-existe-detalhes");
        }
        ZionPlanoContratadoModeloAPI zionPlanoContratadoModeloAPI = new ZionPlanoContratadoModeloAPI(zionPlanoContratadoResumido);
        return zionPlanoContratadoModeloAPI;
    }

    @Transactional
    public ZionPlanoContratado gerarContrato(ZionPlanoContratadoInclusaoModeloAPI zionPlanoContratadoInclusaoModeloAPI) {
        final Cliente cliente = this.clienteServico.buscarClientePorCNPJ(zionPlanoContratadoInclusaoModeloAPI.getDocumentoIdentificacaoCliente());
        final ZionPlanoContratado zionPlanoContratadoSalvo = salvarContrato(zionPlanoContratadoInclusaoModeloAPI, cliente);
        this.criarNovoContrato(zionPlanoContratadoSalvo, cliente);
        return zionPlanoContratadoSalvo;
    }

    @Transactional
    public void aceitarContrato(String codigoContrato, StatusAceiteContratoModeloAPI statusAceiteContratoModeloAPI) {
        final ZionPlanoContratado zionPlanoContratado = this.buscarContrato(codigoContrato);
        final Cliente clienteEncontrado = this.clienteServico.buscarClientePorCNPJ(zionPlanoContratado.getDocumentoIdentificacaoEmpresa());
        if (clienteEncontrado.contratoAceito()) {
            this.messageSourceUtil.negocioException("zionplanocontratado.ja-existe-aceite", "zionplanocontratado.ja-existe-aceite-detalhes");
        }
        final ZionPlanoPrincipal zionPlanoPrincipal = this.zionPlanosContratacaoRepositorio.buscarPlanoPrincipalPorContrato(zionPlanoContratado.getCodigo());
        zionPlanoContratado.definirPeriodoContrato(zionPlanoPrincipal);
        if (statusAceiteContratoModeloAPI.contratoNaoAceito()) {
            this.cancelarContrato(zionPlanoContratado.getCodigo());
            return;
        }
        zionPlanoContratado.setStatusPagamento("PG");
        this.zionPlanoContratadoRepositorio.save(zionPlanoContratado);
        final ZionPlanoContratadoPadrao zionPlanoContratadoPadrao = new ZionPlanoContratadoPadrao();
        BeanUtils.copyProperties(zionPlanoContratado, zionPlanoContratadoPadrao);
        this.zionPlanoContratadoPadraoRepositorio.save(zionPlanoContratadoPadrao);
        clienteEncontrado.aceitarContrato();
        if (zionPlanoContratado.temPDV()) {
            clienteEncontrado.gerarSenhaPDV(this.passwordEncoder);
        }
        this.clienteServico.alterarCliente(clienteEncontrado);
        this.pdvServico.salvarPdvs(clienteEncontrado, zionPlanoContratado.getPdvs());
        this.ativarSistema(clienteEncontrado, zionPlanoContratado);
    }

    @Transactional
    public void cancelarContrato(String codigoContrato) {
        final ZionPlanoContratado contratoEncontrado = this.buscarContrato(codigoContrato);
        final Cliente cliente = this.clienteServico.buscarClientePorCNPJ(contratoEncontrado.getDocumentoIdentificacaoEmpresa());
        this.zionPlanoContratadoRepositorio.cancelarContrato(codigoContrato, LocalDate.now());
        this.zionPlanoContratadoPadraoRepositorio.cancelarContrato(codigoContrato, LocalDate.now());
        cliente.removerAceite();
        this.clienteServico.alterarCliente(cliente);
    }

    @Transactional
    public void alterarStatusPagamento(String cnpj, String status) {
        final Optional<ZionPlanoContratado> contratoEncontrado = this.buscarContratoAtivoPorCnpjEmpresa(cnpj);
        if (contratoEncontrado.isEmpty()) return;
        final ZionPlanoContratado zionPlanoContratado = contratoEncontrado.get();
        zionPlanoContratado.setStatusPagamento(status);
        this.zionPlanoContratadoRepositorio.save(zionPlanoContratado);
    }

    private Optional<ZionPlanoContratado> buscarContratoAtivoPorCnpjEmpresa(String cnpj) {
        return this.zionPlanoContratadoRepositorio.buscarContratoPorCliente(cnpj);
    }

    private ZionPlanoContratado salvarContrato(ZionPlanoContratadoInclusaoModeloAPI zionPlanoContratadoInclusaoModeloAPI, Cliente cliente) {
        final Optional<ZionPlanoContratado> zionPlanoContratadoOptional = this.zionPlanoContratadoRepositorio.buscarContratoPorCliente(zionPlanoContratadoInclusaoModeloAPI.getDocumentoIdentificacaoCliente());
        if (zionPlanoContratadoOptional.isPresent()) {
            this.messageSourceUtil.recursoJaExiste("zionplanocontratado.ja-existe", "zionplanocontratado.ja-existe-detalhes");
        }
        if (cliente.isNotGrupoEmpresa()) {
            this.messageSourceUtil.recursoNaoExiste("cliente.grupo-empresa-nao-existe", "cliente.grupo-empresa-nao-existe-detalhes");
        }
        final Optional<String> grupoEmpresaOptional = this.zionPlanoContratadoRepositorio.buscarGrupoEmpresa(cliente.getCodigoGrupoEmpresa().getCodigo());
        final String codigoEmpresa = grupoEmpresaOptional.map(s -> String.format("%03d", Integer.parseInt(s) + 1)).orElse("001");
        final ZionPlanoContratado zionPlanoContratado = zionPlanoContratadoInclusaoModeloAPI.converterParaZionPlanoContratado();
        zionPlanoContratado.setCodigo(UUID.randomUUID().toString());
        zionPlanoContratado.setStatus(true);
        zionPlanoContratado.setStatusPagamento("BL");
        zionPlanoContratado.setCodigoEmpresa(codigoEmpresa);
        zionPlanoContratado.setDiasTolerancia(10);
        final ZionPlanoContratado zionPlanoContratadoSalvo = this.zionPlanoContratadoRepositorio.save(zionPlanoContratado);
        final List<ZionPlanosContratacao> planosContratacao = zionPlanoContratadoInclusaoModeloAPI.converterParaZionPlanosContratacao(zionPlanoContratadoSalvo.getCodigo());
        this.zionPlanosContratacaoRepositorio.salvar(planosContratacao);
        final ZionPlanoContratadoPadrao zionPlanoContratadoPadrao = new ZionPlanoContratadoPadrao();
        BeanUtils.copyProperties(zionPlanoContratado, zionPlanoContratadoPadrao);
        final List<ZionPlanosContratacaoPadrao> planoContratacaoPadrao = planosContratacao.stream().map(planoContratacao -> {
            ZionPlanosContratacaoPadrao zionPlanosContratacaoPadrao = new ZionPlanosContratacaoPadrao();
            zionPlanosContratacaoPadrao.setId(new ZionPlanosContratacaoPadraoPK());
            BeanUtils.copyProperties(planoContratacao, zionPlanosContratacaoPadrao);
            BeanUtils.copyProperties(planoContratacao.getId(), zionPlanosContratacaoPadrao.getId());
            return zionPlanosContratacaoPadrao;
        }).collect(Collectors.toList());
        this.zionPlanoContratadoPadraoRepositorio.save(zionPlanoContratadoPadrao);
        this.zionPlanosContratacaoPadraoRepositorio.salvar(planoContratacaoPadrao);
        return zionPlanoContratado;
    }

    private void criarNovoContrato(ZionPlanoContratado zionPlanoContratado, Cliente cliente) {
        zionPlanoContratado.setGrupoEmpresa(cliente.getCodigoGrupoEmpresa().getCodigo());
        this.zionPlanoContratadoRepositorio.save(zionPlanoContratado);
        final ZionPlanoPrincipal zionPlanoPrincipal = this.zionPlanosContratacaoRepositorio.buscarPlanoPrincipalPorContrato(zionPlanoContratado.getCodigo());
        if (zionPlanoPrincipal.getTeste()) {
            this.gerarBancoDeDados(cliente, zionPlanoContratado);
            this.mailer.enviarEmailAcessoUsuario(cliente);
            return;
        }
        this.mailer.enviarEmailContrato(zionPlanoContratado, cliente);
    }

    private void ativarSistema(Cliente cliente, ZionPlanoContratado zionPlanoContratado) {
        this.gerarBancoDeDados(cliente, zionPlanoContratado);
        if (zionPlanoContratado.isEmpresaNova()) {
            return;
        }
        this.mailer.enviarEmailAcessoUsuario(cliente);
//        this.gerarCobrancas(); TODO: faremos isso futuramente!
    }

    private void gerarBancoDeDados(Cliente cliente, ZionPlanoContratado zionPlanoContratado) {
        final List<ZionAplicacoesContratada> aplicacoesContratadas = this.zionPlanoContratadoRepositorio.listarAplicacoesContratadas(zionPlanoContratado.getCodigo());
        final List<PDVListaModeloAPI> pdvs = this.pdvServico.listarPDVs(
                cliente.getCnpj(),
                cliente.getCodigoRepresentante());
        final GeraBancoDeDados geraBancoDeDados = new GeraBancoDeDados(
                this.resourceLoader,
                cliente,
                zionPlanoContratado,
                aplicacoesContratadas,
                pdvs
        );
        try {
            geraBancoDeDados.gerarBanco();
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            this.messageSourceUtil.negocioException("ativacaocliente.bancodedados", "ativacaocliente.bancodedados-detalhes");
        }
    }

    private void gerarCobrancas() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
