package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.representante.Representante;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente.Cliente;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "pedido")
public class Pedido implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    
	@Column(name = "numero")
	private Integer numero;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    
	@Column(name = "data")
	private LocalDate data;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    
	@Column(name = "data_marcacao_instalacao")
	private LocalDateTime dataMarcacaoInstalacao;

    @ManyToOne
    @JoinColumn(name = "representante")
    
	private Representante representante;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "cliente")
    
	private Cliente cliente;

    @NotNull
    
	@Column(name = "tipo_comissao")
	private Integer tipoComissao;

    @NotNull
    
	@Column(name = "total")
	private Double total;

    @NotNull
    
	@Column(name = "valor_mensalidade")
	private Double valorMensalidade;

    @NotNull
    @Convert(converter = PedidoStatus.Mapeador.class)
    
	@Column(name = "status")
	private PedidoStatus status;

    
	@Column(name = "email")
	private String email;

    
	@Column(name = "obs")
	private String obs;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public LocalDateTime getDataMarcacaoInstalacao() {
        return dataMarcacaoInstalacao;
    }

    public void setDataMarcacaoInstalacao(LocalDateTime dataMarcacaoInstalacao) {
        this.dataMarcacaoInstalacao = dataMarcacaoInstalacao;
    }

    public Representante getRepresentante() {
        return representante;
    }

    public void setRepresentante(Representante representante) {
        this.representante = representante;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getTipoComissao() {
        return tipoComissao;
    }

    public void setTipoComissao(Integer tipoComissao) {
        this.tipoComissao = tipoComissao;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(Double valorMensalidade) {
        this.valorMensalidade = valorMensalidade;
    }

    public PedidoStatus getStatus() {
        return status;
    }

    public void setStatus(PedidoStatus status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.numero);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido other = (Pedido) obj;
        return Objects.equals(this.numero, other.numero);
    }

    @Override
    public String toString() {
        return "Pedido{" + "data=" + data + ", dataMarcacaoInstalacao=" + dataMarcacaoInstalacao + ", representante=" + representante + ", cliente=" + cliente + ", tipoComissao=" + tipoComissao + ", total=" + total + ", valorMensalidade=" + valorMensalidade + ", status=" + status + ", email=" + email + ", obs=" + obs + '}';
    }

    public void atualizarStatusPedidoCliente(PedidoClienteStatus pedidoClienteStatus) {
        pedidoClienteStatus.alterarStatusPedidoCliente(this);
    }

    public byte[] gerarContrato() throws IOException, FileNotFoundException, InvalidFormatException {
        return this.cliente.gerarContrato(this);
    }
    
    public void gerarNovoContrato() throws IOException, FileNotFoundException, InvalidFormatException {
        this.cliente.gerarNovoContrato(this);
    }

}
