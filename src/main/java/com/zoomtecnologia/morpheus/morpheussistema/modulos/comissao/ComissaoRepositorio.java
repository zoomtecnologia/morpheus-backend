package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ComissaoRepositorio extends JpaRepository<Comissao, Integer>, ComissaoRepositorioQuery {

    static final String CONDICOES
            = "comissao.codigoRepresentante like %:pesquisa% or "
            + "comissao.codigoCliente like %:pesquisa% or "
            + "comissao.status like %:pesquisa% or "
            + "comissao.tipo like %:pesquisa%";

    @Query("select comissao from Comissao comissao where " + CONDICOES)
    public Page<Comissao> listarTodasPaginado(Pageable pageable, @Param("pesquisa") String pesquisa);

    @Query("select comissao from Comissao comissao where comissao.status like %?2% and comissao.codigoRepresentante=?3 and comissao.codigoCliente like %?1% ")
    public Page<Comissao> listarComissoesPorRepresentante(String pesquisaConteudo, String status, String codigoRepresentante, Pageable pageable);

    @Query("select new com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoTotaisModeloAPI("
            + "coalesce(sum(CASE WHEN comissao.status = 'LIBERADO' THEN comissao.valor ELSE 0 END),0)"
            + ",coalesce(sum(CASE WHEN comissao.status = 'PENDENTE' THEN comissao.valor ELSE 0 END),0)"
            + ",coalesce(sum(CASE WHEN comissao.status = 'PAGO' THEN comissao.valor ELSE 0 END),0)"
            + ",comissao.codigoRepresentante.nome"
            + ") from Comissao comissao where comissao.codigoRepresentante.documento like %:codigoRepresentante% and comissao.dataMensalidade between :dataInicial and :dataFinal")
    public ComissaoTotaisModeloAPI listarTotalDeComissoes(@Param("codigoRepresentante") String representante, LocalDate dataInicial, LocalDate dataFinal);

    @Query("select new com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaModeloAPI("
            + "comissao.codigo"
            + ",comissao.codigoCliente.razaoSocial"
            + ",comissao.tipo"
            + ",function('DATE_FORMAT',comissao.dataMensalidade,'%d/%m/%Y')"
            + ",comissao.valor"
            + ",comissao.valorMensalidade"
            + ",comissao.valorInstalacao"
            + ",comissao.status"
            + ") from Comissao comissao where comissao.codigoRepresentante.documento like %:codigoRepresentante% and comissao.status=:status and comissao.dataMensalidade between :dataInicial and :dataFinal order by comissao.dataMensalidade")
    public List<ComissaoListaModeloAPI> listarComissaoes(@Param("codigoRepresentante") String codigoRepresentante, @Param("status") String status, LocalDate dataInicial, LocalDate dataFinal);

    @Query(value = "select new com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaPagamentosModeloAPI("
            + "function('DATE_FORMAT',comissao.dataSolicitacao,'%d/%m/%Y')"
            + ",function('DATE_FORMAT',comissao.dataPagamento,'%d/%m/%Y')"
            + ",comissao.codigoSolicitacao"
            + ",SUM(comissao.valor)"
            + ",comissao.status"
            + ",representante.nome"
            + ",representante.email"
            + ",representante.chavePix"
            + ",representante.cidade"
            + ",representante.documento"
            + ") from Comissao comissao inner join comissao.codigoRepresentante representante where representante.documento like %:documentoRepresentante% "
            + " and comissao.status=:status"
            + " and representante.nome like %:nomeRepresentante%"
            + " group by comissao.codigoSolicitacao order by comissao.dataPagamento desc")
    public List<ComissaoListaPagamentosModeloAPI> listarPagamentosComissao(@Param("documentoRepresentante") String documentoRepresentante, @Param("status") String status, @Param("nomeRepresentante") String nomeRepresentante);

    @Query("select comissao.codigoCliente.cnpj from Comissao comissao where comissao.dataMensalidade < CURRENT_DATE group by comissao.codigoCliente.cnpj")
    public List<String> listarPessoasComMensalidadesEmAberto();

    @Query("select comissao from Comissao comissao where comissao.tipo=:tipoComissao and comissao.codigoRepresentante=:codigoRepresentante and comissao.codigoCliente=:codigoCliente")
    public Optional<Comissao> buscarComissaoPorTipo(String tipoComissao, String codigoRepresentante, String codigoCliente);

    @Query(value = "select new com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoListaPagamentosModeloAPI("
            + "function('DATE_FORMAT',comissao.dataSolicitacao,'%d/%m/%Y')"
            + ",function('DATE_FORMAT',comissao.dataPagamento,'%d/%m/%Y')"
            + ",comissao.codigoSolicitacao"
            + ",SUM(comissao.valor)"
            + ",comissao.status"
            + ",representante.nome"
            + ",representante.email"
            + ",representante.chavePix"
            + ",representante.cidade"
            + ",representante.documento"
            + ") from Comissao comissao inner join comissao.codigoRepresentante representante where comissao.codigoSolicitacao=:codigoSolicitacao")
    public ComissaoListaPagamentosModeloAPI buscarSolicitacao(String codigoSolicitacao);

    public boolean existsByDataMensalidadeAndCodigoCliente_Cnpj(LocalDate dataMensalidade, String cnpj);

    @Modifying
    @Query(value = "update Comissao set status=:status where codigo in :comissoes")
    public void atualizarStatus(@Param("comissoes") List<Integer> comissoes, @Param("status") String status);

    @Modifying
    @Query(value = "update Comissao set status=:status,dataSolicitacao=:dataSolicitacao,codigoSolicitacao=concat(codigoRepresentante.documento,'-',DATE_FORMAT(current_date,'%d%m%Y'),'-',DATE_FORMAT(current_time,'%H%i%s')) where codigo in :comissoes ")
    public void socilitarLiberacao(@Param("comissoes") List<Integer> comissoes, @Param("status") String status, @Param("dataSolicitacao") LocalDate dataSolicitacao);

    @Modifying
    @Query("update Comissao set dataMensalidade=:dataMensalidade where codigoCliente.cnpj=:codigoCliente and tipo='MENSALIDADE' and status='PENDENTE' and codigoRepresentante.documento=:codigoRepresentante")
    public void alterarDataProximoVencimento(@Param("dataMensalidade") LocalDate dataMensalidade, @Param("codigoCliente") String codigoCliente, @Param("codigoRepresentante") String codigoRepresentante);

    @Modifying
    @Query("update Comissao set dataPagamento=:dataPagamento,status='PAGO' where codigoSolicitacao=:codigoSolicitacao")
    public void realizarPagamentoSolicitacao(@Param("codigoSolicitacao") String codigoSolicitacao, @Param("dataPagamento") LocalDate dataPagamento);

    @Modifying
    @Query("update Comissao set status=:status where codigoCliente.cnpj=:cnpj and dataMensalidade=:dataMensalidade")
    public void atualizarStatusComissaoPorDataMensalidade(String status, String cnpj, LocalDate dataMensalidade);

    @Modifying
    @Query("update Cliente cliente set cliente.statusPagamento='V' where cliente in( select comissao.codigoCliente from Comissao comissao where comissao.dataMensalidade < CURRENT_DATE and comissao.status='PENDENTE') ")
    public void bloquearClienteComPagamentosVencidos();

    @Modifying
    @Query("update Cliente cliente set cliente.statusPagamento='P' where cliente in( select comissao.codigoCliente from Comissao comissao where comissao.dataMensalidade < CURRENT_DATE and comissao.status='PAGO') ")
    public void ativarClienteComPagamentosQuitados();

}
