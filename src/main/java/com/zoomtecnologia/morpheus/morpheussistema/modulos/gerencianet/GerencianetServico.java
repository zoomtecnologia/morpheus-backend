package com.zoomtecnologia.morpheus.morpheussistema.modulos.gerencianet;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
public class GerencianetServico {

    private WebDriver driver;

    public GerencianetServico() {

    }

    public String fazerLogin(String username, String password) {
        try {
            WebDriverManager.chromedriver().setup();

            ChromeOptions options = initChrome();

            this.driver = new ChromeDriver(options);

            // Remover a flag que denuncia o Selenium para o navegador
            ((JavascriptExecutor) driver).executeScript("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})");
            driver.get("https://login.sejaefi.com.br/oauth/authorize?redirect_to=https://app.sejaefi.com.br/home");

            // Simular tempo de carregamento da página
            Thread.sleep(3000);

            // Simular digitação humana no campo de usuário
            WebElement campoUsuario = driver.findElement(By.id("username"));
            for (char c : username.toCharArray()) {
                campoUsuario.sendKeys(String.valueOf(c));
                Thread.sleep(200); // Simular tempo de digitação
            }

            // Simular digitação humana no campo de senha
            WebElement campoSenha = driver.findElement(By.id("password"));
            for (char c : password.toCharArray()) {
                campoSenha.sendKeys(String.valueOf(c));
                Thread.sleep(200);
            }

            // Pequeno atraso antes de clicar no botão
            Thread.sleep(1500);
            WebElement botaoEntrar = driver.findElement(By.id("buttonSubmitLogin"));
            botaoEntrar.click();

            // Esperar pelo redirecionamento após login
            Thread.sleep(5000);

            // Retornar URL final e título da página
            return "Login bem-sucedido. Redirecionado para: " + driver.getCurrentUrl() + " | Título: " + driver.getTitle();
        } catch (Exception e) {
            return "Erro ao realizar login: " + e.getMessage();
        } finally {
            driver.quit();
        }
    }

    private static ChromeOptions initChrome() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-blink-features=AutomationControlled"); // Evita detecção pelo Selenium
        options.addArguments("--disable-popup-blocking"); // Evita bloqueio de pop-ups
        options.addArguments("--headless"); // Remove se quiser ver o navegador abrir
        options.addArguments("--incognito"); // Modo anônimo para evitar cache
        options.addArguments("--disable-gpu"); // Para compatibilidade
        options.addArguments("--window-size=1920,1080"); // Define tamanho da janela
        options.addArguments("--remote-allow-origins=*"); // Evita erros de CORS

        // Define um User-Agent para parecer um usuário normal
        options.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.5481.177 Safari/537.36");
        return options;
    }

    public void fecharDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

}
