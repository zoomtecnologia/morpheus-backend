package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao;

import javax.validation.constraints.NotNull;

public class PermissaoAplicacaoInclusaoModeloAPI {

    @NotNull
    private Integer codigoPermissao;

    @NotNull
    private Integer codigoAplicacao;

    @NotNull
    private Boolean status;

    @NotNull
    private Boolean acessar;

    @NotNull
    private Boolean incluir;

    @NotNull
    private Boolean alterar;

    @NotNull
    private Boolean excluir;

    public Integer getCodigoPermissao() {
        return codigoPermissao;
    }

    public void setCodigoPermissao(Integer codigoPermissao) {
        this.codigoPermissao = codigoPermissao;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getAcessar() {
        return acessar;
    }

    public void setAcessar(Boolean acessar) {
        this.acessar = acessar;
    }

    public Boolean getIncluir() {
        return incluir;
    }

    public void setIncluir(Boolean incluir) {
        this.incluir = incluir;
    }

    public Boolean getAlterar() {
        return alterar;
    }

    public void setAlterar(Boolean alterar) {
        this.alterar = alterar;
    }

    public Boolean getExcluir() {
        return excluir;
    }

    public void setExcluir(Boolean excluir) {
        this.excluir = excluir;
    }

    public Integer getCodigoAplicacao() {
        return codigoAplicacao;
    }

    public void setCodigoAplicacao(Integer codigoAplicacao) {
        this.codigoAplicacao = codigoAplicacao;
    }

    @Override
    public String toString() {
        return "PermissaoAplicacaoInclusaoModeloAPI{" + "codigoPermissao=" + codigoPermissao + ", codigoAplicacao=" + codigoAplicacao + ", status=" + status + ", acessar=" + acessar + ", incluir=" + incluir + ", alterar=" + alterar + ", excluir=" + excluir + '}';
    }
}
