package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planoscontratacao;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ZionPlanosContratacaoPK implements Serializable {

    @Column(name="codigo_contratacao")
    private String codigoContratacao;

    @Column(name="codigo_plano")
    private String codigoPlano;
}
