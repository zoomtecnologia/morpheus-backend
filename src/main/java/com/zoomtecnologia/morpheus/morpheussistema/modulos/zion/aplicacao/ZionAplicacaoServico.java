package com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.aplicacao;

import com.zoomtecnologia.morpheus.morpheussistema.padrao.aplicacao.ZionAplicacaoPadrao;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.aplicacao.ZionAplicacaoPadraoPK;
import com.zoomtecnologia.morpheus.morpheussistema.padrao.aplicacao.ZionAplicacaoPadraoRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ZionAplicacaoServico {

    private final ZionAplicacaoPadraoRepositorio zionAplicacaoPadraoRepositorio;
    private final ZionAplicacaoRepositorio aplicacaoRepositorio;
    private final MessageSourceUtil messageSourceUtil;

    public List<ZionAplicacao> listarAplicacoes(String pesquisa) {
        return this.aplicacaoRepositorio.listarAplicacoes(pesquisa);
    }

    public ZionAplicacao buscarAplicacao(String codigo, String modulo) {
        final Optional<ZionAplicacao> zionAplicaOptional = this.aplicacaoRepositorio.buscarAplicacao(codigo, modulo);
        if (zionAplicaOptional.isEmpty()) {
            this.messageSourceUtil.recursoNaoExiste("zionaplicacao.nao-existe", "zionaplicacao.nao-existe-detalhes");
        }
        return zionAplicaOptional.get();
    }

    @Transactional
    public ZionAplicacao salvar(ZionAplicacao aplicacao) {
        final boolean aplicacaoEncontrada = this.aplicacaoRepositorio.existsById(aplicacao.getId());
        if (aplicacaoEncontrada) {
            this.messageSourceUtil.recursoJaExiste("zionaplicacao.ja-existe", "zionaplicacao.ja-existe-detalhes");
        }
        ZionAplicacao zionAplicacaoSalva = this.aplicacaoRepositorio.save(aplicacao);
        ZionAplicacaoPadrao zionAplicacaoPadrao = new ZionAplicacaoPadrao();
        BeanUtils.copyProperties(zionAplicacaoSalva, zionAplicacaoPadrao);
        zionAplicacaoPadrao.setId(new ZionAplicacaoPadraoPK());
        BeanUtils.copyProperties(zionAplicacaoSalva.getId(), zionAplicacaoPadrao.getId());
        this.zionAplicacaoPadraoRepositorio.save(zionAplicacaoPadrao);
        return zionAplicacaoSalva;
    }

    @Transactional
    public void alterarAplicacao(ZionAplicacao aplicacao) {
        final ZionAplicacao zionAplicacaoEncontrada = this.buscarAplicacao(aplicacao.getId().getCodigo(), aplicacao.getId().getModulo());
        BeanUtils.copyProperties(aplicacao, zionAplicacaoEncontrada, "id");
        this.aplicacaoRepositorio.save(zionAplicacaoEncontrada);
        ZionAplicacaoPadrao zionAplicacaoPadrao = new ZionAplicacaoPadrao();
        zionAplicacaoPadrao.setId(new ZionAplicacaoPadraoPK());
        BeanUtils.copyProperties(zionAplicacaoEncontrada.getId(), zionAplicacaoPadrao.getId());
        BeanUtils.copyProperties(zionAplicacaoEncontrada, zionAplicacaoPadrao);
        this.zionAplicacaoPadraoRepositorio.save(zionAplicacaoPadrao);
    }

}
