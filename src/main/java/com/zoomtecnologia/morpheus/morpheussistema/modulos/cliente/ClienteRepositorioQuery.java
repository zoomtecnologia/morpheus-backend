package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClienteRepositorioQuery {
    
    public Page<ClienteRetornoModeloAPI> filtrarClientes(ClienteFiltro cliente, Pageable pageable);
    public Page<ClienteRetornoModeloAPI> listarClientes(String pesquisa,String representante, Pageable pageable);

}
