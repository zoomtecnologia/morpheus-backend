package com.zoomtecnologia.morpheus.morpheussistema.modulos.pedido;

public class PedidoStatusModeloAPI {

    private PedidoStatus pedidoStatus;
    private String status;

    public PedidoStatusModeloAPI(PedidoStatus pedidoStatus, PedidoStatus status) {
        this.pedidoStatus = pedidoStatus;
        this.status = status.getStatus();
    }

    public PedidoStatus getPedidoStatus() {
        return pedidoStatus;
    }

    public void setPedidoStatus(PedidoStatus pedidoStatus) {
        this.pedidoStatus = pedidoStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    @Override
    public String toString() {
        return "PedidoStatusModeloAPI{" + "pedidoStatus=" + pedidoStatus + ", status=" + status + '}';
    }
    
}
