package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("comissoes")
@CrossOrigin
public class ComissaoControle {

    @Autowired
    private ComissaoServico comissaoServico;

    @GetMapping
    public Page<Comissao> listarTodasPaginado(Pageable pageable,
            @RequestParam(value = "pesquisa", required = false, defaultValue = "") String pesquisa) {
        return this.comissaoServico.listarTodasPaginado(pageable, pesquisa);
    }

    @GetMapping("comissoes-status")
    public List<ComissaoListaModeloAPI> listarTodasPaginado(FiltroComissao filtroComissao) {
        return this.comissaoServico.listarComissoes(filtroComissao);
    }

    @GetMapping("solicitacao-pagamentos")
    public List<ComissaoListaPagamentosModeloAPI> listarSolicitacoesPagamentos(FiltroComissao filtroComissao) {
        return this.comissaoServico.listarSolicitacoesPagamento(filtroComissao);
    }

    @GetMapping("{codigo}")
    public ResponseEntity<Comissao> buscarPorCodigo(@PathVariable("codigo") Integer codigoComissao) {
        Comissao comissaoEncontrada = this.comissaoServico.buscarPorCodigo(codigoComissao);
        return ResponseEntity.ok(comissaoEncontrada);
    }

    @GetMapping("totais-representante")
    public ResponseEntity<ComissaoTotaisModeloAPI> consultarTotaisComissaoPorRepresentante(
            @RequestParam(value = "representante", required = false, defaultValue = "") String representante,
            @RequestParam(value = "dataInicial",required = false,defaultValue = "") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataInicial,
            @RequestParam(value = "dataFinal",required = false,defaultValue = "") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataFinal) {
        ComissaoTotaisModeloAPI totalComissaoPorRepresentante = this.comissaoServico.consultarTotalComissaoPorRepresentante(representante,dataInicial,dataFinal);
        return ResponseEntity.ok(totalComissaoPorRepresentante);
    }

    @GetMapping("{codigoSolicitacao}/solicitacao-por-codigo")
    public ResponseEntity<ComissaoListaPagamentosModeloAPI> buscarSolicitacaoDeComissaoPorCodigo(@PathVariable("codigoSolicitacao") String codigoSolicitacao) {
        ComissaoListaPagamentosModeloAPI solocitacaoComissaoPorCodigo = this.comissaoServico.buscarSolocitacaoComissaoPorCodigo(codigoSolicitacao);
        return ResponseEntity.ok(solocitacaoComissaoPorCodigo);
    }

    @PutMapping("{codigo}")
    public void alterarComissao(@PathVariable("codigo") Integer codigoComissao,
            @RequestBody @Valid ComissaoAlteracaoModeloAPI comissaoAlteracaoModeloAPI) {
        this.comissaoServico.alterarComissao(comissaoAlteracaoModeloAPI, codigoComissao);
    }

    @PutMapping("status-comissao")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarStatus(@RequestBody @Valid List<ComissaoAlteracaoStatusModeloAPI> comissoes) {
        this.comissaoServico.alterarStatus(comissoes);
    }

    @PutMapping("solicitar-comissao")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Comissao> solicitarComissao(@RequestBody @Valid List<Integer> comissoes) {
        return ResponseEntity.ok(this.comissaoServico.solicitarComissoes(comissoes));
    }

    @PostMapping("manutencao")
    public ResponseEntity<Comissao> salvarComissaoManutencao(@RequestBody @Valid ComissaoManutencaoInclusaoModeloAPI comissaoManutencaoInclusaoModeloAPI) {
        this.comissaoServico.salvarComissaoManutencao(comissaoManutencaoInclusaoModeloAPI);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("pagamento-solicitacao/{codigoSolicitacao}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void realizarPagamentoDeSolicitacao(@PathVariable("codigoSolicitacao") String codigoSolicitacao) {
        this.comissaoServico.realizarPagamentoDeSolicitacao(codigoSolicitacao);
    }

    @PostMapping("enviar-email-solicitacao-pagamento/{codigoSolicitacao}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enviarEmailDeSolicitacaoDeComissao(@PathVariable("codigoSolicitacao") String codigoSolicitacao) {
        this.comissaoServico.enviarEmailSolicitacaoPagamento(codigoSolicitacao);
    }

}
