package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissaoaplicacao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.aplicacao.Aplicacao;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao.Permissao;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonRootName(value = "permissaoAplicacao")
public class PermissaoAplicacaoPK implements Serializable {

    @ManyToOne
    @JoinColumn(name = "codigo_permissao")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Permissao permissao;

    @ManyToOne
    @JoinColumn(name = "codigo_aplicacao")
    private Aplicacao aplicacao;

    public PermissaoAplicacaoPK(Permissao permissao, Aplicacao aplicacao) {
        this.permissao = permissao;
        this.aplicacao = aplicacao;
    }

    public PermissaoAplicacaoPK() {
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public Aplicacao getAplicacao() {
        return aplicacao;
    }

    public void setAplicacao(Aplicacao aplicacao) {
        this.aplicacao = aplicacao;
    }

    @Override
    public String toString() {
        return "PermissaoAplicacaoPK{" + "permissao=" + permissao + ", aplicacao=" + aplicacao + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.permissao);
        hash = 13 * hash + Objects.hashCode(this.aplicacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PermissaoAplicacaoPK other = (PermissaoAplicacaoPK) obj;
        if (!Objects.equals(this.permissao, other.permissao)) {
            return false;
        }
        if (!Objects.equals(this.aplicacao, other.aplicacao)) {
            return false;
        }
        return true;
    }

}
