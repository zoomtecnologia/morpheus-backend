package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao.ComissaoServico;
import com.zoomtecnologia.morpheus.morpheussistema.email.Mailer;
import com.zoomtecnologia.morpheus.morpheussistema.exception.RecursoNaoEncontradoException;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa.GrupoEmpresa;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa.GrupoEmpresaRepositorio;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.grupoempresa.GrupoEmpresaServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv.PDVServico;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.zion.planocontratado.ZionPlanoContratadoServico;
import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import lombok.AllArgsConstructor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ClienteServico {

    private final ClienteRepositorio clienteRepositorio;
    private final GrupoEmpresaServico grupoEmpresaServico;
    private final ModelMapper modelMapper;
    private final MessageSourceUtil messageSourceUtil;
    private final ComissaoServico comissaoServico;
    private final PDVServico pdvServico;
    private final Mailer mailer;

    public Page<?> listarClientes(String pesquisa, String representante, Pageable pageable, ClienteFiltro clienteFiltro) {
        final Cliente cliente = this.modelMapper.map(clienteFiltro, Cliente.class);
        if (clienteFiltro.naoTemFiltro()) {
            return this.clienteRepositorio.listarClientes(pesquisa, representante, pageable);
        }
        cliente.setContrato(null);
        if (clienteFiltro.getCodigoRepresentante() == null && !representante.isEmpty()) {
            clienteFiltro.setCodigoRepresentante(representante);
        }
        return this.clienteRepositorio.filtrarClientes(clienteFiltro, pageable);
    }

    public List<String> listrarSeguimentos() {
        return this.clienteRepositorio.listarSeguimentos();
    }

    public List<Integer> listarDiasVencimentos(){
        return this.clienteRepositorio.listarDiasVencimentos();
    }

    public List<Cliente> listarClientesComDiaVencimentoMenorOuIgualAoInformado(Integer diaVencimentoMaisProximo) {
        return this.clienteRepositorio.listarClientesComDiaVencimentoMenorOuIgualAoInformado(diaVencimentoMaisProximo);
    }

    public Cliente buscarClientePorCNPJ(String cnpj) {
        final Optional<Cliente> clienteOptional = clienteRepositorio.findById(cnpj);
        if (clienteOptional.isEmpty()) {
            this.clienteNaoEncontrado();
        }
        return clienteOptional.get();
    }

    @Transactional
    public Cliente salvarCliente(ClienteInclusaoModeloAPI clienteInclusaoModeloAPI) {
        if (this.clienteRepositorio.existsById(clienteInclusaoModeloAPI.getCnpj())) {
            this.messageSourceUtil.recursoJaExiste("cliente.ja-existe", "cliente.ja-existe-detalhes");
        }
        final Cliente cliente = this.modelMapper.map(clienteInclusaoModeloAPI, Cliente.class);
        final Cliente clienteSalvo = this.salvarCliente(cliente);
        this.pdvServico.salvarPdvs(cliente, clienteInclusaoModeloAPI.getQuantidadePDVs());
        return clienteSalvo;
    }

    public Cliente salvarCliente(Cliente cliente) {
        cliente.setDataCadastro(LocalDate.now());
        cliente.setAtualizouSistema("S");
        cliente.setStatusAtividade("A");
        cliente.setStatusPagamento("P");
        cliente.setStatusPedido("SEM PEDIDO");
        if (cliente.getDiaVencimento() == null) {
            cliente.setDiaVencimento(5);
        }
        if (cliente.getPrazoMaximo() == null) {
            cliente.setPrazoMaximo(30);
        }
        return this.clienteRepositorio.save(cliente);
    }

    @Transactional
    public void alterarCliente(String numeroCnpj, ClienteAlteracaoModeloAPI clienteAlteracaoModeloAPI) {
        final Cliente clienteEncontrado = this.buscarClientePorCNPJ(numeroCnpj);
        final String cnpjClienteEncontrado = clienteEncontrado.getCnpj();
        final String codigoRepresentanteClienteEncontrado = clienteEncontrado.getCodigoRepresentante();
        int diaDeVencimentoClienteEncontrado = clienteEncontrado.getDiaVencimento();
        BeanUtils.copyProperties(clienteAlteracaoModeloAPI, clienteEncontrado, "codigoGrupoEmpresa");
        if (clienteAlteracaoModeloAPI.getCodigoGrupoEmpresa() == null) {
            clienteEncontrado.setCodigoGrupoEmpresa(null);
        } else {
            if (clienteAlteracaoModeloAPI.getCodigoGrupoEmpresa() != null
                    && clienteEncontrado.getCodigoGrupoEmpresa() != null
                    && !clienteAlteracaoModeloAPI.getCodigoGrupoEmpresa()
                            .equals(clienteEncontrado.getCodigoGrupoEmpresa().getCodigo())) {
                clienteEncontrado.setCodigoGrupoEmpresa(new GrupoEmpresa(clienteAlteracaoModeloAPI.getCodigoGrupoEmpresa()));
            }
            if (clienteEncontrado.getCodigoGrupoEmpresa() == null && clienteAlteracaoModeloAPI.getCodigoGrupoEmpresa() != null) {
                final GrupoEmpresa grupoEncontrado = this.grupoEmpresaServico.buscarPorCodigo(clienteAlteracaoModeloAPI.getCodigoGrupoEmpresa());
                clienteEncontrado.setCodigoGrupoEmpresa(grupoEncontrado);
            }
        }
        if (diaDeVencimentoClienteEncontrado != clienteAlteracaoModeloAPI.getDiaVencimento()) {
            this.comissaoServico.alterarDataProximoVencimento(clienteEncontrado);
            clienteEncontrado.gerarChave();
        }
        this.clienteRepositorio.save(clienteEncontrado);
        this.pdvServico.excluirPDVs(cnpjClienteEncontrado, codigoRepresentanteClienteEncontrado);
        this.pdvServico.salvarPdvs(clienteEncontrado, clienteAlteracaoModeloAPI.getQuantidadePDVs());
    }

    public void deletarCliente(String cnpj) {
        this.clienteRepositorio.deleteById(cnpj);
    }

    public void alterarStatusPagamento(String cnpj, String status) {
        if (!clienteRepositorio.existsById(cnpj)) {
            this.clienteNaoEncontrado();
        }
        this.clienteRepositorio.alterarStatusPagamento(cnpj, status);
    }

    public void alterarStatusPedido(String cnpj, String statusPedido) {
        if (!clienteRepositorio.existsById(cnpj)) {
            this.clienteNaoEncontrado();
        }
        this.clienteRepositorio.atualizarStatusPedido(cnpj, statusPedido);
    }

    public void bloquearClientePorRepresentante(String codigoRepresentante, String status) {
        this.clienteRepositorio.bloquearClienteDoRepresentante(codigoRepresentante, status);
    }

    public void aceitarContrato(String cnpj) {
        if (!this.clienteRepositorio.existsById(cnpj)) {
            this.clienteNaoEncontrado();
        }
        int periodoDeUmAno = 365;
        LocalDate dataFimVigenciaContrato = LocalDate.now().plusDays(periodoDeUmAno);
        this.clienteRepositorio.aceitarContrato(cnpj, dataFimVigenciaContrato);
    }

    public Cliente gerarSenhaCliente(String cnpj) {
        Cliente cliente = this.buscarClientePorCNPJ(cnpj);
        cliente.gerarChave();
        return this.clienteRepositorio.save(cliente);
    }

    public void enviarEmailContratoAceito(String cnpj) {
        Cliente cliente = this.buscarClientePorCNPJ(cnpj);
        this.mailer.enviarEmailContratoAceito(cliente);
    }

    @Transactional(transactionManager = "morpheusTransactionManager")
    public void alterarCliente(Cliente cliente) {
        this.clienteRepositorio.save(cliente);
    }

    private void clienteNaoEncontrado() throws RecursoNaoEncontradoException {
        this.messageSourceUtil.recursoNaoExiste("cliente.recurso-nao-encontrado", "cliente.recurso-nao-encontrado-detalhes");
    }
}
