package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import com.zoomtecnologia.morpheus.morpheussistema.exception.NegocioException;
import java.util.Base64;
import java.util.regex.Pattern;

public class ChaveAcesso {

    private final String chave;

    public ChaveAcesso(String chave) {
        if (chave == null || chave.trim().isEmpty()) {
            throw new NegocioException("Chave não existe", "chave do pdv está nulo ou vazia.");
        }
        this.chave = chave;
    }

    public ChaveDesserializacao desserializar() throws IllegalArgumentException {
        final String chaveAcesso = this.chave.trim();
        byte[] decode;
        try {
            decode = Base64.getDecoder().decode(chaveAcesso);
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("Essa chave não é uma base64 valida");
        }
        if (decode == null) {
            return null;
        }
        if (decode.length == 0) {
            return null;
        }
        final String decripitado = new String(decode);
        final String[] valores = decripitado.split(Pattern.quote("."));
        final ChaveDesserializacao chaveDesserializacao = new ChaveDesserializacao(gerarCNPJDesserializacao(valores[0]),
                gerarCodigoRepresentanteDesserializacao(valores[1]),
                gerarDataDesserializacao(valores[2]),
                true,
                chaveAcesso,
                Integer.parseInt(valores[3]));
        if (valores.length == 5) {
            chaveDesserializacao.setIdentificacaoAPIRetaguarda(valores[4]);
        }
        return chaveDesserializacao;
    }

    private long gerarDataDesserializacao(String dataVencimentoSerializada) {
        long dataVencimento = (Long.parseLong(dataVencimentoSerializada)) / 653;
        return dataVencimento;
    }

    private String gerarCNPJDesserializacao(String cnpj) {
        long cnpjCalculo = (Long.parseLong(cnpj)) / 653;
        return String.format("%014d", cnpjCalculo);
    }

    private String gerarCodigoRepresentanteDesserializacao(String codigoRepresentate) {
        return String.valueOf((Long.parseLong(codigoRepresentate)) / 653);
    }

}
