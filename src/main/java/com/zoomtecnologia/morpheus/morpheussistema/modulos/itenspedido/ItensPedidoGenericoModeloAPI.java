package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

public abstract class ItensPedidoGenericoModeloAPI {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer numeroPedido;

    @NotNull
    private Integer codigoItem;
    
    @NotNull
    private Integer quantidade;

    @NotNull
    private Double valor;
    
    @NotNull
    private String status;

    public Integer getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public Integer getCodigoItem() {
        return codigoItem;
    }

    public void setCodigoItem(Integer codigoItem) {
        this.codigoItem = codigoItem;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ItensPedidoGenericoModeloAPI{" + "numeroPedido=" + numeroPedido + ", codigoItem=" + codigoItem + ", quantidade=" + quantidade + ", valor=" + valor + ", status=" + status + '}';
    }

    
}
