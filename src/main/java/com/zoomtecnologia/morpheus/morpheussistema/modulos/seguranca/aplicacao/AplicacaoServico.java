package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.aplicacao;

import com.zoomtecnologia.morpheus.morpheussistema.util.MessageSourceUtil;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AplicacaoServico {

    @Autowired
    private AplicacaoRepositorio aplicacaoRepositorio;

    @Autowired
    private MessageSourceUtil messageSourceUtil;

    public List<Aplicacao> listarAplicacoes() {
        return this.aplicacaoRepositorio.findAll();
    }

    public Aplicacao buscarAplicacaoPorCodigo(Integer codigo) {
        Optional<Aplicacao> aplicacaoOptinal = this.aplicacaoRepositorio.findById(codigo);
        if (!aplicacaoOptinal.isPresent()) {
            this.messageSourceUtil.recursoNaoExiste("aplicacao.nao-existe", "aplicacao.nao-existe");
        }
        return aplicacaoOptinal.get();
    }

    public Aplicacao salvarAplicacao(AplicacaoModeloAPI aplicacaoInclusaoModeloAPI) {
        Aplicacao aplicacao = new Aplicacao();
        aplicacao.setNome(aplicacaoInclusaoModeloAPI.getNome());
        aplicacao.setDescricao(aplicacaoInclusaoModeloAPI.getDescricao());
        return this.aplicacaoRepositorio.save(aplicacao);
    }

    public void alterarAplicacao(Integer codigo, AplicacaoModeloAPI aplicacao) {
        Aplicacao aplicacaoEncontrada = this.buscarAplicacaoPorCodigo(codigo);
        BeanUtils.copyProperties(aplicacao, aplicacaoEncontrada, "codigo");
        this.aplicacaoRepositorio.save(aplicacaoEncontrada);
    }

    public void excluirAplicacao(Integer codigo) {
        this.aplicacaoRepositorio.deleteById(codigo);
    }

}
