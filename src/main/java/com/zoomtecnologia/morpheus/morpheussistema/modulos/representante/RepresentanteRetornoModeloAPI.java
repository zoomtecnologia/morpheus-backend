package com.zoomtecnologia.morpheus.morpheussistema.modulos.representante;

import java.time.LocalDate;

public interface RepresentanteRetornoModeloAPI {

    String getDocumento();

    String getNome();

    String getNomeSocial();

    String getCelular();

    String getEmail();

    String getTipo();

    LocalDate getDataCadastro();
    
    boolean getGeraReceita();

    Integer getDiaPagamentoComissao();

    LocalDate getDataUltimoAcesso();

    String getNivel();

    String getStatus();

    byte[] getFoto();
    
    String getCodigoIndicacao();
}
