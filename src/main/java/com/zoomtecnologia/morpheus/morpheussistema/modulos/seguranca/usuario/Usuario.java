package com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.representante.Representante;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.permissao.Permissao;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import net.bytebuddy.utility.RandomString;
import org.springframework.format.annotation.DateTimeFormat;

@Entity()
@Table(name = "usuario")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Usuario implements Serializable {

    private static final long serialVersionUID = -6559886335352279320L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    
	@Column(name = "codigo")
	private Integer codigo;

    
	@Column(name = "nome")
	private String nome;

    
	@Column(name = "email")
	private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    
	@Column(name = "senha")
	private String senha;

    @ManyToOne
    @JoinColumn(name = "codigo_representante", referencedColumnName = "documento")
    
	private Representante representante;

    
	@Column(name = "status")
	private String status;

    @ManyToOne
    @JoinColumn(name = "codigo_permissao")
    
	private Permissao permissao;

    
	@Column(name = "recuperando_senha")
	private String recuperandoSenha;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    
	@Column(name = "data_recuperacao_senha")
	private LocalDate dataRecuperacaoSenha;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    
	@Column(name = "data_acesso")
	private LocalDate dataAcesso;

    public Usuario() {
    }

    public LocalDate getDataAcesso() {
        return dataAcesso;
    }

    public void setDataAcesso(LocalDate dataAcesso) {
        this.dataAcesso = dataAcesso;
    }

    public Usuario(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Representante getRepresentante() {
        return representante;
    }

    public void setRepresentante(Representante representante) {
        this.representante = representante;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public String getRecuperandoSenha() {
        return recuperandoSenha;
    }

    public void setRecuperandoSenha(String recuperandoSenha) {
        this.recuperandoSenha = recuperandoSenha;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public LocalDate getDataRecuepracaoSenha() {
        return dataRecuperacaoSenha;
    }

    public void setDataRecuepracaoSenha(LocalDate dataRecuepracaoSenha) {
        this.dataRecuperacaoSenha = dataRecuepracaoSenha;
    }

    @Override
    public String toString() {
        return "Usuario{" + "codigo=" + codigo + ", nome=" + nome + ", email=" + email + ", senha=" + senha + ", representante=" + representante + ", status=" + status + ", permissao=" + permissao + ", recuperandoSenha=" + recuperandoSenha + ", dataCadastro=" + dataCadastro + ", dataRecuepracaoSenha=" + dataRecuperacaoSenha + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        return Objects.equals(this.codigo, other.codigo);
    }

    public void gerarCodigoRecuperacaoSenha() {
        this.recuperandoSenha = RandomString.make(20);
    }

    public void limparRecuperarSenha() {
        this.recuperandoSenha = null;
    }

}
