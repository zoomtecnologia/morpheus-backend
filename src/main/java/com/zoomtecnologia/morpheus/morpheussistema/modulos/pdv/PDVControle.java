/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pdvs")
@CrossOrigin("*")
@AllArgsConstructor
public class PDVControle {

    private final PDVServico pdvServico;

    @GetMapping
    public List<PDVListaModeloAPI> listarPDVs(
            @RequestParam("cnpj") String cnpj,
            @RequestParam("codigoRepresentante") String codigoRepresentante) {
        return this.pdvServico.listarPDVs(cnpj, codigoRepresentante);
    }

}
