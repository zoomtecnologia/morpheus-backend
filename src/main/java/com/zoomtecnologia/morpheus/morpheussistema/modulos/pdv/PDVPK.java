/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.generico.NaoAtualizaAtributo;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PDVPK implements Serializable{
    
    @Column(name="numero_pdv")
    private int numeroPdv;

    @Column(name="cnpj")
    @NaoAtualizaAtributo
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String cnpj;

    @Column(name="codigo_representante")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String codigoRepresentante;
    
}
