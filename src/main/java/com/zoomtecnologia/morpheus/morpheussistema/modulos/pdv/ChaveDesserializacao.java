/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zoomtecnologia.morpheus.morpheussistema.modulos.pdv;

import java.time.LocalDate;
import lombok.ToString;

/**
 *
 * @author eudes
 */
@ToString
public class ChaveDesserializacao {

    private String cnpj;
    private String codigoRepresentante;
    private long dataVencimento;
    private boolean valida;
    private String chaveFinal;
    private int numeroCaixa;
    private String identificacaoAPIRetaguarda;

    public ChaveDesserializacao() {
    }

    public ChaveDesserializacao(
            String cnpj,
            String codigoRepresentante,
            long dataVencimento,
            boolean valida,
            String chaveFinal,
            int numeroCaixa) {
        this.cnpj = cnpj;
        this.codigoRepresentante = codigoRepresentante;
        this.dataVencimento = dataVencimento;
        this.valida = valida;
        this.chaveFinal = chaveFinal;
        this.numeroCaixa = numeroCaixa;
    }

    public String getIdentificacaoAPIRetaguarda() {
        return identificacaoAPIRetaguarda;
    }

    public void setIdentificacaoAPIRetaguarda(String identificacaoAPIRetaguarda) {
        this.identificacaoAPIRetaguarda = identificacaoAPIRetaguarda;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCodigoRepresentante() {
        return codigoRepresentante;
    }

    public void setCodigoRepresentante(String codigoRepresentante) {
        this.codigoRepresentante = codigoRepresentante;
    }

    public long getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(long dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public boolean isValida() {
        return valida;
    }

    public String getChaveFinal() {
        return chaveFinal;
    }

    public int getNumeroCaixa() {
        return numeroCaixa;
    }

    public void setNumeroCaixa(int numeroCaixa) {
        this.numeroCaixa = numeroCaixa;
    }

    public boolean verificarValidaDaChave() {
        final int dataAtual = Integer.parseInt(LocalDate.now().toString().replace("-", ""));
        return this.dataVencimento > dataAtual;
    }

}
