package com.zoomtecnologia.morpheus.morpheussistema.modulos.comissao;

import java.util.List;

public interface ComissaoRepositorioQuery {
    
    public List<ComissaoListaModeloAPI> listarComissaoes(ComissaoRepositorioQueryImpl.FiltroComissaoQuery filtroComissaoQuery);
    
}
