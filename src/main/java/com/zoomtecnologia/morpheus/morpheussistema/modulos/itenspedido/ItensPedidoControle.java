package com.zoomtecnologia.morpheus.morpheussistema.modulos.itenspedido;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pedidos/{numero-pedido}/itens")
@CrossOrigin
public class ItensPedidoControle {

    @Autowired
    private ItensPedidoServico itensPedidoServico;

    @GetMapping
    public List<ItensPedidoRetornoModeloAPI> listarItensPedido(@PathVariable("numero-pedido") Integer numeroPedido) {
        return this.itensPedidoServico.listarItensPedido(numeroPedido);
    }

    @GetMapping("{codigo-item}")
    public ResponseEntity<ItensPedidoRetornoModeloAPI> buscarItemPedido(@PathVariable("numero-pedido") Integer numeroPedido,
            @PathVariable("codigo-item") Integer codigoItem) {
        ItensPedidoRetornoModeloAPI itensPedidoRetornoModeloAPI = this.itensPedidoServico.buscarItensPedido(numeroPedido, codigoItem);
        return ResponseEntity.ok(itensPedidoRetornoModeloAPI);
    }

    @PostMapping
    public ResponseEntity<List<ItensPedido>> salvarItemPedido(
            @PathVariable("numero-pedido") Integer numeroPedido,
            @RequestBody @Valid List<ItensPedidoInclusaoModeloAPI> itensPedidoInclusaoModeloAPI) {
        List<ItensPedido> itensPedidoSalvo = this.itensPedidoServico.salvarItemPedido(numeroPedido, itensPedidoInclusaoModeloAPI);
        return ResponseEntity.status(HttpStatus.CREATED).body(itensPedidoSalvo);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarItemPedido(@RequestBody @Valid List<ItensPedidoAlteracaoModeloAPI> itensPedidoAlteracaoModeloAPI, @PathVariable("numero-pedido") Integer numeroPedido) {        
        this.itensPedidoServico.alterarItemPedido(numeroPedido,itensPedidoAlteracaoModeloAPI);
    }

    @DeleteMapping("{codigo-item}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarItemPedido(@PathVariable("numero-pedido") Integer numeroPedido, @PathVariable("codigo-item") Integer codigoItem) {
        this.itensPedidoServico.deletarItemPedido(numeroPedido, codigoItem);
    }

}
