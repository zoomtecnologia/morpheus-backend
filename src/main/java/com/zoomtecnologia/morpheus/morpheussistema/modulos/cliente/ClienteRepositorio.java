package com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ClienteRepositorio extends JpaRepository<Cliente, String>, ClienteRepositorioQuery {

    static final String PACOTE = "com.zoomtecnologia.morpheus.morpheussistema.modulos.cliente";
    static final String INSTANCIA_CLIENTE_RETORNO_MODELO = "new " + PACOTE + ".ClienteRetornoModeloAPI";

    @Query("select cliente.seguimento from Cliente cliente where cliente.seguimento is not null and cliente.seguimento <> '' group by cliente.seguimento order by cliente.seguimento")
    public List<String> listarSeguimentos();

    @Query("select cliente from Cliente cliente where cliente.diaVencimento=:diaPagamento")
    public List<Cliente> listarClientePorDiaPagamento(Integer diaPagamento);

    @Query(value = "select cliente.diaVencimento from Cliente cliente where cliente.statusAtividade='A' group by cliente.diaVencimento")
    public List<Integer> listarDiasVencimentos();

    @Query("SELECT c FROM Cliente c JOIN FETCH c.codigoGrupoEmpresa g " +
            "WHERE " +
            "c.statusAtividade = 'A' " +
            "AND c.diaVencimento <= :diaVencimentoMaisProximo " +
            "AND c.statusPagamento = 'P' " +
            "AND c.dataProximoVencimento < STR_TO_DATE(CONCAT(YEAR(CURRENT_DATE), '-', MONTH(CURRENT_DATE)+1, '-', c.diaVencimento), '%Y-%m-%d') " +
            "AND c.codigoGrupoEmpresa IS NOT NULL ")
    List<Cliente> listarClientesComDiaVencimentoMenorOuIgualAoInformado(Integer diaVencimentoMaisProximo);

    @Transactional
    @Modifying
    @Query("update Cliente cliente set cliente.statusPagamento=:statusPagamento where cliente.cnpj=:cnpj")
    public void alterarStatusPagamento(@Param("cnpj") String numeroCnpj, @Param("statusPagamento") String statusPagamento);

    @Transactional
    @Modifying
    @Query("update Cliente cliente set cliente.aceiteContrato='A',cliente.dataAceite= CURRENT_DATE,cliente.dataFimVigenciaContrato=:dataFimVigenciaContrato where cliente.cnpj=:cnpj")
    public void aceitarContrato(@Param("cnpj") String numeroCnpj, LocalDate dataFimVigenciaContrato);

    @Transactional
    @Modifying
    @Query("update Cliente cliente set cliente.statusPedido=:statusPedido where cliente.cnpj=:cnpj")
    public void atualizarStatusPedido(@Param("cnpj") String cnpj, @Param("statusPedido") String statusPedido);

    @Transactional
    @Modifying
    @Query("update Cliente cliente set cliente.statusPagamento=:status where cliente.codigoRepresentante=:codigoRepresentante")
    public void bloquearClienteDoRepresentante(@Param("codigoRepresentante") String codigoRepresentante, @Param("status") String status);
}
