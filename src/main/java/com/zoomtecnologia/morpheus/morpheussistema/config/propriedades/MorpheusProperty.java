package com.zoomtecnologia.morpheus.morpheussistema.config.propriedades;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("morpheus")
public class MorpheusProperty {
    
    private final Email email = new Email();
    private final FrontEnd frontEnd = new FrontEnd();
    private final Seguranca seguranca = new Seguranca();

    public Email getEmail() {
        return email;
    }

    public FrontEnd getFrontEnd() {
        return frontEnd;
    }

    public Seguranca getSeguranca() {
        return seguranca;
    }
    
    public static class Seguranca{
        private boolean ativarHttps;

        public boolean isAtivarHttps() {
            return ativarHttps;
        }

        public void setAtivarHttps(boolean ativarHttps) {
            this.ativarHttps = ativarHttps;
        }

        @Override
        public String toString() {
            return "Seguranca{" + "ativarHttps=" + ativarHttps + '}';
        }
        
    }

    public static class Email{
        private String host;
        private Integer porta;
        private String usuario;
        private String senha;        

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public Integer getPorta() {
            return porta;
        }

        public void setPorta(Integer porta) {
            this.porta = porta;
        }

        public String getUsuario() {
            return usuario;
        }

        public void setUsuario(String usuario) {
            this.usuario = usuario;
        }

        public String getSenha() {
            return senha;
        }

        public void setSenha(String senha) {
            this.senha = senha;
        }

        @Override
        public String toString() {
            return "Email{" + "host=" + host + ", porta=" + porta + ", usuario=" + usuario + ", senha=" + senha + '}';
        }        
    }
    
    public static class FrontEnd{
        private String endereco;

        public String getEndereco() {
            return endereco;
        }

        public void setEndereco(String endereco) {
            this.endereco = endereco;
        }
        
        @Override
        public String toString() {
            return "Servidor{" + "endereco=" + endereco + '}';
        }        
    }
    
}
