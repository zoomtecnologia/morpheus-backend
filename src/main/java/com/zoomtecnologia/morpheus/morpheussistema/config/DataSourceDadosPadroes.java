package com.zoomtecnologia.morpheus.morpheussistema.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = {"com.zoomtecnologia.morpheus.morpheussistema.padrao"},
        entityManagerFactoryRef = "padraoEntityManager",
        transactionManagerRef = "transactionManager")
public class DataSourceDadosPadroes {

    private final String PARAMETROS_JDBC;

    public DataSourceDadosPadroes() {
        this.PARAMETROS_JDBC = "?useSSL=false&zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&allowMultiQueries=true&allowPublicKeyRetrieval=true";
    }

    @Bean
    @ConfigurationProperties(prefix = "padrao.datasource")
    public DataSource padraoDataSource() {
        return this.newDataSourceBuilderNuvem();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean padraoEntityManager(
            EntityManagerFactoryBuilder builder,
            @Qualifier("padraoDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource)
                .persistenceUnit("padrao")
                .packages("com.zoomtecnologia.morpheus.morpheussistema.padrao")
                .build();
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @Qualifier("padraoEntityManager") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    private DataSource newDataSourceBuilderNuvem() {
        HikariDataSource dataSource = DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .url("jdbc:mysql://195.35.17.210:3306/zoomtecn_zion" + PARAMETROS_JDBC)
                .username("zoomtecn_adm")
                .password("noiz@#$1030")
                .driverClassName("com.mysql.cj.jdbc.Driver")
                .build();
        dataSource.setMaximumPoolSize(5);
        dataSource.setMinimumIdle(2);
        dataSource.setConnectionTimeout(30000); // Tempo máximo de espera para obter uma conexão, em milissegundos
        dataSource.setIdleTimeout(600000);  // Tempo máximo que uma conexão pode ficar ociosa, em milissegundos
        dataSource.setMaxLifetime(540000);
        return dataSource;// Tempo máximo de vida de uma conexão, em milissegundos
    }

}
