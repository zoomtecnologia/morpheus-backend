package com.zoomtecnologia.morpheus.morpheussistema.config.seguranca;

import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioModeloAPI;
import com.zoomtecnologia.morpheus.morpheussistema.modulos.seguranca.usuario.UsuarioRepositorio;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();
        Optional<UsuarioModeloAPI> usuarioOptional = this.usuarioRepositorio.buscarPorEmail(email);
        if (!usuarioOptional.isPresent()) {
            return null;
        }
        if (this.usuarioRepositorio.countByEmailAndSenha(email, password) == 0) {
            return null;
        }
        UsuarioModeloAPI usuario = usuarioOptional.get();
        if(usuario.getRepresentante().getStatus().equals("B")){
            return null;
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(usuario.getPermissao().getNome()));
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(email, password, authorities);
        return usernamePasswordAuthenticationToken;
    }

    @Override
    public boolean supports(Class<?> type) {
        return type.equals(UsernamePasswordAuthenticationToken.class);
    }

}
