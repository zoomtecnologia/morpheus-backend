package com.zoomtecnologia.morpheus.morpheussistema.config.seguranca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(this.customAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/usuarios/login").permitAll()
                .antMatchers("/usuarios/*/recuperar-senha").permitAll()
                .antMatchers("/usuarios/verificar-email-cadastrado").permitAll()
                .antMatchers("/usuarios/recuperar-senha").permitAll()
                .antMatchers("/usuarios/enviar-email-redefinir-senha").permitAll()
                .antMatchers("/pedidos/*/contrato").permitAll()
                .antMatchers("/pedidos/*").permitAll()
                .antMatchers(HttpMethod.POST, "/indicacoes").permitAll()
                .antMatchers("/representantes/revendedores").permitAll()
                .antMatchers("/clientes/*/aceite").permitAll()
                .antMatchers("/clientes/*/enviar-email-contrato-aceito").permitAll()
                .antMatchers("/zion/contratos/*").permitAll()
                .antMatchers("/zion/contratos/aceite-contrato/*").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
