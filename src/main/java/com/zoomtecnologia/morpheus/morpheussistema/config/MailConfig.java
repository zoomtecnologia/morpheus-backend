package com.zoomtecnologia.morpheus.morpheussistema.config;

import com.zoomtecnologia.morpheus.morpheussistema.config.propriedades.MorpheusProperty;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {

    @Autowired
    private MorpheusProperty morpheusProperty;

    @Bean
    public JavaMailSender javaMailSender() {

        Properties properties = new Properties();
        properties.put("mail.transport.protocolo", "smtp");
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", true);
        properties.put("mail.smtp.connectiontimeout", 5000);

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setJavaMailProperties(properties);
        mailSender.setHost(this.morpheusProperty.getEmail().getHost());
        mailSender.setPort(this.morpheusProperty.getEmail().getPorta());
        mailSender.setUsername(this.morpheusProperty.getEmail().getUsuario());
        mailSender.setPassword(this.morpheusProperty.getEmail().getSenha());
        return mailSender;
    }

}
