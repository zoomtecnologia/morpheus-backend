package com.zoomtecnologia.morpheus.morpheussistema.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@EnableJpaRepositories(
        basePackages = {"com.zoomtecnologia.morpheus.morpheussistema.modulos"},
        entityManagerFactoryRef = "entityManager"
)
public class DataSourceConfig {

    private final String PARAMETROS_JDBC;

    public DataSourceConfig() {
        this.PARAMETROS_JDBC = "?createDatabaseIfNotExist=true&useSSL=false&zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&allowMultiQueries=true&allowPublicKeyRetrieval=true";
    }

    @Bean
    @Primary
    public DataSource dataSourcePrimary() throws SQLException {
        return this.newDataSourceBuilderNuvem();
    }

    @Bean
    @Primary
    public PlatformTransactionManager morpheusTransactionManager(
            @Qualifier("entityManager") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManager(
           EntityManagerFactoryBuilder builder,
            @Qualifier("dataSourcePrimary") DataSource dataSource) {
        return builder.dataSource(dataSource)
                .packages("com.zoomtecnologia.morpheus.morpheussistema.modulos")
                .build();
    }

    private DataSource newDataSourceBuilderLocal() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url("jdbc:mysql://localhost:3306/" + PARAMETROS_JDBC);
        dataSourceBuilder.username("root");
        dataSourceBuilder.password("123456");
        dataSourceBuilder.driverClassName("com.mysql.cj.jdbc.Driver");
        return dataSourceBuilder.build();
    }

    private DataSource newDataSourceBuilderNuvem() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url("jdbc:mysql://zoomtecnologia.com:3306/zoomtecn_morpheus" + PARAMETROS_JDBC);
        dataSourceBuilder.username("zoomtecn_adm");
        dataSourceBuilder.password("zoom12345");
        dataSourceBuilder.driverClassName("com.mysql.cj.jdbc.Driver");
        return dataSourceBuilder.build();
    }

}
