package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoaplicacoes;

import java.util.List;

public interface ZionPlanoAplicacoesPadraoRepositorioQuery {
    public void salvar(List<ZionPlanoAplicacoesPadrao> aplicacoes );
}
