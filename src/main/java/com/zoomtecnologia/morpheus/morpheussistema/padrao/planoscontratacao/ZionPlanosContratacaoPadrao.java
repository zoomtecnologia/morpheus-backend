package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoscontratacao;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "planos_contratacao")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ZionPlanosContratacaoPadrao implements Serializable {

    @EmbeddedId
    private ZionPlanosContratacaoPadraoPK id;

    @Column(name = "status")
    private boolean status;

    @Column(name = "data_cadastro")
    private LocalDateTime dataCadastro;

    @Column(name = "usuario_contratacao")
    private String usuarioContratacao;

}
