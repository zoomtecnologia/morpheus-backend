package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoscontratacao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionPlanosContratacaoPadraoRepositorio extends JpaRepository<ZionPlanosContratacaoPadrao, ZionPlanosContratacaoPadraoPK>, ZionPlanosContratacaoPadraoRepositorioQuery {

}
