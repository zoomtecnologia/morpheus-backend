package com.zoomtecnologia.morpheus.morpheussistema.padrao.planos;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "planos")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"codigo"})
public class ZionPlanoPadrao implements Serializable {

    @Id

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "nome")
    private String nome;

    @Column(name = "status")
    private boolean status;

    @Column(name = "valor")
    private double valor;

    @Column(name = "valor_usuario_adicional")
    private Double valorUsuarioAdicional;

    @Column(name = "valor_pdv_adicional")
    private Double valorPDVAdicional;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "tipo_plano")
    private String tipoPlano;

    @Column(name = "gera_cobranca")
    private boolean geraCobranca;

    @Column(name = "data_cadastro")
    private LocalDateTime dataCadastro;

    @Column(name = "usuarios")
    private int usuarios;

    @Column(name = "pdvs")
    private int pdvs;

    @Column(name = "teste")
    private Boolean teste;

    @Column(name = "dias_para_teste")
    private Integer diasParaTeste;

}
