package com.zoomtecnologia.morpheus.morpheussistema.padrao.novidades;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class NovidadeServico {

    private final NovidadeRepositorio novidadeRepositorio;

    public List<Novidade> listarNovidades() {
        return this.novidadeRepositorio.findAll();
    }

    public Optional<Novidade> buscarNovidade(Integer codigo) {
        return this.novidadeRepositorio.findById(codigo);
    }

    public Novidade salvarNovidade(Novidade novidade) {
        return this.novidadeRepositorio.save(novidade);
    }
    
    public void excluirNovidade(Integer codigo){
        this.novidadeRepositorio.deleteById(codigo);
    }

}
