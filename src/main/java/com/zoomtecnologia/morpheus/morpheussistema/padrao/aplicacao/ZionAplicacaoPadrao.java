package com.zoomtecnologia.morpheus.morpheussistema.padrao.aplicacao;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "aplicacoes")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
public class ZionAplicacaoPadrao implements Serializable {

    @EmbeddedId
    @NotNull
    @Valid
    private ZionAplicacaoPadraoPK id;

    @Size(max = 50)
    @NotBlank
    @Column(name = "nome")
    private String nome;

    @NotBlank
    @Column(name = "permissoes")
    private String permissoes;

    @Column(name = "status")
    private boolean status;

}
