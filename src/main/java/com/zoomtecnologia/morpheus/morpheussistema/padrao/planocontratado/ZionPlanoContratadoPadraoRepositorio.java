package com.zoomtecnologia.morpheus.morpheussistema.padrao.planocontratado;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ZionPlanoContratadoPadraoRepositorio extends JpaRepository<ZionPlanoContratadoPadrao, String> {

    @Modifying
    @Transactional(transactionManager = "transactionManager")
    @Query("update ZionPlanoContratadoPadrao zionPlanoContratado "
            + "set "
            + "zionPlanoContratado.dataCancelamento=:dataCancelamento, "
            + "zionPlanoContratado.status=false "
            + "where "
            + "zionPlanoContratado.codigo=:codigoContrato")
    public void cancelarContrato(String codigoContrato, LocalDate dataCancelamento);

    @Modifying
    @Transactional(transactionManager = "transactionManager")
    @Query("update ZionPlanoContratadoPadrao zionPlanoContratado "
            + "set "
            + "zionPlanoContratado.statusPagamento=:statusPagamento "
            + "where "
            + "zionPlanoContratado.documentoIdentificacaoEmpresa=:cnpj and zionPlanoContratado.status=true")
    public void alterarStatusPagamento(String cnpj, String statusPagamento);

    @Transactional(transactionManager = "transactionManager")
    @Modifying
    @Query("update ZionPlanoContratadoPadrao zionPlanoContratado "
            + "set "
            + "zionPlanoContratado.dataProximoVencimento=:dataProximoVencimento "
            + "where "
            + "zionPlanoContratado.documentoIdentificacaoEmpresa=:cnpj and zionPlanoContratado.status=true")
    public void atualizarProximoVencimento(String cnpj, LocalDate dataProximoVencimento);
}
