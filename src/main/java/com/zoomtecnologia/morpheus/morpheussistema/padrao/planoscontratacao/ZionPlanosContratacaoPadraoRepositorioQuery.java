package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoscontratacao;

import java.util.List;

public interface ZionPlanosContratacaoPadraoRepositorioQuery {
     public void salvar(List<ZionPlanosContratacaoPadrao> planosContratacao);
}
