package com.zoomtecnologia.morpheus.morpheussistema.padrao.novidades;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("novidades")
@AllArgsConstructor
public class NovidadeControle {

    private final NovidadeServico novidadeServico;

    @GetMapping
    public List<Novidade> listarNovidades() {
        return novidadeServico.listarNovidades();
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Novidade> buscarNovidade(@PathVariable Integer codigo) {
        Optional<Novidade> novidade = novidadeServico.buscarNovidade(codigo);
        return novidade.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Novidade> salvarNovidade(@RequestBody Novidade novidade) {
        Novidade novidadeSalva = novidadeServico.salvarNovidade(novidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(novidadeSalva);
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity<Void> excluirNovidade(@PathVariable Integer codigo) {
        if (novidadeServico.buscarNovidade(codigo).isPresent()) {
            novidadeServico.excluirNovidade(codigo);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
