package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoscontratacao;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ZionPlanosContratacaoPadraoRepositorioQueryImpl implements ZionPlanosContratacaoPadraoRepositorioQuery{

    @PersistenceContext(unitName = "padrao")
    private EntityManager entityManager;

    @Transactional(transactionManager = "transactionManager")
    @Override
    public void salvar(List<ZionPlanosContratacaoPadrao> planosContratacao) {
        if (planosContratacao == null || planosContratacao.isEmpty()) {
            return;
        }
        final String cabecalhoInsert = "insert into planos_contratacao(codigo_contratacao,codigo_plano,status,data_cadastro,usuario_contratacao) values ";
        StringBuilder conteudo = new StringBuilder();
        for (ZionPlanosContratacaoPadrao zionPlanoContratacao : planosContratacao) {
            conteudo.append(",(");
            conteudo.append("'").append(zionPlanoContratacao.getId().getCodigoContratacao()).append("',");
            conteudo.append("'").append(zionPlanoContratacao.getId().getCodigoPlano()).append("',");
            conteudo.append(zionPlanoContratacao.isStatus() ? 1 : 0).append(" ,");
            conteudo.append("'").append(zionPlanoContratacao.getDataCadastro()).append("',");
            conteudo.append("'").append(zionPlanoContratacao.getUsuarioContratacao()).append("'");
            conteudo.append(")");
        }
        final String atualizacao = " ON DUPLICATE KEY UPDATE codigo_plano = values(codigo_plano), status = values(status);";
        final String sql = cabecalhoInsert + conteudo.toString().replaceFirst(",", "") + atualizacao;
        this.entityManager.createNativeQuery(sql).executeUpdate();
    }
    
}
