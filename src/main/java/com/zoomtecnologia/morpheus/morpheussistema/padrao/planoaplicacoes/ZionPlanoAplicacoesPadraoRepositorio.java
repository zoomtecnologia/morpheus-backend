package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoaplicacoes;

import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionPlanoAplicacoesPadraoRepositorio extends JpaRepository<ZionPlanoAplicacoesPadrao, ZionPlanoAplicacoesPadraoPK>,ZionPlanoAplicacoesPadraoRepositorioQuery{

    @Modifying
    @Query("delete from ZionPlanoAplicacoesPadrao zionPlano where zionPlano.id.codigoPlano=:codigoPlano")
    public void excluir(String codigoPlano);

    @Query("select aplicacao.id.codigoAplicacao as codigoAplicacao, "
            + "aplicacao.id.moduloAplicacao as moduloAplicacao, "
            + "zionAplicacao.nome as nome "
            + "from ZionPlanoAplicacoesPadrao aplicacao "
            + "inner join ZionAplicacaoPadrao zionAplicacao on(zionAplicacao.id.codigo = aplicacao.id.codigoAplicacao "
            + "and zionAplicacao.id.modulo = aplicacao.id.moduloAplicacao)"
            + "where aplicacao.id.codigoPlano=:codigoPlano")
    public Set<ZionPlanoAplicacoesPadraoModeloAPI> listarAplicacoesPorPlano(String codigoPlano);
    
   
}
