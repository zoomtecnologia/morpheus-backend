package com.zoomtecnologia.morpheus.morpheussistema.padrao.aplicacao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionAplicacaoPadraoRepositorio extends JpaRepository<ZionAplicacaoPadrao, ZionAplicacaoPadraoPK> {

    
}
