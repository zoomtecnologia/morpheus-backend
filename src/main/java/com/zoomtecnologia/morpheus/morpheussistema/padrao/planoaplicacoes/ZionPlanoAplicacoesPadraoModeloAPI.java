package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoaplicacoes;

public interface ZionPlanoAplicacoesPadraoModeloAPI {

    String getCodigoAplicacao();

    String getModuloAplicacao();

    String getNome();

}
