package com.zoomtecnologia.morpheus.morpheussistema.padrao.planoaplicacoes;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "planos_aplicacao")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
public class ZionPlanoAplicacoesPadrao implements Serializable {

    @EmbeddedId
    private ZionPlanoAplicacoesPadraoPK id;
}
