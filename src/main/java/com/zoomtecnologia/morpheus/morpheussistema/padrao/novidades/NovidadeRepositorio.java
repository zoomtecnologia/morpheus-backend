package com.zoomtecnologia.morpheus.morpheussistema.padrao.novidades;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NovidadeRepositorio extends JpaRepository<Novidade,Integer> {
}
