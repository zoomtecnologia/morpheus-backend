package com.zoomtecnologia.morpheus.morpheussistema.padrao.planos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZionPlanoPadraoRepositorio extends JpaRepository<ZionPlanoPadrao, String> {

}
