package com.zoomtecnologia.morpheus.morpheussistema.padrao.aplicacao;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ZionAplicacaoPadraoPK implements Serializable {

    @Size(max = 36)
    @NotBlank
    @Column(name="codigo")
    private String codigo;

    @Size(max = 36)
    @NotBlank
    @Column(name="modulo")
    private String modulo;

}
