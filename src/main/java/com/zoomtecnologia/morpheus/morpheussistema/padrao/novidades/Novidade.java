package com.zoomtecnologia.morpheus.morpheussistema.padrao.novidades;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "novidade")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Novidade {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer codigo;

    @Column(name = "titulo", nullable = false, length = 150, columnDefinition = "VARCHAR(150) COLLATE 'utf8mb4_0900_ai_ci'")
    private String titulo;

    @Column(name = "texto_principal", length = 250, columnDefinition = "VARCHAR(250) COLLATE 'utf8mb4_0900_ai_ci'")
    private String textoPrincipal;

    @Column(name = "texto_secundario", length = 250, columnDefinition = "VARCHAR(250) COLLATE 'utf8mb4_0900_ai_ci'")
    private String textoSecundario;

    @Column(name = "data_cadastro", nullable = false)
    private LocalDateTime dataCadastro;

    @Column(name = "data_expiracao", nullable = false)
    private LocalDateTime dataExpiracao;

    @Column(name = "imagem", length = 250, columnDefinition = "VARCHAR(250) COLLATE 'utf8mb4_0900_ai_ci'")
    private String imagem;

    @Column(name = "link", length = 150, columnDefinition = "VARCHAR(150) COLLATE 'utf8mb4_0900_ai_ci'")
    private String link;
}
