package com.zoomtecnologia.morpheus.morpheussistema.padrao.planocontratado;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class ZionPlanoContratadoPadraoServico {

    private final ZionPlanoContratadoPadraoRepositorio zionPlanoContratadoPadraoRepositorio;

    @Transactional
    public void alterarStatusPagamento(String cnpj, String statusPagamento) {
        String status = "PG";
        if (statusPagamento.equals("B")) {
            status = "BL";
        } else if (statusPagamento.equals("V")) {
            status = "VC";
        }
        this.zionPlanoContratadoPadraoRepositorio.alterarStatusPagamento(cnpj, status);
    }

    public void atualizarProximoVencimento(LocalDate dataProximoVencimento, String cnpj) {
        this.zionPlanoContratadoPadraoRepositorio.atualizarProximoVencimento(cnpj, dataProximoVencimento);
    }
}
